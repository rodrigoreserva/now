﻿using SimpleInjector;
using Now.Infrastructure;
using Now.Domain.Contracts.Repositories;
using Now.Infrastructure.Repositories;
using Now.Domain.Contracts.Services;
using Now.Business.Services;

namespace Now.Startup
{
    public class CustomDependency
    {
        public static void Resolve(Container container)
        {
            container.RegisterSingleton(new NowContext());
            container.Register<IUsuarioRepository, UsuarioRepository>(Lifestyle.Scoped);
            container.Register<IUsuarioService, UsuarioService>(Lifestyle.Scoped);
            container.Register<IAgendaRepository, AgendaRepository>(Lifestyle.Scoped);
            container.Register<IAgendaService, AgendaService>(Lifestyle.Scoped);
            container.Register<IClienteRepository, ClienteRepository>(Lifestyle.Scoped);
            container.Register<IClienteService, ClienteService>(Lifestyle.Scoped);
            container.Register<IDashboardRepository, DashboardRepository>(Lifestyle.Scoped);
            container.Register<IDashboardService, DashboardService>(Lifestyle.Scoped);
            container.Register<IVendaRepository, VendaRepository>(Lifestyle.Scoped);
            container.Register<IVendaService, VendaService>(Lifestyle.Scoped);
            container.Register<IContatoRepository, ContatoRepository>(Lifestyle.Scoped);
            container.Register<IContatoService, ContatoService>(Lifestyle.Scoped);
            container.Register<IRetiradaLojaRepository, RetiradaLojaRepository>(Lifestyle.Scoped);
            container.Register<IRetiradaLojaService, RetiradaLojaService>(Lifestyle.Scoped);
            container.Register<ILojaRepository, LojaRepository>(Lifestyle.Scoped);
            container.Register<ILojaService, LojaService>(Lifestyle.Scoped);
            container.Register<IRelatorioRepository, RelatorioRepository>(Lifestyle.Scoped);
            container.Register<IRelatorioService, RelatorioService>(Lifestyle.Scoped);
            container.Register<IVendedorRepository, VendedorRepository>(Lifestyle.Scoped);
            container.Register<IVendedorService, VendedorService>(Lifestyle.Scoped);
            container.Register<IInfoVendasRepository, InfoVendasRepository>(Lifestyle.Scoped);
            container.Register<IInfoVendasService, InfoVendasService>(Lifestyle.Scoped);
            container.Register<IFraseRepository, FraseRepository>(Lifestyle.Scoped);
            container.Register<IFraseService, FraseService>(Lifestyle.Scoped);
            container.Register<ILembreteRepository, LembreteRepository>(Lifestyle.Scoped);
            container.Register<ILembreteService, LembreteService>(Lifestyle.Scoped);
            container.Register<IMetricasRepository, MetricasRepository>(Lifestyle.Scoped);
            container.Register<IMetricasService, MetricasService>(Lifestyle.Scoped);
            container.Register<IVisitaLojasRepository, VisitaLojasRepository>(Lifestyle.Scoped);
            container.Register<IVisitaLojasService, VisitaLojasService>(Lifestyle.Scoped);
            container.Register<IClientesDistribuicaoRepository, ClientesDistribuicaoRepository>(Lifestyle.Scoped);
            container.Register<IClientesDistribuicaoService, ClientesDistribuicaoService>(Lifestyle.Scoped);
            container.Register<IOmniAcompanhamentoRepository, OmniAcompanhamentoRepository>(Lifestyle.Scoped);
            container.Register<IOmniAcompanhamentoService, OmniAcompanhamentoService>(Lifestyle.Scoped);
            container.Register<IContatoAtgRepository, ContatoAtgRepository>(Lifestyle.Scoped);
            container.Register<IContatoAtgService, ContatoAtgService>(Lifestyle.Scoped);
            container.Register<IManualRepository, ManualRepository>(Lifestyle.Scoped);
            container.Register<IManualService, ManualService>(Lifestyle.Scoped);
            container.Register<IPerguntasQuemEleRepository, PerguntasQuemEleRepository>(Lifestyle.Scoped);
            container.Register<IPerguntasQuemEleService, PerguntasQuemEleService>(Lifestyle.Scoped);
            container.Register<IPerguntasQuemEleRespostasRepository, PerguntasQuemEleRespostasRepository>(Lifestyle.Scoped);
            container.Register<IPerguntasQuemEleRespostasService, PerguntasQuemEleRespostasService>(Lifestyle.Scoped);
            container.Register<ICatalogoProdutosRepository, CatalogoProdutosRepository>(Lifestyle.Scoped);
            container.Register<ICatalogoProdutosService, CatalogoProdutosService>(Lifestyle.Scoped);
            container.Register<IVendedoresLicenciadosRepository, VendedoresLicenciadosRepository>(Lifestyle.Scoped);
            container.Register<IVendedoresLicenciadosService, VendedoresLicenciadosService>(Lifestyle.Scoped);
            container.Register<IDistribuicaoAgendaRepository, DistribuicaoAgendaRepository>(Lifestyle.Scoped);
            container.Register<IDistribuicaoAgendaService, DistribuicaoAgendaService>(Lifestyle.Scoped);
            container.Register<IProdutoRepository, ProdutoRepository>(Lifestyle.Scoped);
            container.Register<IProdutoService, ProdutoService>(Lifestyle.Scoped);
            container.Register<IConversaRepository, ConversaRepository>(Lifestyle.Scoped);
            container.Register<IConversaService, ConversaService>(Lifestyle.Scoped);
            container.Register<INotificacaoRepository, NotificacaoRepository>(Lifestyle.Scoped);
            container.Register<INotificacaoService, NotificacaoService>(Lifestyle.Scoped);
            container.Register<IAcessoExternoRepository, AcessoExternoRepository>(Lifestyle.Scoped);
            container.Register<IAcessoExternoService, AcessoExternoService>(Lifestyle.Scoped);
            container.Register<INowManutencaoRepository, NowManutencaoRepository>(Lifestyle.Scoped);
            container.Register<INowManutencaoService, NowManutencaoService>(Lifestyle.Scoped);

        }
    }
}
