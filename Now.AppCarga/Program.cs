﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.IO;
using System.Configuration;
using Now.Domain;
using Now.Domain.Contracts.Services;
using System.Globalization;
using SimpleInjector;
using Now.Domain.Contracts.Repositories;
using Now.Infrastructure.Repositories;
using Now.Infrastructure;
using Now.Business.Services;
using System.Speech.Recognition;

namespace Now.AppCarga
{
    class Program
    {
        static CultureInfo cult = new CultureInfo(ConfigurationManager.AppSettings["DefaultCulture"]);
        
        //public static INowManutencaoService _service;
        //public static IClienteService _serviceCliente;
        //static Program()
        //{
        //    INowManutencaoService service;
        //    IClienteService serviceCliente;

        //    _service = service;
        //    _serviceCliente = serviceCliente;
        //}
        //public static INowManutencaoService _service;
        //public static IClienteService _serviceCliente;
        //static Program(INowManutencaoService service, IClienteService serviceCliente)
        //{
        //    _service = service;
        //    _serviceCliente = new INowManutencaoService;
        //    //container = new Container();

        //    //container.Register<IClienteRepository, ClienteRepository>(Lifestyle.Scoped);
        //    //container.Register<IClienteService, ClienteService>(Lifestyle.Scoped);

        //    //container.Verify();
        //}

        static readonly Container container;

        static Program()
        {
            //container = new Container();

            ////container.RegisterSingleton(new NowContext());
            ////container.Register<IClienteRepository, ClienteRepository>(Lifestyle.Scoped);
            ////container.Register<IClienteService, ClienteService>(Lifestyle.Scoped);

            //container.Verify();
        }

        static void Main(string[] args)
        {
            //SpeechRecognitionEngine reconizer = new SpeechRecognitionEngine();
            //reconizer.SetInputToDefaultAudioDevice();
            //reconizer.LoadGrammar(new DictationGrammar());
            //reconizer.RecognizeAsync(RecognizeMode.Multiple);
            //reconizer.SpeechRecognized += new EventHandler<SpeechRecognizedEventArgs>(reconizer_reconizer);

            //while(true)
            //{
            //    Console.ReadKey();
            //}

            //FinalizarCarga();
            //ValidacaoCarga();
            //ProcessaLojaVendedorPreferencia();
        }

        private static void reconizer_reconizer(object sender, SpeechRecognizedEventArgs e)
        {
            Console.WriteLine(e.Result.Text);
        }

        public static void FinalizarCarga()
        {
            Thread.CurrentThread.CurrentCulture = cult;
            Thread.CurrentThread.CurrentUICulture = cult;
            // Atualiza Tabela de Controle Finalização de Carga
        }

        //public static void ValidacaoCarga()
        //{
        //    if (VerificaCarga())
        //    {
        //        bool cargaFinaliza = false;
        //        // Verifica na tabela de controle se a carga foi finalizada
        //        string CaminhoAplicacao = ConfigurationManager.AppSettings["CaminhoAplicacao"];
        //        string NomeAquivoAppOffLine = ConfigurationManager.AppSettings["NomeAquivoAppOffLine"];

        //        // Se a carga não for finalizada colocamos um aviso de manutenção e desativamos o sistema
        //        if (cargaFinaliza)
        //        {
        //            if (File.Exists(CaminhoAplicacao + NomeAquivoAppOffLine + ".htm"))
        //            {
        //                File.Move(CaminhoAplicacao + NomeAquivoAppOffLine + ".htm", CaminhoAplicacao + NomeAquivoAppOffLine + "_.htm");

        //                Console.WriteLine("O sistema está no AR!");
        //            }
        //            else Console.WriteLine("O arquivo NÃO EXISTE (" + CaminhoAplicacao + NomeAquivoAppOffLine + ".htm" + ")");
        //        }
        //        else if (File.Exists(CaminhoAplicacao + NomeAquivoAppOffLine + "_.htm"))
        //        {
        //            File.Move(CaminhoAplicacao + NomeAquivoAppOffLine + "_.htm", CaminhoAplicacao + NomeAquivoAppOffLine + ".htm");

        //            Console.WriteLine("O sistema foi colocado em manutenção!");
        //        }
        //        else Console.WriteLine("O arquivo NÃO EXISTE (" + CaminhoAplicacao + NomeAquivoAppOffLine + "_.htm" + ")");

        //        Thread.Sleep(5000);
        //    }
        //}

        //private static bool VerificaCarga()
        //{
        //    // Valida se o sistema teve sua carga inicial e final

        //    var list = _service.ListarLogCarga();
        //    var inicioCarga = (list.Where(x => (x.data_carga.ToString("ddMMyyyy") == DateTime.Now.ToString("ddMMyyyy") && x.status == "INICIO")).Count() > 0);
        //    var fimCarga = (list.Where(x => (x.data_carga.ToString("ddMMyyyy") == DateTime.Now.ToString("ddMMyyyy") && x.status == "FINALIZADO")).Count() > 0);

        //    return (inicioCarga && fimCarga);
        //}


        #region "Loja e Vendedor de Preferencia"

        private static void ProcessaLojaVendedorPreferencia()
        {
            var _serviceCliente = container.GetInstance<IClienteService>();
            #region "Loja de Preferencia"
            var clientes = _serviceCliente.ClientesAptosAgenda();
            #endregion "Loja de Preferencia"

        }

        #endregion "Loja e Vendedor de Preferencia"

    }
}

