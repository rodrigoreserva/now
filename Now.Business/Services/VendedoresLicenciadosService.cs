﻿using Now.Common.Validation;
using Now.Domain;
using Now.Domain.Contracts.Repositories;
using Now.Domain.Contracts.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Now.Business.Services
{
    public class VendedoresLicenciadosService : IVendedoresLicenciadosService
    {
        IVendedoresLicenciadosRepository _repositorio;
        IClienteRepository _repositorioCliente;
        public VendedoresLicenciadosService(IVendedoresLicenciadosRepository repositorio, IClienteRepository repositorioCliente)
        {
            _repositorio = repositorio;
            _repositorioCliente = repositorioCliente;
        }

        public void Salvar(VendedoresLicenciados vendedoresLicenciado)
        {
            _repositorio.Salvar(vendedoresLicenciado);
        }

        public void DistribuirClientes(int vendedorId)
        {
            _repositorio.DistribuirClientes(vendedorId);
        }        

        public IList<VendedoresLicenciados> Listar(string idLoja, string idVendedor)
        {
            var list = _repositorio.Listar(idLoja, idVendedor).ToList();

            return list;
        }

        public void Deletar(int id)
        {
            var VendedoresLicenciado = _repositorio.BuscarPorId(id);

            if (VendedoresLicenciado == null)
                throw new Exception("A licença informada não foi encontrada!");

            _repositorio.Deletar(id);
        }

        public VendedoresLicenciados BuscarPorId(int id)
        {
            return _repositorio.BuscarPorId(id);
        }

        public IList<VendedoresLicenciadosTipo> ListarMotivos()
        {
            return _repositorio.ListarMotivos();
        }

        public IList<VendedoresAgendaDistribuicao> BuscarAgendaDistribuidas(int vendedorId)
        {
            var list = _repositorio.BuscarAgendaDistribuidas(vendedorId);

            return list;
        }
    }
}
