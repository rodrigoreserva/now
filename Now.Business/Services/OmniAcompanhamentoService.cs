﻿using Now.Common.Validation;
using Now.Domain;
using Now.Domain.Contracts.Repositories;
using Now.Domain.Contracts.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Now.Business.Services
{
    public class OmniAcompanhamentoService : IOmniAcompanhamentoService
    {
        IOmniAcompanhamentoRepository _repositorio;
        
        public OmniAcompanhamentoService(IOmniAcompanhamentoRepository repositorio)
        {
            _repositorio = repositorio;            
        }
        

        public IList<OmniAcompanhamento> Listar(string IdLojaSeleconado, string vendedorId)
        {
            var list = _repositorio.Listar(IdLojaSeleconado, vendedorId).ToList();

            return list;
        }

        public void AtualizarStatusOmni(string pedido)
        {
            _repositorio.AtualizarStatusOmni(pedido);
        }        
    }
}
