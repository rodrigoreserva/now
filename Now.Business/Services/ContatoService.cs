﻿using Now.Common.Validation;
using Now.Domain;
using Now.Domain.Contracts.Repositories;
using Now.Domain.Contracts.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Now.Business.Services
{
    public class ContatoService : IContatoService
    {
        IContatoRepository _repositorio;
        public ContatoService(IContatoRepository repositorio)
        {
            _repositorio = repositorio;
        }

        public IList<FraseContato> ContatosPorCliente(string idcliente)
        {
            var lista = _repositorio.ContatosPorCliente(idcliente);

            return lista;
        }

        public Contato AdicionarContato(Contato contato)
        {
            return _repositorio.AdicionarContato(contato);
        }

        public Contato AtualizarTermometroContato(string id, string termometro)
        {
            return _repositorio.AtualizarTermometroContato(id, termometro);
        }

        public IList<Produto> ListarProduto(string id)
        {
            var lista = _repositorio.ListarProduto(id);

            return lista;
        }

        public Contato BuscarContato(string idContato)
        {
            return _repositorio.BuscarContato(idContato);
        }


        public ContatoCliente BuscarCliente(string telefone)
        {
            return _repositorio.BuscarCliente(telefone);
        }

        public ContatoCliente BuscarContatoCliente(string telefone)
        {
            return _repositorio.BuscarContatoCliente(telefone);
        }

        public Contato AdicionarAvaliacao(string idContato, string vlrAvaliacao, string observacao, int flgContatoFeito, string texto)
        {
            return _repositorio.AdicionarAvaliacao(idContato, vlrAvaliacao, observacao, flgContatoFeito, texto);

        }

        public IList<ContatoClienteAvaliacao> ReenvioAvaliacao()
        {
            return _repositorio.ReenvioAvaliacao();
        }

        public ContatoCliente AdicionarCliente(string idContato, string nome, string cpf, string telefone,string email)
        {
            return _repositorio.AdicionarCliente(idContato, nome, cpf, telefone, email);
        }
    }
}
