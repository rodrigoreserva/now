﻿using Now.Common.Validation;
using Now.Domain;
using Now.Domain.Contracts.Repositories;
using Now.Domain.Contracts.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Now.Business.Services
{
    public class FraseService : IFraseService
    {
        IFraseRepository _repositorio;
        public FraseService(IFraseRepository repositorio)
        {
            _repositorio = repositorio;
        }

        public IEnumerable<IGrouping<string, Frase>> FraseClienteDetalhes(string idcliente)
        {
            var list = _repositorio.FraseClienteDetalhes(idcliente).ToList();

            return list;
        }


        public IList<Frase> FrasesMotivoLigacao(string idcliente)
        {
            var list = _repositorio.FrasesMotivoLigacao(idcliente).ToList();

            return list;
        }
    }
}
