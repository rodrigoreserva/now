﻿using Now.Common.Validation;
using Now.Domain;
using Now.Domain.Contracts.Repositories;
using Now.Domain.Contracts.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Now.Business.Services
{
    public class VisitaLojasService : IVisitaLojasService
    {
        IVisitaLojasRepository _repositorio;
        IClienteRepository _repositorioCliente;
        public VisitaLojasService(IVisitaLojasRepository repositorio, IClienteRepository repositorioCliente)
        {
            _repositorio = repositorio;
            _repositorioCliente = repositorioCliente;
        }

        public void SalvarVisitaLojas(VisitaLojas visitalojas)
        {
            _repositorio.SalvarVisitaLojas(visitalojas);
        }

        public IList<VisitaLojas> ListarVisitaLojas(string idLoja)
        {
            var list = _repositorio.ListarVisitaLojas(idLoja).ToList();

            return list;
        }


        public void DeleteVisitaLojas(int id)
        {
            _repositorio.DeleteVisitaLojas(id);
        }        

    }
}
