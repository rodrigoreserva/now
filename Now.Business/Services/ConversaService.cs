﻿using Now.Common.Validation;
using Now.Domain;
using Now.Domain.Contracts.Repositories;
using Now.Domain.Contracts.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Now.Business.Services
{
    public class ConversaService : IConversaService
    {
        IConversaRepository _repositorio;
        public ConversaService(IConversaRepository repositorio)
        {
            _repositorio = repositorio;
        }

        public IList<Conversa> SalvarConversa(Conversa Conversa)
        {
            return _repositorio.SalvarConversa(Conversa);
        }

        public IList<Conversa> ListarConversa()
        {
            var list = _repositorio.ListarConversa().ToList();

            return list;
        }        

        public IList<Conversa> DeleteConversa(int id)
        {
           return _repositorio.DeleteConversa(id);
        }

    }
}
