﻿using Now.Common.Validation;
using Now.Domain;
using Now.Domain.Contracts.Repositories;
using Now.Domain.Contracts.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Now.Business.Services
{
    public class VendedorService : IVendedorService
    {
        IVendedorRepository _repositorio;
        public VendedorService(IVendedorRepository repositorio)
        {
            _repositorio = repositorio;
        }

        public void SalvarImgPerfilVendedor(string src, string id)
        {
            _repositorio.SalvarImgPerfilVendedor(src, id);
        }

        public IList<Vendedor> GetVendedor(string idloja, string idvendedor, int idusuario)
        {
            var list = _repositorio.GetVendedor(idloja, idvendedor, idusuario).ToList();

            return list;
        }

        public IList<Vendedor> ListarVendedores(string idloja = "", string idvendedor = "", int idusuario = 0)
        {
            var list = _repositorio.ListarVendedores(idloja, idvendedor, idusuario).ToList();

            return list;
        }

        #region "Controle Vendedor"


        public IList<VendedoresControleTipo> ListarVendedoresControleTipo()
        {
            var list = _repositorio.ListarVendedoresControleTipo().ToList();

            return list;
        }

        public IList<VendedoresControle> SalvarVendedoresControle(VendedoresControle vendedoresControle)
        {
           return _repositorio.SalvarVendedoresControle(vendedoresControle);
        }

        #endregion "Controle Vendedor"

    }
}
