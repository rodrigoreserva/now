﻿using Now.Common.Validation;
using Now.Domain;
using Now.Domain.Contracts.Repositories;
using Now.Domain.Contracts.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Now.Business.Services
{
    public class AgendaService : IAgendaService
    {
        IAgendaRepository _repositorio;
        public AgendaService(IAgendaRepository repositorio)
        {
            _repositorio = repositorio;
        }

        public IList<MenuAgendamento> TypeCalendar(Usuario UsuarioLogado, string idusuario = "", string idloja = "", string idvendedor = "", string param = "", bool _lembrete = false)
        {
            var listMenuAgend = _repositorio.TypeCalendar(UsuarioLogado, UsuarioLogado.id.ToString(), idloja, idvendedor, param, _lembrete).ToList();

            return listMenuAgend;
        }

        public IList<Agendamento> FindCalendars(string type, string strBusca, string idusuario = "", string idloja = "", string idvendedor = "", bool administrador = false, string idCliente = "")
        {
            var listMenuAgend = _repositorio.FindCalendars(type, strBusca, idusuario, idloja, idvendedor, administrador, idCliente) .ToList();

            return listMenuAgend;
        }

        public IList<MenuTipoAgendamento> ListarAgendas(string idloja)
        {
            var listMenuAgend = _repositorio.ListarAgendas(idloja).ToList();

            return listMenuAgend;
        }

        public void AgendaNaoFalou(MenuAgendamentoNaoFalou naoFalou)
        {
            _repositorio.AgendaNaoFalou(naoFalou);
        }
    }
}
