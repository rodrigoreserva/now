﻿using Now.Common.Validation;
using Now.Domain;
using Now.Domain.Contracts.Repositories;
using Now.Domain.Contracts.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Now.Business.Services
{
    public class ClienteService : IClienteService
    {
        IClienteRepository _repositorio;
        public ClienteService(IClienteRepository repositorio)
        {
            _repositorio = repositorio;
        }

        public Cliente FindClientById(string id, int idUsuario, string idLoja, string idVendedorSelecionado)
        {
            var cliente = _repositorio.FindClientById(id, idUsuario, idLoja, idVendedorSelecionado);

            if (cliente == null) throw new Exception("Nenhum Cliente Localizado");

            return cliente;
        }


        public void AtualizarPreferenciaContato(int id, string preferenciaContato)
        {
            _repositorio.SalvarPreferenciaContato(id, preferenciaContato);
        }

        public IList<Cliente> BuscarClienteCpfOrigemLoja(string origemLoja, string cpf)
        {
            var cliente = _repositorio.BuscarClienteCpfOrigemLoja(origemLoja, cpf);            

            return cliente;
        }
        

        public IList<Cliente> BuscarCliente(string origemLoja, string nome = "", string cpf = "", string telefone = "", string idLoja = "", string idVendedor = "", string filtroPesquisa = "")
        {
            var cliente = _repositorio.BuscarCliente(origemLoja, nome, cpf, telefone, idLoja, idVendedor, filtroPesquisa);

            if (cliente == null) throw new Exception("Nenhum Cliente Localizado");

            return cliente;
        }

        public IList<ClienteDependente> BuscarDependentes(int idCliente)
        {
            var dependente = _repositorio.BuscarDependentes(idCliente);           

            return dependente;
        }

        public ClienteDependente ObterDependente(string IdDependente)
        {
            var dependente = _repositorio.ObterDependente(IdDependente);

            return dependente;
        }

        public void RemoverDependente(int idDependente)
        {
            _repositorio.RemoverDependente(idDependente);
            
        }
        public IList<ClienteNovo> BuscarClienteNovo(string idLoja, string idVendedor, string idClienteNovo)
        {
            var cliente = _repositorio.BuscarClienteNovo(idLoja, idVendedor, idClienteNovo);

            if (cliente == null) throw new Exception("Nenhum Cliente Localizado");

            return cliente;
        }

        public IndicacaoProduto BuscarIndicacaoProdutosPorIndicacao(string id)
        {
            var obj = _repositorio.BuscarIndicacaoProdutosPorIndicacao(id);            

            return obj;
        }

        public void SalvarDependenteCliente(ClienteDependente clienteDependente)
        {
            _repositorio.SalvarDependenteCliente(clienteDependente);
        }

        public void SalvarClienteQuemEEle(int idcliente, string caracteristica)
        {
            _repositorio.SalvarClienteQuemEEle(idcliente, caracteristica);
        }

        public void SalvarClienteObs(int idcliente, string caracteristica)
        {
            _repositorio.SalvarClienteObs(idcliente, caracteristica);
        }

        public void SalvarFavoritos(int idcliente, int idVendedor, string cliFavarito)
        {
            _repositorio.SalvarFavoritos(idcliente, idVendedor, cliFavarito);
        }        

        public void SalvarClienteNovo(ClienteNovo clienteNovo)
        {
            _repositorio.SalvarClienteNovo(clienteNovo);
        }

        public void SalvarDependenteClienteNovo(ClienteNovoDependente clienteNovoDependente)
        {
            _repositorio.SalvarDependenteClienteNovo(clienteNovoDependente);
        }        

        public void ExluirClienteNovo(string Id)
        {
            _repositorio.ExluirClienteNovo(Id);
        }
                
        public int SalvarIndicacaoProdutos(int clienteId, int? lojaId, int? vendedorId, IList<Produto> prodSelecionados)
        {
            return _repositorio.SalvarIndicacaoProdutos(clienteId, lojaId, vendedorId, prodSelecionados);
        }

        public void SalvarEnvioIndicacaoProdutos(int idIndicacao, IList<IndicacaoProdutoItens> indicacaoProdutoItens)
        {
            _repositorio.SalvarEnvioIndicacaoProdutos(idIndicacao, indicacaoProdutoItens);
        }
        public IList<Cliente> ClientesAptosAgenda()
        {
            var cliente = _repositorio.ClientesAptosAgenda();            

            return cliente;
        }        
    }
}
