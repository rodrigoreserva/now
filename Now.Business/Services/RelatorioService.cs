﻿using Now.Common.Validation;
using Now.Domain;
using Now.Domain.Contracts.Repositories;
using Now.Domain.Contracts.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Now.Business.Services
{
    public class RelatorioService : IRelatorioService
    {
        IRelatorioRepository _repositorio;
        public RelatorioService(IRelatorioRepository repositorio)
        {
            _repositorio = repositorio;
        }

        public IList<ReportEfetividade> ListarEfetividadeLojas(string nomeProcedure, string idLoja, string dtIni, string dtFim)
        {
            var list = _repositorio.ListarEfetividadeLojas(nomeProcedure, idLoja, dtIni, dtFim).ToList();

            return list;
        }

        public IList<ReportEfetividade> ListarEfetividadeLojas(string idLoja, string dtIni, string dtFim)
        {
            var list = _repositorio.ListarEfetividadeLojas(idLoja, dtIni, dtFim).ToList();

            return list;
        }

        public IList<ReportEfetividadeAgenda> ListarReportEfetividadeAgenda(string nomeProcedure, string idLoja, string dtIni, string dtFim)
        {
            var list = _repositorio.ListarReportEfetividadeAgenda(nomeProcedure, idLoja, dtIni, dtFim).ToList();

            return list;
        }

        public IList<ReportEfetividadeAgenda> ListarReportEfetividadeAgendaDashboard(string nomeProcedure, string idVendedor, string idLoja, string dtIni, string dtFim)
        {
            var list = _repositorio.ListarReportEfetividadeAgendaDashboard(nomeProcedure, idVendedor, idLoja, dtIni, dtFim).ToList();

            return list;
        }

        public IList<ReportEfetividadeAgenda> ListarReportEfetividadeAgendaDashboard(string idVendedor, string idLoja, string dtIni, string dtFim)
        {
            var list = _repositorio.ListarReportEfetividadeAgendaDashboard(idVendedor, idLoja, dtIni, dtFim).ToList();

            return list;
        }

        public IList<ReportDedoDuro> ListarReportDedoDuro(string nomeProcedure, string idLoja, string dtIni, string dtFim)
        {
            var list = _repositorio.ListarReportDedoDuro(nomeProcedure, idLoja, dtIni, dtFim).ToList();

            return list;
        }

        public IList<DashboardEfetividadesVendedores> ListarDashboardEfetividadesVendedores(string nomeProcedure, string idLoja, string dtIni, string dtFim)
        {
            var list = _repositorio.ListarDashboardEfetividadesVendedores(nomeProcedure, idLoja, dtIni, dtFim).ToList();

            return list;
        }

        public IList<DashboardEfetividadesVendedores> ListarDashboardEfetividadesVendedores(string idLoja, string dtIni, string dtFim)
        {
            var list = _repositorio.ListarDashboardEfetividadesVendedores(idLoja, dtIni, dtFim).ToList();

            return list;
        }

        public IList<RelatorioEfetividadePorMes> ListarEfetividadePorMes(string nomeProcedure, string idLoja, int ano)
        {
            var list = _repositorio.ListarEfetividadePorMes(nomeProcedure, idLoja, ano).ToList();

            return list;
        }

        public IList<RelatorioVisitasLojas> ListarVisitasLojas(string nomeProcedure, string idLoja, string dtIni, string dtFim)
        {
            var list = _repositorio.ListarVisitasLojas(nomeProcedure, idLoja, dtIni, dtFim).ToList();

            return list;
        }

        public IList<ReportRankingVendedores> ListarRankVendedores(string idLoja, string dtIni, string dtFim, int? gerente)
        {
            var list = _repositorio.ListarRankVendedores(idLoja, dtIni, dtFim, gerente).ToList();

            return list;
        }

        public IList<ReportRankingCornerMini> ListarRankingCornerMini(string idLoja, string dtIni, string dtFim, int? gerente)
        {
            var list = _repositorio.ListarRankingCornerMini(idLoja, dtIni, dtFim, gerente).ToList();

            return list;
        }

        public IList<ReportDedoDuro> ListarDedoDuro(string idLoja, string dtIni, string dtFim, int? gerente)
        {
            var list = _repositorio.ListarDedoDuro(idLoja, dtIni, dtFim, gerente).ToList();

            return list;
        }

        public IList<ReportEfetividadeAgendaLojas> ListarEfetividadesPorAgendaLojaVendedor(string idLoja, string dtIni, string dtFim, int IdAgenda, int? gerente)
        {
            var list = _repositorio.ListarEfetividadesPorAgendaLojaVendedor(idLoja, dtIni, dtFim, IdAgenda, gerente).ToList();

            return list;
        }

        public IList<T> ListarRelatorios<T>(string nomeProcedure, string idLoja, string dtIni, string dtFim, int? gerente)
        {
            var list = _repositorio.ListarRelatorios<T>(nomeProcedure, idLoja, dtIni, dtFim, gerente).ToList();

            return list;
        }

        public IList<T> ListarRelatorioComVendedor<T>(string nomeProcedure, string idVendedor)
        {
            var list = _repositorio.ListarRelatorioComVendedor<T>(nomeProcedure, idVendedor).ToList();

            return list;
        }       

    }
}
