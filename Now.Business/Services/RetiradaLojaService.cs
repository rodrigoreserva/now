﻿using Now.Common.Validation;
using Now.Domain;
using Now.Domain.Contracts.Repositories;
using Now.Domain.Contracts.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Now.Business.Services
{
    public class RetiradaLojaService : IRetiradaLojaService
    {
        IRetiradaLojaRepository _repositorio;
        public RetiradaLojaService(IRetiradaLojaRepository repositorio)
        {
            _repositorio = repositorio;
        }

        public IList<PedidosRetiradaLoja> ListarRetiradas6Meses(string idCliente)
        {
            var lista = _repositorio.ListarRetiradas6Meses(idCliente);

            return lista;
        }
    }
}
