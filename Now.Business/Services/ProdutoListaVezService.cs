﻿using Now.Common.Validation;
using Now.Domain;
using Now.Domain.Contracts.Repositories;
using Now.Domain.Contracts.Services;
using Now.Domain.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Now.Business.Services
{
    public class ProdutoListaVezService : IProdutoListaVez
    {
        IProdutoListaVezRepository _repositorio;
        public ProdutoListaVezService(IProdutoListaVezRepository repositorio)
        {
            _repositorio = repositorio;
        }


        public IList<ProdutoDTO> ListarProdutos(string descricao, string grupo)
        {
          return  _repositorio.ListarProdutos(descricao, grupo);
        }
    }
}
