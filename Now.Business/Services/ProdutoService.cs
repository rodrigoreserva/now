﻿using Now.Common.Validation;
using Now.Domain;
using Now.Domain.Contracts.Repositories;
using Now.Domain.Contracts.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Now.Business.Services
{
    public class ProdutoService : IProdutoService
    {
        IProdutoRepository _repositorio;
        public ProdutoService(IProdutoRepository repositorio)
        {
            _repositorio = repositorio;
        }

        public Produto ObterProduto(string produto, string cor_produto)
        {
            var _produto = _repositorio.ObterProduto(produto, cor_produto);

            return _produto;
        }
    }
}
