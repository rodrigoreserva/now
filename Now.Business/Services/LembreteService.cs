﻿using Now.Common.Validation;
using Now.Domain;
using Now.Domain.Contracts.Repositories;
using Now.Domain.Contracts.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Now.Business.Services
{
    public class LembreteService : ILembreteService
    {
        ILembreteRepository _repositorio;
        public LembreteService(ILembreteRepository repositorio)
        {
            _repositorio = repositorio;
        }

        public void SalvarLembrete(Lembrete lembrete)
        {
            _repositorio.SalvarLembrete(lembrete);
        }

        public IList<Lembrete> ListarLembretes(int idUsuario, string idLoja, string idLembrete)
        {
            var list = _repositorio.ListarLembretes(idUsuario, idLoja, idLembrete).ToList();

            return list;
        }
    }
}
