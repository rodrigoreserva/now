﻿using Now.Common.Validation;
using Now.Domain;
using Now.Domain.Contracts.Repositories;
using Now.Domain.Contracts.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Now.Business.Services
{
    public class ClientesDistribuicaoService : IClientesDistribuicaoService
    {
        IClientesDistribuicaoRepository _repositorio;
        public ClientesDistribuicaoService(IClientesDistribuicaoRepository repositorio)
        {
            _repositorio = repositorio;
        }

        public IList<ClientesDistribuicao> ListarClientesDistribuicao(int idLoja, string tipoAprovacao, string tipoDistribuicao)
        {
            var list = _repositorio.ListarClientesDistribuicao(idLoja, tipoAprovacao, tipoDistribuicao).ToList();

            return list;
        }

        public void AtualizarTrocaVendedor(string lojaId, string vendedorId, string clienteId)
        {
            _repositorio.AtualizarTrocaVendedor(lojaId, vendedorId, clienteId);
        }

    }
}
