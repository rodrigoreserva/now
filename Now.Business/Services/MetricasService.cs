﻿using Now.Common.Validation;
using Now.Domain;
using Now.Domain.Contracts.Repositories;
using Now.Domain.Contracts.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Now.Business.Services
{
    public class MetricasService : IMetricasService
    {
        IMetricasRepository _repositorio;
        IClienteRepository _repositorioCliente;
        public MetricasService(IMetricasRepository repositorio, IClienteRepository repositorioCliente)
        {
            _repositorio = repositorio;
            _repositorioCliente = repositorioCliente;
        }


        public IList<Metricas> ListarMetricas(string nomeProcedure, string idLoja, string idVendedor, string dtIni, string dtFim)
        {
            var list = _repositorio.ListarMetricas(nomeProcedure, idLoja, idVendedor, dtIni, dtFim).ToList();

            return list;
        }

        public IList<Metricas> ListarMetricas(string idLoja, string idVendedor, string dtIni, string dtFim)
        {
            var list = _repositorio.ListarMetricas(idLoja, idVendedor, dtIni, dtFim).ToList();

            return list;
        }


    }
}
