﻿using Now.Common.Validation;
using Now.Domain;
using Now.Domain.Contracts.Repositories;
using Now.Domain.Contracts.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Now.Business.Services
{
    public class PerguntasQuemEleRespostasService : IPerguntasQuemEleRespostasService
    {
        IPerguntasQuemEleRespostasRepository _repositorio;        
        public PerguntasQuemEleRespostasService(IPerguntasQuemEleRespostasRepository repositorio)
        {
            _repositorio = repositorio;            
        }

        public IList<PerguntasQuemEleRespostas> SalvarPerguntasQuemEleRespostas(PerguntasQuemEleRespostas perguntasQuemEle)
        {
           return _repositorio.SalvarPerguntasQuemEleRespostas(perguntasQuemEle);
        }

        public IList<PerguntasQuemEleRespostas> ListarPerguntasQuemEleRespostas(int idPergunta)
        {
            var list = _repositorio.ListarPerguntasQuemEleRespostas(idPergunta).ToList();

            return list;
        }

        public IList<PerguntasQuemEleRespostas> ListarTodasPerguntasQuemEleRespostas()
        {
            var list = _repositorio.ListarTodasPerguntasQuemEleRespostas().ToList();

            return list;
        }       

        public IList<PerguntasQuemEleRespostas> DeletePerguntasQuemEleRespostas(int id, int idPergunta)
        {
           return _repositorio.DeletePerguntasQuemEleRespostas(id, idPergunta);
        }

    }
}
