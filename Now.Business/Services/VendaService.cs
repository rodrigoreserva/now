﻿using Now.Common.Validation;
using Now.Domain;
using Now.Domain.Contracts.Repositories;
using Now.Domain.Contracts.Services;
using Now.Domain.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Now.Business.Services
{
    public class VendaService : IVendaService
    {
        IVendaRepository _repositorio;
        public VendaService(IVendaRepository repositorio)
        {
            _repositorio = repositorio;
        }


        public IList<Venda> HistoricoVendas(string idcliente)
        {
            var list = _repositorio.HistoricoVendas(idcliente).ToList();

            return list;
        }

        public IList<GrupoProdutoItem> GrupoProdutos(string idcliente)
        {
            var list = _repositorio.GrupoProdutos(idcliente).ToList();

            return list;
        }

        public InfoVendasDTO ObterInfoVendas(string vendedor_id, string loja_id, string data)
        {
            var list = _repositorio.ObterInfoVendas(vendedor_id, loja_id, data);

            return list;
        }
    }
}
