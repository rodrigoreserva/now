﻿using Now.Common.Validation;
using Now.Domain;
using Now.Domain.Contracts.Repositories;
using Now.Domain.Contracts.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Now.Business.Services
{
    public class ContatoAtgService : IContatoAtgService
    {
        IContatoAtgRepository _repositorio;
        public ContatoAtgService(IContatoAtgRepository repositorio)
        {
            _repositorio = repositorio;
        }

        public IList<ContatoAtg> BuscarContatosPorData(DateTime data)
        {
            var lista = _repositorio.BuscarContatosPorData(data);

            return lista;
        }

        public void AdicionarContatoAtg(ContatoAtg contato)
        {
            _repositorio.AdicionarContatoAtg(contato);
        }

    }
}
