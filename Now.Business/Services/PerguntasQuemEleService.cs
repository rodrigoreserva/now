﻿using Now.Common.Validation;
using Now.Domain;
using Now.Domain.Contracts.Repositories;
using Now.Domain.Contracts.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Now.Business.Services
{
    public class PerguntasQuemEleService : IPerguntasQuemEleService
    {
        IPerguntasQuemEleRepository _repositorio;
        public PerguntasQuemEleService(IPerguntasQuemEleRepository repositorio)
        {
            _repositorio = repositorio;
        }

        public IList<PerguntasQuemEle> SalvarPerguntasQuemEle(PerguntasQuemEle perguntasQuemEle)
        {
            return _repositorio.SalvarPerguntasQuemEle(perguntasQuemEle);
        }

        public IList<PerguntasQuemEle> ListarPerguntasQuemEle()
        {
            var list = _repositorio.ListarPerguntasQuemEle().ToList();

            return list;
        }        

        public IList<PerguntasQuemEle> DeletePerguntasQuemEle(int id)
        {
           return _repositorio.DeletePerguntasQuemEle(id);
        }

    }
}
