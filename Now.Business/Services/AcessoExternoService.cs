﻿using Now.Common.Validation;
using Now.Domain;
using Now.Domain.Contracts.Repositories;
using Now.Domain.Contracts.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Now.Business.Services
{
    public class AcessoExternoService : IAcessoExternoService
    {
        IAcessoExternoRepository _repositorio;
        IClienteRepository _repositorioCliente;
        public AcessoExternoService(IAcessoExternoRepository repositorio, IClienteRepository repositorioCliente)
        {
            _repositorio = repositorio;
            _repositorioCliente = repositorioCliente; 
        }

        public void SalvarVisitaLojas(VisitaLojas visitalojas)
        {
            _repositorio.SalvarVisitaLojas(visitalojas);
        }

        public IList<VisitaLojas> ListarVisitaLojas(string idLoja)
        {
            var list = _repositorio.ListarVisitaLojas(idLoja).ToList();

            return list;
        }

        public IList<AbandonoCarrinhoClienteProduto> BuscarAbandonoCarrinhoClienteProduto(string cpf)
        {
            var list = _repositorio.BuscarAbandonoCarrinhoClienteProduto(cpf).ToList();

            return list;
        }

        public IList<AbandonoCarrinhoNowLinxSolicitacao> BuscarAbandonoCarrinhoNowLinxSolicitacao(string cpf)
        {
            var list = _repositorio.BuscarAbandonoCarrinhoNowLinxSolicitacao(cpf).ToList();

            return list;
        }

        public void DeleteVisitaLojas(int id)
        {
            _repositorio.DeleteVisitaLojas(id);
        }

        public void InserirProdutoAbandonoCarrinho(string cpf, string produto, string cor_produto, string codigo_filial, string grade, string sku, DateTime _dataControle, DateTime dataExperimente, string periodo, DateTime dataAbandono)
        {
            _repositorio.InserirProdutoAbandonoCarrinho(cpf, produto, cor_produto, codigo_filial, grade, sku, _dataControle, dataExperimente, periodo, dataAbandono);
        }

        public IList<AbandonoCarrinhoClienteProdutoEstoque> BuscarProdutoClienteExperimenteEstoque(string produto)
        {
           return _repositorio.BuscarProdutoClienteExperimenteEstoque(produto);
        }

        public bool InsereClienteReservadoAgenda(string cpf, string lojaid, string telefone, string tipo, DateTime? datasolicitacao)
        {
            return _repositorio.InsereClienteReservadoAgenda(cpf, lojaid, telefone, tipo, datasolicitacao);
        }

        public IList<ClienteReservadoAgendada> BuscarClienteReservado(string cpf, string lojaid)
        {
            return _repositorio.BuscarClienteReservado(cpf, lojaid);
        }


        
    }
}
