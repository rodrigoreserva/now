﻿using Now.Common.Validation;
using Now.Domain;
using Now.Domain.Contracts.Repositories;
using Now.Domain.Contracts.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Now.Business.Services
{
    public class CatalogoProdutosService : ICatalogoProdutosService
    {
        ICatalogoProdutosRepository _repositorio;

        public CatalogoProdutosService(ICatalogoProdutosRepository repositorio)
        {
            _repositorio = repositorio;
        }

        public IList<CatalogoProdutos> ListarCatalogoProdutos(string strPesqProduto, int pageIndex, ref int PageCount)
        {
            var list = _repositorio.ListarCatalogoProdutos(strPesqProduto, pageIndex, ref PageCount).ToList();

            return list;
        }


        public IList<String> ListaGrupoProdutos(string griffe)
        {
            var list = _repositorio.ListaGrupoProdutos(griffe);

            return list;
        }
        public IList<String> ListaGriffe()
        {
            var list = _repositorio.ListaGriffe();

            return list;
        }

    }
}
