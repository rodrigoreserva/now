﻿using Now.Common.Validation;
using Now.Domain;
using Now.Domain.Contracts.Repositories;
using Now.Domain.Contracts.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Now.Business.Services
{
    public class NotificacaoService : INotificacaoService
    {
        INotificacaoRepository _repositorio;
        public NotificacaoService(INotificacaoRepository repositorio)
        {
            _repositorio = repositorio;
        }

        public IList<Notificacao> Obter(string LojaId, string vendedorId)
        {
            var _Notificacao = _repositorio.Obter(LojaId,vendedorId);

            return _Notificacao;
        }

        public IList<Notificacao> ObterTodas(string LojaId, string vendedorId)
        {
            var _Notificacao = _repositorio.ObterTodas(LojaId, vendedorId);

            return _Notificacao;
        }
        public void AtualizarVistoNotificacao(string Id)
        {            
            try
            {
                _repositorio.AtualizarVistoNotificacao(Id);                
            }
            catch (Exception)
            {         
            }
        }        
    }
}
