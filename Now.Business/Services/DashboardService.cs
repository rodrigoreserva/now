﻿using Now.Common.Validation;
using Now.Domain;
using Now.Domain.Contracts.Repositories;
using Now.Domain.Contracts.Services;
using Now.Domain.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Now.Business.Services
{
    public class DashboardService : IDashboardService
    {
        IDashboardRepository _repositorio;
        public DashboardService(IDashboardRepository repositorio)
        {
            _repositorio = repositorio;
        }

        public IList<ReportRankingVendedores> ListarRankingVendedores(string nomeProcedure, string idLoja, string dtIni, string dtFim)
        {
            var list = _repositorio.ListarRankingVendedores(nomeProcedure, idLoja, dtIni, dtFim).ToList();

            return list;
        }

        public IList<DashboardRankingVendedores> ListarDashboardRankingVendedores(string nomeProcedure, string idLoja, string dtIni, string dtFim)
        {
            var list = _repositorio.ListarDashboardRankingVendedores(nomeProcedure, idLoja, dtIni, dtFim).ToList();

            return list;
        }

        public IList<DashboardRankingVendedores> ListarDashboardRankingVendedores(string idLoja, string dtIni, string dtFim)
        {
            var list = _repositorio.ListarDashboardRankingVendedores(idLoja, dtIni, dtFim).ToList();

            return list;
        }

        public IList<DashboardGraficoEfetividade> ListarDashboardGraficoEfetividade(string nomeProcedure, string idLoja, string idVendedor, string idTodasLojas, string dtIni, string dtFim)
        {
            var list = _repositorio.ListarDashboardGraficoEfetividade(nomeProcedure, idLoja, idVendedor, idTodasLojas, dtIni, dtFim).ToList();

            return list;
        }

        public IList<Dashboard> ListarMetricas(string idLoja, string idVendedor, string dtIni, string dtFim)
        {
            var list = _repositorio.ListarMetricas(idLoja, idVendedor, dtIni, dtFim).ToList();

            return list;
        }

        public IList<MetricasDTO> ListarNovoDashBoard(string nomeProcedure, string idLoja, string idVendedor, string dtIni, string dtFim)
        {
            var list = _repositorio.ListarNovoDashBoard(nomeProcedure, idLoja, idVendedor, dtIni, dtFim).ToList();

            return list;
        }

    }
}
