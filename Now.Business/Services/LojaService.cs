﻿using Now.Common.Validation;
using Now.Domain;
using Now.Domain.Contracts.Repositories;
using Now.Domain.Contracts.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Now.Business.Services
{
    public class LojaService : ILojaService
    {
        ILojaRepository _repositorio;
        public LojaService(ILojaRepository repositorio)
        {
            _repositorio = repositorio;
        }

        public IList<Loja> ListarLojasVisiveisPorUsuario(int idUsuario, string idLoja)
        {
            var list = _repositorio.ListarLojasVisiveisPorUsuario(idUsuario, idLoja).ToList();

            return list;
        }

        public IList<Loja> ListarLojas(string idLoja)
        {
            var list = _repositorio.ListarLojas(idLoja).ToList();

            return list;
        }

        public IList<Loja> ListarLojas()
        {
            var list = _repositorio.ListarLojas().ToList();

            return list;
        }

        public IList<Loja> ListarTodasLojas()
        {
            var list = _repositorio.ListarTodasLojas().ToList();

            return list;
        }
        public IList<Loja> ListarLojasFormatados()
        {
            var list = _repositorio.ListarLojasFormatados().ToList();

            return list;
        }
        
        public IList<LojaDistribuicaoPerfil> ListarLojasDistribuicao()
        {
            var list = _repositorio.ListarLojasDistribuicao().ToList();

            return list;
        }

        public IList<DistribuicaoSupervisaoOrg> ListarSupervisaoDistribuicao()
        {
            var list = _repositorio.ListarSupervisaoDistribuicao().ToList();

            return list;
        }

        public IList<LojaDistribuicaoOrigemDestino> ListarLojaDistribuicaoOrigemDestino()
        {
            var list = _repositorio.ListarLojaDistribuicaoOrigemDestino().ToList();

            return list;
        }

        public IList<LojaDistribuicaoOrigemDestino> SalvarLojaDistribuicaoOrigemDestino(LojaDistribuicaoOrigemDestino obj)
        {
            var list = _repositorio.SalvarLojaDistribuicaoOrigemDestino(obj).ToList();

            return list;
        }

        public void UpdateLoja(Loja loja)
        {
            _repositorio.UpdateLoja(loja);
        }

        public void UpdatePerfilSupervisao(string idUsuario, string idLoja, string idPerfil)
        {
            _repositorio.UpdatePerfilSupervisao(idUsuario, idLoja, idPerfil);            
        }        
    }
}
