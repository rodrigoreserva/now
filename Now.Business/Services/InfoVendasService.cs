﻿using Now.Common.Validation;
using Now.Domain;
using Now.Domain.Contracts.Repositories;
using Now.Domain.Contracts.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Now.Business.Services
{
    public class InfoVendasService : IInfoVendasService
    {
        IInfoVendasRepository _repositorio;
        IClienteRepository _repositorioCliente;
        public InfoVendasService(IInfoVendasRepository repositorio, IClienteRepository repositorioCliente)
        {
            _repositorio = repositorio;
            _repositorioCliente = repositorioCliente;
        }

        public void SalvarInfoVenda(InfoVendas infoVenda)
        {
            _repositorio.SalvarInfoVenda(infoVenda);
        }

        public IList<InfoVendas> ListarInfoVendas(int idUsuario, string idLoja, string idVendedor)
        {
            var list = _repositorio.ListarInfoVendas(idUsuario, idLoja, idVendedor).ToList();

            return list;
        }

        public InfoVendas BuscarInfoVendas(int id)
        {
            var list = _repositorio.BuscarInfoVendas(id);

            return list;
        }

        public InfoVendas BuscarInfoVendasPorCliente(int idCliente, DateTime dataVenda, int LojaId)
        {
            var info = _repositorio.BuscarInfoVendasPorCliente(idCliente, dataVenda, LojaId);

            return info;
        }       

        public void DeleteInfoVenda(int id)
        {
            var infovenda = _repositorio.BuscarInfoVendas(id);

            if (infovenda == null)
                throw new Exception("A Info Vendas informado não foi encontrado");

            _repositorio.DeleteInfoVenda(id);
        }

    }
}
