﻿using Now.Common.Validation;
using Now.Domain;
using Now.Domain.Contracts.Repositories;
using Now.Domain.Contracts.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Now.Business.Services
{
    public class DistribuicaoAgendaService : IDistribuicaoAgendaService
    {
        IDistribuicaoAgendaRepository _repositorio;
        public DistribuicaoAgendaService(IDistribuicaoAgendaRepository repositorio)
        {
            _repositorio = repositorio;
        }

        public IList<LogTransferenciaAgenda> ListarLogDistribuicao(string LojaId)
        {
            var list = _repositorio.ListarLogDistribuicao(LojaId);

            return list;
        }        

        //public void AtualizarTrocaVendedor(string lojaId, string vendedorId, string clienteId)
        //{
        //    _repositorio.AtualizarTrocaVendedor(lojaId, vendedorId, clienteId);
        //}

        public void SalvarLogTransferencia(LogTransferenciaAgenda entity)
        {
            _repositorio.SalvarLogTransferencia(entity);
        }


        public LogTransferenciaAgenda ObterLogDistribuicao(string idLog)
        {
            return  _repositorio.ObterLogDistribuicao(idLog);
        }
    }
}
