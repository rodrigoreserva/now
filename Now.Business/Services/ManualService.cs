﻿using Now.Common.Validation;
using Now.Domain;
using Now.Domain.Contracts.Repositories;
using Now.Domain.Contracts.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Now.Business.Services
{
    public class ManualService : IManualService
    {
        IManualRepository _repositorio;
        IClienteRepository _repositorioCliente;
        public ManualService(IManualRepository repositorio, IClienteRepository repositorioCliente)
        {
            _repositorio = repositorio;
            _repositorioCliente = repositorioCliente;
        }

        public void Salvar(Manual entity)
        {
            _repositorio.Salvar(entity);
        }

        public IList<Manual> ListarManual()
        {
            var list = _repositorio.ListarManual().ToList();

            return list;
        }

        public Manual BuscarManual(int id)
        {
            var list = _repositorio.BuscarManual(id);

            return list;
        }        

        public void Delete(int id)
        {
            var infovenda = _repositorio.BuscarManual(id);

            if (infovenda == null)
                throw new Exception("A Info Vendas informado não foi encontrado");

            _repositorio.Delete(id);
        }

    }
}
