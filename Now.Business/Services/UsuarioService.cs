﻿using Now.Common.Validation;
using Now.Domain;
using Now.Domain.Contracts.Repositories;
using Now.Domain.Contracts.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Now.Business.Services
{
    public class UsuarioService : IUsuarioService
    {
        IUsuarioRepository _repositorio;
        public UsuarioService(IUsuarioRepository repositorio)
        {
            _repositorio = repositorio;
        }
        public Usuario Autentica(string email, string senha)
        {
            var usuario = _repositorio.Get(email, "", "");

            if (usuario == null) throw new Exception("Usuario ou Senha não localizado!");

            usuario.ValidaSenha(senha);

            return usuario;
        }

        public Usuario AutenticaDistribuicao(string email, string senha)
        {
            var usuario = _repositorio.Get(email, "", "");

            if (usuario == null) throw new Exception("Usuario ou Senha não localizado!");

            usuario.ValidaSenhaDistribuicao(senha);

            return usuario;
        }        

        public IList<UsuarioPerfil> ListarUsuarios(string IdsLojas)
        {
            return _repositorio.ListarUsuarios(IdsLojas);
        }

        public IList<Perfil> ListarPerfis()
        {
            return _repositorio.ListarPerfis();
        }

        public IList<SupervisorLojas> ListarSupervisores()
        {
            return _repositorio.ListarSupervisores();
        }        

        public IList<Usuario> ListarUsuariosPorPerfil(string IdPerfil)
        {
            return _repositorio.ListarUsuariosPorPerfil(IdPerfil);
        }

        public void SalvarUsuario(Usuario usuario)
        {
            //usuario.remember_token = PasswordAssertionConcern.Encrypt(usuario.remember_token);

            _repositorio.SalvarUsuario(usuario);
        }

        public void UpdateUsuario(Usuario usuario, bool alterarToken)
        {
            if (alterarToken)
                usuario.remember_token = PasswordAssertionConcern.Encrypt(usuario.remember_token);

            _repositorio.UpdateUsuario(usuario, alterarToken);
        }

        public void SalvaConfiguracoes(VendedoresConfiguracao configuracao)
        {
            _repositorio.SalvaConfiguracoes(configuracao);
        }

        public void SalvarImgPerfil(string src, string idUsuario)
        {
            _repositorio.SalvarImgPerfil(src, idUsuario);
        }

        public Usuario ExiteUsuario(string email)
        {
            var usuario = _repositorio.Get(email, "", "");

            if (usuario == null) throw new Exception("Usuario não localizado!");

            return usuario;
        }

        public Usuario BuscarUsuario(string id, string vendedorId)
        {
            var usuario = _repositorio.Get("", id, vendedorId);

            if (usuario == null) throw new Exception("Usuario não localizado!");

            return usuario;
        }


        public Usuario BuscarUsuarioPorCPF(string cpf)
        {
            var usuario = _repositorio.Get(cpf, "", "");

            if (usuario == null) throw new Exception("Usuario não localizado!");

            return usuario;
        }

        public void TrocarSenha(string email, string senhaRecebida, string senhaNova, string senhaConfirmar)
        {
            var usuario = _repositorio.Get(email, "", "");

            usuario.ValidaTrocaSenha(senhaRecebida);

            PasswordAssertionConcern.AssertArgumentEquals(senhaNova, senhaConfirmar, "Senha confirmada inválida!");

            usuario.password = PasswordAssertionConcern.Encrypt(senhaNova);
            usuario.recriar_senha = false;
            //Atualiza o Login
            _repositorio.UpdateUsuario(usuario);
        }

        public void TrocarSenhaAgenda(string email, string senhaRecebida)
        {
            var usuario = _repositorio.Get(email, "", "");            

            usuario.senha_agenda_distribuicao = PasswordAssertionConcern.Encrypt(senhaRecebida);            
            //Atualiza o Login
            _repositorio.UpdateUsuario(usuario);
        }

        

        public void AdicionarErro(string metodo, string tela, string erro, int? vendedorId = null, int? clienteId = null, int? lojaId = null, int? userId = null)
        {
            _repositorio.AdicionarErro(metodo, tela, erro, vendedorId, clienteId, lojaId, userId);
        }

        public void AdicionarLogAcesso(string browser, string login, string senha, string ip, int? acesso = 0)
        {
            _repositorio.AdicionarLogAcesso(browser, login, senha, ip, acesso);
        }

        public void AdicionarLogSistema(LogSistema logSistema)
        {
            _repositorio.AdicionarLogSistema(logSistema);
        }

        public Usuario BuscarUsuarioPor(string token)
        {
            var usuario = _repositorio.UsuarioPorToken(token);

            if (usuario == null) throw new Exception("Usuario não localizado!");

            return usuario;
        }
    }
}
