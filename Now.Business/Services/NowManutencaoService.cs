﻿using Now.Common.Validation;
using Now.Domain;
using Now.Domain.Contracts.Repositories;
using Now.Domain.Contracts.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Now.Business.Services
{
    public class NowManutencaoService : INowManutencaoService
    {
        INowManutencaoRepository _repositorio;
        IClienteRepository _repositorioCliente;
        public NowManutencaoService(INowManutencaoRepository repositorio, IClienteRepository repositorioCliente)
        {
            _repositorio = repositorio;
            _repositorioCliente = repositorioCliente;
        }

        public void SalvarNowManutencao(NowManutencao NowManutencao)
        {
            _repositorio.SalvarNowManutencao(NowManutencao);
        }

        public IList<NowControleCarga> ListarLogCarga()
        {
            var list = _repositorio.ListarLogCarga().ToList();

            return list;
        }

        public void DeleteNowManutencao(int id)
        {
            _repositorio.DeleteNowManutencao(id);
        }        

    }
}
