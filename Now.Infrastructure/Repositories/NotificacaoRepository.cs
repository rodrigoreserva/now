﻿using Now.Domain.Contracts.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using Now.Domain;


namespace Now.Infrastructure.Repositories
{
    public class NotificacaoRepository : INotificacaoRepository
    {
        private NowContext _context;

        public NotificacaoRepository(NowContext context)
        {
            _context = context;
        }

        public IList<Notificacao> Obter(string LojaId, string vendedorId)
        {
            var sql = @"SELECT top 10 [id]
                              ,[icone]
                              ,[titulo]
                              ,[descricao]
                              ,[data_notificacao]
                              ,[data_visto]
                              ,[controler]
                              ,[action]
                              ,[loja_id]
                              ,[vendedor_id]
                          FROM [pd].[dbo].[NOTIFICACAO]
                         where loja_id = @LojaId
                           and vendedor_id = @vendedorId                           
                       order by data_notificacao desc
                           
                                ";

            return _context.ContextDapper.Query<Notificacao>(sql, new { LojaId = LojaId, vendedorId = vendedorId }).ToList();            
        }

        public IList<Notificacao> ObterTodas(string LojaId, string vendedorId)
        {
            var sql = @"SELECT [id]
                              ,[icone]
                              ,[titulo]
                              ,[descricao]
                              ,[data_notificacao]
                              ,[data_visto]
                              ,[controler]
                              ,[action]
                              ,[loja_id]
                              ,[vendedor_id]
                          FROM [pd].[dbo].[NOTIFICACAO]
                         where loja_id = @LojaId
                           and vendedor_id = @vendedorId                           
                       order by data_notificacao desc
                           
                                ";

            return _context.ContextDapper.Query<Notificacao>(sql, new { LojaId = LojaId, vendedorId = vendedorId }).ToList();
        }

        public void AtualizarVistoNotificacao(string Id)
        {
            var sql = @"Update [pd].[dbo].[NOTIFICACAO]
                           set data_visto = getdate()                             
                         where id = @Id
                                ";

            _context.ContextDapper.Execute(sql, new { Id = Id });
        }
    }
}
