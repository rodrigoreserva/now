﻿using Now.Domain.Contracts.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using Now.Domain;
using System.Data;
using Now.Domain.DTO;

namespace Now.Infrastructure.Repositories
{
    public class DashboardRepository : IDashboardRepository
    {
        private NowContext _context;

        public DashboardRepository(NowContext context)
        {
            _context = context;
        }

        public IList<ReportRankingVendedores> ListarRankingVendedores(string nomeProcedure, string idLoja, string dtIni, string dtFim)
        {
            return Listar<ReportRankingVendedores>(nomeProcedure, idLoja, dtIni, dtFim);
        }

        public IList<DashboardRankingVendedores> ListarDashboardRankingVendedores(string nomeProcedure, string idLoja, string dtIni, string dtFim)
        {
            return Listar<DashboardRankingVendedores>(nomeProcedure, idLoja, dtIni, dtFim);
        }

        public IList<DashboardRankingVendedores> ListarDashboardRankingVendedores(string idLoja, string dtIni, string dtFim)
        {
            string sql = @"                                 

DECLARE @DT DATETIME = '@data_inicio'
DECLARE @DT_INICIO_SEMANA DATE 
DECLARE @DT_FIM_SEMANA DATE 
DECLARE @DT_FIM_CONSULTA1 DATE = '@data_final'
DECLARE @DT_INICIO_MES DATE 
DECLARE @DT_FIM_MES DATE 

SET @DT_INICIO_SEMANA = DATEADD(DAY,-1 * (DATEPART(DW,@DT)-1), @DT)
SET @DT_FIM_SEMANA = DATEADD(DAY,6, @DT_INICIO_SEMANA)

 IF (DAY('@data_final') > 2)
   BEGIN
      -- Se a semana for a primeira pego dois dias antes da data de calculo
      SET @DT_FIM_CONSULTA1 = dateadd(day, -2, '@data_final')
   END   

-------- Insere os valores Anterior na 
IF OBJECT_ID('tempdb..#tmp_rank_vendedores') IS NOT NULL
	DROP TABLE #tmp_rank_vendedores
   	
	CREATE TABLE #tmp_rank_vendedores (		
		ranking int,
      loja_id bigint,
		loja varchar(255),
      vendedor_id bigint,
		vendedor varchar(100),
		valor decimal(18,2),				
		vendeu decimal(18,2),				
		gerou decimal(18,2)
	)	

insert into #tmp_rank_vendedores
SELECT 
        ranking
      , R.loja_id
      , LTRIM(replace(loja.filial, 'FRANQUIA', '')) as loja      
      , R.vendedor_id     
      , case when vede.data_desativacao is not null then vede.nome + ' <span style=''''color: red'''' > (Desligado) </span>'
              when loja.id<> vede.loja_id then  vede.nome + ' <span style=''''color:blue'''' > (Transferido)</span>'
              else vede.nome end vendedor
      , R.valor
      , valor_venda_now
      , valor_contato_now

  FROM
  (select

          x.loja_id
      , x.vendedor_id
      , sum(x.venda) venda
      , sum(x.cliente) cliente
      , sum(x.pecas) pecas
      , sum(x.valor) valor
      , sum(x.valor_venda_now) valor_venda_now
      , sum(x.valor_contato_now) as valor_contato_now
     , ROW_NUMBER() OVER(ORDER BY sum(x.valor_contato_now) DESC) as ranking
  from(

        SELECT distinct
                  rerv.loja_id
                , rerv.vendedor_id
                , SUM(rerv.venda) as venda
                , SUM(rerv.cliente) as cliente
                , SUM(rerv.pecas) as pecas
                , SUM(rerv.valor) as valor
                , 0 valor_venda_now
                , 0 valor_contato_now
          FROM  rel_ranking_vendedores rerv with(nolock)
         WHERE cast(rerv.data as date) BETWEEN cast(@DT as date) AND cast(@DT_FIM_CONSULTA1 as date)

               and rerv.loja_id in (@loja_id)
         GROUP BY rerv.loja_id, rerv.vendedor_id
UNION ALL


        select loja_id
              , vendedor_id
              , 0
              , 0
              , 0
              , 0
              , SUM(valor_venda_now) as valor
              , 0
                from rel_vendeu_vendedores
               where cast(data as date) BETWEEN cast(@DT as date) AND cast(@DT_FIM_CONSULTA1 as date)

                and loja_id in (@loja_id)

                 --and valor_venda_now > 0
            group by loja_id, vendedor_id

UNION ALL


        select loja_id
             , vendedor_id
             , 0
             , 0
             , 0
             , 0
             , 0
             , SUM(valor_contato_now) as valor
                from rel_gerou_vendedores
               where cast(data as date) BETWEEN cast(@DT as date) AND cast(@DT_FIM_CONSULTA1 as date)               

               and loja_id in (@loja_id)

             group by loja_id, vendedor_id
      )x

      group by      x.loja_id
      , x.vendedor_id

  )R

 join lojas loja with(nolock)
         ON(R.loja_id = loja.id)


 join vendedores vede
              on(vede.id = R.vendedor_id)


/*select * from #tmp_rank_vendedores*/
-- Consulta

SELECT
        ROW_NUMBER() OVER(ORDER BY R.valor_contato_now DESC) as ranking
      , LTRIM(replace(loja.filial, 'FRANQUIA', '')) as loja
      , R.vendedor_id
      , R.loja_id
      , case when vede.data_desativacao is not null then vede.nome + ' <span style=''''color:red'''' > (Desligado) </span>'
              when loja.id<> vede.loja_id then  vede.nome + ' <span style=''''color:blue'''' > (Transferido)</span>'
              else vede.nome end vendedor
      , R.valor
      , R.valor_venda_now
      , R.valor_contato_now
      
      , case when cons1.ranking is null then 0
             when cons1.ranking > ROW_NUMBER() OVER(ORDER BY R.valor_contato_now DESC) then 1
             when cons1.ranking < ROW_NUMBER() OVER(ORDER BY R.valor_contato_now DESC) then - 1
             else 0 end posicao
  FROM
  (
   select

        x.loja_id
      , x.vendedor_id
      , sum(x.venda) venda
      , sum(x.cliente) cliente
      , sum(x.pecas) pecas
      , sum(x.valor) valor
      , sum(x.valor_venda_now) valor_venda_now
      , sum(x.valor_contato_now) as valor_contato_now


  from(

        SELECT distinct
                  rerv.loja_id
                , rerv.vendedor_id
                , SUM(rerv.venda) as venda
                , SUM(rerv.cliente) as cliente
                , SUM(rerv.pecas) as pecas
                , SUM(rerv.valor) as valor
                , 0 valor_venda_now
                , 0 valor_contato_now
          FROM  rel_ranking_vendedores rerv with(nolock)
         WHERE cast(rerv.data as date) BETWEEN cast('@data_inicio' as date) AND cast('@data_final' as date)

           and rerv.loja_id in (@loja_id)
           
         GROUP BY rerv.loja_id, rerv.vendedor_id
UNION ALL


        select loja_id
              , vendedor_id
              , 0
              , 0
              , 0
              , 0
              , SUM(valor_venda_now) as valor
              , 0
                from rel_vendeu_vendedores
               where cast(data as date) BETWEEN cast('@data_inicio' as date) AND cast('@data_final' as date)


                and loja_id in (@loja_id)

                 --and valor_venda_now > 0
            group by loja_id, vendedor_id

UNION ALL


        select loja_id
             , vendedor_id
             , 0
             , 0
             , 0
             , 0
             , 0
             , SUM(valor_contato_now) as valor
                from rel_gerou_vendedores
               where cast(data as date) BETWEEN cast('@data_inicio' as date) AND cast('@data_final' as date)


               and loja_id in (@loja_id)

             group by loja_id, vendedor_id
      )x

      group by      x.loja_id
      , x.vendedor_id
  )R

 join lojas loja with(nolock)
         ON(R.loja_id = loja.id)


 join vendedores vede with(nolock)
              on(vede.id = R.vendedor_id)

left join #tmp_rank_vendedores cons1 
       on(cons1.vendedor_id = R.vendedor_id and R.loja_id = cons1.loja_id)

ORDER BY valor_contato_now DESC




            ";

            sql = sql.Replace("@loja_id", idLoja);
            sql = sql.Replace("@data_inicio", dtIni);
            sql = sql.Replace("@data_final", dtFim);

            return _context.ContextDapper.Query<DashboardRankingVendedores>(sql).ToList();

        }

        public IList<DashboardGraficoEfetividade> ListarDashboardGraficoEfetividade(string nomeProcedure, string idLoja, string idVendedor, string idTodasLojas, string dtIni, string dtFim)
        {
            // Chamada Antiga
            //return ListarGrafico<DashboardGraficoEfetividade>(nomeProcedure, idVendedor, idLoja, idTodasLojas, dtIni, dtFim);


            string sql = @"        

IF OBJECT_ID('tempdb..#tmp_grafico') IS NOT NULL
	DROP TABLE #tmp_grafico

   CREATE TABLE #tmp_grafico (		
		dia int,
      media_perc_efet_vendedor decimal(18,4),
      media_perc_efet_loja decimal(18,4),
      media_perc_efet_empresa decimal(18,4)
	)

   DECLARE @DataAtual datetime  =  '@data_inicio'

	WHILE (@DataAtual < dateadd(day,1,'@data_final'))
	BEGIN
      
            insert into #tmp_grafico
            SELECT       
                     d.dia
	                , CASE WHEN ISNULL(sum(vendeuVendedor.valor),0) <> 0 and ISNULL(sum(RVendedor.valor),0) <> 0  
                          THEN ISNULL(sum(vendeuVendedor.valor),0) / ISNULL(sum(RVendedor.valor),0) 
                          ELSE 0 
                      END  as  media_perc_efet_vendedor                       
                   , CASE WHEN ISNULL(sum(vendeu.valor),0) <> 0 and ISNULL(sum(RLoja.valor),0) <> 0 
                          THEN ISNULL(sum(vendeu.valor),0) / ISNULL(sum(RLoja.valor),0) 
                          ELSE 0 
                      END  as  media_perc_efet_loja
                  , CASE WHEN ISNULL(sum(vendeuEmpresa.valor),0) <> 0 and ISNULL(sum(REmpresa.valor),0) <> 0
                          THEN ISNULL(sum(vendeuEmpresa.valor),0) / ISNULL(sum(REmpresa.valor),0) 
                          ELSE 0 
                      END  as  media_perc_efet_empresa

              FROM (select day(@DataAtual) dia) d

            left join (
                     SELECT 
                                day(@DataAtual) as dia
                              , SUM(rerv.valor) as valor
                     FROM	rel_ranking_vendedores rerv with(nolock)      

                     WHERE cast(rerv.data as date) BETWEEN cast('@data_inicio' as date) AND cast(@DataAtual as date)                       
					   and rerv.loja_id in (@loja_id)                      
              )RLoja on RLoja.dia = d.dia

            LEFT JOIN	(
                          -- GEROU
                          select 
                                 day(@DataAtual) as dia
                               , SUM(valor_contato_now)as valor
                            from rel_gerou_vendedores
                           where cast(data as date) BETWEEN cast('@data_inicio' as date) AND cast(@DataAtual as date)  
                             and loja_id in (@loja_id) 
                        )vendeu on (vendeu.dia = d.dia)  			  
				  
             LEFT JOIN (
                     SELECT 
                              day(@DataAtual) as dia
                            , SUM(rerv.valor) as valor

                     FROM	rel_ranking_vendedores rerv with(nolock)      

                     WHERE cast(rerv.data as date) BETWEEN cast('@data_inicio' as date) AND cast(@DataAtual as date)
                       AND rerv.vendedor_id = @IdVendedor                  
              )RVendedor on (RVendedor.dia = d.dia)  	
		
            LEFT JOIN	(
                          -- GEROU
                          select 
                                   day(@DataAtual) as dia
                                 , SUM(valor_contato_now)as valor
                            from rel_gerou_vendedores
                           where cast(data as date) BETWEEN cast('@data_inicio' as date) AND cast(@DataAtual as date)                       
					        AND vendedor_id = @IdVendedor                         
                      )vendeuVendedor on (vendeuVendedor.dia = d.dia)
             
            LEFT JOIN  (
                     SELECT 
                                 day(@DataAtual) as dia
                               , SUM(rerv.valor) as valor
                     FROM	rel_ranking_vendedores rerv with(nolock)      
                
                     WHERE cast(rerv.data as date) BETWEEN cast('@data_inicio' as date) AND cast(@DataAtual as date)                                  
					     and rerv.loja_id in (@Loja_idTodas) 
              )REmpresa on (REmpresa.dia = d.dia)        
        
            LEFT JOIN	(
                          -- GEROU

                          select 
                                   day(@DataAtual) as dia
                                 , SUM(valor_contato_now)as valor
                            from rel_gerou_vendedores
                           where cast(data as date) BETWEEN cast('@data_inicio' as date) AND cast(@DataAtual as date)                             
							 and loja_id in (@Loja_idTodas)                                             
                        )vendeuEmpresa on (vendeuEmpresa.dia = d.dia)          
            group by d.dia

            SET @DataAtual = DATEADD(DAY, 1,@DataAtual)
   END

     
select dia,
      media_perc_efet_vendedor,
      media_perc_efet_loja,
      media_perc_efet_empresa  from  #tmp_grafico order by dia

            ";

            sql = sql.Replace("@loja_id", idLoja);
            sql = sql.Replace("@Loja_idTodas", idTodasLojas);
            sql = sql.Replace("@IdVendedor", string.IsNullOrEmpty(idVendedor) ? "0" : idVendedor);
            sql = sql.Replace("@data_inicio", dtIni);
            sql = sql.Replace("@data_final", dtFim);

            return _context.ContextDapper.Query<DashboardGraficoEfetividade>(sql, commandTimeout: 0).ToList();

        }

        private IList<T> ListarGrafico<T>(string nomeProcedure, string idVendedor, string idLoja, string idTodasLojas, string dtIni, string dtFim)
        {
            return _context.ContextDapper.Query<T>(nomeProcedure, new { @VENDEDOR_ID = idVendedor, @LOJA_ID = idLoja, @todas_loja_id = idTodasLojas, @data_inicio = Convert.ToDateTime(dtIni), @data_final = Convert.ToDateTime(dtFim) }, null, true, 60, CommandType.StoredProcedure).ToList();
        }

        //private IList<T> ListarNovoDashBoard<T>(string nomeProcedure, string idVendedor, string idLoja, string dtIni, string dtFim)
        //{
        //    return _context.ContextDapper.Query<T>(nomeProcedure, new { @VENDEDOR_ID = idVendedor, @LOJA_ID = idLoja, @data_inicio = Convert.ToDateTime(dtIni), @data_final = Convert.ToDateTime(dtFim) }, null, true, 60, CommandType.StoredProcedure).ToList();
        //}

        private IList<T> Listar<T>(string nomeProcedure, string idLoja, string dtIni, string dtFim)
        {
            return _context.ContextDapper.Query<T>(nomeProcedure, new { @LOJA_ID = idLoja, @data_inicio = Convert.ToDateTime(dtIni), @data_final = Convert.ToDateTime(dtFim) }, null, true, 60, CommandType.StoredProcedure).ToList();
        }

        public IList<Dashboard> ListarMetricas(string idLoja, string idVendedor, string dtIni, string dtFim)
        {
            var sql = new StringBuilder(@"
                    SELECT 
                           0 as loja_id
                         , 0 as vendedor_id                         
                         , sum(taxa_conversao)  as taxa_conversao
                         , sum(valor_efetividade)  as valor_efetividade
                         , sum(qtde_ligacoes_dispon)  as qtde_ligacoes_dispon
                         , sum(qtde_ligacoes_feitas)  as qtde_ligacoes_feitas
                         , sum(valor_reservado)  as valor_reservado
                         , sum(total_vendas_reservado)  as total_vendas_reservado
                         , sum(valor_delivery)  as valor_delivery
                         , sum(total_vendas_delivery)  as total_vendas_delivery
                         , sum(total_contato_direto)  as qtde_ligacoes_direto
                    FROM dashboard
                    WHERE 1 = 1 @idLoja
                                @idVendedor
                                @dtIni
                                @dtFim
                ");

            //Filtros por loja
            sql = sql.Replace("@idLoja", string.IsNullOrWhiteSpace(idLoja) ? "" : " AND loja_id in (" + idLoja + ") ");

            //Filtros por vendedor
            sql = sql.Replace("@idVendedor", string.IsNullOrWhiteSpace(idVendedor) ? "" : " AND vendedor_id in (" + idVendedor + ") ");

            sql = sql.Replace("@dtIni", string.IsNullOrWhiteSpace(dtIni) ? "" : " AND data >= '" + dtIni + "'");

            sql = sql.Replace("@dtFim", string.IsNullOrWhiteSpace(dtFim) ? "" : " AND data <= '" + dtFim + "'");

            var result = _context.ContextDapper.Query<Dashboard>(sql.ToString());

            return result.ToList();
        }

        public IList<MetricasDTO> ListarNovoDashBoard(string nomeProcedure, string idLoja, string idVendedor, string dtIni, string dtFim)
        {
            return _context.ContextDapper.Query<MetricasDTO>(nomeProcedure, new { vendedorId = idVendedor, lojaId = idLoja, @data_inicio = Convert.ToDateTime(dtIni), @data_final = Convert.ToDateTime(dtFim) }, null, true, 60, CommandType.StoredProcedure).ToList();
        }
    }
}
