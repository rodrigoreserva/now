﻿using Now.Domain.Contracts.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using Now.Domain;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Linq.Expressions;

namespace Now.Infrastructure.Repositories
{
    public class VisitaLojasRepository : IVisitaLojasRepository
    {
        private NowContext _context;

        public VisitaLojasRepository(NowContext context)
        {
            _context = context;
        }

        public void SalvarVisitaLojas(VisitaLojas visitalojas)
        {
            var query = @"  insert into visita_lojas
                                 values(@data_visita,@loja_id,@observacao)";

            DynamicParameters parametros = new DynamicParameters();

            parametros.Add("data_visita", visitalojas.data_visita);
            parametros.Add("loja_id", visitalojas.loja_id);
            parametros.Add("observacao", visitalojas.observacao);
            
            _context.ContextDapper.Execute(query, parametros);
        }

        public void DeleteVisitaLojas(int id)
        {
            var query = @"     delete from visita_lojas 
                                    where id = @Id ";

            _context.ContextDapper.Execute(query, new { id = id });
        }

        public IList<VisitaLojas> ListarVisitaLojas(string idLoja)
        {

            var query = @"     select v.*
                                             , l.filial
                                          from visita_lojas v with(nolock)
                                    inner join lojas l on l.id = v.loja_id ";

            var result = _context.ContextDapper.Query<VisitaLojas>(query);

            return result.ToList();
        }

        /// <summary>
        /// Retorna InfoVendas
        /// </summary>
        /// <param name="idcliente"></param>
        /// <returns></returns>
        public VisitaLojas BuscarVisitaLojas(int id)
        {
            const string sql = @"
                SELECT *
                FROM visita_lojas with(nolock)
                WHERE id = @Id                
            ";

            var result = _context.ContextDapper.Query<VisitaLojas>(sql, new { id = id });

            if (result.ToList().Count() > 0)
                return result.ToList().FirstOrDefault();
            else
                return null;
        }

        public VisitaLojas BuscarLojasPorDataVisita(string data)
        {
            const string sql = @"
                SELECT *
                FROM visita_lojas with(nolock)
                WHERE data_visita = @data                
            ";

            var result = _context.ContextDapper.Query<VisitaLojas>(sql, new { data = data });

            if (result.ToList().Count() > 0)
                return result.ToList().FirstOrDefault();
            else
                return null;
        }
    }
}
