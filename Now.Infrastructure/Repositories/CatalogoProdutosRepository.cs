﻿using Now.Domain.Contracts.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using Now.Domain;
using System.Data;

namespace Now.Infrastructure.Repositories
{
    public class CatalogoProdutosRepository : ICatalogoProdutosRepository
    {
        private NowContext _context;

        public CatalogoProdutosRepository(NowContext context)
        {
            _context = context;
        }

        public IList<CatalogoProdutos> ListarCatalogoProdutos(string strPesqProduto, int pageIndex, ref int PageCount)
        {
            DynamicParameters parametros = new DynamicParameters();

            parametros.Add("str_pesquisa", strPesqProduto);
            parametros.Add("PageIndex", pageIndex);
            parametros.Add("PageSize", 10);
            parametros.Add("PageCount", PageCount, DbType.Int32, ParameterDirection.Output, 4);           

            var list = _context.ContextDapper.Query<CatalogoProdutos>("SP_RelatorioProdutos", parametros, null, true, 60, CommandType.StoredProcedure).ToList();
            PageCount = parametros.Get<Int32>("PageCount");

            return list;
        }

        public IList<String> ListaGriffe()
        {
            var sql = @" select distinct griffe from produtos with(nolock) where griffe in ('RESERVA MININA', 'RESERVA MINI', 'BEBE', 'MINI PENETRAS', 'PENETRINHAS','RESERVA','PENETRAS','40076', 'EVA') order by griffe ";           

            var list = _context.ContextDapper.Query<String>(sql).ToList();

            return list;
        }

        public IList<String> ListaGrupoProdutos(string griffe)
        {
            var sql = @" select distinct grupo_produto from produtos with(nolock) @condicao order by grupo_produto ";

            sql = string.IsNullOrEmpty(griffe) ? sql.Replace("@condicao", " where griffe in ('RESERVA MININA', 'RESERVA MINI', 'BEBE', 'MINI PENETRAS', 'PENETRINHAS','RESERVA','PENETRAS','40076', 'EVA')") : sql.Replace("@condicao", " where griffe ='" + griffe + "'");

            var list = _context.ContextDapper.Query<String>(sql).ToList();

            return list;
        }
    }
}
