﻿using Now.Domain.Contracts.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using Now.Domain;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Linq.Expressions;

namespace Now.Infrastructure.Repositories
{
    public class ManualRepository : IManualRepository
    {
        private NowContext _context;

        public ManualRepository(NowContext context)
        {
            _context = context;
        }

        public void Salvar(Manual entity)
        {
            var query = @" ";
            DynamicParameters parametros = new DynamicParameters();

            if (entity.id == 0)
            {
                query = @"  insert into manual
                                 values(@titulo, @conteudo, @data_inclusao, @data_alteracao, @data_desativacao, @ordem)";
            }
            else {
                query = @"  update manual
                               set titulo             = @titulo
                                 , conteudo          = @conteudo
                                 , data_alteracao       = @data_alteracao
                                 , data_desativacao                = @data_desativacao                                 
                                 , ordem          = @ordem
                              where id = @id
                                ";

                parametros.Add("id", entity.id);
            }

            parametros.Add("titulo", entity.titulo);
            parametros.Add("conteudo", entity.conteudo);
            parametros.Add("data_inclusao", entity.data_inclusao);
            parametros.Add("data_alteracao", entity.data_alteracao);
            parametros.Add("data_desativacao", entity.data_desativacao);
            parametros.Add("ordem", entity.ordem);
                        
            _context.ContextDapper.Execute(query, parametros);
        }
        public void Delete(int id)
        {
            var query = @"  update info_vendas
                               set deleted_at = @deleted_at
                                 , status = @status
                             where id = @id ";

            DynamicParameters parametros = new DynamicParameters();

            parametros.Add("id", id);
            parametros.Add("deleted_at", DateTime.Now);
            parametros.Add("status", 0);

            _context.ContextDapper.Execute(query, parametros);
        }

        /// <summary>
        /// Lista 
        /// </summary>
        /// <param name="idcliente"></param>
        /// <returns></returns>
        public IList<Manual> ListarManual()
        {
            var sql = @"
                SELECT [id]
                      ,[titulo]
                      ,[conteudo]
                      ,[data_inclusao]
                      ,[data_alteracao]
                      ,[data_desativacao]
                      ,[ordem]
                  FROM [pd].[dbo].[manual]  
            ";

            var orderby = " ORDER BY [ordem]";            

            var query = sql + orderby;

            var result = _context.ContextDapper.Query<Manual>(query);

            return result.ToList();
        }

        /// <summary>
        /// Retorna Manual
        /// </summary>
        /// <param name="idcliente"></param>
        /// <returns></returns>
        public Manual BuscarManual(int id)
        {
            const string sql = @"

              SELECT info.*, cli.cpf
                  , cli.nome as nome_cliente
                  , loja.filial
                  , vend.nome as vendedor                  
                FROM info_vendas info with(nolock)
          inner join clientes cli with(nolock) on cli.id = info.cliente_id 
          inner join lojas loja with(nolock) on loja.id = info.loja_id
          inner join vendedores vend with(nolock) on vend.id = info.vendedor_id

                WHERE info.id = @Id    
            ";

            var result = _context.ContextDapper.Query<Manual>(sql, new { id = id });

            if (result.ToList().Count() > 0)
                return result.ToList().FirstOrDefault();
            else
                return null;
        }

    }
}
