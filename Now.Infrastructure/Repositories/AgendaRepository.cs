﻿using Now.Domain.Contracts.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using Now.Domain;

namespace Now.Infrastructure.Repositories
{
    public class AgendaRepository : IAgendaRepository
    {
        private NowContext _context;

        public AgendaRepository(NowContext context)
        {
            _context = context;
        }

        public IList<MenuAgendamento> TypeCalendar(Usuario UsuarioLogado, string idusuario = "", string idloja = "", string idvendedor = "", string param = "", bool _lembrete = false)
        {
            var sql = @"
                
                 SELECT t.titulo, t.id, t.prioridade, t.parametro, isnull(meag.totalAgenda,0) as qtdeTotal, isnull(agenda.total,0) as qtdeDisponivel,
                        isnull((meag.totalAgenda - agenda.total),0) as qtdeLigacoes,
                       CASE WHEN (meag.totalAgenda - isnull(agenda.total, 0)) <> 0
                         THEN convert(decimal(18,2), (meag.totalAgenda - isnull(agenda.total, 0))) / convert(decimal(18,2), meag.totalAgenda)
                         ELSE 0 
                         END  percentual      
                FROM menu_tipo_agendamentos t with(nolock)
                LEFT JOIN (
                            select 
                                   tipo_agendamento_id 
                                 , count(id) totalAgenda
                              from                 
                                   menu_agendamentos a with(nolock)
                                        @subUsuario
				                WHERE
					                1 = 1
                                    -- Filtros personalizados
                                    and vendedor_id in (select id from vendedores where data_desativacao is null)
                                    {0}
                                    @LojaId2
                               group by tipo_agendamento_id
                           )meag on
                    meag.tipo_agendamento_id = t.id
                LEFT JOIN
				( SELECT 
                          a.agendamento
                        , isnull(count(tipo_agendamento_id), 0) total     

                    FROM  v_agendamentos a
                        @subUsuario
				    WHERE
					    1 = 1
                        and vendedor_id in (select id from vendedores where data_desativacao is null)
                        -- Filtros personalizados
                        {0}
                        @LojaId2
                 GROUP BY a.agendamento
			    ) agenda
                ON agenda.agendamento = t.parametro
            ";

            //Filtros personalizados
            var filter = "";

            //Filtros por vendedor
            filter += string.IsNullOrWhiteSpace(idvendedor) ? "" : " AND vendedor_id in (" + idvendedor + ") ";

            //Filtros por usuário
            if (!string.IsNullOrWhiteSpace(idusuario) && (!UsuarioLogado.Rede))
            {
                var subSql = @" inner join (
                                SELECT  
                                        upl.id_loja 
                                    , p.rede 
                                FROM users u with(nolock)
                                INNER JOIN  users_perfil up with(nolock)
                                ON up.id_user = u.id
                                INNER JOIN perfil p with(nolock)
                                ON p.id_perfil = up.id_perfil
                                LEFT JOIN users_perfil_lojas upl with(nolock)
                                ON upl.id_user = u.id
                                WHERE 
                                u.id = @UsuarioId
                                @LojaId
                               )s on s.id_loja = a.loja_id OR s.rede = 1";

                subSql = subSql.Replace("@UsuarioId", idusuario);

                sql = sql.Replace("@subUsuario", subSql);
            }
            if (UsuarioLogado.Rede && !string.IsNullOrWhiteSpace(idloja))
            {
                sql = sql.Replace("@LojaId2", " and a.loja_id in (" + idloja + ") ");
            }

            sql = sql.Replace("@LojaId2", "");

            //Filtros por loja
            if (!string.IsNullOrWhiteSpace(idloja))
            {
                sql = sql.Replace("@LojaId", " AND upl.id_loja in (" + idloja + ") ");
            }

            sql = sql.Replace("@LojaId", "");
            sql = sql.Replace("@subUsuario", "");

            //filter += string.IsNullOrWhiteSpace(idloja) ? "" : " AND loja_id in (" + idloja + ") ";


            //Adicionando o filtro dinamico na query final
            sql = String.Format(sql, filter);
            sql += " WHERE t.Exibir = 1";

            // Valida se a loja tem permissão para acessar a agenda
            // Se uma loja for selecionada coloco essa condição
            if (!string.IsNullOrWhiteSpace(idloja))
            {
                sql += @" 
                    and exists ( select 
								        *
			                        from menu_tipo_agendamentos_loja aglo
						            where aglo.tipo_agendamento_id = t.id
						                and aglo.loja_id in (" + idloja + @") 
						            )
                ";
            }

            // Se for passado false na agenda lembre não exibo ela
            if (!_lembrete)
            {
                sql += " and t.id <> 8 ";
            }

            //Valida se a agenda está dentro do período
            sql += @" 
                        and (			      
				           (
				            data_fim_vigencia is null
					         and 
					        cast(data_inicio_vigencia as date) <= cast(getdate() as date)
					        )
			               or 
				          (cast(data_inicio_vigencia as date) <= cast(getdate() as date)
				            and
				           cast(data_fim_vigencia as date) >= cast(getdate() as date)
				           )
				         )
                   ";

            //Filtros por parametro
            sql += string.IsNullOrWhiteSpace(param) ? string.Empty : string.Format(@" AND t.parametro = '{0}'"
             , param);
            //Ordenação e  agrupamento
            sql += @"
                    /*GROUP BY  t.titulo,  t.parametro, prioridade, agenda.total*/
                    ORDER BY prioridade";

            var result = _context.ContextDapper.Query<MenuAgendamento>(sql);

            // Agenda Inativo e Inativando só aparecer se as agendas até a Aniversário estiveram comcluídas.
            //if (!string.IsNullOrWhiteSpace(idvendedor))
            //{
            //    if (result.Where(x => x.prioridade < 6 && x.qtdeDisponivel > 0).Count() > 0)
            //    {
            //        // Não exibir as agendas após o reservado
            //        result = result.Where(x => x.prioridade < 6).ToList();
            //    }
            //}

            return result.ToList();
        }

        public IList<Agendamento> FindCalendars(string type, string strBusca, string idusuario = "", string idloja = "", string idvendedor = "", bool administrador = false, string idCliente = "")
        {

            var sql = new StringBuilder(@"
                    IF OBJECT_ID('tempdb..#tmp_v_Agendamentos') IS NOT NULL
	                   DROP TABLE #tmp_v_Agendamentos

                    SELECT top 150 *
					into #tmp_v_Agendamentos
                    FROM v_agendamentos agenda
                    inner join (
                                SELECT  
                                        upl.id_loja 
                                    , p.rede
                                FROM users u with(nolock)
                                INNER JOIN  users_perfil up with(nolock)
                                ON up.id_user = u.id
                                INNER JOIN perfil p with(nolock)
                                ON p.id_perfil = up.id_perfil
                                LEFT JOIN users_perfil_lojas upl with(nolock)
                                ON upl.id_user = u.id
                                WHERE 
                                u.id = @UsuarioId
                               )s on s.id_loja = agenda.loja_id OR s.rede = 1
                    WHERE 1 = 1 

                ");

            //Filtros personalizados
            var filter = "";

            filter += string.IsNullOrWhiteSpace(type) ? "" : " AND agenda.tipo_agendamento_id = " + type;

            //Filtros por loja
            filter += string.IsNullOrWhiteSpace(idloja) ? "" : " AND loja_id in (" + idloja + ") ";

            // Filtro por cliente
            filter += string.IsNullOrWhiteSpace(idCliente) ? "" : " AND cliente_id = " + idCliente;

            //Filtros por vendedor
            filter += string.IsNullOrWhiteSpace(idvendedor) ? "" : " AND vendedor_id in (" + idvendedor + ") ";

            filter += string.IsNullOrWhiteSpace(strBusca) ? "" : @" AND (
                                                                     nome like '%" + strBusca + @"%'               
                                                                   OR cpf like '%" + strBusca + @"%'               
                                                                   OR cidade like '%" + strBusca + @"%'  
                                                                   OR bairro like '%" + strBusca + @"%' 
                                                                   OR uf like '%" + strBusca + @"%' 
                                                                   OR titulo_tipo_agendamento like '%" + strBusca + @"%'      
                                                                   OR filial like '%" + strBusca + @"%'      
                                                                   OR nome_vendedor like '%" + strBusca + @"%'  
                                                                    )       
                                                                  ";

            sql = sql.Replace("@UsuarioId", idusuario);

            //Filtros por usuário
            //            filter += string.IsNullOrWhiteSpace(idusuario) ? "" : string.Format(@" 
            //                AND exists 
            //                (
            //
            //                    SELECT  1 
            //                    FROM users u
            //                    INNER JOIN  users_perfil up
            //                    ON up.id_user = u.id
            //                    INNER JOIN perfil p
            //                    ON p.id_perfil = up.id_perfil
            //                    LEFT JOIN users_perfil_lojas upl
            //                    ON upl.id_user = u.id
            //                    WHERE 
            //                    u.id = {0}
            //                    AND (upl.id_loja = agenda.loja_id OR p.rede = 1)
            //
            //                 )"
            //            , idusuario);

            sql.Append(filter);

            //Ordenação
            //TODO Criar uma coluna para ordenação no agendamento, pra ter apenas uma forma de ordenação
            //var ordernacao = _context.ContextDapper.Query<string>(@"SELECT ordenacao FROM  menu_tipo_agendamentos where id = @Tipo ", new { Tipo = type }).FirstOrDefault();

            //if (!String.IsNullOrWhiteSpace(ordernacao))
            //{

            sql.Append(" ORDER BY pos_venda_delivery desc, valor_venda desc ");

            sql.Append(" select * from #tmp_v_Agendamentos ORDER BY pos_venda_delivery desc, valor_venda desc");
            //}

            var result = _context.ContextDapper.Query<Agendamento>(sql.ToString());

            return result.ToList();
        }

        public IList<MenuTipoAgendamento> ListarAgendas(string idloja)
        {
            var sql = @"                
                  SELECT *    
                    FROM menu_tipo_agendamentos with(nolock)               
			       WHERE Exibir = 1
            ";

            //Filtros personalizados
            // var filter = "";

            //Filtros por Loja
            // filter += string.IsNullOrWhiteSpace(idloja) ? "" : " AND vendedor_id in (" + idvendedor + ") ";


            sql += @" ORDER BY titulo";

            var result = _context.ContextDapper.Query<MenuTipoAgendamento>(sql);
            return result.ToList();
        }

        public void AgendaNaoFalou(MenuAgendamentoNaoFalou naoFalou)
        {
            // retorna a agenda cadastrada
            var agenda = _context.ContextDapper.Query<MenuAgendamentos>(@" 

                 select 
                       meag.*
                  from menu_agendamentos meag with(nolock)
                inner join menu_tipo_agendamentos tiag with(nolock)
                                                 on tiag.id = meag.tipo_agendamento_id
                 where meag.loja_id = @loja_id 
                   --and meag.vendedor_id = @vendedor_id
                   and tiag.parametro = @parametro
                   and meag.cliente_id = @cliente_id

                ", new { loja_id = naoFalou.loja_id, vendedor_id = naoFalou.vendedor_id, parametro = naoFalou.agenda, cliente_id = naoFalou.cliente_id }).FirstOrDefault();

            // Mantem o histórico do registro da ligação
            var query = @" insert into menu_agendamentos_nao_falou
                            values(@cliente_id,@nome,@cpf,@bairro,@cidade,@vendedor_id,@loja_id,@filial,@data_venda,@valor_venda,@data_carga,@nome_vendedor,@tipo_agendamento_id,@dica_lembrete,@vendedor_id_ligou,@loja_id_ligou,@data_ligou)";

            DynamicParameters parametros = new DynamicParameters();

            parametros.Add("cliente_id", agenda.cliente_id);
            parametros.Add("nome", agenda.nome);
            parametros.Add("cpf", agenda.cpf);
            parametros.Add("bairro", agenda.bairro);
            parametros.Add("cidade", agenda.cidade);
            parametros.Add("vendedor_id", agenda.vendedor_id);
            parametros.Add("loja_id", agenda.loja_id);
            parametros.Add("filial", agenda.filial);
            parametros.Add("data_venda", agenda.data_venda);
            parametros.Add("valor_venda", agenda.valor_venda);
            parametros.Add("data_carga", agenda.data_carga);
            parametros.Add("nome_vendedor", agenda.nome_vendedor);
            parametros.Add("tipo_agendamento_id", agenda.tipo_agendamento_id);
            parametros.Add("dica_lembrete", agenda.dica_lembrete);
            parametros.Add("vendedor_id_ligou", null);
            parametros.Add("loja_id_ligou", null);
            parametros.Add("data_ligou", DateTime.Now);

            _context.ContextDapper.Execute(query, parametros);

            // Excluir o cliente agendado



            // Excluir o histórico do agendamento


            // 

        }
    }
}
