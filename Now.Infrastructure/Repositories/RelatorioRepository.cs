﻿using Now.Domain.Contracts.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using Now.Domain;
using System.Data;


namespace Now.Infrastructure.Repositories
{
    public class RelatorioRepository : IRelatorioRepository
    {
        private NowContext _context;

        public RelatorioRepository(NowContext context)
        {
            _context = context;
        }

        public IList<T> ListarRelatorios<T>(string nomeProcedure, string idLoja, string dtIni, string dtFim, int? gerente = null)
        {
            return Listar<T>(nomeProcedure, idLoja, dtIni, dtFim, gerente);
        }

        public IList<ReportEfetividade> ListarEfetividadeLojas(string nomeProcedure, string idLoja, string dtIni, string dtFim)
        {
            return Listar<ReportEfetividade>(nomeProcedure, idLoja, dtIni, dtFim);
        }

        public IList<ReportEfetividade> ListarEfetividadeLojas(string idLoja, string dtIni, string dtFim)
        {
            string sql = @"                                 


SELECT 
        ROW_NUMBER() OVER (ORDER BY CASE WHEN ISNULL(sum(vendeu.valor),0) <> 0 AND ISNULL(sum(R.valor),0) <> 0
              THEN ISNULL(sum(vendeu.valor),0) / ISNULL(sum(R.valor),0) 
              ELSE 0 
          END DESC) as ranking
      , loja.id loja_id
      , LTRIM(replace(loja.filial, 'FRANQUIA', '')) as loja          
      , replace(sum(vendeu.valor), '.', ',') as efetividade 
      , replace(sum(R.valor), '.', ',') as valor_linx
      , replace(sum(vendeuSemContDireto.valor), '.', ',') as  valor_now
      , replace(sum(vendeuSoContDireto.valor), '.', ',') as  contato_direto
      , CASE WHEN ISNULL(sum(vendeu.valor),0) <> 0 AND ISNULL(sum(R.valor),0) <> 0
              THEN ISNULL(sum(vendeu.valor),0) / ISNULL(sum(R.valor),0) 
              ELSE 0 
          END  as  perc_efetividade
  FROM
  (
         SELECT distinct
                  rerv.loja_id
                , SUM(rerv.venda) as venda
                , SUM(rerv.cliente) as cliente
                , SUM(rerv.pecas) as pecas
                , SUM(rerv.valor) as valor
                --, SUM(rerv.valor_venda_now) as valor_venda_now
                --, SUM(rerv.valor_contato_now) as valor_contato_now

         FROM	rel_ranking_vendedores rerv with(nolock)      

         WHERE cast(rerv.data as date) BETWEEN cast('@data_inicio' as date) AND cast('@data_final' as date)           
		   and rerv.loja_id in (@loja_id)				

         GROUP BY rerv.loja_id
  )R

LEFT JOIN	(
              -- GEROU

              select loja_id, SUM(valor_contato_now)as valor
                from rel_gerou_vendedores
               where cast(data as date) BETWEEN cast('@data_inicio' as date) AND cast('@data_final' as date)
                 --and valor_venda_now > 0
            group by loja_id
            )vendeu on (
             vendeu.loja_id = R.loja_id)             

  LEFT JOIN	(
              -- GEROU - SEM CONTATO DIRETO 

              select loja_id, SUM(valor_contato_now)as valor
                from rel_gerou_vendedores
               where cast(data as date) BETWEEN cast('@data_inicio' as date) AND cast('@data_final' as date)
                and tipo_agendamento_id <> 1
                 --and valor_venda_now > 0
            group by loja_id
            )vendeuSemContDireto on (
             vendeuSemContDireto.loja_id = R.loja_id)      

  LEFT JOIN	(
              -- GEROU  - SÓ CONTATO DIRETO 

              select loja_id, SUM(valor_contato_now)as valor
                from rel_gerou_vendedores
               where cast(data as date) BETWEEN cast('@data_inicio' as date) AND cast('@data_final' as date)
                 and tipo_agendamento_id = 1
                 --and valor_venda_now > 0
            group by loja_id
            )vendeuSoContDireto on (
             vendeuSoContDireto.loja_id = R.loja_id)      

 join lojas loja with(nolock) 
         ON(R.loja_id = loja.id)

GROUP BY loja.filial, loja.id
ORDER BY CASE WHEN ISNULL(sum(vendeu.valor),0) <> 0 AND ISNULL(sum(R.valor),0) <> 0
              THEN ISNULL(sum(vendeu.valor),0) / ISNULL(sum(R.valor),0) 
              ELSE 0 
          END DESC


            ";

            sql = sql.Replace("@loja_id", idLoja);
            sql = sql.Replace("@data_inicio", dtIni);
            sql = sql.Replace("@data_final", dtFim);

            return _context.ContextDapper.Query<ReportEfetividade>(sql).ToList();
        }


        public IList<ReportEfetividadeAgenda> ListarReportEfetividadeAgenda(string nomeProcedure, string idLoja, string dtIni, string dtFim)
        {
            return Listar<ReportEfetividadeAgenda>(nomeProcedure, idLoja, dtIni, dtFim);
        }

        public IList<ReportEfetividadeAgenda> ListarReportEfetividadeAgendaDashboard(string nomeProcedure, string idVendedor, string idLoja, string dtIni, string dtFim)
        {
            return ListarComVendedor<ReportEfetividadeAgenda>(nomeProcedure, idVendedor, idLoja, dtIni, dtFim);
        }

        public IList<ReportEfetividadeAgenda> ListarReportEfetividadeAgendaDashboard(string idVendedor, string idLoja, string dtIni, string dtFim)
        {
            string sql = @"                                 
select 
            x.agendamento
          , ISNULL(x.ligacoes_disponiveis, 0) as ligacoes_disponiveis
			 , ISNULL(x.contatos_efetuados, 0) as contatos_efetuados          
          , ISNULL(x.valor_venda_now,0) as valor_venda_now
          , case when ISNULL(x.ligacoes_disponiveis, 0) <> 0 then
					         convert(decimal(18,2),x.contatos_efetuados) / convert(decimal(18,2),x.ligacoes_disponiveis) 
					         else
					         convert(decimal(18,2),0)
					         end produtividade
from (
select      meta.titulo as agendamento
          , sum(ISNULL(rel.ligacoes_disponiveis, 0)) as ligacoes_disponiveis
			 , sum(ISNULL(rel.contatos_efetuados, 0)) as contatos_efetuados          
          , sum(rel.valor_venda_now) as valor_venda_now

       from rel_efetividade_agenda_dashboard rel
       
   inner join menu_tipo_agendamentos meta
           on meta.id = rel.tipo_agendamento_id

   where 
     (
   ( 
            @vendedor_id <> 0
            AND rel.vendedor_id = @vendedor_id                                   
            )
         OR ( 
              @vendedor_id = 0  AND
              rel.loja_id in (@loja_id)              
            )
         )

	 and (ISNULL( rel.contatos_efetuados, 0) > 0 
	      or 
		  ISNULL(rel.ligacoes_disponiveis, 0) > 0
        or 
        ISNULL(rel.valor_venda_now, 0) <> 0
		 )


     and cast(rel.data as date) BETWEEN cast('@data_inicio' as date) AND cast('@data_final' as date)	   

     group by meta.titulo

         )x

         order by 
         x.valor_venda_now desc


            ";

            sql = sql.Replace("@loja_id", idLoja);
            sql = sql.Replace("@vendedor_id", String.IsNullOrEmpty(idVendedor) ? "0" : idVendedor);
            sql = sql.Replace("@data_inicio", dtIni);
            sql = sql.Replace("@data_final", dtFim);

            return _context.ContextDapper.Query<ReportEfetividadeAgenda>(sql).ToList();
        }

        public IList<ReportDedoDuro> ListarReportDedoDuro(string nomeProcedure, string idLoja, string dtIni, string dtFim)
        {
            return Listar<ReportDedoDuro>(nomeProcedure, idLoja, dtIni, dtFim);
        }

        public IList<DashboardEfetividadesVendedores> ListarDashboardEfetividadesVendedores(string nomeProcedure, string idLoja, string dtIni, string dtFim)
        {
            return Listar<DashboardEfetividadesVendedores>(nomeProcedure, idLoja, dtIni, dtFim);
        }

        public IList<DashboardEfetividadesVendedores> ListarDashboardEfetividadesVendedores(string idLoja, string dtIni, string dtFim)
        {
            string sql = @"                                 

 
select * from dashboard_efetividades_vendedores where
loja_id in (@loja_id)

/*    

******* COMENTADO PARA TESTAR A QUERY ACIMA

SELECT 
        ROW_NUMBER() OVER (ORDER BY R.efetividade DESC) as ranking
      , R.loja_id
      , LTRIM(replace(loja.filial, 'FRANQUIA', '')) as loja      
      , R.vendedor_id     
      , case when vede.data_desativacao is not null then vede.nome + ' <span style=''''color: red''''> (Desligado) </span>'
              when loja.id<> vede.loja_id then  vede.nome + ' <span style=''''color:blue'''' > (Transferido)</span>'
              else vede.nome end vendedor
      , R.lig_efetuadas
      , case when ISNULL(R.LigDisponiveis, 0) <> 0 then

                convert(decimal(18, 2), R.lig_efetuadas) / convert(decimal(18, 2), R.LigDisponiveis)
				else
				convert(decimal(18, 2), 0)

                end produtividade
      , R.LigDisponiveis
      , R.efetividade

  FROM
  (
select

          x.loja_id
      , x.vendedor_id
      , sum(x.lig_efetuadas) lig_efetuadas
      , sum(x.LigDisponiveis) LigDisponiveis
      , sum(x.efetividade) as efetividade
  from(

       ----Ligações Disponíveis
       select
              vend.loja_id
               , vend.id as vendedor_id
            , 0 lig_efetuadas
                , isnull(sum(h.LigDisponiveis), 0) as LigDisponiveis
            , 0 efetividade

            from vendedores vend


            left
            join (
               select
                           vendedor_id,
                           vend.loja_id,
                           vend.nome,
                                    count(distinct cliente_id) LigDisponiveis,
                                    tipo_agendamento_id

                            from[dbo].[agendamento_cliente_historico] h
                     inner join vendedores vend on vend.id = h.vendedor_id

                            where

                            (cast(data_entrada as date) >= cast('@data_inicio' as date)

                        AND
                  cast(data_entrada as date) <= cast('@data_final' as date)

                        OR(cast(data_entrada as date) <= cast('@data_final' as date)  and(data_saida is null or cast(data_saida as date) > cast('@data_final' as date)))
                  )
                  AND tipo_agendamento_id in (SELECT id from menu_tipo_agendamentos where Exibir = 1)
                  AND(vend.data_desativacao is null or(cast(vend.data_desativacao as date) >= cast('@data_inicio' as date)

                        AND cast(vend.data_desativacao as date) <= cast('@data_final' as date)))
                  group by vendedor_id, vend.loja_id, vend.nome, tipo_agendamento_id
                       )h
                on vend.id = h.vendedor_id
         group by vend.id
                , vend.loja_id

UNION ALL

          select
                 C1.loja_id
               , V1.id
               , COUNT(V1.nome) as QtdLigacoes
               , 0
               , 0

              from contatos  as C1 with(nolock)


        inner join vendedores V1 with(nolock)

                   on C1.vendedor_id = V1.id


      inner join menu_tipo_agendamentos meta  with(nolock)
            on meta.id = c1.tipo_agendamento_id


               where cast(C1.created_at as date) BETWEEN cast('@data_inicio' as date) AND cast('@data_final' as date)

                and C1.status <> 'Telefone inválido'

          group by V1.id, C1.loja_id

UNION ALL

      ---- Efetividade

           select loja_id
             , vendedor_id
             , 0
          , 0
             , SUM(valor_contato_now) as valor
                from rel_gerou_vendedores
               where cast(data as date) BETWEEN cast('@data_inicio' as date) AND cast('@data_final' as date)
               --AND loja_id in (select item from fnSplit(@LOJA_ID,',')) 
			 group by loja_id, vendedor_id
)x
group by x.loja_id
	  , x.vendedor_id
)R
 join lojas loja with(nolock)
         ON(R.loja_id = loja.id)


 join vendedores vede
              on(vede.id = R.vendedor_id)

where(
       (vede.data_desativacao is not null
       and
       R.efetividade > 0
       )
       or vede.data_desativacao is null
      )

  and loja.id in (@loja_id)  */


            ";

            sql = sql.Replace("@loja_id", idLoja);
            //sql = sql.Replace("@data_inicio", dtIni);
            //sql = sql.Replace("@data_final", dtFim);

            return _context.ContextDapper.Query<DashboardEfetividadesVendedores>(sql).ToList();
        }

        public IList<RelatorioVisitasLojas> ListarVisitasLojas(string nomeProcedure, string idLoja, string dtIni, string dtFim)
        {
            return Listar<RelatorioVisitasLojas>(nomeProcedure, idLoja, dtIni, dtFim);
        }

        public IList<RelatorioEfetividadePorMes> ListarEfetividadePorMes(string nomeProcedure, string idLoja, int ano)
        {
            return _context.ContextDapper.Query<RelatorioEfetividadePorMes>(nomeProcedure, new { @LOJA_ID = idLoja, @ano = ano }, null, true, 60, CommandType.StoredProcedure).ToList();
        }

        private IList<T> ListarComVendedor<T>(string nomeProcedure, string idVendedor, string idLoja, string dtIni, string dtFim)
        {
            return _context.ContextDapper.Query<T>(nomeProcedure, new { @VENDEDOR_ID = idVendedor, @LOJA_ID = idLoja, @data_inicio = Convert.ToDateTime(dtIni), @data_final = Convert.ToDateTime(dtFim) }, null, true, 60, CommandType.StoredProcedure).ToList();
        }

        private IList<T> Listar<T>(string nomeProcedure, string idLoja, string dtIni, string dtFim, int? gerente = null)
        {
            if (gerente == null)
                return _context.ContextDapper.Query<T>(nomeProcedure, new { @LOJA_ID = idLoja, @data_inicio = Convert.ToDateTime(dtIni), @data_final = Convert.ToDateTime(dtFim) }, null, true, 60, CommandType.StoredProcedure).ToList();
            else
                return _context.ContextDapper.Query<T>(nomeProcedure, new { @LOJA_ID = idLoja, @data_inicio = Convert.ToDateTime(dtIni), @data_final = Convert.ToDateTime(dtFim), @gerente = gerente }, null, true, 60, CommandType.StoredProcedure).ToList();
        }


        public IList<ReportRankingVendedores> ListarRankVendedores(string idLoja, string dtIni, string dtFim, int? gerente = null)
        {
            string sql = @"                                 

SELECT 
        ROW_NUMBER() OVER (ORDER BY r.valor_contato_now DESC) as ranking
      , LTRIM(replace(loja.filial, 'FRANQUIA', '')) as loja
      , loja.supervisor as supervisor      
      , case when vede.data_desativacao is not null then vede.nome + ' <span style=''''color: red''''> (Desligado) </span>'
              when loja.id<> vede.loja_id then  vede.nome + ' <span style=''''color:blue''''> (Transferido)</span>'
              else vede.nome end vendedor
      , venda
      , cliente
      , pecas
      , R.valor
      , valor_venda_now
      , valor_contato_now
  FROM(
         select

        x.loja_id
      , x.vendedor_id
      , sum(x.venda) venda
      , sum(x.cliente) cliente
      , sum(x.pecas) pecas
      , sum(x.valor) valor
      , sum(x.valor_venda_now) valor_venda_now
      , sum(x.valor_contato_now) as valor_contato_now


  from(

        SELECT distinct
                  rerv.loja_id
                , rerv.vendedor_id
                , SUM(rerv.venda) as venda
                , SUM(rerv.cliente) as cliente
                , SUM(rerv.pecas) as pecas
                , SUM(rerv.valor) as valor
                , 0 valor_venda_now
                , 0 valor_contato_now
          FROM  rel_ranking_vendedores rerv with(nolock)
         WHERE cast(rerv.data as date) BETWEEN cast('@data_inicio' as date) AND cast('@data_final' as date)
           and rerv.loja_id in (@loja_id)  
          
         GROUP BY rerv.loja_id, rerv.vendedor_id
UNION ALL
        select loja_id
              , vendedor_id
              , 0
              , 0
              , 0
              , 0
              , SUM(valor_venda_now) as valor
              , 0
                from rel_vendeu_vendedores
               where cast(data as date) BETWEEN cast('@data_inicio' as date) AND cast('@data_final' as date)
                 and loja_id in (@loja_id)  

            group by loja_id, vendedor_id
UNION ALL
        select loja_id
             , vendedor_id
             , 0
             , 0
             , 0
             , 0
             , 0
             , SUM(valor_contato_now) as valor
                from rel_gerou_vendedores
               where cast(data as date) BETWEEN cast('@data_inicio' as date) AND cast('@data_final' as date)
                 and loja_id in (@loja_id)                            

             group by loja_id, vendedor_id
      )x

      group by      x.loja_id
      , x.vendedor_id
  )R

 inner join lojas loja with(nolock)
         ON(R.loja_id = loja.id)


 inner join vendedores vede with(nolock)
              on(vede.id = R.vendedor_id)

left join(
                select u.vendedor_id
                     , u.loja_id
                     , up.id_perfil perfil
                  from users u
            inner
                  join users_perfil up on up.id_user = u.id
         )up on up.vendedor_id = vede.id and R.loja_id = up.loja_id

WHERE

      (
            (@gerente = 1 and up.perfil = 5)
            or
            @gerente = 0
         )
    
ORDER BY valor_contato_now DESC

            ";

            sql = sql.Replace("@loja_id", idLoja);
            sql = sql.Replace("@data_inicio", dtIni);
            sql = sql.Replace("@data_final", dtFim);
            sql = sql.Replace("@gerente", gerente.ToString());

            return _context.ContextDapper.Query<ReportRankingVendedores>(sql).ToList();
        }


        public IList<ReportRankingCornerMini> ListarRankingCornerMini(string idLoja, string dtIni, string dtFim, int? gerente = null)
        {
            string sql = @"                                 

SELECT 
         ROW_NUMBER() OVER (ORDER BY b.valor_total DESC) as ranking
       , LTRIM(replace(loja.filial, 'FRANQUIA', '')) as loja
       , loja.supervisor
       , case when vede.data_desativacao is not null then vede.nome + ' <span style=''''color: red''''> (Desligado) </span>'
              when loja.id<> vede.loja_id then  vede.nome + ' <span style=''''color:blue''''> (Transferido)</span>'
              else vede.nome end vendedor
       , b.venda
       , b.cliente
       , b.pecas
       , b.valor
	   , b.valor_total
       , CASE WHEN ISNULL(b.valor_total, 0) <> 0
              THEN ISNULL(b.valor, 0)/ ISNULL(b.valor_total, 0)
              ELSE 0
          END as corner_mini
       , b.valor_venda_now
       , b.valor_contato_now
  FROM
      (
         SELECT distinct

                    recm.loja_id
                , recm.vendedor_id
                , SUM(recm.venda) as venda
                , SUM(recm.cliente) as cliente
                , SUM(recm.pecas) as pecas
                , SUM(recm.valor) as valor
                 , SUM(recm.valor_total) as valor_total
         --     , SUM(recm.corner_mini) as corner_mini
                , SUM(recm.valor_venda_now) as valor_venda_now
                , SUM(recm.valor_contato_now) as valor_contato_now
         FROM   rel_ranking_corner_mini recm with(nolock)

         WHERE cast(recm.data as date) BETWEEN cast('@data_inicio' as date) AND cast('@data_final' as date)
          and recm.loja_id in (@loja_id)

         GROUP BY recm.loja_id, recm.vendedor_id
      )b

    join lojas loja with(nolock)

            ON(b.loja_id = loja.id)

    join vendedores vede with(nolock)
                ON(vede.id = b.vendedor_id)

    left join(
                select u.vendedor_id
                     , u.loja_id
                     , up.id_perfil perfil
                  from users u
            inner
                  join users_perfil up on up.id_user = u.id
         )up on up.vendedor_id = vede.id and b.loja_id = up.loja_id

  WHERE

        (
            (@gerente = 1 and up.perfil = 5)
            or
            @gerente = 0
         )

   AND loja.id not in (select id
                         from lojas
                        where filial like '%EVA%'
                           or filial like '%MINI%'
                           or
                           origem = 'FRANQUIA'
                           or filial like '%COMMERCE%')

ORDER BY b.valor_total DESC

            ";

            sql = sql.Replace("@loja_id", idLoja);
            sql = sql.Replace("@data_inicio", dtIni);
            sql = sql.Replace("@data_final", dtFim);
            sql = sql.Replace("@gerente", gerente.ToString());

            return _context.ContextDapper.Query<ReportRankingCornerMini>(sql).ToList();
        }

        public IList<ReportDedoDuro> ListarDedoDuro(string idLoja, string dtIni, string dtFim, int? gerente = null)
        {
            string sql = @"                                 

   select 
		   loja.supervisor
		 , LTRIM(replace(loja.filial, 'FRANQUIA', '')) as loja
		 , case when vede.data_desativacao is not null then vede.nome + ' <span style=''color: red''> (Desligado) </span>'
                when loja.id<> vede.loja_id then  vede.nome + ' <span style=''color:blue''> (Transferido)</span>'
                else vede.nome end vendedor
	     , meag.titulo as agendamento
         , R.contatos_efetuados
		 , ISNULL(R.ligacoes_disponiveis, 0) as ligacoes_disponiveis
		 , case when ISNULL(R.ligacoes_disponiveis, 0) <> 0 then

                      ISNULL(R.contatos_efetuados / R.ligacoes_disponiveis, 0)

                     when R.contatos_efetuados > 0 and R.ligacoes_disponiveis = 0 then 0
					 else
						  0

                     end produtividade
         , '' as periodo
         , ISNULL(R.valor_venda_now, 0) as valor_venda_now


   from
   (
        select

                sub.loja_id
              , sub.vendedor_id
              , sub.tipo_agendamento_id
              , sum(sub.ligacoes_disponiveis) as ligacoes_disponiveis
              , sum(sub.contatos_efetuados) as contatos_efetuados
              , sum(sub.valor_venda_now) as valor_venda_now

          from(
                   select

                            h.loja_id
                          , vend.id as vendedor_id
                          , meta.id as tipo_agendamento_id
                          , isnull(h.LigDisponiveis, 0) as ligacoes_disponiveis
                          , 0 as contatos_efetuados
                          , 0 as valor_venda_now


                        from vendedores vend


                inner
                        join menu_tipo_agendamentos meta
  
                          on 1 = 1
  

                      left
                        join (

                        select
                                    vendedor_id,
                                    h.loja_id,
                                    vend.nome,
                                            count(distinct cliente_id) LigDisponiveis,
                                            tipo_agendamento_id

                                        from[dbo].[agendamento_cliente_historico] h

                                inner join vendedores vend on vend.id = h.vendedor_id

                                    where

                                        (cast(data_entrada as date) >= cast('@data_inicio' as date)

                                    AND

                            cast(data_entrada as date) <= cast('@data_final' as date)

                                    OR(cast(data_entrada as date) <= cast('@data_final' as date)    and(data_saida is null or cast(data_saida as date) > cast('@data_final' as date)))
                            )
                            -- AND tipo_agendamento_id in (SELECT id from menu_tipo_agendamentos where Exibir = 1)

                            AND(vend.data_desativacao is null or(cast(vend.data_desativacao as date) >= cast('@data_inicio' as date)

                                    AND cast(vend.data_desativacao as date) <= cast('@data_final' as date)))

                            group by vendedor_id, h.loja_id, vend.nome, tipo_agendamento_id
                                )h
                        on meta.id = h.tipo_agendamento_id and vend.id = h.vendedor_id and vend.loja_id = h.loja_id


        union all


             select

                            C1.loja_id as loja_id
						  , V1.id as vendedor_id
						  , meta.id as tipo_agendamento_id
						  , 0 as ligacoes_disponiveis
						  , COUNT(V1.nome) as contatos_efetuados
						  , 0 as valor_venda_now


                           from contatos  as C1 with(nolock)


                      inner join vendedores V1 with(nolock)

                               on C1.vendedor_id = V1.id


                   inner join menu_tipo_agendamentos meta  with(nolock)

                          on meta.id = c1.tipo_agendamento_id


                           where cast(C1.created_at as date) BETWEEN cast('@data_inicio' as date) AND cast('@data_final' as date)

                             and C1.status <> 'Telefone inválido'

                      group by V1.nome, meta.titulo, V1.id, meta.id, C1.loja_id


        union all


                select

                            loja_id
                          , vendedor_id
                          , tipo_agendamento_id
                          , 0 as ligacoes_disponiveis
                          , 0 as contatos_efetuados
                          , SUM(valor_contato_now) as valor_venda_now

                        from rel_gerou_vendedores

                       where cast(data as date) BETWEEN cast('@data_inicio' as date) AND cast('@data_final' as date)

                    group by tipo_agendamento_id, vendedor_id, loja_id

			   )sub
        group by sub.loja_id
			  , sub.vendedor_id
			  , sub.tipo_agendamento_id
	)R

 inner join lojas loja with(nolock)
         ON(R.loja_id = loja.id)


 inner join vendedores vede with(nolock)
              on(vede.id = R.vendedor_id)

 inner join menu_tipo_agendamentos meag with(nolock)
              on(meag.id = R.tipo_agendamento_id)

left join(
                select u.vendedor_id
                     , u.loja_id
                     , up.id_perfil perfil
                  from users u
            inner
                  join users_perfil up on up.id_user = u.id
         )up on up.vendedor_id = vede.id and R.loja_id = up.loja_id

   where loja.id in (@loja_id)

order by
         loja.supervisor, loja.filial, vede.nome, (R.contatos_efetuados)desc, meag.titulo

            ";

            sql = sql.Replace("@loja_id", idLoja);
            sql = sql.Replace("@data_inicio", dtIni);
            sql = sql.Replace("@data_final", dtFim);
            sql = sql.Replace("@gerente", gerente.ToString());

            return _context.ContextDapper.Query<ReportDedoDuro>(sql, commandTimeout: 0).ToList();
        }

        public IList<T> ListarRelatorioComVendedor<T>(string nomeProcedure, string idVendedor)
        {
            return _context.ContextDapper.Query<T>(nomeProcedure, new { @VENDEDOR_ID = idVendedor }, null, true, 60, CommandType.StoredProcedure).ToList();
        }

        public IList<ReportEfetividadeAgendaLojas> ListarEfetividadesPorAgendaLojaVendedor(string idLoja, string dtIni, string dtFim, int IdAgenda, int? gerente)
        {
            return _context.ContextDapper.Query<ReportEfetividadeAgendaLojas>("SP_Email_EfetividadesPorAgendaLojaVendedor", new { @LOJA_ID = idLoja, @data_inicio = Convert.ToDateTime(dtIni), @data_final = Convert.ToDateTime(dtFim), @tipo_agendamento_id = IdAgenda }, null, true, 60, CommandType.StoredProcedure).ToList();

        }
    }
}
