﻿using Now.Domain.Contracts.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using Now.Domain;
using System.Data;


namespace Now.Infrastructure.Repositories
{
    public class DistribuicaoAgendaRepository : IDistribuicaoAgendaRepository
    {
        private NowContext _context;

        public DistribuicaoAgendaRepository(NowContext context)
        {
            _context = context;
        }

        //public IList<ClientesDistribuicao> ListarClientesDistribuicao(int idLoja, string tipoAprovacao, string tipoDistribuicao)
        //{
        //    return _context.ContextDapper.Query<ClientesDistribuicao>("SP_ClientesDistribuidosVendedores", new { @LOJA_ID = idLoja, @TIPOAPROVACAO = tipoAprovacao, @TIPODISTRIBUICAO = tipoDistribuicao }, null, true, 60, CommandType.StoredProcedure).ToList();
        //}

        //public void AtualizarTrocaVendedor(string lojaId, string vendedorId, string clienteId)
        //{
        //    var query = @"  UPDATE clientes_distribuicao_vend_destivados
        //  SET aprovacao_gerente = 'A'
        //    , data_saida = getdate()
        //                         , vendedor_id_dist_gerente = @vendedorId
        //                         , data_aprovacao = getdate()
        // , obs = 'Gerente reprovou a distribuição automatica e selecionou outro vendedor.'
        //WHERE CLIENTE_ID = @clienteId	
        //  AND LOJA_ID = @lojaId
        //  AND aprovacao_gerente = 'P'
        //                 ";

        //    DynamicParameters parametros = new DynamicParameters();

        //    parametros.Add("vendedorId", vendedorId);
        //    parametros.Add("clienteId", clienteId);
        //    parametros.Add("lojaId", lojaId);

        //    _context.ContextDapper.Execute(query, parametros);
        //}


        public void SalvarLogTransferencia(LogTransferenciaAgenda entity)
        {
            var query = @" ";
            DynamicParameters parametros = new DynamicParameters();

            if (entity.id == 0)
            {
                query = @"  
                            insert into LOG_TRANSFERENCIA_AGENDA
                                   values(@loja_id, @vendedor_id_origem, @vendedor_id_destino, @cliente_id, @tipo_agendamento_id, @data_transferencia, @data_cancelamento, @vendedor_id_transferencia)

                            declare @nomeVendedorDestino varchar(25)

                            select @nomeVendedorDestino = nome from vendedores where id = @vendedor_id_destino

                            ---Atualiza a Agenda
                            update agenda
                               set agenda.nome_vendedor = @nomeVendedorDestino
                                 , agenda.vendedor_id = @vendedor_id_destino
                              from menu_agendamentos agenda
                             where vendedor_id = @vendedor_id_origem
                               and tipo_agendamento_id = @tipo_agendamento_id
                               and cliente_id = @cliente_id


                            -- Atualiza o Histórico
                             update mg 
                                set mg.vendedor_id = @vendedor_id_destino
                               from [dbo].[agendamento_cliente_historico_bkp_diario] mg
                              where cast(mg.data_registro as date) = cast(dateadd(hour,-2,getdate()) as date)
                                and mg.tipo_agendamento_id = @tipo_agendamento_id
	                            and cliente_id = @cliente_id
	                            and mg.data_saida is null

                             update mg 
                                set mg.vendedor_id = @vendedor_id_destino
                               from [dbo].[agendamento_cliente_historico] mg
                              where cast(mg.data_registro as date) = cast(dateadd(hour,-2,getdate()) as date)
                                and mg.tipo_agendamento_id = @tipo_agendamento_id
	                            and mg.cliente_id = @cliente_id
	                            and mg.data_saida is null


                         ";
            }
            else
            {
                query = @"  update LOG_TRANSFERENCIA_AGENDA
                               set data_cancelamento             = @data_cancelamento
                                 , vendedor_id_transferencia     = @vendedor_id_transferencia
                              where id = @id
                                ";

                parametros.Add("id", entity.id);
            }

            parametros.Add("loja_id", entity.loja_id);
            parametros.Add("vendedor_id_origem", entity.vendedor_id_origem);
            parametros.Add("vendedor_id_destino", entity.vendedor_id_destino);
            parametros.Add("cliente_id", entity.cliente_id);
            parametros.Add("tipo_agendamento_id", entity.tipo_agendamento_id);
            parametros.Add("data_transferencia", entity.data_transferencia);
            parametros.Add("data_cancelamento", entity.data_cancelamento);
            parametros.Add("vendedor_id_transferencia", entity.vendedor_id_transferencia);            

            _context.ContextDapper.Execute(query, parametros);
        }

        public IList<LogTransferenciaAgenda> ListarLogDistribuicao(string LojaId)
        {
            var sql = @"
                
                             select 
	                               log_.id
                                 , clie.cpf
		                         , clie.nome cliente_nome
		                         , agen.titulo as nome_agenda
                                 , veor.id vendedor_id_origem
		                         , veor.nome as nome_vendedor_origem
                                 , vede.id vendedor_id_destino
		                         , vede.nome as nome_vendedor_destino
		                         , veca.nome as nome_vendedor_transferencia
		                         , case when log_.data_cancelamento is null then log_.data_transferencia else log_.data_cancelamento end data
		                         , case when log_.data_cancelamento is null then 'TRANSFERIDO' else 'CANCELADO' end status
                              from LOG_TRANSFERENCIA_AGENDA log_
                        inner join clientes clie on clie.id = log_.cliente_id
                        inner join menu_tipo_agendamentos agen on agen.id = log_.tipo_agendamento_id
                        inner join vendedores veor on veor.id = log_.vendedor_id_origem
                        inner join vendedores vede on vede.id = log_.vendedor_id_destino
                        inner join vendedores veca on veca.id = log_.vendedor_id_transferencia
                             where cast(case when log_.data_cancelamento is null then log_.data_transferencia else log_.data_cancelamento end as date) = cast(getdate() as date)
                               and log_.loja_id = @lojaId
                          order by case when log_.data_cancelamento is null then log_.data_transferencia else log_.data_cancelamento end desc

                  
            ";

            DynamicParameters parametros = new DynamicParameters();

            parametros.Add("lojaId", LojaId);

            var result = _context.ContextDapper.Query<LogTransferenciaAgenda>(sql, parametros);

            return result.ToList();
        }

        public LogTransferenciaAgenda ObterLogDistribuicao(string idLog)
        {

            var sql = @"  select * from LOG_TRANSFERENCIA_AGENDA
                                             where id = @id ";

            DynamicParameters parametro = new DynamicParameters();

            parametro.Add("id", idLog);            

            return _context.ContextDapper.Query<LogTransferenciaAgenda>(sql, parametro).ToList().FirstOrDefault();            
        }
    }
}
