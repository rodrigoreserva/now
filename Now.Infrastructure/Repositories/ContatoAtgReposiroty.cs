﻿using Now.Domain.Contracts.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using Now.Domain;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Migrations;
using System.Linq.Expressions;

namespace Now.Infrastructure.Repositories
{
    public class ContatoAtgRepository : IContatoAtgRepository
    {
        private NowContext _context;

        public ContatoAtgRepository(NowContext context)
        {
            _context = context;
        }
     
        public IList<ContatoAtg> BuscarContatosPorData(DateTime data)
        {
            const string sql = @"
               			    SELECT *
                FROM contatos_atg
                WHERE cast(data_cadastro as date) = @data
                ORDER BY data_cadastro desc
            ";

            var result = _context.ContextDapper.Query<ContatoAtg>(sql, new { data = data });

            return result.ToList();
        }
         
        public void AdicionarContatoAtg(ContatoAtg contato)
        {
            DynamicParameters parametros = new DynamicParameters();

            parametros.Add("cpf", contato.cpf);
            parametros.Add("status", contato.status);
            parametros.Add("modo", contato.modo);
            parametros.Add("termometro", contato.termometro);
            parametros.Add("quem_ligou", contato.quem_ligou);
            parametros.Add("quem_ele", contato.quem_ele);
            parametros.Add("obs_ligacao", contato.obs_ligacao);
            parametros.Add("data_cadastro", DateTime.Now);

            const string sql = @"
               			    SELECT *
                              FROM contatos_atg
                             WHERE cpf = @cpf
                               and status = @status
                               and modo = @modo
                               and termometro = @termometro
                               and quem_ligou = @quem_ligou
                               and quem_ele = @quem_ele
                               and obs_ligacao = @obs_ligacao                               
                               ";

            var result = _context.ContextDapper.Query<ContatoAtg>(sql, parametros);

            if (result.Count() == 0)
            {
                var query = @" insert into contatos_atg
                            values(@cpf, @status, @modo, @termometro, @quem_ligou, @quem_ele, @obs_ligacao, @data_cadastro)";


                _context.ContextDapper.Execute(query, parametros);
            }
            else
            {
                throw new Exception("Contato já cadastro!");
            }
        }

    }
}
