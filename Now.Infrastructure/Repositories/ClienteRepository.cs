﻿using Now.Domain.Contracts.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using Now.Domain;

namespace Now.Infrastructure.Repositories
{
    public class ClienteRepository : IClienteRepository
    {
        private NowContext _context;
        public ClienteRepository(NowContext context)
        {
            _context = context;
        }

        public IList<Cliente> BuscarClienteCpfOrigemLoja(string origemLoja, string cpf)
        {
            var sql = @" SELECT c.*, '' as NomeLoja, 0 as IdLoja, '' as NomeVendedor, 0 as IdVendedor FROM Clientes c with(nolock) ";

            //Filtros dinamicos
            var filter = "";

            filter += string.IsNullOrWhiteSpace(cpf) ? "" : " where c.cpf = '" + cpf + "' ";
            filter += string.IsNullOrWhiteSpace(origemLoja) ? "" : " AND c.origem_cliente = '" + origemLoja + "' ";

            sql += filter;

            var clientes = _context.ContextDapper.Query<Cliente>(sql).ToList();

            return clientes;
        } 

        public IList<Cliente> ClientesAptosAgenda()
        {
            var sql = @" select * from vw_clientes_apto_agenda ";

            var clientes = _context.ContextDapper.Query<Cliente>(sql).ToList();

            return clientes;
        }

        public IList<Cliente> BuscarCliente(string origemLoja, string nome, string cpf, string telefone, string idLoja, string idVendedor, string filtroPesquisa)
        {
            var sql = @" 
select 
		 top 50 
          x.*
         , clim.acumulado_12_reserva
         , clim.acumulado_12_mini
         , clim.acumulado_12_eva
         , clim.acumulado_total_reserva
         , clim.acumulado_total_mini
         , clim.acumulado_total_eva
		 , 
						case when lres.filial is null then '' ELSE lres.filial END
                        + case when lmin.filial is null then '' ELSE + ', ' + lmin.filial END
						+ case when leva.filial is null then '' ELSE + ', ' + leva.filial END
                        as NomeLoja,

                        case when lres.id is not null then lres.id                             
                             when lmin.id is not null then lmin.id                              
						           when leva.id is not null then leva.id 
                         end IdLoja,

                        case when vres.nome is null then '' ELSE vres.nome END
                        + case when vmin.nome is null then '' ELSE + ', ' + vmin.nome END
						+ case when veva.nome is null then '' ELSE + ', ' + veva.nome END                         
                        as NomeVendedor,

                        case when vres.id is not null then vres.id 
                             when vmin.id is not null then vmin.id 
						           when veva.id  is not null then veva.id 
                          end IdVendedor
  from (

SELECT top 50 c.* FROM Clientes c with(nolock)                       

                        WHERE 1 = 1  
						and c.nome like '%" + nome + @"%'
union all
-- CPF
SELECT top 50 c.* FROM Clientes c with(nolock)                      

                        WHERE 1 = 1  
						and c.cpf like '" + cpf + @"%' 
union all
-- Telefone
SELECT top 50 c.* FROM Clientes c with(nolock)                       

                        WHERE 1 = 1  
						and c.tel like '" + telefone + @"%'
union all
-- Celular		
SELECT top 50 c.* FROM Clientes c with(nolock)                       

                        WHERE 1 = 1  
						and c.cel like '" + telefone + @"%'
)x

 left join CLIENTES_METRICAS_GERAIS clim with(nolock)
                               on clim.cliente_id = x.id

 LEFT JOIN lojas lres with(nolock)
                               on lres.id = x.loja_id --@lojaReserva

                        LEFT JOIN lojas lmin with(nolock)
                               on lmin.id = x.loja_id_mini --@lojaMini

                        LEFT JOIN lojas leva with(nolock)
                               on leva.id = x.loja_id_eva --@lojaEva

                        LEFT JOIN vendedores vres with(nolock)
                               on vres.id = x.vendedor_id

                        LEFT JOIN vendedores vmin with(nolock)
                               on vmin.id = x.vendedor_id_mini

                        LEFT JOIN vendedores veva with(nolock)
                               on veva.id = x.vendedor_id_eva

                        WHERE 1 = 1 ";

            //Filtros dinamicos
            var filter = "";
            //if (!string.IsNullOrWhiteSpace(nome) && !string.IsNullOrWhiteSpace(cpf) && !string.IsNullOrWhiteSpace(telefone))
            //{
            //    filter += string.IsNullOrWhiteSpace(nome) ? "" : " AND ( c.nome like '%" + nome + "%' ";
            //    filter += string.IsNullOrWhiteSpace(cpf) ? "" : " OR c.cpf like '" + cpf + "%' ";
            //    //TODO Colocar inteligência para buscar celular e os ddds.
            //    filter += string.IsNullOrWhiteSpace(telefone) ? "" : " OR (c.tel like '" + telefone + "%' or c.cel like '" + telefone + "%'))";
            //}
            //else
            //{
            //    filter += string.IsNullOrWhiteSpace(nome) ? "" : " AND c.nome like '%" + nome + "%' ";
            //    filter += string.IsNullOrWhiteSpace(cpf) ? "" : " AND c.cpf = '" + cpf + "' ";
            //    //TODO Colocar inteligência para buscar celular e os ddds.
            //    filter += string.IsNullOrWhiteSpace(telefone) ? "" : " AND (c.tel like '" + telefone + "%' or c.cel like '" + telefone + "%')";
            //}

            filter += string.IsNullOrWhiteSpace(origemLoja) ? "" : " AND x.origem_cliente = '" + origemLoja + "' ";

            //filter += string.IsNullOrWhiteSpace(idLoja) ? "" : " AND c.origem_cliente = '" + origemLoja + "' ";

            // Loja de Preferencia
            /*
             * A partir de agora o vendedor só conseguirá consultar clientes que:

            - Sejam preferenciais da sua loja
            - Sejam seus por preferência
            - Já tenham sido atendidos por ele em outra loja da Cia.   

             */
            filter += string.IsNullOrWhiteSpace(filtroPesquisa) ? "" : " AND ((lres.id = " + idLoja + " or lmin.id = " + idLoja + " or leva.id = " + idLoja + ")  " + (string.IsNullOrWhiteSpace(idVendedor) ? "" : " OR exists(select count(1) from vendas_detalhe vend  with(nolock) where vend.cliente_id = x.id and vend.vendedor_id = " + idVendedor + " having  count(1) > 0 )") + " )";

            sql += filter;

            var clientes = _context.ContextDapper.Query<Cliente>(sql).ToList();

            return clientes;
        }

        public IList<ClienteDependente> BuscarDependentes(int idCliente)
        {
            var sql = @"
                    SELECT *
                      FROM [clientes_dependentes]
                     WHERE data_desativacao is null
                       AND cliente_id = @idCliente
             ";

            DynamicParameters paraDependente = new DynamicParameters();

            paraDependente.Add("idCliente", idCliente);

            var list = _context.ContextDapper.Query<ClienteDependente>(sql, paraDependente).ToList();

            return list;
        }

        public IList<ClienteNovo> BuscarClienteNovo(string idLoja, string idVendedor, string idClienteNovo)
        {
            var sql = @"SELECT TOP 50 cn.[id]
                  ,cn.[loja_id]
                  ,cn.[vendedor_id]
                  ,cn.[info_vendas]
                  ,cn.[cpf]
                  ,cn.[quem_ele]
                  ,cn.[data_venda]
                  ,cn.[data_cadastro]
                  , l.filial
                  , v.nome as vendedor
                  , cn.status
              FROM [pd].[dbo].[clientes_novos] cn with(nolock)
        inner join lojas l with(nolock) on l.id = cn.loja_id
        inner join vendedores v with(nolock) on v.id = cn.vendedor_id 
             WHERE 1 = 1
               #LOJA#
               #VENDEDOR#
               #CLIENTE#
             ";

            sql = string.IsNullOrEmpty(idLoja) ? sql.Replace("#LOJA#", "") : sql.Replace("#LOJA#", " AND cn.loja_id = " + idLoja);
            sql = string.IsNullOrEmpty(idClienteNovo) ? sql.Replace("#CLIENTE#", "") : sql.Replace("#CLIENTE#", " AND cn.id = " + idClienteNovo);
            sql = string.IsNullOrEmpty(idVendedor) ? sql.Replace("#VENDEDOR#", "") : sql.Replace("#VENDEDOR#", " AND cn.vendedor_id = " + idVendedor);

            sql = sql + " order by cn.data_cadastro desc";

            var clientes = _context.ContextDapper.Query<ClienteNovo>(sql).ToList();

            return clientes;
        }

        public void SalvarDependenteCliente(ClienteDependente clienteDependente)
        {
            var query = string.Empty;
            DynamicParameters parametros = new DynamicParameters();
            parametros.Add("data_nascimento", clienteDependente.data_nascimento);
            parametros.Add("grau_parentesco", clienteDependente.grau_parentesco);
            parametros.Add("nome", clienteDependente.nome);
            parametros.Add("sexo", clienteDependente.sexo);
            parametros.Add("loja_id", clienteDependente.loja_id == 0 ? null : clienteDependente.loja_id);
            parametros.Add("vendedor_id", clienteDependente.vendedor_id == 0 ? null : clienteDependente.vendedor_id);
            parametros.Add("observacao", clienteDependente.observacao);

            if (clienteDependente.id == 0)
            {
                query = @"  insert into clientes_dependentes
                     values(@data_nascimento, @nome, @grau_parentesco, @observacao, @cliente_id, @data_inclusao, @data_alteracao, @data_desativacao, @loja_id, @vendedor_id, @sexo) SELECT  @@IDENTITY ";

                parametros.Add("cliente_id", clienteDependente.cliente_id);
                parametros.Add("data_inclusao", DateTime.Now);
                parametros.Add("data_alteracao", clienteDependente.data_alteracao);
                parametros.Add("data_desativacao", clienteDependente.data_desativacao);

                clienteDependente.id = Convert.ToInt32(_context.ContextDapper.ExecuteScalar(query, parametros).ToString());
            }
            else
            {
                /*
                 *  Conforme conversamos, criamos um tipo de venda baseado no aniversário do dependente. 
                 *  Porém a única forma de controle se aquele cliente tem ou não aquele dependente aniversariante seria  pelo cadastro do now.  
                 *  Solicitamos uma planilha no qual nos informa  a venda, o CPF do cliente, os Dados do dependentes no NOW, a data de Nascimento e o dia de cadastro. 
                 *  Porém, para evitar que ocorra problemas futuros, o Fábio solicitou que tivesse uma trava de alteração da data de nascimento do dependente
                 * */
                //query = @"  update clientes_dependentes
                //               set data_nascimento = @data_nascimento
                //                 , nome = @nome
                //              , grau_parentesco = @grau_parentesco
                //              , observacao = @observacao	                             
                //              , data_alteracao = @data_alteracao
                //                 , loja_id = @loja_id
                //                 , vendedor_id = @vendedor_id
                //                 , sexo = @sexo
                //             where id = @id";

                query = @"  update clientes_dependentes
                               set nome = @nome
	                             , grau_parentesco = @grau_parentesco
	                             , observacao = @observacao	                             
	                             , data_alteracao = @data_alteracao
                                 , loja_id = @loja_id
                                 , vendedor_id = @vendedor_id
                                 , sexo = @sexo
                             where id = @id";

                parametros.Add("data_alteracao", DateTime.Now);
                parametros.Add("id", clienteDependente.id);

                _context.ContextDapper.Execute(query, parametros);
            }
        }

        public void RemoverDependente(int idDependente)
        {

            const string sql = @"
                                    UPDATE clientes_dependentes 
                                       SET data_alteracao = @data_alteracao
                                         , data_desativacao = @data_desativacao
                                     WHERE id = @IdCliente
                                ";

            DynamicParameters parametros = new DynamicParameters();

            parametros.Add("IdCliente", idDependente);
            parametros.Add("data_alteracao", DateTime.Now);
            parametros.Add("data_desativacao", DateTime.Now);

            _context.ContextDapper.Execute(sql, parametros);
        }

        public Cliente FindClientById(string id, int idUsuario, string idLoja, string idVendedorSelecionado)
        {
            var sql = @"SELECT 
                      c.id
                    , c.id_cliente
                    , c.cpf
                    , upper(c.nome) nome
                    , c.tel_ddd
                    , c.tel
                    , c.cel_ddd
                    , c.cel
                    , c.tipo_log
                    , c.logradouro
                    , c.numero
                    , c.complemento
                    , c.bairro
                    , c.cidade
                    , upper(c.uf) uf
                    , c.cep
                    , c.dt_nasc
                    , c.email
                    , upper(ltrim(isnull(c.sexo, ''))) sexo
                    , c.dt_cadastro
                    , c.estado_civil
                    , c.tipo_varejo
                    , c.sem_credito
                    , c.origem_cliente
                    , c.data_para_transferencia
                    , c.caracteristicas
                    , c.vendedor
                    , c.created_at
                    , c.updated_at
                    , c.loja_id
                    , c.loja_id_mini
                    , c.loja_id_eva
                    , c.loja_id_original
                    , c.perfil_reservado
                    , c.flag_experimentador
                    , c.classificacao_cliente
                    , c.AcumuladoVenda12
                    , c.AcumuladoVendaGeral
                    , c.vendedor_id
                    , c.vendedor_id_mini
                    , c.vendedor_id_eva
                    , c.acumulado_loja
                    , c.acumulado_loja_mini
                    , c.acumulado_loja_eva
                    , c.perfil_reservado_eva
                    , c.qtde_retirada_loja
                    , c.obs
                    , c.preferencia_contato
                    , isnull(clim.apto_reservado_reserva, 0) apto_reservado_reserva
                    , isnull(clim.apto_reservado_mini, 0) apto_reservado_mini
                    , isnull(clim.apto_reservado_eva, 0) apto_reservado_eva
                    , isnull(clim.acumulado_12_reserva, 0) acumulado_12_reserva
                    , isnull(clim.acumulado_12_mini, 0) acumulado_12_mini
                    , isnull(clim.acumulado_12_eva, 0) acumulado_12_eva
                    , isnull(clim.acumulado_total_reserva, 0) acumulado_total_reserva
                    , isnull(clim.acumulado_total_mini, 0) acumulado_total_mini
                    , isnull(clim.acumulado_total_eva, 0) acumulado_total_eva
                    , case when clfa.cliente_id is not null then 1
					        else 0 end cliente_favorito
            ,case when lres.filial is null then '' ELSE lres.filial END
            + case when lmin.filial is null then '' ELSE + ', ' + lmin.filial END
            + case when leva.filial is null then '' ELSE + ', ' + leva.filial END
            as NomeLoja,

            case when lres.id is not null then lres.id                             
                 when lmin.id is not null then lmin.id                              
                       when leva.id is not null then leva.id 
             end IdLoja,

            case when vres.nome is null then '' ELSE vres.nome END
            + case when vmin.nome is null then '' ELSE + ', ' + vmin.nome END
            + case when veva.nome is null then '' ELSE + ', ' + veva.nome END                         
            as NomeVendedor,

            case when vres.id is not null then vres.id 
                 when vmin.id is not null then vmin.id 
                       when veva.id  is not null then veva.id 
              end IdVendedor
            -- , reco.recomendacao
            FROM Clientes c with(nolock)

             left join CLIENTES_METRICAS_GERAIS clim with(nolock)
                               on clim.cliente_id = c.id

            LEFT JOIN lojas lres with(nolock)
                   on lres.id = c.loja_id --@lojaReserva

            LEFT JOIN lojas lmin with(nolock)
                   on lmin.id = c.loja_id_mini --@lojaMini

            LEFT JOIN lojas leva with(nolock)
                   on leva.id = c.loja_id_eva --@lojaEva

            LEFT JOIN vendedores vres with(nolock)
                   on vres.id = c.vendedor_id

            LEFT JOIN vendedores vmin with(nolock)
                   on vmin.id = c.vendedor_id_mini

            LEFT JOIN vendedores veva with(nolock)
                   on veva.id = c.vendedor_id_eva

            LEFT JOIN clientes_favoritos clfa
			       on (clfa.cliente_id = c.id and clfa.data_desativacao is null and clfa.data_sem_contato is null
                        and @idVendedorSelecionado = clfa.vendedor_id
                        )
             /*   
             LEFT JOIN (
                     select 
							reco2.cpf
						   , replace(replace(replace(replace(replace(replace(recommendation,'[', '{'),']', '}'),'{{', '[{'),'}}', '}]'),'{""', '{ ""produto"":""'),'"", ""','"", ""cor_produto"":""') recomendacao                               
                      from (
					    select cpf                             
							 , max(data_insercao) data1
                         from RSV_SALEBASED_USER2ITEM_CF reco						  
				     group by  cpf
					       )reco 
					inner join  RSV_SALEBASED_USER2ITEM_CF reco2 on (reco.cpf = reco2.cpf and reco.data1 = reco2.data_insercao)
                      )reco on reco.cpf = c.cpf */

            --@CondicaoPerfil

            WHERE c.id = @IdCliente ";

            //Verifica se o usuário é perfil Rede
            #region "Perfil de Rede"

            var sqlPerfilUsuario = @" select count(*) from users_perfil up
                            inner join perfil p on p.id_perfil = up.id_perfil
                            where p.rede = 1 ";

            //Filtros dinamicos
            var filterPerfil = "";
            filterPerfil += idUsuario != 0 ? " and up.id_user = " + idUsuario.ToString() : "";

            var sqlPerfil = sqlPerfilUsuario + filterPerfil;

            int perfilRede = _context.ContextDapper.Query<int>(sqlPerfil).FirstOrDefault();

            #endregion "Perfil de Rede"

            ///// METODO COMENTADO POIS TODOS OS USUARIOS PODEM VER CLIENTES

            //var sqlCondicaoPerfil = "";
            //if (perfilRede == 0)
            //{
            //        var condicaoPerfil = @" inner join (select loja.id
            //                                       from lojas loja
            //                                     inner join users_perfil_lojas uspl
            //                                       on uspl.id_loja = loja.id
            //                                       where 1 = 1
            //                                       -- Filtros personalizados
            //                                        {0}
            //                                      ) subu on (subu.id = c.loja_id or subu.id = c.loja_id_mini or subu.id = c.loja_id_eva) ";

            //    //Filtros dinamicos
            //    var filter = "";
            //    filter += idUsuario != 0 ? " and uspl.id_user = " + idUsuario.ToString() : "";

            //    sqlCondicaoPerfil = String.Format(condicaoPerfil, filter);

            //    sql = sql.Replace("@CondicaoPerfil", sqlCondicaoPerfil);
            //}            
            //else
            //    sql = sql.Replace("@CondicaoPerfil", "");

            //if (!string.IsNullOrEmpty(idLoja))
            //{
            //    sql = sql.Replace("@lojaReserva", " and lres.id = " + idLoja + "").Replace("@lojaMini", " and lmin.id = " + idLoja + "").Replace("@lojaEva", " and leva.id = " + idLoja + "");
            //}
            //else
            //{
            //    sql = sql.Replace("@lojaReserva", "").Replace("@lojaMini", "").Replace("@lojaEva", "");
            //}

            var query = sql; // + (string.IsNullOrEmpty(idLoja) ? "" : " and (c.loja_id = " + idLoja + " or c.loja_id_mini = " + idLoja + " or c.loja_id_eva = " + idLoja + ")");

            var cliente = _context.ContextDapper.Query<Cliente>(query, new { IdCliente = id, idVendedorSelecionado = idVendedorSelecionado });

            return cliente.FirstOrDefault();
        }

        public void SalvarPreferenciaContato(int idcliente, string preferenciaContato)
        {
            //Validando
            if (idcliente < 1)
                throw new Exception("");

            const string sql = @"
                                UPDATE clientes 
                                SET preferencia_contato = @preferenciaContato
                                WHERE id = @IdCliente
                                ";
            _context.ContextDapper.Execute(sql, new { IdCliente = idcliente, preferenciaContato = preferenciaContato });
        }

        public void SalvarClienteQuemEEle(int idcliente, string caracteristica)
        {
            //Validando
            if (idcliente < 1)
                throw new Exception("");

            const string sql = @"
        UPDATE clientes 
        SET caracteristicas = @Caracteristica
        WHERE id = @IdCliente
    ";
            _context.ContextDapper.Execute(sql, new { IdCliente = idcliente, Caracteristica = caracteristica });
        }

        public void SalvarClienteObs(int idcliente, string observacao)
        {
            //Validando
            if (idcliente < 1)
                throw new Exception("");

            const string sql = @"
        UPDATE clientes 
        SET obs = @Observacao
        WHERE id = @IdCliente
    ";
            _context.ContextDapper.Execute(sql, new { IdCliente = idcliente, Observacao = observacao });
        }

        public void SalvarFavoritos(int idCliente, int idVendedor, string cliFavarito)
        {
            #region "Verifica se Existe"

            var sqlExisteCliente = @" 
                                 select 
	 	                                *
                                   from clientes_favoritos
                                  where cliente_id	= @idCliente 
                                    and vendedor_id = @idVendedor
	                                and data_sem_contato is null
	                                and data_desativacao is null
                                ";

            var consultaExiste = _context.ContextDapper.Query<ClientesFavoritos>(sqlExisteCliente, new { idCliente = idCliente, idVendedor = idVendedor });

            #endregion "Perfil de Rede"

            if (consultaExiste.Count() > 0)
            {
                if (cliFavarito == "0")
                {
                    string sql1 = @"
                                     update clientes_favoritos
                                    set data_desativacao = getdate()
                                  where cliente_id	= @idCliente 
                                    and vendedor_id = @idVendedor
                                ";
                    _context.ContextDapper.Execute(sql1, new { idCliente = idCliente, idVendedor = idVendedor });
                }
            }

            if (consultaExiste.Count() == 0 && cliFavarito == "1")
            {
                string sqlInsert = @"
                                   insert into clientes_favoritos 
                               values (@idCliente, getdate(), @idVendedor, null, null, null)
                                ";
                _context.ContextDapper.Execute(sqlInsert, new { idCliente = idCliente, idVendedor = idVendedor });
            }
        }

        public void SalvarDependenteClienteNovo(ClienteNovoDependente clienteNovoDependente)
        {
            var query = @"  insert into clientes_novos_dependentes
                     values(@data_nascimento, @nome, @grau_parentesco, @observacao, @cliente_id, @data_inclusao, @data_alteracao, @data_desativacao, @loja_id, @vendedor_id, @sexo)";

            DynamicParameters parametros = new DynamicParameters();

            parametros.Add("cliente_id", clienteNovoDependente.cliente_id);
            parametros.Add("data_alteracao", clienteNovoDependente.data_alteracao);
            parametros.Add("data_desativacao", clienteNovoDependente.data_desativacao);
            parametros.Add("data_inclusao", clienteNovoDependente.data_inclusao);
            parametros.Add("data_nascimento", clienteNovoDependente.data_nascimento);
            parametros.Add("grau_parentesco", clienteNovoDependente.grau_parentesco);
            parametros.Add("nome", clienteNovoDependente.nome);
            parametros.Add("sexo", clienteNovoDependente.sexo);
            parametros.Add("loja_id", clienteNovoDependente.loja_id);
            parametros.Add("vendedor_id", clienteNovoDependente.vendedor_id);
            parametros.Add("observacao", clienteNovoDependente.observacao);

            _context.ContextDapper.Execute(query, parametros);
        }

        public void SalvarClienteNovo(ClienteNovo clienteNovo)
        {
            var query = @"  insert into clientes_novos
                     values(@loja_id, @vendedor_id, @info_vendas, @cpf, @quem_ele, @data_venda, @data_cadastro, @status) SELECT  @@IDENTITY ";

            DynamicParameters parametros = new DynamicParameters();

            parametros.Add("loja_id", clienteNovo.loja_id);
            parametros.Add("vendedor_id", clienteNovo.vendedor_id);
            parametros.Add("info_vendas", clienteNovo.info_vendas);
            parametros.Add("cpf", clienteNovo.cpf);
            parametros.Add("quem_ele", clienteNovo.quem_ele);
            parametros.Add("data_venda", clienteNovo.data_venda);
            parametros.Add("data_cadastro", clienteNovo.data_cadastro);
            parametros.Add("status", clienteNovo.status);

            clienteNovo.id = Convert.ToInt32(_context.ContextDapper.ExecuteScalar(query, parametros).ToString());
        }

        public void ExluirClienteNovo(string Id)
        {
            // Deleta os dependentes
            var queryDependentes = @"  delete from clientes_novos_dependentes
                                             where cliente_id = @id ";

            DynamicParameters paraDependente = new DynamicParameters();

            paraDependente.Add("id", Id);

            _context.ContextDapper.Execute(queryDependentes, paraDependente);

            // Deleta o cliente
            var query = @"  delete from clientes_novos
                    where id = @id ";

            DynamicParameters parametros = new DynamicParameters();

            parametros.Add("id", Id);

            _context.ContextDapper.Execute(query, parametros);
        }

        public ClienteDependente ObterDependente(string IdDependente)
        {
            // Deleta os dependentes
            var queryDependentes = @"  select * from clientes_dependentes
                                             where id = @id ";

            DynamicParameters paraDependente = new DynamicParameters();

            paraDependente.Add("id", IdDependente);

            var dependente = _context.ContextDapper.Query<ClienteDependente>(queryDependentes, paraDependente).ToList().FirstOrDefault();

            return dependente;
        }


        #region "Indicação de Produtos"
        public int SalvarIndicacaoProdutos(int clienteId, int? lojaId, int? vendedorId, IList<Produto> prodSelecionados)
        {
            int codIndicacao = 0;
            /// Salva a Indicação
            var query = @" insert into CLIENTE_INDICACAO_PRODUTO (cliente_id,loja_id,vendedor_id,data_indicacao)
                     values(@cliente_id, @loja_id, @vendedor_id, @data_indicacao) SELECT  @@IDENTITY ";

            DynamicParameters parametros = new DynamicParameters();

            parametros.Add("loja_id", (lojaId == 0 ? null : lojaId));
            parametros.Add("vendedor_id", (vendedorId == 0 ? null : vendedorId));
            parametros.Add("cliente_id", clienteId);
            parametros.Add("data_indicacao", DateTime.Now);

            codIndicacao = Convert.ToInt32(_context.ContextDapper.ExecuteScalar(query, parametros).ToString());

            /// Salva a Indicação Produtos Itens
            foreach (Produto item in prodSelecionados)
            {
                var queryIndicacao = @" insert into CLIENTE_INDICACAO_PRODUTO_ITENS (cliente_indicacao_id, produto_id)
                     values(@cliente_indicacao_id, @produto_id)";

                DynamicParameters paramItens = new DynamicParameters();

                paramItens.Add("cliente_indicacao_id", codIndicacao);
                paramItens.Add("produto_id", item.id);

                _context.ContextDapper.ExecuteScalar(queryIndicacao, paramItens);
            }

            return codIndicacao;
        }

        public IndicacaoProduto BuscarIndicacaoProdutosPorIndicacao(string id)
        {
            var sql = @"
                    select prod.id
	                   , prod.cliente_id
	                   , clie.nome nome_cliente
	                   , loj.id loja_id
	                   , loj.filial nome_loja
	                   , vend.id vendedor_id
	                   , vend.nome nome_vendedor
	                   , isnull(cliente_respondeu,0) cliente_respondeu
	                   , cliente_respondeu_data
                       , cliente_respondeu
                       , cliente_respondeu_data
                  from CLIENTE_INDICACAO_PRODUTO prod
                  join clientes clie on clie.id = prod.cliente_id
                  join lojas loj on loj.id = prod.loja_id
                  join vendedores vend on vend.id = prod.vendedor_id
                 where prod.id =  @id
             ";

            DynamicParameters param = new DynamicParameters();

            param.Add("id", id);

            var indicacao = _context.ContextDapper.Query<IndicacaoProduto>(sql, param).ToList().FirstOrDefault();

            if (indicacao != null)
            {
                // Retornar os itens 

                var sqlItens = @"
                             select prIte.id
                                   , prIte.produto_id
	                               , cliente_escolheu
	                               , observacao
	                               , data_cliente
                              from CLIENTE_INDICACAO_PRODUTO_ITENS prIte 
                             where cliente_indicacao_id = @idIndicacao
             ";


                DynamicParameters paramItens = new DynamicParameters();

                paramItens.Add("idIndicacao", indicacao.id);

                var listItens = _context.ContextDapper.Query<IndicacaoProdutoItens>(sqlItens, paramItens).ToList();

                // retorna o produto
                foreach (var prod in listItens)
                {
                    var sqlProduto = @"select distinct 
                               id
                             , produto
                             , cor_produto
                             , descricao_produto
                             , grupo_produto
                          from produtos
                         where id = @idProduto
                                ";

                    var resultProd = _context.ContextDapper.Query<Produto>(sqlProduto, new { idProduto = prod.produto_id });

                    prod.produto = resultProd.FirstOrDefault();
                }

                indicacao.lista_produtos = listItens;
            }

            return indicacao;
        }

        public void SalvarEnvioIndicacaoProdutos(int idIndicacao, IList<IndicacaoProdutoItens> indicacaoProdutoItens)
        {
            var query = string.Empty;
            DynamicParameters parametros = new DynamicParameters();

            query = @"  update CLIENTE_INDICACAO_PRODUTO
                               set cliente_respondeu = @cliente_respondeu
                                 , cliente_respondeu_data = @cliente_respondeu_data	                           
                             where id = @id";

            parametros.Add("cliente_respondeu_data", DateTime.Now);
            parametros.Add("cliente_respondeu", 1);
            parametros.Add("id", idIndicacao);

            _context.ContextDapper.Execute(query, parametros);

            foreach (var item in indicacaoProdutoItens.Where(x=> x.cliente_escolheu == 1).ToList())
            {                
                var queryItem = string.Empty;
                DynamicParameters parItens = new DynamicParameters();

                queryItem = @"  update CLIENTE_INDICACAO_PRODUTO_ITENS
                               set cliente_escolheu = @cliente_escolheu
                                 , observacao = @observacao
                                 , data_cliente = @data_cliente	                           
                             where produto_id = @produto_id
                               and cliente_indicacao_id = @idIndicacao";

                parItens.Add("cliente_escolheu", item.cliente_escolheu);
                parItens.Add("observacao", item.observacao);
                parItens.Add("data_cliente", DateTime.Now);
                parItens.Add("produto_id", item.produto_id);
                parItens.Add("idIndicacao", idIndicacao);

                _context.ContextDapper.Execute(queryItem, parItens);
            }           
        }


        #endregion "Indicação de Produtos"
    }
}
