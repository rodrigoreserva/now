﻿using Now.Domain.Contracts.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using Now.Domain;


namespace Now.Infrastructure.Repositories
{
    public class RetiradaLojaRepository : IRetiradaLojaRepository
    {
        private NowContext _context;

        public RetiradaLojaRepository(NowContext context)
        {
            _context = context;
        }

        public IList<PedidosRetiradaLoja> ListarRetiradas6Meses(string idCliente = "")
        {
            var sql = @"select 
                                   *
                              from pedidos_retirada_loja with(nolock)
                             where data_compra >= DATEADD(day,-180, GETDATE())     
                               /*and status_pedido = 'Entregue ao cliente'  */
                               and cliente_id = @IdCliente
                          order by data_compra desc
                                ";

            var result = _context.ContextDapper.Query<PedidosRetiradaLoja>(sql, new { IdCliente = idCliente });

            return result.ToList();
        }
    }
}
