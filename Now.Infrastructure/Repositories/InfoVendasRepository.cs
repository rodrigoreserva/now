﻿using Now.Domain.Contracts.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using Now.Domain;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Linq.Expressions;

namespace Now.Infrastructure.Repositories
{
    public class InfoVendasRepository : IInfoVendasRepository
    {
        private NowContext _context;

        public InfoVendasRepository(NowContext context)
        {
            _context = context;
        }

        public void SalvarInfoVenda(InfoVendas infoVenda)
        {
            var query = @" ";
            DynamicParameters parametros = new DynamicParameters();

            if (infoVenda.id == 0)
            {
                query = @"  insert into info_vendas
                                 values(@loja_id, @cliente_id, @data_registro, @info, @created_at, @updated_at, @deleted_at, @status, @vendedor_id)";
            }
            else {
                query = @"  update info_vendas
                               set loja_id             = @loja_id
                                 , cliente_id          = @cliente_id
                                 , data_registro       = @data_registro
                                 , info                = @info                                 
                                 , updated_at          = @updated_at
                                 , status              = @status
                                 , vendedor_id         = @vendedor_id
                              where id = @id
                                ";

                parametros.Add("id", infoVenda.id);
            }                        

            parametros.Add("loja_id", infoVenda.loja_id);
            parametros.Add("cliente_id", infoVenda.cliente_id);
            parametros.Add("data_registro", infoVenda.data_registro);
            parametros.Add("info", infoVenda.info);
            parametros.Add("created_at", infoVenda.created_at);
            parametros.Add("updated_at", infoVenda.updated_at);
            parametros.Add("deleted_at", infoVenda.deleted_at);
            parametros.Add("status", infoVenda.status);
            parametros.Add("vendedor_id", infoVenda.vendedor_id);

            _context.ContextDapper.Execute(query, parametros);
        }
        public void DeleteInfoVenda(int id)
        {
            var query = @"  update info_vendas
                               set deleted_at = @deleted_at
                                 , status = @status
                             where id = @id ";

            DynamicParameters parametros = new DynamicParameters();

            parametros.Add("id", id);
            parametros.Add("deleted_at", DateTime.Now);
            parametros.Add("status", 0);

            _context.ContextDapper.Execute(query, parametros);
        }

        /// <summary>
        /// Lista 
        /// </summary>
        /// <param name="idcliente"></param>
        /// <returns></returns>
        public IList<InfoVendas> ListarInfoVendas(int idUsuario, string idLoja, string idVendedor)
        {
            //Verifica se o usuário é perfil Rede
            #region "Perfil de Rede"

            var sqlPerfilUsuario = @" select count(*) from users_perfil up
                                        inner join perfil p on p.id_perfil = up.id_perfil
                                        where p.rede = 1 ";

            //Filtros dinamicos
            var filterPerfil = "";
            filterPerfil += idUsuario != 0 ? " and up.id_user = " + idUsuario.ToString() : "";

            var sqlPerfil = sqlPerfilUsuario + filterPerfil;

            int perfilRede = _context.ContextDapper.Query<int>(sqlPerfil).FirstOrDefault();

            #endregion "Perfil de Rede"

            var sql = @"
                SELECT top 50 loj.filial, ven.nome as vendedor, inve.*, clie.cpf, clie.nome as nome_cliente
                FROM info_vendas    inve
          inner join clientes       clie 
                  on clie.id = inve.cliente_id
          inner join lojas          loj 
                  on loj.id = inve.loja_id   
          left join vendedores      ven
                  on ven.id = inve.vendedor_id
                WHERE 1 = 1  
                  and inve.status = 1     
            ";

            var orderby = " ORDER BY inve.created_at desc";

            var sqlCondicaoPerfil = "";
            if (perfilRede == 0)
            {
                var condicaoPerfil = @" and inve.loja_id in (select loja.id
                                                      from lojas loja
                                                inner join users_perfil_lojas uspl
                                                        on uspl.id_loja = loja.id
                                                     where 1 = 1
                                                        -- Filtros personalizados
                                                           {0}
                                                    )         ";


                //Filtros dinamicos
                var filter = "";
                filter += idUsuario != 0 ? " and uspl.id_user = " + idUsuario.ToString() : "";

                sqlCondicaoPerfil = String.Format(condicaoPerfil, filter);
            }

            sqlCondicaoPerfil += string.IsNullOrEmpty(idLoja) ? "" : " and loj.id = " + idLoja;

            sqlCondicaoPerfil += string.IsNullOrEmpty(idVendedor) ? "" : " and ven.id = " + idVendedor;

            var query = sql + sqlCondicaoPerfil + orderby;

            var result = _context.ContextDapper.Query<InfoVendas>(query);

            return result.ToList();
        }

        /// <summary>
        /// Retorna InfoVendas
        /// </summary>
        /// <param name="idcliente"></param>
        /// <returns></returns>
        public InfoVendas BuscarInfoVendas(int id)
        {
            const string sql = @"

              SELECT info.*, cli.cpf
                  , cli.nome as nome_cliente
                  , loja.filial
                  , vend.nome as vendedor                  
                FROM info_vendas info with(nolock)
          inner join clientes cli with(nolock) on cli.id = info.cliente_id 
          inner join lojas loja with(nolock) on loja.id = info.loja_id
          inner join vendedores vend with(nolock) on vend.id = info.vendedor_id

                WHERE info.id = @Id    
            ";

            var result = _context.ContextDapper.Query<InfoVendas>(sql, new { id = id });

            if (result.ToList().Count() > 0)
                return result.ToList().FirstOrDefault();
            else
                return null;
        }

        public InfoVendas BuscarInfoVendasPorCliente(int idCliente, DateTime dataVenda, int LojaId)
        {
            const string sql = @"

              SELECT info.*, cli.cpf
                  , cli.nome as nome_cliente
                  , loja.filial
                  , vend.nome as vendedor                  
                FROM info_vendas info with(nolock)
          inner join clientes cli with(nolock) on cli.id = info.cliente_id 
          inner join lojas loja with(nolock) on loja.id = info.loja_id
          inner join vendedores vend with(nolock) on vend.id = info.vendedor_id

                WHERE info.cliente_id = @_idCliente    
                  AND info.data_registro = @_dataVenda
                  AND info.loja_id = @_lojaId   
                  AND info.deleted_at is null
            ";

            var result = _context.ContextDapper.Query<InfoVendas>(sql, new { _idCliente = idCliente, _dataVenda = Convert.ToDateTime(dataVenda.ToString("yyyy-MM-dd")), _lojaId = LojaId });

            if (result.ToList().Count() > 0)
                return result.ToList().FirstOrDefault();
            else
                return null;
        }

    }
}
