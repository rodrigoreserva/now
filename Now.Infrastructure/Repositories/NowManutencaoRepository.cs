﻿using Now.Domain.Contracts.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using Now.Domain;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Linq.Expressions;

namespace Now.Infrastructure.Repositories
{
    public class NowManutencaoRepository : INowManutencaoRepository
    {
        private NowContext _context;

        public NowManutencaoRepository(NowContext context)
        {
            _context = context;
        }

        public void SalvarNowManutencao(NowManutencao NowManutencao)
        {
            var query = @"  insert into visita_lojas
                                 values(@data_visita,@loja_id,@observacao)";

            DynamicParameters parametros = new DynamicParameters();

            parametros.Add("data_visita", NowManutencao.data_visita);
            parametros.Add("loja_id", NowManutencao.loja_id);
            parametros.Add("observacao", NowManutencao.observacao);
            
            _context.ContextDapper.Execute(query, parametros);
        }

        public void DeleteNowManutencao(int id)
        {
            var query = @"     delete from visita_lojas 
                                    where id = @Id ";

            _context.ContextDapper.Execute(query, new { id = id });
        }

        public IList<NowControleCarga> ListarLogCarga()
        {

            var query = @"  select 
                                   id
	                              , data_carga
	                              , status
	                              , descricao
                              from controle_carga ";

            var result = _context.ContextDapper.Query<NowControleCarga>(query);

            return result.ToList();
        }
       
        public NowManutencao BuscarNowManutencao(int id)
        {
            const string sql = @"
                SELECT *
                FROM visita_lojas with(nolock)
                WHERE id = @Id                
            ";

            var result = _context.ContextDapper.Query<NowManutencao>(sql, new { id = id });

            if (result.ToList().Count() > 0)
                return result.ToList().FirstOrDefault();
            else
                return null;
        }

        public NowManutencao BuscarLojasPorDataVisita(string data)
        {
            const string sql = @"
                SELECT *
                FROM visita_lojas with(nolock)
                WHERE data_visita = @data                
            ";

            var result = _context.ContextDapper.Query<NowManutencao>(sql, new { data = data });

            if (result.ToList().Count() > 0)
                return result.ToList().FirstOrDefault();
            else
                return null;
        }
    }
}
