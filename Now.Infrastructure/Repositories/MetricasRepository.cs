﻿using Now.Domain.Contracts.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using Now.Domain;
using System.Data;


namespace Now.Infrastructure.Repositories
{
    public class MetricasRepository : IMetricasRepository
    {
        private NowContext _context;

        public MetricasRepository(NowContext context)
        {
            _context = context;
        }

        public IList<Metricas> ListarMetricas(string nomeProcedure, string idLoja, string idVendedor, string dtIni, string dtFim)
        {
            return Listar<Metricas>(nomeProcedure, idVendedor, idLoja, dtIni, dtFim);
        }

        public IList<Metricas> ListarMetricas(string idLoja, string idVendedor, string dtIni, string dtFim)
        {

            if (!String.IsNullOrEmpty(idLoja))
            {
                string sql1 = @"                                 
                SELECT 
                 case when acum.qtde_ticket = 0 then 0 
                      else (convert(decimal(18,2),acum.qtde) / convert(decimal(18,2),acum.qtde_ticket))
                      end pa

               , case when acum.qtde_ticket = 0 then 0 
                      else (convert(decimal(18,2),acum.valor) / convert(decimal(18,2),acum.qtde_ticket))
                      end tm     
      
           FROM
               (
                     select sum(valor_venda) valor
                          , sum(qtde_venda) qtde
                          , sum(qtde_ticket) qtde_ticket
                          , count(distinct cast(data_venda as date)) dias_trabalhado
                       from rel_qtde_vendas with(nolock)
                      where cast(data_venda as date) BETWEEN cast('@data_inicio' as date) AND cast('@data_final' as date)                        
						and loja_id in (@loja_id)
                        				        
					         
                   ) acum      

            ";

                sql1 = sql1.Replace("@loja_id", idLoja);
                sql1 = sql1.Replace("@idVendedor", " and vendedor_id in (" + idVendedor + ")");
                sql1 = sql1.Replace("@data_inicio", dtIni);
                sql1 = sql1.Replace("@data_final", dtFim);

                return _context.ContextDapper.Query<Metricas>(sql1).ToList();

            }
            else
            {
                string sql2 = @"                                 

         SELECT 
                 case when acum.qtde_ticket = 0 then 0 
                      else (convert(decimal(18,2),acum.qtde) / convert(decimal(18,2),acum.qtde_ticket))
                      end pa

               , case when acum.qtde_ticket = 0 then 0 
                      else (convert(decimal(18,2),acum.valor) / convert(decimal(18,2),acum.qtde_ticket))
                      end tm     
      
           FROM
               (
                    select sum(valor_venda) valor
                          , sum(qtde_venda) qtde
                          , sum(qtde_ticket) qtde_ticket
                          , count(distinct cast(data_venda as date)) dias_trabalhado
                       from rel_qtde_vendas with(nolock)
                      where cast(data_venda as date) BETWEEN cast('@data_inicio' as date) AND cast('@data_final' as date)
                        AND vendedor_id = @IdVendedor
                   ) acum      


            ";

                sql2 = sql2.Replace("@IdVendedor", idVendedor);
                sql2 = sql2.Replace("@data_inicio", dtIni);
                sql2 = sql2.Replace("@data_final", dtFim);

                return _context.ContextDapper.Query<Metricas>(sql2).ToList();
            }
        }

        private IList<T> Listar<T>(string nomeProcedure, string idVendedor, string idLoja, string dtIni, string dtFim)
        {
            return _context.ContextDapper.Query<T>(nomeProcedure, new { @VENDEDOR_ID = idVendedor, @LOJA_ID = idLoja, @data_inicio = Convert.ToDateTime(dtIni), @data_final = Convert.ToDateTime(dtFim) }, null, true, 60, CommandType.StoredProcedure).ToList();
        }
    }
}
