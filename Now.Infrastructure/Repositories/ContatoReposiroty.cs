﻿using Now.Domain.Contracts.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using Now.Domain;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Migrations;
using System.Linq.Expressions;

namespace Now.Infrastructure.Repositories
{
    public class ContatoRepository : IContatoRepository
    {
        private NowContext _context;

        public ContatoRepository(NowContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Retorna todos os contatos efetuados com o cliente.
        /// </summary>
        /// <param name="idcliente"></param>
        /// <returns></returns>
        public IList<FraseContato> ContatosPorCliente(string idcliente)
        {
            const string sql = @"
                SELECT *
                FROM [V_HistoricoTimeline]
                WHERE cliente_id = @IdCliente                  
                ORDER BY data_contato desc
            ";

            var result = _context.ContextDapper.Query<FraseContato>(sql, new { IdCliente = idcliente }).ToList();

            foreach (FraseContato item in result)
            {
                if (item.tipo == "INDICACAO")
                {
                    item.ListaProdutos = BuscarIndicacaoProdutosPorIndicacao(item.id.ToString());
                }
            }

            return result.ToList();
        }

        public IList<ContatoClienteAvaliacao> ReenvioAvaliacao()
        {
            var sqlProduto = @"select 
                                      cont.*
	                                  , clie.email clienteEmail
	                                  , clie.nome as clienteNome
	                                  , loja.filial LojaNome
	                                  , vend.nome VendedorNome
	                                  , vend.img_perfil_intranet
	                                  , case when loja.tipo_loja in ('RESERVA', 'RESERVA+MINI') then 'RESERVA'
	                                         else loja.tipo_loja end   tipo_loja
	                                  , case 
							                                when loja.tipo_loja in ('RESERVA','RESERVA+MINI', 'INTERNET') then 'https://imagens.usereserva.com.br/rsv_assets/img/logo.png'
							                                when loja.tipo_loja in ('EVA') then 'https://imagens.usereserva.com.br/rsv_assets/img/logo_eva.png'
							                                when loja.tipo_loja in ('MINI') then 'https://imagens.usereserva.com.br/rsv_assets/img/logo_mini.png'
							                                when loja.tipo_loja in ('AHLMA') then 'https://imagens.usereserva.com.br/rsv_assets/img/svgs/logos/ahlma.svg'
							                                when loja.tipo_loja in ('FACAVOCE') then 'https://imagens.usereserva.com.br/rsv_assets/img/logo_diy.png'
							                                when loja.tipo_loja in ('OFICINA') then 'https://imagens.usereserva.com.br/rsv_assets/img/svgs/logos/oficina.svg'
					                                   end url_avaliacao
	                                  from CONTATOS cont
	                                join clientes clie on clie.id = cont.cliente_id 
	                                join lojas loja on loja.id = cont.loja_id
	                                join vendedores vend on vend.id = cont.vendedor_id 
                                 where cont.created_at > dateadd(day, -5, getdate()) 
                                   and cont.created_at < '2019-03-20 15:00:00'
                                   and cont.loja_id not in (select id from lojas where tipo_loja = 'EVA')
                                   and status not in ('Telefone inválido', 'Não consegui falar')
                                   and tipo_agendamento_id <> 1
                                   and clie.email <> ''                              
                                                                ";

            return _context.ContextDapper.Query<ContatoClienteAvaliacao>(sqlProduto).ToList();
        }

        public IList<Produto> ListarProduto(string id)
        {
            var sqlProduto = @"select distinct 
                               p.id
                             , produto
                             , cor_produto
                             , descricao_produto
                             , grupo_produto
                          from produtos p 
				    inner join [dbo].[CLIENTE_INDICACAO_PRODUTO_ITENS] psel on psel.produto_id = p.id
					inner join [dbo].[CLIENTE_INDICACAO_PRODUTO] pcli on pcli.id = psel.cliente_indicacao_id
                         where pcli.id = @id 
                               
                                ";

            return _context.ContextDapper.Query<Produto>(sqlProduto, new { id = id }).ToList();
        }

        public IndicacaoProduto BuscarIndicacaoProdutosPorIndicacao(string id)
        {
            var sql = @"
                    select prod.id
	                   , prod.cliente_id
	                   , clie.nome nome_cliente
	                   , loj.id loja_id
	                   , loj.filial nome_loja
	                   , vend.id vendedor_id
	                   , vend.nome nome_vendedor
	                   , isnull(cliente_respondeu,0) cliente_respondeu
	                   , cliente_respondeu_data
                       , cliente_respondeu
                       , cliente_respondeu_data
                  from CLIENTE_INDICACAO_PRODUTO prod
                  join clientes clie on clie.id = prod.cliente_id
                  join lojas loj on loj.id = prod.loja_id
                  join vendedores vend on vend.id = prod.vendedor_id
                 where prod.id =  @id
             ";

            DynamicParameters param = new DynamicParameters();

            param.Add("id", id);

            var indicacao = _context.ContextDapper.Query<IndicacaoProduto>(sql, param).ToList().FirstOrDefault();

            if (indicacao != null)
            {
                // Retornar os itens 

                var sqlItens = @"
                             select prIte.id
                                   , prIte.produto_id
	                               , cliente_escolheu
	                               , observacao
	                               , data_cliente
                              from CLIENTE_INDICACAO_PRODUTO_ITENS prIte 
                             where cliente_indicacao_id = @idIndicacao
             ";


                DynamicParameters paramItens = new DynamicParameters();

                paramItens.Add("idIndicacao", indicacao.id);

                var listItens = _context.ContextDapper.Query<IndicacaoProdutoItens>(sqlItens, paramItens).ToList();

                // retorna o produto
                foreach (var prod in listItens)
                {
                    var sqlProduto = @"select distinct 
                               id
                             , produto
                             , cor_produto
                             , descricao_produto
                             , grupo_produto
                          from produtos
                         where id = @idProduto
                                ";

                    var resultProd = _context.ContextDapper.Query<Produto>(sqlProduto, new { idProduto = prod.produto_id });

                    prod.produto = resultProd.FirstOrDefault();
                }

                indicacao.lista_produtos = listItens;
            }

            return indicacao;
        }

        public Contato AtualizarTermometroContato(string idContato, string termometro)
        {
            var query = @" update contatos set termometro= @termometro where id=@id";

            DynamicParameters parametros = new DynamicParameters();

            parametros.Add("id", idContato);
            parametros.Add("termometro", termometro);

            _context.ContextDapper.Execute(query, parametros);

            return BuscarContato(idContato);
        }

        public Contato AdicionarContato(Contato contato)
        {
            var query = @" insert into contatos
                            values(@created_at, @updated_at, @obs, @cliente_id, @vendedor_id, @status, @termometro, @modo, @deleted_at, @tipo, @flagReservado, @dt_Reservado, @agendamento, @caracteristicas, @tipo_agendamento_id, @loja_id, @data_criacao, @data_envio_avaliacao) 
                            
                            SELECT CAST(SCOPE_IDENTITY() as int)

                            ";

            DynamicParameters parametros = new DynamicParameters();

            parametros.Add("created_at", DateTime.Now);
            parametros.Add("updated_at", DateTime.Now);
            parametros.Add("obs", contato.obs);
            parametros.Add("cliente_id", contato.cliente_id);
            parametros.Add("vendedor_id", contato.vendedor_id);
            parametros.Add("status", contato.status);
            parametros.Add("termometro", contato.termometro);
            parametros.Add("modo", contato.modo);
            parametros.Add("deleted_at", null);
            parametros.Add("tipo", null);
            parametros.Add("flagReservado", contato.flagReservado);
            parametros.Add("dt_Reservado", null);
            parametros.Add("agendamento", contato.agendamento);
            parametros.Add("caracteristicas", contato.caracteristicas);
            parametros.Add("tipo_agendamento_id", contato.tipo_agendamento_id);
            parametros.Add("loja_id", contato.loja_id);
            parametros.Add("data_criacao", DateTime.Now);
            parametros.Add("data_envio_avaliacao", contato.data_envio_avaliacao);


            var _idContato = _context.ContextDapper.Query<int>(query, parametros).Single();

            return BuscarContato(_idContato.ToString());
        }

        public Contato BuscarContato(string idContato)
        {
            var sql = @"
                  select 
                       cont.*
		             , isnull(coav.id, 0) as avaliado
		             , vend.nome as  VendedorNome
		             , loja.filial as  LojaNome
		             , meta.titulo as  AgendaNome
                     , clie.nome clienteNome
                     , case 
							when loja.tipo_loja in ('RESERVA','RESERVA+MINI', 'INTERNET') then 'https://imagens.usereserva.com.br/rsv_assets/img/logo.png'
							when loja.tipo_loja in ('EVA') then 'https://imagens.usereserva.com.br/rsv_assets/img/logo_eva.png'
							when loja.tipo_loja in ('MINI') then 'https://imagens.usereserva.com.br/rsv_assets/img/logo_mini.png'
							when loja.tipo_loja in ('AHLMA') then 'https://imagens.usereserva.com.br/rsv_assets/img/svgs/logos/ahlma.svg'
							when loja.tipo_loja in ('FACAVOCE') then 'https://imagens.usereserva.com.br/rsv_assets/img/logo_diy.png'
							when loja.tipo_loja in ('OFICINA') then 'https://imagens.usereserva.com.br/rsv_assets/img/svgs/logos/oficina.svg'
					   end url_avaliacao
                     , coav.flg_contato_feito
                     , case 
							when loja.tipo_loja in ('RESERVA','RESERVA+MINI', 'INTERNET') then 'RESERVA'
							else loja.tipo_loja
					   end tipo_loja
                  from contatos cont
            inner join clientes clie on clie.id = cont.cliente_id
            inner join vendedores vend on vend.id = cont.vendedor_id
            inner join lojas loja on loja.id = cont.loja_id
            inner join menu_tipo_agendamentos meta on meta.id = cont.tipo_agendamento_id
             left join CONTATOS_AVALICAO coav on coav.contato_id = cont.id
                 where cont.id = @idContato
             ";

            DynamicParameters _para = new DynamicParameters();

            _para.Add("idContato", idContato);

            var contato = _context.ContextDapper.Query<Contato>(sql, _para).ToList().FirstOrDefault();

            return contato;
        }

        public ContatoCliente BuscarContatoCliente(string telefone)
        {
            var sql = @"
                  select
                       vendedor_id
                     , nome
		             , cpf
		             , telefone
		             , flg_contato_enviado
                  from CONTATOS_CLIENTE
                 where telefone = @telefone
             ";

            DynamicParameters _para = new DynamicParameters();

            _para.Add("telefone", telefone);

            var contato = _context.ContextDapper.Query<ContatoCliente>(sql, _para).ToList().FirstOrDefault();

            return contato;
        }

        public ContatoCliente BuscarCliente(string telefone)
        {
            var sql = @"
                  select
                       vendedor_id
                     , nome
		             , cpf
		             , case when cel = ''  then tel else cel end telefone
                  from clientes
                 where cel=@telefone or tel=@telefone
             ";

            DynamicParameters _para = new DynamicParameters();

            _para.Add("telefone", telefone);

            var contato = _context.ContextDapper.Query<ContatoCliente>(sql, _para).ToList().FirstOrDefault();

            return contato;
        }

        public Contato AdicionarAvaliacao(string idContato, string vlrAvaliacao, string observacao, int flgContatoFeito, string texto)
        {
            var query = @" insert into CONTATOS_AVALICAO
                            values(@Contato, @avaliacao, @descricao, @flg_contato_feito, @data_inclusao, @data_alteracao, @data_exclusao, @texto)
                         ";

            DynamicParameters parametros = new DynamicParameters();

            parametros.Add("data_inclusao", DateTime.Now);
            parametros.Add("data_alteracao", DateTime.Now);
            parametros.Add("data_exclusao", null);
            parametros.Add("Contato", idContato);
            parametros.Add("avaliacao", vlrAvaliacao);
            parametros.Add("descricao", observacao);
            parametros.Add("flg_contato_feito", flgContatoFeito);
            parametros.Add("texto", texto);

            _context.ContextDapper.Query<Contato>(query, parametros);

            return BuscarContato(idContato);
        }

        public ContatoCliente AdicionarCliente(string vendedor_id, string nome, string cpf, string telefone, string email)
        {
            var query = @" insert into CONTATOS_CLIENTE
                            (vendedor_id,nome,cpf,telefone,flg_contato_enviado,data_alteracao,data_inclusao, email)
                            values(@vendedor_id, @nome, @cpf, @telefone, @flg_contato_enviado, @data_alteracao,  @data_inclusao, @email)
                         ";

            DynamicParameters parametros = new DynamicParameters();

            parametros.Add("data_inclusao", DateTime.Now);
            parametros.Add("data_alteracao", DateTime.Now);            
            parametros.Add("vendedor_id", vendedor_id);
            parametros.Add("nome", nome);
            parametros.Add("cpf", cpf);
            parametros.Add("flg_contato_enviado", false);
            parametros.Add("telefone", telefone);
            parametros.Add("email", email);

            _context.ContextDapper.Query<Contato>(query, parametros);

            return BuscarContatoCliente(vendedor_id);
        }
    }
}
