﻿using Now.Domain.Contracts.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using Now.Domain;
using System.Data;


namespace Now.Infrastructure.Repositories
{
    public class ClientesDistribuicaoRepository : IClientesDistribuicaoRepository
    {
        private NowContext _context;

        public ClientesDistribuicaoRepository(NowContext context)
        {
            _context = context;
        }

        public IList<ClientesDistribuicao> ListarClientesDistribuicao(int idLoja, string tipoAprovacao, string tipoDistribuicao)
        {
            return _context.ContextDapper.Query<ClientesDistribuicao>("SP_ClientesDistribuidosVendedores", new { @LOJA_ID = idLoja, @TIPOAPROVACAO = tipoAprovacao, @TIPODISTRIBUICAO = tipoDistribuicao }, null, true, 60, CommandType.StoredProcedure).ToList();
        }

        public void AtualizarTrocaVendedor(string lojaId, string vendedorId, string clienteId)
        {
            var query = @"  UPDATE clientes_distribuicao_vend_destivados
							   SET aprovacao_gerente = 'A'
							     , data_saida = getdate()
                                 , vendedor_id_dist_gerente = @vendedorId
                                 , data_aprovacao = getdate()
								 , obs = 'Gerente reprovou a distribuição automatica e selecionou outro vendedor.'
							 WHERE CLIENTE_ID = @clienteId	
							   AND LOJA_ID = @lojaId
							   AND aprovacao_gerente = 'P'
                         ";

            DynamicParameters parametros = new DynamicParameters();

            parametros.Add("vendedorId", vendedorId);
            parametros.Add("clienteId", clienteId);
            parametros.Add("lojaId", lojaId);
            
            _context.ContextDapper.Execute(query, parametros);
        }
    }
}
