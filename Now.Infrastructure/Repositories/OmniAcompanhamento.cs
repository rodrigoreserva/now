﻿using Now.Domain.Contracts.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using Now.Domain;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Linq.Expressions;

namespace Now.Infrastructure.Repositories
{
    public class OmniAcompanhamentoRepository : IOmniAcompanhamentoRepository
    {
        private NowContext _context;

        public OmniAcompanhamentoRepository(NowContext context)
        {
            _context = context;
        }

        public IList<OmniAcompanhamento> Listar(string IdLojaSeleconado, string vendedorId)
        {
            var sql = @"
                
                           select 		
		                      omni.id      
                            , omni.loja_id 
                            , omni.vendedor_id 
                            , cliente_id 
                            , clie.cpf
                            , pedido
                            , data_compra as pedido_data 
                            , qtde as quatidade 
                            , valor_venda as valor      
                            , case when status_pedido = 'FINISHED'  then 'Finalizado'
							       when status_pedido = 'IN_STORE'  then 'Na Loja'
								   when status_pedido = 'PICKING'  then 'Em Separação'
								   when status_pedido = 'PRE_SENT'  then 'Em Expedição'
								   when status_pedido = 'SENT'  then 'Enviado'
								   when status_pedido = 'INVOICED'  then 'Faturado'
				                   when status_pedido = 'CANCELLED_MANUAL'  then 'Cancelado'
								   else status_pedido end status_pedido
                            , visto_flag 
                            , visto_data      
                            , loja.filial      
                            , vend.nome as vendedor 
                            , clie.nome as nome_cliente
                            from omni_acompanhamento omni
                        left join lojas loja on
                            loja.id = omni.loja_id
                        left join vendedores vend on
                            vend.id = omni.vendedor_id
                        left join clientes clie on
                            clie.id = omni.cliente_id

                WHERE status_pedido not in ('PRE_SENT', 'PICKING', 'PACKAGED')
                  
            ";

            var orderby = " ORDER BY pedido_data desc";

            var sqlCondicaoVendedor = String.IsNullOrEmpty(vendedorId) ? "" : " and omni.vendedor_id = " + vendedorId;

            var sqlCondicaoLoja = String.IsNullOrEmpty(IdLojaSeleconado) ? "" : " and omni.loja_id = " + IdLojaSeleconado;

            var query = sql + sqlCondicaoVendedor + sqlCondicaoLoja + orderby;

            var result = _context.ContextDapper.Query<OmniAcompanhamento>(query);

            return result.ToList();
        }

        public void AtualizarStatusOmni(string pedido)
        {
            const string sql = @"
                     update OMNI_ACOMPANHAMENTO
                       set visto_flag = 1
                          , visto_data = GETDATE()
                     where visto_flag = 0
                       and pedido = @pedido
                ";
            _context.ContextDapper.Execute(sql, new { pedido = pedido });
        }


       
    }
}
