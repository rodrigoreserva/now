﻿using Now.Domain.Contracts.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using Now.Domain;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Linq.Expressions;

namespace Now.Infrastructure.Repositories
{
    public class PerguntasQuemEleRespostasRepository : IPerguntasQuemEleRespostasRepository
    {
        private NowContext _context;

        public PerguntasQuemEleRespostasRepository(NowContext context)
        {
            _context = context;
        }

        public IList<PerguntasQuemEleRespostas> SalvarPerguntasQuemEleRespostas(PerguntasQuemEleRespostas entity)
        {
            var query = @" ";
            DynamicParameters parametros = new DynamicParameters();

            if (entity.id == 0)
            {
                query = @"  insert into PERGUNTAS_QUEM_ELE_RESPOSTAS
                                 values(@resposta, @id_perguntas_quem_ele, @data_inclusao, @data_alteracao, @data_desativacao, @id_perguntas_quem_ele_subsequente)";
            }
            else {
                query = @"  update PERGUNTAS_QUEM_ELE_RESPOSTAS
                               set resposta                 = @resposta
                                 , id_perguntas_quem_ele    = @id_perguntas_quem_ele                                 
                                 , id_perguntas_quem_ele_subsequente = @id_perguntas_quem_ele_subsequente
                                 , data_inclusao            = @data_inclusao                                 
                                 , data_alteracao           = @data_alteracao
                                 , data_desativacao         = @data_desativacao                                 
                              where id = @id
                                ";

                parametros.Add("id", entity.id);
            }                        

            parametros.Add("resposta", entity.resposta);
            parametros.Add("id_perguntas_quem_ele", entity.id_perguntas_quem_ele);
            parametros.Add("id_perguntas_quem_ele_subsequente", entity.id_perguntas_quem_ele_subsequente);            
            parametros.Add("data_inclusao", entity.data_inclusao);
            parametros.Add("data_alteracao", entity.id == 0 ? entity.data_alteracao : DateTime.Now);
            parametros.Add("data_desativacao", entity.data_desativacao);            

            _context.ContextDapper.Execute(query, parametros);

            return ListarPerguntasQuemEleRespostas(entity.id_perguntas_quem_ele);
        }
        public IList<PerguntasQuemEleRespostas> DeletePerguntasQuemEleRespostas(int id, int idPergunta)
        {
            var query = @"  update PERGUNTAS_QUEM_ELE_RESPOSTAS
                               set data_desativacao = @data_desativacao
                                 , data_alteracao   = @data_alteracao
                             where id = @id ";

            DynamicParameters parametros = new DynamicParameters();

            parametros.Add("id", id);
            parametros.Add("data_desativacao", DateTime.Now);
            parametros.Add("data_alteracao", DateTime.Now);

            _context.ContextDapper.Execute(query, parametros);

            return ListarPerguntasQuemEleRespostas(idPergunta);
        }     
        public IList<PerguntasQuemEleRespostas> ListarPerguntasQuemEleRespostas(int idPergunta)
        {
            var sql = @"
             SELECT RESP.*, isnull(PERG.pergunta,'Sem Pergunta') AS perg_subsequente
                FROM PERGUNTAS_QUEM_ELE_RESPOSTAS  RESP
		   LEFT JOIN PERGUNTAS_QUEM_ELE PERG ON PERG.id = RESP.id_perguntas_quem_ele_subsequente
               WHERE RESP.data_desativacao  is null
                 and id_perguntas_quem_ele = @idPergunta
            ";

            DynamicParameters parametros = new DynamicParameters();

            parametros.Add("idPergunta", idPergunta);

            var result = _context.ContextDapper.Query<PerguntasQuemEleRespostas>(sql, parametros);

            return result.ToList();
        }

        public IList<PerguntasQuemEleRespostas> ListarTodasPerguntasQuemEleRespostas()
        {
            var sql = @"
             SELECT RESP.*, isnull(PERG.pergunta,'Sem Pergunta') AS perg_subsequente
                FROM PERGUNTAS_QUEM_ELE_RESPOSTAS  RESP
		   LEFT JOIN PERGUNTAS_QUEM_ELE PERG ON PERG.id = RESP.id_perguntas_quem_ele_subsequente
               WHERE RESP.data_desativacao  is null                 
            ";

            var result = _context.ContextDapper.Query<PerguntasQuemEleRespostas>(sql);

            return result.ToList();
        }
    }
}
