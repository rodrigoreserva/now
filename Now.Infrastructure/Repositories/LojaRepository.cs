﻿using Now.Domain.Contracts.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using Now.Domain;

namespace Now.Infrastructure.Repositories
{
    public class LojaRepository : ILojaRepository
    {

        private NowContext _context;

        public LojaRepository(NowContext context)
        {
            _context = context;
        }


        /// <summary>
        /// Retorna um cliente através do id do cliente
        /// </summary>
        /// <param name="id">id do cliente</param>
        /// <returns></returns>
        public IList<Loja> ListarLojasVisiveisPorUsuario(int idUsuario, string idLoja = "")
        {
            #region "Verifica se o usuário é perfil Rede"

            var sqlPerfilUsuario = @" select count(*) from users_perfil up with(nolock)
                                        inner join perfil p with(nolock) on p.id_perfil = up.id_perfil
                                        where p.rede = 1 ";

            //Filtros dinamicos
            var filterPerfil = "";
            filterPerfil += idUsuario != 0 ? " and up.id_user = " + idUsuario.ToString() : "";

            var sqlPerfil = sqlPerfilUsuario + filterPerfil;

            int perfilRede = _context.ContextDapper.Query<int>(sqlPerfil).FirstOrDefault();

            #endregion "Verifica se o usuário é perfil Rede"

            var sqlLoja = @"select loja.*
                          from lojas loja with(nolock) ";

            var condicoes = "where 1 = 1 and visivel = 1 ";

            var orderby = " order by loja.filial";


            var joinUsuarioLojas = "";
            // Se o perfil for de rede não faz o inner join com o perfil lojas
            if (perfilRede == 0)
                joinUsuarioLojas = "inner join users_perfil_lojas uspl with(nolock) on uspl.id_loja = loja.id ";

            //Filtros dinamicos
            var filter = "";

            // Se o perfil for de rede não faz o inner join com o perfil lojas
            if (perfilRede == 0)
                filter += idUsuario != 0 ? " and uspl.id_user = " + idUsuario.ToString() : "";

            filter += string.IsNullOrEmpty(idLoja) ? "" : " and loja.id = " + idLoja;

            var query = sqlLoja + joinUsuarioLojas + condicoes + filter + orderby;

            return _context.ContextDapper.Query<Loja>(query).ToList();
        }

        public IList<Loja> ListarLojas(string idLoja)
        {
            var sqlLoja = @"select loja.*, ci.codigo_clima_inpe codigo_clima
                          from lojas loja with(nolock)

                      LEFT JOIN CIDADE_COD_CLIMA_INPE ci with(nolock)
                 ON ci.cidade = loja.cidade COLLATE DATABASE_DEFAULT   ";

            var condicoes = "where 1 = 1 #idLoja#";

            var orderby = " order by loja.filial";

            var query = sqlLoja + condicoes.Replace("#idLoja#", (idLoja == "" ? "" : (" and loja.id = " + idLoja))) + orderby;

            var lojas = _context.ContextDapper.Query<Loja>(query).ToList();

            foreach (var item in lojas)
            {
                // retorna os supervisores atrelados
                var sqlSuper = @"select 
                                     usr.*, 0 as Rede, per.id_perfil, '' as Perfil
                                  from users_perfil_lojas upl with(nolock)
                             inner join users usr with(nolock) on usr.id = upl.id_user
                             inner join users_perfil per with(nolock) on per.id_user = usr.id
                                 where upl.id_loja = @IdLoja
                                   and per.id_perfil = 3";

                var _supervisores = _context.ContextDapper.Query<Usuario>(sqlSuper, new { IdLoja = item.id }).ToList();

                item.Supervisores = _supervisores;

                // retorna os gerentes atrelados
                var sqlGerentes = @"select 
                                     usr.*, 0 as Rede, per.id_perfil, '' as Perfil
                                  from users_perfil_lojas upl with(nolock)
                             inner join users usr with(nolock) on usr.id = upl.id_user
                             inner join users_perfil per with(nolock) on per.id_user = usr.id
                                 where upl.id_loja = @IdLoja
                                   and per.id_perfil = 5";

                var _gerentes = _context.ContextDapper.Query<Usuario>(sqlGerentes, new { IdLoja = item.id }).ToList();

                item.Gerentes = _gerentes;
            }

            return lojas;
        }

        public IList<Loja> ListarLojas()
        {
            var sqlLoja = @"select loja.*, ci.codigo_clima_inpe codigo_clima
                          from lojas loja with(nolock)

                      LEFT JOIN CIDADE_COD_CLIMA_INPE ci with(nolock)
                 ON ci.cidade = loja.cidade COLLATE DATABASE_DEFAULT   ";

            var condicoes = "where 1 = 1 and visivel = 1 ";

            var orderby = " order by loja.filial";

            var query = sqlLoja + condicoes + orderby;

            var lojas = _context.ContextDapper.Query<Loja>(query).ToList();

            return lojas;
        }

        public IList<Loja> ListarLojasFormatados()
        {
              var sqlLoja = @"select 
                  loja.id
                , loja.id_loja
                , case when lofo.id_loja is not null then lofo.filial_formatado else loja.filial end filial
                , loja.origem
                , loja.supervisor
                , loja.pais
                , loja.regiao
                , loja.uf
                , loja.cidade
                , loja.visivel
                , loja.created_at
                , loja.updated_at
                , loja.tipo_loja
                , loja.email
                , loja.inativar_now_antigo
                , loja.cep
                , loja.endereco
                , loja.bairro
                , loja.numero
                from lojas loja with(nolock)
                left join lojas_formatado lofo on lofo.id_loja = loja.id_loja    ";

            var condicoes = "where 1 = 1 and visivel = 1 ";

            var orderby = " order by loja.filial";

            var query = sqlLoja + condicoes + orderby;

            var lojas = _context.ContextDapper.Query<Loja>(query).ToList();

            return lojas;
        }

        public IList<Loja> ListarTodasLojas()
        {
            var sqlLoja = @"select loja.*, ci.codigo_clima_inpe codigo_clima
                          from lojas loja with(nolock)

                      LEFT JOIN CIDADE_COD_CLIMA_INPE ci with(nolock)
                 ON ci.cidade = loja.cidade COLLATE DATABASE_DEFAULT  ";

            var orderby = " order by loja.visivel, loja.filial";

            var query = sqlLoja + orderby;

            var lojas = _context.ContextDapper.Query<Loja>(query).ToList();

            return lojas;
        }

        public void UpdateLoja(Loja loja)
        {
            var query = @"  update lojas set 
                                      id_loja			= @id_loja
                                    , filial			= @filial
                                    , origem			= @origem
                                    , supervisor		= @supervisor
                                    , pais				= @pais
                                    , regiao			= @regiao
                                    , uf				= @uf	
                                    , cidade			= @cidade
                                    , visivel			= @visivel                                    
                                    , updated_at		= @updated_at
                                    , tipo_loja			= @tipo_loja
                                    , email				= @email
                            where id = @id
                        ";

            DynamicParameters parametros = new DynamicParameters();

            parametros.Add("id", loja.id);
            parametros.Add("id_loja", loja.id_loja);
            parametros.Add("filial", loja.filial);
            parametros.Add("origem", loja.origem);
            parametros.Add("supervisor", loja.supervisor);
            parametros.Add("pais", loja.pais);
            parametros.Add("regiao", loja.regiao);
            parametros.Add("uf", loja.uf);
            parametros.Add("cidade", loja.cidade);
            parametros.Add("visivel", loja.visivel);            
            parametros.Add("updated_at", DateTime.Now);
            parametros.Add("tipo_loja", loja.tipo_loja);
            parametros.Add("email", loja.email);

            _context.ContextDapper.Execute(query, parametros);
        }

        #region "Manutenção"

        public IList<LojaDistribuicaoPerfil> ListarLojasDistribuicao()
        {
            const string sql = @"
                        select 
			                    loja.id 
		                      , loja.filial 
		                      , uger.idGerente
		                      , usur.idRegional
		                      , usub.idBrasil	
							  , isnull(qsug.Qtde, 0) QtdeGerente
							  , isnull(qsur.Qtde, 0) QtdeRegional
							  , isnull(qsub.Qtde, 0) QtdeBrasil
							  	  
                          from lojas loja with(nolock)
                     left join (
			                     select 
				                        upl.id_loja 
				                      , uger.id as idGerente
			                       from users uger with(nolock)
	                         inner join users_perfil perfil with(nolock) on perfil.id_user = uger.id
		                     inner join users_perfil_lojas upl with(nolock) on upl.id_user = uger.id
		                          where id_perfil = 5 -- Gerente
		                        )uger on uger.id_loja = loja.id

                    left join (
			                     select 
				                        upl.id_loja 
				                      , uger.id as idRegional				                      
			                       from users uger with(nolock)
	                         inner join users_perfil perfil with(nolock) on perfil.id_user = uger.id
		                     inner join users_perfil_lojas upl with(nolock) on upl.id_user = uger.id
		                          where id_perfil = 3 -- Supervisor Regional
		                        )usur on usur.id_loja = loja.id

                    left join (
			                     select 
				                        upl.id_loja 
				                      , uger.id as idBrasil				                      
			                       from users uger with(nolock)
	                         inner join users_perfil perfil with(nolock) on perfil.id_user = uger.id
		                     inner join users_perfil_lojas upl with(nolock) on upl.id_user = uger.id
		                          where id_perfil = 2 -- Supervisor Brasil
		                        )usub on usub.id_loja = loja.id

					-- Contador Supervisor Regional
					left join (
			                     select 
				                        upl.id_loja 
				                      , count(uger.id) as Qtde		                      
			                       from users uger  with(nolock)
	                         inner join users_perfil perfil with(nolock) on perfil.id_user = uger.id
		                     inner join users_perfil_lojas upl with(nolock) on upl.id_user = uger.id
		                          where id_perfil = 3 -- Supervisor Regional
								  group by upl.id_loja 						            
		                        )qsur on qsur.id_loja = loja.id

					-- Contador Supervisor Brasil
					left join (
			                     select 
				                        upl.id_loja 
				                      , count(uger.id) as Qtde			                      
			                       from users uger with(nolock)
	                         inner join users_perfil perfil with(nolock) on perfil.id_user = uger.id
		                     inner join users_perfil_lojas upl with(nolock) on upl.id_user = uger.id
		                          where id_perfil = 2 --  Supervisor Brasil
								  group by upl.id_loja 						            
		                        )qsub on qsub.id_loja = loja.id

					-- Contador Gerente
					left join (
			                     select 
				                        upl.id_loja 
				                      , count(uger.id) as Qtde		                      
			                       from users uger with(nolock)
	                         inner join users_perfil perfil with(nolock) on perfil.id_user = uger.id
		                     inner join users_perfil_lojas upl with(nolock) on upl.id_user = uger.id
		                          where id_perfil = 5 --  Gerente
								  group by upl.id_loja 						            
		                        )qsug on qsug.id_loja = loja.id


                         where loja.visivel = 1
                      order by loja.filial
                  
            ";

            //Colocar quando for migrado todas as senhas
            //AND u.password = @Password
            var result = _context.ContextDapper.Query<LojaDistribuicaoPerfil>(sql).ToList();

            return result;
        }

        public IList<DistribuicaoSupervisaoOrg> ListarSupervisaoDistribuicao()
        {
            const string sql = @"
                                                select 
			                    loja.filial 
						      , usub.supBrasil
						      , usur.supRegional
		                      , uger.gerente
		                      
		                      
							  --, isnull(qsug.Qtde, 0) QtdeGerente
							  --, isnull(qsur.Qtde, 0) QtdeRegional
							  --, isnull(qsub.Qtde, 0) QtdeBrasil
							  	  
                          from (
			                     select 
				                        upl.id_loja 
				                      , uger.username supBrasil				                      
			                       from users uger with(nolock)
	                         inner join users_perfil perfil with(nolock) on perfil.id_user = uger.id
		                     inner join users_perfil_lojas upl with(nolock) on upl.id_user = uger.id
		                          where id_perfil = 2 -- Supervisor Brasil
		                        )usub              

                          join (
			                     select 
				                        upl.id_loja 
				                      , uger.username gerente
			                       from users uger with(nolock)
	                         inner join users_perfil perfil with(nolock) on perfil.id_user = uger.id
		                     inner join users_perfil_lojas upl with(nolock) on upl.id_user = uger.id
		                          where id_perfil = 5 -- Gerente
		                        )uger on uger.id_loja = usub.id_loja

                          join (
			                     select 
				                        upl.id_loja 
				                      , uger.username supRegional				                      
			                       from users uger with(nolock)
	                         inner join users_perfil perfil with(nolock) on perfil.id_user = uger.id
		                     inner join users_perfil_lojas upl with(nolock) on upl.id_user = uger.id
		                          where id_perfil = 3 -- Supervisor Regional
		                        )usur on usur.id_loja = usub.id_loja     
						   join lojas loja on loja.id  = uger.id_loja     
				order by supBrasil  
                  
            ";

            //Colocar quando for migrado todas as senhas
            //AND u.password = @Password
            var result = _context.ContextDapper.Query<DistribuicaoSupervisaoOrg>(sql).ToList();

            return result;
        }

        // Update Loja Supervisão e Gerencia

        public void UpdatePerfilSupervisao(string idUsuario, string idLoja, string idPerfil)
        {
            var queryDelete = @"  
                                        delete
                                from users_perfil_lojas
                                where id_user in (select distinct

                                                    upl.id_user
                                                from users_perfil perfil

                                            inner join users_perfil_lojas upl on upl.id_user = perfil.id_user

                                                where id_perfil = @idPerfil
				                                )
                                and id_loja = @loja_id ";

            DynamicParameters parDelete = new DynamicParameters();
            parDelete.Add("loja_id", idLoja);
            parDelete.Add("idPerfil", idPerfil);

            _context.ContextDapper.Execute(queryDelete, parDelete);

            var queryInsersao = @" 
                         --- Inserir um Novo Supervisor
                         insert into users_perfil_lojas 
                              values (@id_user, @id_loja, @deleted_at, @created_at, @updated_at)
                        ";

            DynamicParameters parametros = new DynamicParameters();

            parametros.Add("id_user", idUsuario);
            parametros.Add("id_loja", idLoja);
            parametros.Add("deleted_at", null);
            parametros.Add("created_at", DateTime.Now);
            parametros.Add("updated_at", DateTime.Now);

            _context.ContextDapper.Execute(queryInsersao, parametros);
        }


        //

        public IList<LojaDistribuicaoOrigemDestino> ListarLojaDistribuicaoOrigemDestino()
        {
            const string sql = @"
                        select 
	                           id
	                         , origem_loja_id
	                         , destino_loja_id
	                         , total_distribuicao
	                         , data_inclusao
	                         , data_alteracao
	                         , data_exclusao
                          from LOJA_DISTRIBUICAO_ORIGEM_DESTINO
                         where data_exclusao is null
                      order by data_alteracao desc
                  
            ";

            //Colocar quando for migrado todas as senhas
            //AND u.password = @Password
            var result = _context.ContextDapper.Query<LojaDistribuicaoOrigemDestino>(sql).ToList();

            return result;
        }

        public IList<LojaDistribuicaoOrigemDestino> SalvarLojaDistribuicaoOrigemDestino(LojaDistribuicaoOrigemDestino obj)
        {
            var query = @" ";
            DynamicParameters parametros = new DynamicParameters();

            if (obj.id == 0)
            {
                query = @"  insert into LOJA_DISTRIBUICAO_ORIGEM_DESTINO
                                 values(@origem_loja_id, @destino_loja_id, @total_distribuicao, @data_inclusao, @data_alteracao, @data_exclusao)";

                parametros.Add("data_inclusao", obj.data_inclusao);
            }
            else
            {
                query = @"  update LOJA_DISTRIBUICAO_ORIGEM_DESTINO
                               set origem_loja_id           = @origem_loja_id
                                 , destino_loja_id          = @destino_loja_id
                                 , total_distribuicao       = @total_distribuicao
                                 , data_alteracao           = @data_alteracao
                                 , data_exclusao            = @data_exclusao
                              where id = @id
                                ";

                parametros.Add("id", obj.id);
            }

            parametros.Add("origem_loja_id", obj.origem_loja_id);
            parametros.Add("destino_loja_id", obj.destino_loja_id);
            parametros.Add("total_distribuicao", obj.total_distribuicao);            
            parametros.Add("data_alteracao", obj.data_alteracao);
            parametros.Add("data_exclusao", obj.data_exclusao);            

            _context.ContextDapper.Execute(query, parametros);

            return ListarLojaDistribuicaoOrigemDestino();
        }

        #endregion
    }
}
