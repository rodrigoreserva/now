﻿using Now.Domain.Contracts.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using Now.Domain;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Migrations;
using System.Linq.Expressions;

namespace Now.Infrastructure.Repositories
{
    public class UsuarioRepository : IUsuarioRepository
    {
        private NowContext _context;

        public UsuarioRepository(NowContext context)
        {
            _context = context;
        }

        public Usuario Get(string email, string id, string vendedorId)
        {
            string sql = @"
                    SELECT u.*, CONVERT(bit, p.rede) as Rede, p.id_perfil, p.nome as Perfil, v.img_perfil as img_perfil_vendedor, loja_dns.dns
                    FROM Users u with(nolock)
                    INNER JOIN 
	                    users_perfil up  with(nolock)
                    on u.id = up.id_user
                    INNER JOIN 
	                    perfil p  with(nolock)                    
                    ON p.id_perfil = up.id_perfil
                    LEFT JOIN
                        vendedores v with(nolock)
                            on v.id = u.vendedor_id

                    left join 
					    lojas_dns loja_dns
						  on  loja_dns.loja_id = u.loja_id

                    WHERE 1 = 1 
                        @Email 
                        @Id
                        @VendedorId
            ";

            //Colocar quando for migrado todas as senhas
            //AND u.password = @Password

            sql = string.IsNullOrEmpty(email) ? sql.Replace("@Email", "") : sql.Replace("@Email", " and u.email='" + email + "'");
            sql = string.IsNullOrEmpty(id) ? sql.Replace("@Id", "") : sql.Replace("@Id", " and u.id=" + id + "");
            sql = string.IsNullOrEmpty(vendedorId) ? sql.Replace("@VendedorId", "") : sql.Replace("@VendedorId", " and u.vendedor_id =" + vendedorId + "");

            var result = _context.ContextDapper.Query<Usuario>(sql, new { Email = email });

            //Caso não exista o usuário ele gera uma exception, pra ser tratada na camada acima
            if (result == null || !result.Any()) return new Usuario();// throw new Exception("Usuário ou senha incorretos.");

            var usuario = result.FirstOrDefault();

            //Retornando as lojas
            usuario.Lojas = _context.ContextDapper.Query<Loja>(@"
              SELECT l.*, ci.codigo_clima_inpe codigo_clima
                FROM 
	                  users_perfil_lojas pl with(nolock)
          INNER JOIN 
	                  lojas l with(nolock)
                  ON pl.id_loja = l.id
          
          INNER JOIN users u with(nolock)
                  ON u.id = pl.id_user

          LEFT JOIN vendedores v with(nolock)
                 ON v.id = u.vendedor_id   

          LEFT JOIN CIDADE_COD_CLIMA_INPE ci with(nolock)
                 ON ci.cidade = l.cidade COLLATE DATABASE_DEFAULT   
            
              WHERE pl.id_user = @IdUser
            ", new { IdUser = usuario.id }).ToList();

            return usuario;
        }

        public IList<UsuarioPerfil> ListarUsuarios(string IdsLojas)
        {
            var sql = @"
                    SELECT u.id
                         , vend.id as vendedor_id
                         , l.id loja_id
                         , l.filial
					     , vend.nome
					     , p.nome as Perfil
						 , CONVERT(bit, p.rede) as Rede
					     , u.email as login
						 , case when u.email is not null and u.deleted_at is null then '<span class=''label label-success''>Ativo</span>'
						        when u.email is not null and u.recriar_senha = 1 then '<span class=''label label-warning''>Recriar Senha</span>'
								when u.email is not null and u.acesso_bloqueado = 1 then '<span class=''label label-danger''>Acesso Bloqueado</span>'
								when u.email is null then '<span class=''label label-default''>Não Existe</span>'
						        else '<span class=''label label-default''>Não Existe</span>' end status_login 
                        , case when u.email is not null and u.deleted_at is null then 0
						        when u.email is not null and u.recriar_senha = 1 then 1
								when u.email is not null and u.acesso_bloqueado = 1 then 2
								when u.email is null then 3
						        else 3 end usuario_ordem  
                    FROM vendedores vend
					    join lojas l with(nolock) 
				          on vend.loja_id = l.id
				   
				   left join Users u with(nolock) 
				          on u.vendedor_id = vend.id
                   left join users_perfil up  with(nolock)                    
						  on u.id = up.id_user
                   left join perfil p  with(nolock)                    
						  on p.id_perfil = up.id_perfil
                    WHERE data_desativacao is null
                      #LOJA#                  
            ";

            if (String.IsNullOrEmpty(IdsLojas))
            {
                sql = sql.Replace("#LOJA#", "");
            }
            else
            {
                sql = sql.Replace("#LOJA#", " AND vend.loja_id in (" + IdsLojas + ")");
            }

            //Colocar quando for migrado todas as senhas
            //AND u.password = @Password
            var result = _context.ContextDapper.Query<UsuarioPerfil>(sql).ToList();

            foreach (var item in result)
            {
                //Retornando as lojas
                item.Lojas = _context.ContextDapper.Query<Loja>(@"
              SELECT l.*
                FROM 
	                  users_perfil_lojas pl with(nolock)
          INNER JOIN 
	                  lojas l with(nolock)
                  ON pl.id_loja = l.id
          
          INNER JOIN users u with(nolock)
                  ON u.id = pl.id_user

          LEFT JOIN vendedores v with(nolock)
                 ON v.id = u.vendedor_id    
            
              WHERE pl.id_user = @IdUser
            ", new { IdUser = item.id }).ToList();
            }


            return result;
        }

        public void SalvarUsuario(Usuario usuario)
        {
            var query = @"  insert into users
                                 values(@username,@email,@password,@remember_token,@deleted_at,@created_at,@updated_at,@loja_id,@vendedor_id,@recriar_senha,@acesso_bloqueado,@img_perfil)";

            DynamicParameters parametros = new DynamicParameters();

            parametros.Add("username", usuario.username);
            parametros.Add("email", usuario.email);
            parametros.Add("password", usuario.password);
            parametros.Add("remember_token", usuario.remember_token);
            parametros.Add("deleted_at", null);
            parametros.Add("created_at", DateTime.Now);
            parametros.Add("updated_at", DateTime.Now);
            parametros.Add("loja_id", usuario.loja_id);
            parametros.Add("vendedor_id", usuario.vendedor_id);
            parametros.Add("recriar_senha", usuario.recriar_senha);
            parametros.Add("acesso_bloqueado", usuario.acesso_bloqueado);
            parametros.Add("img_perfil", usuario.img_perfil);

            _context.ContextDapper.Execute(query, parametros);
        }


        public void UpdateUsuario(Usuario usuario, bool alterarToken)
        {
            var _usuario = Get("", "", usuario.vendedor_id.ToString());

            DynamicParameters parametros = new DynamicParameters();
            var query = "";
            if (_usuario.id != 0)
            {
                query = @"  update users set
                               username                  = @username
                             , email                     = @email
                             , password                  = @password
                             , remember_token            = @remember_token
                             , deleted_at                = @deleted_at                             
                             , updated_at                = @updated_at
                             , loja_id                   = @loja_id
                             , vendedor_id               = @vendedor_id
                             , recriar_senha             = @recriar_senha
                             , acesso_bloqueado          = @acesso_bloqueado
                             , senha_agenda_distribuicao = @senha_agenda_distribuicao
                             , img_perfil                = @img_perfil
                           where id = @id
                        ";
            } else
            {
                //SalvarUsuario(usuario);

                //_usuario = Get("", "", usuario.vendedor_id.ToString());                
            }

            parametros.Add("id", usuario.id);
            parametros.Add("username", usuario.username);
            parametros.Add("email", usuario.email);
            parametros.Add("password", usuario.password);
            parametros.Add("remember_token", usuario.remember_token);
            parametros.Add("senha_agenda_distribuicao", usuario.senha_agenda_distribuicao);            
            parametros.Add("deleted_at", null);            
            parametros.Add("updated_at", DateTime.Now);
            parametros.Add("loja_id", usuario.loja_id);
            parametros.Add("vendedor_id", usuario.vendedor_id);
            parametros.Add("recriar_senha", usuario.recriar_senha);
            parametros.Add("acesso_bloqueado", usuario.acesso_bloqueado);
            parametros.Add("img_perfil", usuario.img_perfil);

           _context.ContextDapper.Execute(query, parametros);
        }

        public void SalvarImgPerfil(string src, string idUsuario)
        {
            const string sql = @"
                    UPDATE users 
                    SET img_perfil = @Src
                    WHERE id = @IdUsuario
                ";
            _context.ContextDapper.Execute(sql, new { IdUsuario = idUsuario, Src = src });
        }


        public IList<Usuario> ListarUsuariosPorPerfil(string IdPerfil)
        {
            const string sql = @"
                                 select 
				                    uger.*
			                   from users uger with(nolock)
	                     inner join users_perfil perfil with(nolock) on perfil.id_user = uger.id		 
		                      where id_perfil = @perfil 
            ";

            return _context.ContextDapper.Query<Usuario>(sql, new { perfil = IdPerfil }).ToList();
        }


        public void SalvaConfiguracoes(VendedoresConfiguracao configuracao)
        {
            var _confVendedor = BuscarConfiguracao(configuracao.loja_id.ToString(), configuracao.vendedor_id.ToString());


            DynamicParameters parametros = new DynamicParameters();
            var query = "";

            if (_confVendedor != null)
            {
                query = @"  update VENDEDORES_CONFIGURACAO 
                               set
                               loja_id                          = @loja_id
                             , vendedor_id                      = @vendedor_id
                             , flg_gerente                      = @flg_gerente
                             , flg_vendedor                     = @flg_vendedor
                             , flg_operador_caixa               = @flg_operador_caixa                             
                             , flg_bloqueado_now                = @flg_bloqueado_now
                             , data_alteracao                   = @data_alteracao                                                     
                           where loja_id = @loja_id and vendedor_id = @vendedor_id
                        ";
            }
            else
            {
                query = @"  insert into VENDEDORES_CONFIGURACAO 
                                values (@loja_id, @vendedor_id,@flg_gerente,@flg_vendedor,@flg_operador_caixa,@flg_bloqueado_now, @data_inclusao,@data_alteracao,@data_desativacao)                                                     
                        ";

                parametros.Add("data_inclusao", DateTime.Now);
                parametros.Add("data_desativacao", null);
            }

            parametros.Add("data_alteracao", DateTime.Now);
            parametros.Add("loja_id", configuracao.loja_id);
            parametros.Add("vendedor_id", configuracao.vendedor_id);
            parametros.Add("flg_gerente", configuracao.flg_gerente);
            parametros.Add("flg_vendedor", configuracao.flg_vendedor);
            parametros.Add("flg_operador_caixa", configuracao.flg_operador_caixa);
            parametros.Add("flg_bloqueado_now", configuracao.flg_bloqueado_now);       

            _context.ContextDapper.Execute(query, parametros);
        }


        public VendedoresConfiguracao BuscarConfiguracao(string lojaId, string vendedorId)
        {
            string sql = @"
                        select * 
                      from VENDEDORES_CONFIGURACAO
                     where loja_id = @lojaId
                       and vendedor_id = @vendedorId
            ";

            DynamicParameters parametros = new DynamicParameters();
            parametros.Add("lojaId", lojaId);
            parametros.Add("vendedorId", vendedorId);

            var result = _context.ContextDapper.Query<VendedoresConfiguracao>(sql, parametros);

            if (result != null)
                return result.ToList().FirstOrDefault();
            else return new VendedoresConfiguracao();
        }

        #region "Perfil"

        public IList<Perfil> ListarPerfis()
        {
            const string sql = @"
                    SELECT *
                    FROM Perfil with(nolock)
            ";

            return _context.ContextDapper.Query<Perfil>(sql).ToList();
        }

        #endregion "Perfil"

        #region "Supervisores"

        public IList<SupervisorLojas> ListarSupervisores()
        {
            string sql = @"
                        select 
			                    loja.id as id_loja 
		                      , loja.filial 		  
		                      , usur.idRegional as id_supervisor
		                      , usur.nome_supervisor
                          from lojas loja
                    inner join (
			                     select 
				                        upl.id_loja 
				                      , uger.id as idRegional
				                      , uger.email
				                      , uger.username as nome_supervisor
			                       from users uger 
	                         inner join users_perfil perfil on perfil.id_user = uger.id
		                     inner join users_perfil_lojas upl on upl.id_user = uger.id
		                          where id_perfil = 3 
		                        )usur on usur.id_loja = loja.id
                         where loja.visivel = 1
                      order by usur.nome_supervisor, loja.filial 
            ";

            return _context.ContextDapper.Query<SupervisorLojas>(sql).ToList();
        }

        #endregion "Supervisores"

        #region "Log"

        public void AdicionarErro(string metodo, string tela, string erro, int? vendedorId, int? clienteId, int? lojaId, int? userId)
        {
            var query = @"  insert into log_erro
                                 values(@metodo,@tela,@erro,@vendedor_id,@cliente_id,@loja_id,@user_id,@data) ";

            DynamicParameters parametros = new DynamicParameters();

            parametros.Add("metodo", metodo);
            parametros.Add("tela", tela);
            parametros.Add("erro", erro);
            parametros.Add("vendedor_id", vendedorId);
            parametros.Add("cliente_id", clienteId);
            parametros.Add("loja_id", lojaId);
            parametros.Add("user_id", userId);
            parametros.Add("data", DateTime.Now);

            _context.ContextDapper.Execute(query, parametros);
        }

        public void AdicionarLogAcesso(string browser, string login, string senha, string ip, int? acesso = 0)
        {
            var query = @"  insert into log_acesso
                                 values(@browser,@ip,@login,@senha,@data,@acesso) ";

            DynamicParameters parametros = new DynamicParameters();

            parametros.Add("browser", browser);
            parametros.Add("ip", ip);
            parametros.Add("login", login);
            parametros.Add("senha", senha);
            parametros.Add("data", DateTime.Now);
            parametros.Add("acesso", acesso);

            _context.ContextDapper.Execute(query, parametros);
        }

        public void AdicionarLogSistema(LogSistema logSistema)
        {
            var query = @"  insert into log_sistema_now
                                 values(@metodo,@tela,@login,@ip,@tipo_log, @processo_minutos,@acao,@erro,@vendedor_id,@cliente_id,@loja_id,@user_id,@data) ";            

            DynamicParameters parametros = new DynamicParameters();

            parametros.Add("metodo", logSistema.metodo);
            parametros.Add("tela", logSistema.tela);
            parametros.Add("login", logSistema.login);
            parametros.Add("ip", logSistema.ip);
            parametros.Add("tipo_log", logSistema.tipo_log);
            parametros.Add("acao", logSistema.acao);
            parametros.Add("erro", logSistema.erro);
            parametros.Add("vendedor_id", logSistema.vendedor_id);
            parametros.Add("cliente_id", logSistema.cliente_id);
            parametros.Add("loja_id", logSistema.loja_id);
            parametros.Add("user_id", logSistema.user_id);
            parametros.Add("data", logSistema.data);
            parametros.Add("processo_minutos", logSistema.processo_minutos);
            

            _context.ContextDapper.Execute(query, parametros);
        }

        public Usuario UsuarioPor(string cpf)
        {
            throw new NotImplementedException();
        }

        public Usuario UsuarioPorToken(string token)
        {
            string sql = @"
                    SELECT u.*, CONVERT(bit, p.rede) as Rede, p.id_perfil, p.nome as Perfil, v.img_perfil as img_perfil_vendedor, loja_dns.dns
                    FROM Users u with(nolock)
                    INNER JOIN 
	                    users_perfil up  with(nolock)
                    on u.id = up.id_user
                    INNER JOIN 
	                    perfil p  with(nolock)                    
                    ON p.id_perfil = up.id_perfil
                    LEFT JOIN
                        vendedores v with(nolock)
                            on v.id = u.vendedor_id

                    left join 
					    lojas_dns loja_dns
						  on  loja_dns.loja_id = u.loja_id

                    WHERE 1 = 1 
                        @Token
            ";

            //Colocar quando for migrado todas as senhas
            //AND u.password = @Password

            sql = string.IsNullOrEmpty(token) ? sql.Replace("@Token", "") : sql.Replace("@Token", " and u.remember_token='" + token + "'");

            var result = _context.ContextDapper.Query<Usuario>(sql);

            //Caso não exista o usuário ele gera uma exception, pra ser tratada na camada acima
            if (result == null || !result.Any()) return new Usuario();// throw new Exception("Usuário ou senha incorretos.");

            var usuario = result.FirstOrDefault();

            //Retornando as lojas
            usuario.Lojas = _context.ContextDapper.Query<Loja>(@"
              SELECT l.*, ci.codigo_clima_inpe codigo_clima
                FROM 
	                  users_perfil_lojas pl with(nolock)
          INNER JOIN 
	                  lojas l with(nolock)
                  ON pl.id_loja = l.id
          
          INNER JOIN users u with(nolock)
                  ON u.id = pl.id_user

          LEFT JOIN vendedores v with(nolock)
                 ON v.id = u.vendedor_id   

          LEFT JOIN CIDADE_COD_CLIMA_INPE ci with(nolock)
                 ON ci.cidade = l.cidade COLLATE DATABASE_DEFAULT   
            
              WHERE pl.id_user = @IdUser
            ", new { IdUser = usuario.id }).ToList();

            return usuario;
        }

        #endregion

    }
}
