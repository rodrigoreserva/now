﻿using Now.Domain.Contracts.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using Now.Domain;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Linq.Expressions;

namespace Now.Infrastructure.Repositories
{
    public class AcessoExternoRepository : IAcessoExternoRepository
    {
        private NowContext _context;

        public AcessoExternoRepository(NowContext context)
        {
            _context = context;
        }

        public void SalvarVisitaLojas(VisitaLojas visitalojas)
        {
            var query = @"  insert into visita_lojas
                                 values(@data_visita,@loja_id,@observacao)";

            DynamicParameters parametros = new DynamicParameters();

            parametros.Add("data_visita", visitalojas.data_visita);
            parametros.Add("loja_id", visitalojas.loja_id);
            parametros.Add("observacao", visitalojas.observacao);

            _context.ContextDapper.Execute(query, parametros);
        }

        public void DeleteVisitaLojas(int id)
        {
            var query = @"     delete from visita_lojas 
                                    where id = @Id ";

            _context.ContextDapper.Execute(query, new { id = id });
        }

        public IList<VisitaLojas> ListarVisitaLojas(string idLoja)
        {

            var query = @"     select v.*
                                             , l.filial
                                          from visita_lojas v with(nolock)
                                    inner join lojas l on l.id = v.loja_id ";

            var result = _context.ContextDapper.Query<VisitaLojas>(query);

            return result.ToList();
        }

        /// <summary>
        /// Retorna InfoVendas
        /// </summary>
        /// <param name="idcliente"></param>
        /// <returns></returns>
        public VisitaLojas BuscarVisitaLojas(int id)
        {
            const string sql = @"
                SELECT *
                FROM visita_lojas with(nolock)
                WHERE id = @Id                
            ";

            var result = _context.ContextDapper.Query<VisitaLojas>(sql, new { id = id });

            if (result.ToList().Count() > 0)
                return result.ToList().FirstOrDefault();
            else
                return null;
        }

        public VisitaLojas BuscarLojasPorDataVisita(string data)
        {
            const string sql = @"
                SELECT *
                FROM visita_lojas with(nolock)
                WHERE data_visita = @data                
            ";

            var result = _context.ContextDapper.Query<VisitaLojas>(sql, new { data = data });

            if (result.ToList().Count() > 0)
                return result.ToList().FirstOrDefault();
            else
                return null;
        }


        #region "Abandono de Carrinho"

        public IList<AbandonoCarrinhoClienteProduto> BuscarAbandonoCarrinhoClienteProduto(string cpf)
        {
            const string sql = @"
                                   select top 10
                                       cpf
	                                  , cliente_nome	  
	                                  , produto_id
	                                  , produto
	                                  , descricao_produto
	                                  , cor_produto
	                                  , grade
	                                  , tamanho 
                                      , sku
	                                  , data_abandono
									  , preco									  
                                   from [CLIENTE_CARRINHO_ABANDONADO] car
                                  where cpf = @cpf
	                                and exists (  select disponivel
                                       from [10.1.1.2].[Reserva].[dbo].W_RSV_ESTOQUE_DISPONIVEL_VERTICAL ESTV  
                                       join [10.1.1.2].[Reserva].[dbo].produtos_barra prba on (prba.produto = ESTV.produto and prba.cor_produto = ESTV.cor_produto and prba.tamanho = ESTV.tamanho )
                                  left join [10.1.1.2].[Reserva].[dbo].[W_RSV_PRODUTOS_PRECOS_COM_VARIACAO] vpre on (vpre.PRODUTO = prba.produto and vpre.COR_PRODUTO = prba.COR_PRODUTO and vpre.CODIGO_TAB_PRECO ='04')
                                      where ESTV.produto = car.produto
                                        and disponivel > 0
                                        and ESTV.filial like 'tscr01%'	   )
                               order by [data_abandono] desc   
                                     ";

            var result = _context.ContextDapper.Query<AbandonoCarrinhoClienteProduto>(sql, new { cpf = cpf });

            IList<AbandonoCarrinhoClienteProduto> list = new List<AbandonoCarrinhoClienteProduto>();

            foreach (var item in result.ToList())
            {                
                if (list.Where(x => x.cor_produto == item.cor_produto && x.produto == item.produto).Count() == 0)
                {
                    list.Add(item);
                }

            }

            return list.ToList();
        }

        public IList<AbandonoCarrinhoClienteProduto> BuscarProdutoClienteExperimente(string cpf)
        {
            const string sql = @"
                               
							     select 
                                        clie.cpf
	                                  , clie.nome as cliente_nome	  
	                                  , prod.id produto_id
	                                  , prod.produto
	                                  , prod.descricao_produto
	                                  , prod.cor_produto
	                                  , prod.grade
	                                  , '' tamanho 
                                      , car.sku
	                                  , max(car.data) [data_abandono]
                                  from abandono_carrinho_now_linx car 
						    inner join clientes clie on clie.cpf = car.cpf
							inner join produtos prod on 
                                       (prod.produto = car.produto and prod.cor_produto = car.cor_produto)		
								  where car.cpf = @cpf
								  group by clie.cpf
	                                  , prod.id
	                                  , prod.produto
	                                  , prod.descricao_produto
	                                  , prod.cor_produto
	                                  , prod.grade
                                      , sku
	                                  , clie.nome
	                                              
                                     ";

            var result = _context.ContextDapper.Query<AbandonoCarrinhoClienteProduto>(sql, new { cpf = cpf });           

            return result.ToList();
        }

        public IList<AbandonoCarrinhoClienteProdutoEstoque> BuscarProdutoClienteExperimenteEstoque(string produto)
        {
            const string sql = @"                               
							         select prba.codigo_barra skui
		                                  , ESTV.produto
		                                  , ESTV.cor_produto
		                                  , vpre.preco
	                                      , prba.grade
	                                      , ESTV.disponivel
                                       from [10.1.1.2].[Reserva].[dbo].W_RSV_ESTOQUE_DISPONIVEL_VERTICAL ESTV  
                                       join [10.1.1.2].[Reserva].[dbo].produtos_barra prba on (prba.produto = ESTV.produto and prba.cor_produto = ESTV.cor_produto and prba.tamanho = ESTV.tamanho )
                                  left join [10.1.1.2].[Reserva].[dbo].[W_RSV_PRODUTOS_PRECOS_COM_VARIACAO] vpre on (vpre.PRODUTO = prba.produto and vpre.COR_PRODUTO = prba.COR_PRODUTO and vpre.CODIGO_TAB_PRECO ='04')
                                      where ESTV.produto = @produto
                                        and ESTV.filial like 'tscr01%'	                                              
                                     ";

            var result = _context.ContextDapper.Query<AbandonoCarrinhoClienteProdutoEstoque>(sql, new { produto = produto });

            return result.ToList();
        }



        public void InserirProdutoAbandonoCarrinho(string cpf, string produto, string cor_produto, string codigo_filial, string grade, string sku, DateTime _dataControle, DateTime dataExperimente, string periodo, DateTime dataAbandono)
        {
            const string sqlConsulta = @"
                                select 
                                       *
                                  from abandono_carrinho_now_linx
                                 where cpf = @cpf
                                   and cast(data as date) <> cast(@data as date)
                                     ";
           var result = _context.ContextDapper.Query<AbandonoCarrinhoNowLinx>(sqlConsulta, new { cpf = cpf , data = _dataControle });
            
            if (result.Count() == 0)
            {
                var query = @"  insert into abandono_carrinho_now_linx
                             values(@cpf,@produto,@cor_produto,@codigo_filial,@sku, @data_experimente, @periodo, @data, @data_abandono)";

                DynamicParameters parametros = new DynamicParameters();

                parametros.Add("cpf", cpf);
                parametros.Add("produto", produto);
                parametros.Add("cor_produto", cor_produto);
                parametros.Add("codigo_filial", codigo_filial);
                parametros.Add("sku", sku);
                parametros.Add("data", _dataControle);
                parametros.Add("data_experimente", dataExperimente);
                parametros.Add("periodo", periodo);
                parametros.Add("data_abandono", dataAbandono);

                _context.ContextDapper.Execute(query, parametros);
            }
        }

        public IList<AbandonoCarrinhoNowLinxSolicitacao> BuscarAbandonoCarrinhoNowLinxSolicitacao(string cpf)
        {
            const string sql = @"  
                                select distinct data
										, codigo_filial
										, filial
										, periodo
                                        , data_experimente
										, loja.endereco + ' ' + loja.numero + ', Bairro ' + loja.bairro + ' - ' + loja.uf as endereco
									from abandono_carrinho_now_linx car 
							  inner join lojas loja on loja.id_loja = car.codigo_filial
									where  cpf = @cpf
                                      and data_experimente >= cast(getdate() as date)     
                                     ";

            var result = _context.ContextDapper.Query<AbandonoCarrinhoNowLinxSolicitacao>(sql, new { cpf = cpf });

            if (result.Count() > 0 )
            {
                result.FirstOrDefault().produtos_experimente = BuscarProdutoClienteExperimente(cpf);
            }

            return result.ToList();
        }

        #endregion "Abandono de Carrinho"

        #region "Reservado - Solicitação do Cliente Agendado"

        public IList<ClienteReservadoAgendada> BuscarClienteReservado(string cpf, string lojaid)
        {
            const string sql = @"                               
				                  select
                                        clie.nome cliente_nome
		                              , clie.cpf
				                      , clie.tel_ddd + '-' + clie.tel as telefone
				                      , clra.cpf as cpf_alterado
				                      , clra.telefone as telefone_alterado
                                      , case when (clra.id is not null and clra.data_solicitacao is not null) then 1 else 0 end solicitou
                                    from clientes clie	  
                            left join cliente_reservado_agendado clra on clra.cliente_id = clie.id
	                            where clie.cpf = @cpf
								  and (clie.loja_id = @loja or clie.loja_id_mini = @loja)
                                  and clie.origem_cliente = (select case when origem = 'LOJA' then 'VAREJO' else origem end from lojas where id = @loja)
                    ";

            var result = _context.ContextDapper.Query<ClienteReservadoAgendada>(sql, new { cpf = cpf, loja = lojaid });

            return result.ToList();
        }

        public bool InsereClienteReservadoAgenda(string cpf, string lojaid, string telefone, string tipo, DateTime? datasolicitacao)
        {
            const string sqlBusca = @"                               
				                  select 
                                        clie.nome cliente_nome
		                              , clie.cpf
				                      , clie.tel_ddd + '-' + clie.tel as telefone
				                      , clra.cpf as cpf_alterado
				                      , clra.telefone as telefone_alterado
                                      , case when (clra.id is not null and clra.data_solicitacao is not null) then 1 else 0 end solicitou
                                    from clientes clie	  
                                    join cliente_reservado_agendado clra on clra.cliente_id = clie.id
	                               where clie.cpf = @cpf
								     and (clie.loja_id = @loja or clie.loja_id_mini = @loja)
                                     and clie.origem_cliente = (select case when origem = 'LOJA' then 'VAREJO' else origem end from lojas where id = @loja)
                    ";

            var resultBusca = _context.ContextDapper.Query<ClienteReservadoAgendada>(sqlBusca, new { cpf = cpf, loja = lojaid });

            if (resultBusca.ToList().Count > 0)
            {
                const string sql = @"                               
                                    update cliente_reservado_agendado
                                       set telefone = @telefone
                                         , data_solicitacao = @datasolicitacao
                                     where cpf = @cpf
                                       and loja_id = @lojaid        
                                        ";

                _context.ContextDapper.Query(sql, new { lojaid = lojaid, cpf = cpf, telefone = telefone, datasolicitacao = datasolicitacao });
            }
            else
            { 
                const string sql = @"                               
                                     insert into cliente_reservado_agendado
                                          select 
                                              clie.id cliente_id
		                                     , @lojaid
		                                     , @cpf
		                                     , @telefone
		                                     , @datasolicitacao
		                                     , getdate()
                                         from clientes clie	  
                                    left join cliente_reservado_agendado clra on clra.cliente_id = clie.id
	                                    where clie.cpf = @cpf
	                                      and not exists (select 1 from vendas_detalhe v where v.cliente_id = clie.id and v.data_venda < clra.data_solicitacao and v.DESC_OPERACAO_VENDA like '%RESERVADO%')                                   
        
                                        ";

                _context.ContextDapper.Query(sql, new { lojaid = lojaid, cpf = cpf, telefone = telefone, datasolicitacao = datasolicitacao });
            }

            return true;
        }

        #endregion "Reservado - Solicitação do Cliente Agendado"
    }
}
