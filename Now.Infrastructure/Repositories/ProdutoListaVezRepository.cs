﻿using Now.Domain.Contracts.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using Now.Domain;
using Now.Domain.Contracts.Services;
using Now.Domain.DTO;
using System.Xml.Serialization;
using System.Runtime.Serialization;
using System.Web.Script.Serialization;

namespace Now.Infrastructure.Repositories
{
    public class ProdutoListaVezRepository : IProdutoListaVezRepository
    {
        public IList<ProdutoDTO> ListarProdutos(string descricao,string grupo)
        {
            NowContext context = new NowContext();
            var sqlProduto = @" select top 30 produto_id , p.produto, p.cor_produto, p.descricao_produto + ' ('+p.produto+')' as descricao_produto, p.grade, griffe, 'https://imagens.usereserva.com.br/images/853x1280/' + p.produto + p.cor_produto + '_01.jpg' url
                                from vendas_detalhe vd 
                                left join produtos p on vd.produto_id=p.id
                                where 
                                cast(vd.data_venda as date) >= DATEADD(day,-360,GETDATE())
                                and griffe @grupo
                                and (p.descricao_produto @descricao or p.produto @descricao)
                                group by produto_id,produto,cor_produto,descricao_produto,p.grade,griffe,vd.data_venda
                                order by vd.data_venda desc";

            
            sqlProduto = sqlProduto.Replace("@descricao", "like '%" + descricao + "%'").Replace("@grupo", "like '%" + grupo + "%'");
            

            var result = context.ContextDapper.Query<ProdutoDTO>(sqlProduto);


            return result.ToList();
        }

        public IList<ProdutoDTO> ListarTodosProdutos()
        {
            NowContext context = new NowContext();
            var sqlProduto = @" select top 30 produto_id , p.produto, p.cor_produto, p.descricao_produto + ' ('+p.produto+')' as descricao_produto, p.grade, griffe, 'https://imagens.usereserva.com.br/images/853x1280/' + p.produto + p.cor_produto + '_01.jpg' url
                                from vendas_detalhe vd 
                                left join produtos p on vd.produto_id=p.id
                                where 
                                cast(vd.data_venda as date) >= DATEADD(day,-360,GETDATE())
                                and griffe like '%reserva%'
                                group by produto_id,produto,cor_produto,descricao_produto,p.grade,griffe,vd.data_venda
                                order by vd.data_venda desc";


            var result = context.ContextDapper.Query<ProdutoDTO>(sqlProduto);


            return result.ToList();
        }
    }
}
