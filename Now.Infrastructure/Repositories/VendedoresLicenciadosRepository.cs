﻿using Now.Domain.Contracts.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using Now.Domain;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Linq.Expressions;

namespace Now.Infrastructure.Repositories
{
    public class VendedoresLicenciadosRepository : IVendedoresLicenciadosRepository
    {
        private NowContext _context;

        public VendedoresLicenciadosRepository(NowContext context)
        {
            _context = context;
        }

        public void Salvar(VendedoresLicenciados vendedoresLicenciado)
        {
            var query = @" ";
            DynamicParameters parametros = new DynamicParameters();

            if (vendedoresLicenciado.id == 0)
            {
                query = @"  insert into VENDEDORES_LICENCIADOS
                                 values(@vendedor_id,@loja_id,@data_inicio_bloqueio,@data_fim_bloqueio,@flg_bloqueio_indeterminado,@data_inclusao,@data_alteracao,@data_desativacao,@vendedor_licenciados_tipo_id,@motivo)";
            }
            else {
                query = @"  update VENDEDORES_LICENCIADOS
                               set loja_id                      = @loja_id
                                 , vendedor_id                  = @vendedor_id
                                 , data_inicio_bloqueio         = @data_inicio_bloqueio
                                 , data_fim_bloqueio            = @data_fim_bloqueio                                 
                                 , flg_bloqueio_indeterminado   = @flg_bloqueio_indeterminado
                                 , data_inclusao                = @data_inclusao
                                 , data_alteracao               = @data_alteracao
                                 , data_desativacao             = @data_desativacao
                                 , vendedor_licenciados_tipo_id = @vendedor_licenciados_tipo_id
                                 , motivo                       = @motivo
                              where id = @id
                                ";

                parametros.Add("id", vendedoresLicenciado.id);
            }                        

            parametros.Add("loja_id", vendedoresLicenciado.loja_id);
            parametros.Add("vendedor_id", vendedoresLicenciado.vendedor_id);
            parametros.Add("data_inicio_bloqueio", vendedoresLicenciado.data_inicio_bloqueio);
            parametros.Add("data_fim_bloqueio", vendedoresLicenciado.data_fim_bloqueio);
            parametros.Add("flg_bloqueio_indeterminado", vendedoresLicenciado.flg_bloqueio_indeterminado);
            parametros.Add("data_inclusao", vendedoresLicenciado.data_inclusao);
            parametros.Add("data_alteracao", vendedoresLicenciado.data_alteracao);
            parametros.Add("data_desativacao", vendedoresLicenciado.data_desativacao);
            parametros.Add("vendedor_licenciados_tipo_id", vendedoresLicenciado.vendedor_licenciados_tipo_id);
            parametros.Add("motivo", vendedoresLicenciado.motivo);
            

            _context.ContextDapper.Execute(query, parametros);
        }
        public void Deletar(int id)
        {
            var query = @"  update VENDEDORES_LICENCIADOS
                               set data_desativacao = @data_desativacao
                             where id = @id ";

            DynamicParameters parametros = new DynamicParameters();

            parametros.Add("id", id);
            parametros.Add("data_desativacao", DateTime.Now);

            _context.ContextDapper.Execute(query, parametros);
        }

        public IList<VendedoresLicenciados> Listar(string idLoja, string idVendedor)
        {           

            var sql = @"
                 SELECT veli.*, VETI.nome vendedor_licenciados_tipo
                FROM VENDEDORES_LICENCIADOS VELI  
				join VENDEDORES_LICENCIADOS_TIPO VETI ON VETI.id = veli.vendedor_licenciados_tipo_id
                WHERE 1 = 1 

            ";

            var orderby = " ORDER BY data_inclusao desc";

            var sqlCondicaoPerfil = "";            

            sqlCondicaoPerfil += string.IsNullOrEmpty(idLoja) ? "" : " and loja_id = " + idLoja;

            sqlCondicaoPerfil += string.IsNullOrEmpty(idVendedor) ? "" : " and vendedor_id = " + idVendedor;

            var query = sql + sqlCondicaoPerfil + orderby;

            var result = _context.ContextDapper.Query<VendedoresLicenciados>(query);

            return result.ToList();
        }


        public IList<VendedoresLicenciadosTipo> ListarMotivos()
        {
          
            var sql = @"
                select *
                  from VENDEDORES_LICENCIADOS_TIPO 
                 where data_desativacao is null
            ";                      

            var result = _context.ContextDapper.Query<VendedoresLicenciadosTipo>(sql);

            return result.ToList();
        }

        public VendedoresLicenciados BuscarPorId(int id)
        {
            const string sql = @"
                                  SELECT veli.*, VETI.nome vendedor_licenciados_tipo
                                    FROM VENDEDORES_LICENCIADOS VELI  
                                    join VENDEDORES_LICENCIADOS_TIPO VETI ON VETI.id = veli.vendedor_licenciados_tipo_id
                                    WHERE VELI.id = @id
	                                    and veli.data_desativacao is null  
                                ";

            var result = _context.ContextDapper.Query<VendedoresLicenciados>(sql, new { id = id });

            if (result.ToList().Count() > 0)
                return result.ToList().FirstOrDefault();
            else
                return null;
        }

        public IList<VendedoresAgendaDistribuicao> BuscarAgendaDistribuidas(int vendedorId)
        {
            const string sql = @"
                                  IF OBJECT_ID('tempdb..#vendedores_licenciados') IS NOT NULL
	                               DROP TABLE #vendedores_licenciados
  
                            ---- Vendedores Afastados  
                              select *
                                into #vendedores_licenciados
	                            from VENDEDORES_LICENCIADOS veli
	                            where veli.data_desativacao is null
		                            and ((veli.flg_bloqueio_indeterminado = 0
				                            and veli.data_inicio_bloqueio <= cast(getdate() as date) 
				                            and veli.data_fim_bloqueio >= cast(getdate() as date) 
			                            ) 
			                            or 
				                            veli.flg_bloqueio_indeterminado = 1
			                            )
                                 and veli.vendedor_id = @vendedorId


	                            select upper(meti.titulo) agenda
	                                 , count(menu.cliente_id) total 	  
	                              from agendamento_auxiliar_limitado menu
	                              join menu_tipo_agendamentos meti on meti.id = menu.tipo_agendamento_id
	                             where exists (
					                              select *
						                            from #vendedores_licenciados veli
					                               where menu.loja_id		= veli.loja_id 
						                             and menu.vendedor_id	= veli.vendedor_id
					                            )
                              group by meti.titulo
                                ";

            return _context.ContextDapper.Query<VendedoresAgendaDistribuicao>(sql, new { vendedorId = vendedorId }).ToList();           
        }

        public void DistribuirClientes(int vendedorId)
        {
            var query = @"  [dbo].[SP_DISTRIBUICAO_CLIENTES_VENDEDORES_DESATIVADOS] @vendedorId ";

            DynamicParameters parametros = new DynamicParameters();

            parametros.Add("vendedorId", vendedorId);            

            _context.ContextDapper.Execute(query, parametros);
        }


    }
}
