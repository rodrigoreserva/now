﻿using Now.Domain.Contracts.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using Now.Domain;


namespace Now.Infrastructure.Repositories
{
    public class ProdutoRepository : IProdutoRepository
    {
        private NowContext _context;

        public ProdutoRepository(NowContext context)
        {
            _context = context;
        }

        public Produto ObterProduto(string produto, string cor_produto)
        {
            var sql = @"select distinct 
                               id
                             , produto
                             , cor_produto
                             , descricao_produto
                             , grupo_produto
                          from produtos
                         where produto = @produto
                           and cor_produto = @cor_produto
                                ";

            var result = _context.ContextDapper.Query<Produto>(sql, new { produto = produto, cor_produto = cor_produto });

            return result.FirstOrDefault();
        }
    }
}
