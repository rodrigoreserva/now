﻿using Now.Domain.Contracts.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using Now.Domain;

namespace Now.Infrastructure.Repositories
{
    public class VendedorRepository : IVendedorRepository
    {
        private NowContext _context;

        public VendedorRepository(NowContext context)
        {
            _context = context;
        }

        public IList<Vendedor> GetVendedor(string idloja, string idvendedor, int idusuario)
        {
            var sql = new StringBuilder(@" SELECT v.* , p2.nome as perfil
                                            FROM Vendedores v with(nolock)
                                       LEFT JOIN users u2  with(nolock)
                                              ON u2.vendedor_id = v.id
                                       LEFT JOIN  users_perfil up2 with(nolock)
			                                  ON up2.id_user = u2.id
			                           LEFT JOIN perfil p2 with(nolock)
			                                  ON p2.id_perfil = up2.id_perfil
                                            
                                           WHERE data_desativacao IS NULL ");
            //Filter personalizado
            var filter = new StringBuilder();
            filter.Append(string.IsNullOrWhiteSpace(idloja) ? string.Empty : "AND v.loja_id in (" + idloja + ")");
            filter.Append(string.IsNullOrWhiteSpace(idvendedor) ? string.Empty : " AND v.id in (" + idvendedor + ")");

            //Filtro de usuário, retornando somente os vendedores das lojas que o usuário tem acesso
            filter.Append(idusuario == 0 ?
                string.Empty
                : String.Format(@"AND EXISTS 
		                (
			                SELECT  1 
			                FROM users u with(nolock)
			                INNER JOIN  users_perfil up with(nolock)
			                ON up.id_user = u.id
			                INNER JOIN perfil p with(nolock)
			                ON p.id_perfil = up.id_perfil
			                LEFT JOIN users_perfil_lojas upl with(nolock)
			                ON upl.id_user = u.id
			                WHERE 
				                u.id = {0}
				                AND (upl.id_loja = v.loja_id OR p.rede = 1) 
		                )", idusuario));

            sql.Append(filter);

            //Ordenação
            sql.Append(" ORDER BY v.nome");

            var result = _context.ContextDapper.Query<Vendedor>(sql.ToString()).ToList();

            return result;
        }

        public IList<Vendedor> ListarVendedores(string idloja, string idvendedor, int idusuario)
        {
            var sql = new StringBuilder(@"    SELECT v.* , u2.email as login, 
                                                    case when v.data_desativacao is null then '<span class=''label label-success''>Ativo</span>' else '<span class=''label label-danger''>Desativados</span>' end status_linx,
		                                            '' status_controe,
		                                            case when u2.id is not null then '<span class=''label label-success''>Ativo</span>' else '<span class=''label label-danger''> Não Existe</span>' end status_login,
                                            p2.nome as perfil
                                            FROM Vendedores v with(nolock)
                                            LEFT JOIN users u2  with(nolock)
                                                ON u2.vendedor_id = v.id
                                            LEFT JOIN  users_perfil up2 with(nolock)
	                                            ON up2.id_user = u2.id
                                            LEFT JOIN perfil p2 with(nolock)
	                                            ON p2.id_perfil = up2.id_perfil
                                             where 1 = 1 

                                           ");
            //Filter personalizado
            var filter = new StringBuilder();
            filter.Append(string.IsNullOrWhiteSpace(idloja) ? string.Empty : " AND v.loja_id in (" + idloja + ")");
            filter.Append(string.IsNullOrWhiteSpace(idvendedor) ? string.Empty : " AND v.id in (" + idvendedor + ")");

            //Filtro de usuário, retornando somente os vendedores das lojas que o usuário tem acesso
            filter.Append(idusuario == 0 ?
                string.Empty
                : String.Format(@"AND EXISTS 
		                (
			                SELECT  1 
			                FROM users u with(nolock)
			                INNER JOIN  users_perfil up with(nolock)
			                ON up.id_user = u.id
			                INNER JOIN perfil p with(nolock)
			                ON p.id_perfil = up.id_perfil
			                LEFT JOIN users_perfil_lojas upl with(nolock)
			                ON upl.id_user = u.id
			                WHERE 
				                u.id = {0}
				                AND (upl.id_loja = v.loja_id OR p.rede = 1) 
		                )", idusuario));

            sql.Append(filter);

            //Ordenação
            sql.Append(" ORDER BY v.data_desativacao, v.nome");

            var result = _context.ContextDapper.Query<Vendedor>(sql.ToString()).ToList();

            return result;
        }

        public void SalvarImgPerfilVendedor(string src, string id)
        {
            string sql = @"
                    UPDATE vendedores 
                    SET img_perfil = @Src
                    WHERE id = @IdVendedor
                ";
            _context.ContextDapper.Execute(sql, new { IdVendedor = id, Src = src });
        }


        #region "Controle Vendedor"

        public IList<VendedoresControleTipo> ListarVendedoresControleTipo()
        {
            const string sql = @"
                   SELECT *
                     FROM [pd].[dbo].[VENDEDORES_CONTROLE_TIPO]
            ";

            return _context.ContextDapper.Query<VendedoresControleTipo>(sql).ToList();
        }

        public IList<VendedoresControle> ListarVendedoresControle()
        {
            const string sql = @"
                   SELECT *
                     FROM [pd].[dbo].[VENDEDORES_CONTROLE]
            ";

            return _context.ContextDapper.Query<VendedoresControle>(sql).ToList();
        }

        public IList<VendedoresControle> SalvarVendedoresControle(VendedoresControle vendedoresControle)
        {
            var query = @" ";
            DynamicParameters parametros = new DynamicParameters();

            if (vendedoresControle.id == 0)
            {
                query = @"  insert into VENDEDORES_CONTROLE
                                 values(@vendedor_id,@loja_id,@vigencia_inicio,@vigencia_fim,@data_inclusao,@data_alteracao,@data_exclusao)";
            }
            else
            {
                query = @"  update VENDEDORES_CONTROLE
                               set loja_id                      = @loja_id
                                 , vendedor_id                  = @vendedor_id
                                 , vendedores_controle_tipo_id  = @vendedores_controle_tipo_id
                                 , vigencia_inicio              = @vigencia_inicio                                 
                                 , vigencia_fim                 = @vigencia_fim
                                 , data_inclusao                = @data_inclusao
                                 , data_alteracao               = @data_alteracao
                                 , data_exclusao                = @data_exclusao
                                
                              where id = @id
                                ";

                parametros.Add("id", vendedoresControle.id);
            }

            parametros.Add("loja_id", vendedoresControle.loja_id);
            parametros.Add("vendedor_id", vendedoresControle.vendedor_id);
            parametros.Add("vendedores_controle_tipo_id", vendedoresControle.vendedores_controle_tipo_id);
            parametros.Add("vigencia_inicio", vendedoresControle.vigencia_inicio);
            parametros.Add("vigencia_fim", vendedoresControle.vigencia_fim);
            parametros.Add("data_inclusao", vendedoresControle.data_inclusao);
            parametros.Add("data_alteracao", vendedoresControle.data_alteracao);
            parametros.Add("data_exclusao", vendedoresControle.data_exclusao);

            _context.ContextDapper.Execute(query, parametros);

            return ListarVendedoresControle();
        }

        #endregion "Controle Vendedor"
    }
}
