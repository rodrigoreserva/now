﻿using Now.Domain.Contracts.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using Now.Domain;
using Now.Domain.DTO;

namespace Now.Infrastructure.Repositories
{
    public class VendaRepository : IVendaRepository
    {
        private NowContext _context;

        public VendaRepository(NowContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Histórico de venda de um cliente
        /// </summary>
        /// <param name="idcliente"></param>
        /// <returns></returns>
        public IList<Venda> HistoricoVendas(string idcliente)
        {
            const string sqlitem = @"
                SELECT 
	                v.ticket Ticket,
	                v.grade Tamanho, 
	                p.descricao_produto NomeProduto, 
	                p.descricao_cor NomeCor,
	                p.produto IdProduto, 
	                p.cor_produto IdCor,
                    v.qtde_venda Unidade,
	                v.valor_venda ValorUnitario,
                    v.valor_desconto_venda Desconto,
                    v.valor_venda SubTotal,
                    v.DESC_OPERACAO_VENDA as descVenda,
	                CASE WHEN v.valor_venda > 0 THEN 'VENDA' ELSE 'TROCA' END Status
                    
                FROM 
	                vendas_detalhe v  with(nolock)              

                LEFT JOIN 
	                produtos p with(nolock)
                ON
	                v.produto_id = p.id
                WHERE
	                v.cliente_id = @IdCliente
            ";

            var resultitem = _context.ContextDapper.Query<VendaItem>(sqlitem, new { IdCliente = idcliente });

            const string sqlinfovendas = @"
                            SELECT  * 
                            FROM info_vendas  with(nolock)
                            WHERE cliente_id = @IdCliente
                            AND status = 1
                        ";

            var resultinfovendas = _context.ContextDapper.Query<InfoVendas>(sqlinfovendas, new { IdCliente = idcliente });

            const string sql = @"
select 
		*
from (     
	         SELECT 
	                vd.ticket Ticket,
                    l.id IdLoja,
	                l.filial NomeLoja,
                    case when l.tipo_loja in ('RESERVA+MINI', 'RESERVA', 'INTERNET') then 'RESERVA' else l.tipo_loja end tipo_loja,
                    v.id as IdVendedor, 
	                v.apelido NomeVendedor,
	                case when totDescr.totalDescricao > 1 then DescrOp.DESC_OPERACAO_VENDA else vd.DESC_OPERACAO_VENDA end Operacao,
	                case when totDescr.totalDescricao > 1 then DescrOp.data_venda else vd.data_venda end Data,
	                SUM(vd.valor_venda) Total,
                    SUM(vd.valor_desconto_venda) TotalDesconto,
	                SUM(CASE WHEN vd.qtde_venda > 0 THEN vd.qtde_venda ELSE 0 END ) Quantidade,
                    case when r.filial is null then '' else ' - Opção Compra Retirada na Loja ' + r.filial 
                     + case when r.entregue_em is null then ' (PENDENTE)' else ' (ENTREGUE)' end
                   end retirada_loja
                FROM 
	                vendas_detalhe vd with(nolock)               

                LEFT JOIN (
                           select 
                                  cliente_id
                                , cast(data_compra as date) as data_venda
                                , filial
                                , entregue_em
                             from pedidos_retirada_loja with(nolock)
                           ) r
                        on (r.cliente_id = vd.cliente_id and r.data_venda = cast(vd.data_venda as date))

                LEFT JOIN 
	                lojas l with(nolock)
                ON 
	                vd.loja_id = l.id
                LEFT JOIN vendedores v with(nolock)
                ON v.id = vd.vendedor_id

				LEFT JOIN (
											select 
													ticket, 
												cliente_id,
											   vendedor_id,
     											   loja_id,
							 cast(data_venda as date) data,
												count(distinct DESC_OPERACAO_VENDA) totalDescricao
											  from vendas_detalhe											 
										  group by ticket, 
												cliente_id,
											   vendedor_id,
     											   loja_id,
								  cast(data_venda as date)
				          )totDescr on ( totDescr.cliente_id = vd.cliente_id
						             and totDescr.data  = cast(vd.data_venda as date)
									 and totDescr.loja_id = l.id
									 and totDescr.vendedor_id = v.id
                                     and totDescr.ticket = vd.ticket
									   )
				LEFT JOIN (
							                select distinct
											ticket, 
										cliente_id,
									   vendedor_id,
     									   loja_id,
					 cast(data_venda as date) data,
					                    data_venda,
								DESC_OPERACAO_VENDA
									  from vendas_detalhe
									 where  DESC_OPERACAO_VENDA <> 'OMNI' 
				          )DescrOp on ( DescrOp.cliente_id = vd.cliente_id
						             and DescrOp.data  = cast(vd.data_venda as date)
									 and DescrOp.loja_id = l.id
									 and DescrOp.vendedor_id = v.id
                                     and DescrOp.ticket = vd.ticket
									   )
                WHERE 
	                vd.cliente_id = @IdCliente
                GROUP BY 
	                vd.ticket,
                    l.id,
                    v.id,    
	                l.filial,
	                v.apelido,
	                case when totDescr.totalDescricao > 1 then DescrOp.DESC_OPERACAO_VENDA else vd.DESC_OPERACAO_VENDA end,
	                case when totDescr.totalDescricao > 1 then DescrOp.data_venda else vd.data_venda end,
                    case when r.filial is null then '' else ' - Opção Compra Retirada na Loja ' + r.filial 
                     + case when r.entregue_em is null then ' (PENDENTE)' else ' (ENTREGUE)' end
                    end,
                    case when l.tipo_loja in ('RESERVA+MINI', 'RESERVA', 'INTERNET') then 'RESERVA' else l.tipo_loja end
)x
                ORDER BY x.Data DESC
            ";


            const string sqlGerou = @"
                           select 
                                   vend.id as vendedor_id 
	                             , vend.nome as nome_vendedor
	                             , tipo.titulo nome_agenda
	                             , ticket
	                             , valor valor_gerou
	                             , data data_venda
                                 , img_perfil_intranet  
	                             , max(cont.created_at) data_ligacao
                              from rel_gerou_vendedores_clientes gerou
                              join vendedores vend on vend.id = gerou.vendedor_id 
                              join menu_tipo_agendamentos tipo on tipo.id = gerou.tipo_agendamento_id
                              join contatos cont on (cont.cliente_id = gerou.cliente_id and cont.vendedor_id = gerou.vendedor_id and cont.loja_id = gerou.loja_id and cast(cont.created_at as date) <  cast(gerou.data as date))
                              where gerou.cliente_id = @IdCliente
                              group by vend.id , data, vend.nome, tipo.titulo, ticket, valor, img_perfil_intranet
                        ";

            var resultGerou = _context.ContextDapper.Query<VendedorGerou>(sqlGerou, new { IdCliente = idcliente });


            var result = _context.ContextDapper.Query<Venda>(
                sql, new { IdCliente = idcliente }).ToList();

            foreach (var item in result)
            {
                item.vendedorGerou = resultGerou.Where(x =>  Convert.ToInt32(x.ticket) == item.Ticket).FirstOrDefault();

                item.Itens = resultitem.Where(x => x.Ticket == item.Ticket).ToList();

                if (resultinfovendas != null)
                    item.InfosVenda = resultinfovendas.Where(x => x.data_registro.Date == item.Data.Date && x.loja_id == item.IdLoja).ToList();
            }

            return result;
        }

        /// <summary>
        /// Retorna todos a quantidade de produtos que ele o cliente já comprou na Reserva.
        /// Agrupamento Grife, Produto
        /// </summary>
        /// <returns></returns>
        public IList<GrupoProdutoItem> GrupoProdutos(string idcliente)
        {
            var sql = @"
                SELECT * 
                FROM v_VendasProdutosAgrupados 
                WHERE IdCliente = @IdCliente
                ORDER BY grife
            ";

            var result = _context.ContextDapper.Query<GrupoProdutoItem>(sql, new { IdCliente = idcliente });

            return result.ToList();
        }


        public InfoVendasDTO ObterInfoVendas(string vendedor_id, string loja_id, string data)
        {


            var datainicio = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            var datafim = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1).AddMonths(1).AddDays(-1);


            var sql = @"
                select

                      isnull(sum(rqv.qtde_ticket), 0) as qtd_info_vendas_disponiveis
	                , isnull(count(info.total_info), 0) as qtd_info_vendas_feitos

	                , rqv.loja_id
	                , rqv.vendedor_id

                FROM rel_qtde_vendas rqv
                left join(
                                select count(iv.id) total_info
                                    , vendedor_id

                                    , data_registro
                                from info_vendas iv

                            group by vendedor_id, data_registro

                            )info
                            on(info.vendedor_id = rqv.vendedor_id and info.data_registro = cast(rqv.data_venda as date))
                where
                    cast(data_venda as date) BETWEEN cast(@dataInicio as date) AND cast(@dataFinal as date)
	                and rqv.vendedor_id = @idvendedor

                    and loja_id = @idloja
                group by  rqv.vendedor_id, rqv.loja_id
            ";

            var result = _context.ContextDapper.Query<InfoVendasDTO>(sql, new { idvendedor = vendedor_id, idloja = loja_id, dataInicio = datainicio.ToString("yyyy-MM-dd"), dataFinal = datafim.ToString("yyyy-MM-dd") });

            return result.FirstOrDefault();


        }
    }
}
