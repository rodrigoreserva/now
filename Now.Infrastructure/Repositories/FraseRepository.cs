﻿using Now.Domain.Contracts.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using Now.Domain;

namespace Now.Infrastructure.Repositories
{
    public class FraseRepository : IFraseRepository
    {
        private NowContext _context;

        public FraseRepository(NowContext context)
        {
            _context = context;
        }


        /// <summary>
        /// Retorna as frases de detalhes do cliente
        /// Tipo frase com id's menores que 6
        /// </summary>
        /// <param name="idcliente"></param>
        /// <returns>Retorna as lista de frases agrupada por Grife</returns>
        public IEnumerable<IGrouping<string, Frase>> FraseClienteDetalhes(string idcliente)
        {
            const string sql = @"
                SELECT *,
                        CASE WHEN ltrim(griffe) = 'RESERVA' THEN 0
                            WHEN ltrim(griffe) LIKE '%MINI%' THEN 1
                            ELSE 2
                        END order_griffe
                FROM frases_ordenacao with(nolock)
                WHERE cliente_id = @IdCliente
                AND tipo_frase_id < 7
                ORDER BY order_griffe asc, tipo_frase_id asc
            ";

            var result = _context.ContextDapper.Query<Frase>(sql, new { IdCliente = idcliente });

            return result.GroupBy(x => x.griffe.Trim());
        }

        /// <summary>
        /// Retorna os motivos de o porque deve se ligar para o cliente.
        /// </summary>
        /// <param name="idcliente"></param>
        /// <returns></returns>
        public IList<Frase> FrasesMotivoLigacao(string idcliente)
        {
            const string sql = @"
                SELECT frase
                FROM frases_ordenacao with(nolock)
                WHERE cliente_id = @IdCliente
                AND tipo_frase_id > 6
                GROUP BY frase, ordenacao
                ORDER BY ordenacao desc
            ";

            var result = _context.ContextDapper.Query<Frase>(sql, new { IdCliente = idcliente });

            return result.ToList();
        }
    }
}
