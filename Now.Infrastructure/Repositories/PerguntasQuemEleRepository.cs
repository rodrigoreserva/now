﻿using Now.Domain.Contracts.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using Now.Domain;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Linq.Expressions;

namespace Now.Infrastructure.Repositories
{
    public class PerguntasQuemEleRepository : IPerguntasQuemEleRepository
    {
        private NowContext _context;

        public PerguntasQuemEleRepository(NowContext context)
        {
            _context = context;
        }

        public IList<PerguntasQuemEle> SalvarPerguntasQuemEle(PerguntasQuemEle entity)
        {
            var query = @" ";
            DynamicParameters parametros = new DynamicParameters();

            if (entity.id == 0)
            {
                query = @"  insert into PERGUNTAS_QUEM_ELE
                                 values(@pergunta, @tipo_pergunta, @ordem, @data_inclusao, @data_alteracao, @data_desativacao)";
            }
            else {
                query = @"  update PERGUNTAS_QUEM_ELE
                               set pergunta                 = @pergunta
                                 , tipo_pergunta            = @tipo_pergunta
                                 , ordem                    = @ordem
                                 , data_inclusao            = @data_inclusao                                 
                                 , data_alteracao           = @data_alteracao
                                 , data_desativacao         = @data_desativacao                                 
                              where id = @id
                                ";

                parametros.Add("id", entity.id);
            }                        

            parametros.Add("pergunta", entity.pergunta);
            parametros.Add("tipo_pergunta", entity.tipo_pergunta);
            parametros.Add("ordem", entity.ordem);
            parametros.Add("data_inclusao", entity.data_inclusao);
            parametros.Add("data_alteracao", entity.id == 0 ? entity.data_alteracao : DateTime.Now);
            parametros.Add("data_desativacao", entity.data_desativacao);            

            _context.ContextDapper.Execute(query, parametros);

            return ListarPerguntasQuemEle();
        }
        public IList<PerguntasQuemEle> DeletePerguntasQuemEle(int id)
        {
            var query = @"  update PERGUNTAS_QUEM_ELE
                               set data_desativacao = @data_desativacao
                                 , data_alteracao   = @data_alteracao
                             where id = @id ";

            DynamicParameters parametros = new DynamicParameters();

            parametros.Add("id", id);
            parametros.Add("data_desativacao", DateTime.Now);
            parametros.Add("data_alteracao", DateTime.Now);

            _context.ContextDapper.Execute(query, parametros);

            return ListarPerguntasQuemEle();
        }     
        public IList<PerguntasQuemEle> ListarPerguntasQuemEle()
        {
            var sql = @"
              SELECT *
                FROM PERGUNTAS_QUEM_ELE   
               WHERE data_desativacao  is null
            ";

            var result = _context.ContextDapper.Query<PerguntasQuemEle>(sql);

            return result.ToList();
        }
    }
}
