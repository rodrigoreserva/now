﻿using Now.Domain.Contracts.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using Now.Domain;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Migrations;
using System.Linq.Expressions;

namespace Now.Infrastructure.Repositories
{
    public class LembreteRepository : ILembreteRepository
    {
        private NowContext _context;

        public LembreteRepository(NowContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Lista 
        /// </summary>
        /// <param name="idcliente"></param>
        /// <returns></returns>
        public IList<Lembrete> ListarLembretes(int idUsuario, string idLoja, string idLembrete)
        {
            //Verifica se o usuário é perfil Rede
            #region "Perfil de Rede"

            var sqlPerfilUsuario = @" select count(*) from users_perfil up with(nolock)
                                        inner join perfil p with(nolock) on p.id_perfil = up.id_perfil
                                        where p.rede = 1 ";

            //Filtros dinamicos
            var filterPerfil = "";
            filterPerfil += idUsuario != 0 ? " and up.id_user = " + idUsuario.ToString() : "";

            var sqlPerfil = sqlPerfilUsuario + filterPerfil;

            int perfilRede = _context.ContextDapper.Query<int>(sqlPerfil).FirstOrDefault();

            #endregion "Perfil de Rede"

            var sql = @"
                SELECT top 100 loj.filial,vend.nome as nomeVendedor, lemb.*, clie.cpf, clie.nome as nome_cliente, t.titulo, t.id idAgenda, t.parametro prametroAgenda
                FROM lembretes      lemb with(nolock)
           left join menu_tipo_agendamentos t with(nolock) on (t.parametro = lemb.motivo or cast(t.id as varchar) = lemb.motivo)
          inner join clientes       clie with(nolock)
                  on clie.id = lemb.cliente_id
          inner join lojas          loj with(nolock) 
                  on loj.id = lemb.loja_id   
           left join vendedores     vend with(nolock)
                  on vend.id = lemb.vendedorId
                WHERE 1 = 1  
                  and lemb.status = 1     
            ";

            var orderby = " ORDER BY lemb.created_at desc";

            var sqlCondicaoPerfil = "";
            if (perfilRede == 0)
            {
                var condicaoPerfil = @" and lemb.loja_id in (select loja.id
                                                      from lojas loja with(nolock)
                                                inner join users_perfil_lojas uspl with(nolock)
                                                        on uspl.id_loja = loja.id
                                                     where 1 = 1
                                                        -- Filtros personalizados
                                                           {0}
                                                    )         ";


                //Filtros dinamicos
                var filter = "";
                filter += idUsuario != 0 ? " and uspl.id_user = " + idUsuario.ToString() : "";

                sqlCondicaoPerfil = String.Format(condicaoPerfil, filter);
            }

            sqlCondicaoPerfil += string.IsNullOrEmpty(idLoja) ? "" : " and loj.id = " + idLoja;

            sqlCondicaoPerfil += string.IsNullOrEmpty(idLembrete) ? "" : " and lemb.id = " + idLembrete;

            var query = sql + sqlCondicaoPerfil + orderby;

            var result = _context.ContextDapper.Query<Lembrete>(query);

            return result.ToList();
        }

        /// <summary>
        /// Adiciona um novo Lembrete
        /// </summary>
        /// <param name="cpf"></param>
        /// <param name="info"></param>
        /// <param name="lojaId"></param>
        /// <param name="dataRegistro"></param>        
        /// <returns></returns>

        public void SalvarLembrete(Lembrete lembrete)
        {
            var query = @"  insert into lembretes
                                 values(@loja_id,@cliente_id,@data_inicio,@data_final,@dica,@motivo,@created_at,@updated_at,@deleted_at,@status,@vendedorId)";

            DynamicParameters parametros = new DynamicParameters();

            parametros.Add("loja_id", lembrete.loja_id);
            parametros.Add("cliente_id", lembrete.cliente_id);
            parametros.Add("data_inicio", lembrete.data_inicio);
            parametros.Add("data_final", lembrete.data_final);
            parametros.Add("dica", lembrete.dica);
            parametros.Add("motivo", lembrete.motivo);
            parametros.Add("created_at", DateTime.Now);
            parametros.Add("updated_at", DateTime.Now);
            parametros.Add("deleted_at", null);
            parametros.Add("status", 1);
            parametros.Add("vendedorId", lembrete.vendedorId);

            _context.ContextDapper.Execute(query, parametros);
        }

        /// <summary>
        /// Deleta uma lembrete
        /// </summary>
        /// <param name="id"></param>              
        /// <returns></returns>
        public void Deletelembrete(int id)
        {
            //Lembrete lembrete = _context.FirstOrDefault<Lembrete>(x => x.id == id);

            //if (lembrete == null)
            //    throw new Exception("A Info Vendas informado não foi encontrado");

            //lembrete.deleted_at = DateTime.Now;
            //lembrete.status = 0;

            //_context.Lembrete.AddOrUpdate(lembrete);
            //_context.SaveChanges();
        }
    }
}
