﻿using Now.Domain;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Now.Infrastructure
{
    public class NowContext : DbContext
    {
        public NowContext()
            : this("NowConnectionString")
        {
            Database.SetInitializer<NowContext>(null);
        }

        public NowContext(string connectionstring)
            : base(connectionstring)
        {
            //this.Configuration.LazyLoadingEnabled = false;
        }

        public DbConnection ContextDapper
        {
            get
            {
                if (this.Database.Connection.State != System.Data.ConnectionState.Open)
                    this.Database.Connection.Open();

                return this.Database.Connection;
            }
        }

        //public DbSet<InfoVendas> InfoVendas { get; set; }
        //public DbSet<VisitaLojas> VisitaLojas { get; set; }
        //public DbSet<Contato> Contato { get; set; }
        //public DbSet<LogErro> LogErro { get; set; }
        //public DbSet<LogAcesso> LogAcesso { get; set; }
        //public DbSet<Lembrete> Lembrete { get; set; }
        //public DbSet<Usuario> Usuario { get; set; }
        //public DbSet<Vendedor> Vendedor { get; set; }

        //protected override void OnModelCreating(DbModelBuilder modelBuilder)
        //{
        //    modelBuilder.Configurations.Add(new InfoVendasMap());
        //    modelBuilder.Configurations.Add(new ContatoMap());
        //    modelBuilder.Configurations.Add(new LogErroMap());
        //    modelBuilder.Configurations.Add(new LogAcessoMap());
        //    modelBuilder.Configurations.Add(new LembreteMap());
        //    modelBuilder.Configurations.Add(new UsuarioMap());
        //    modelBuilder.Configurations.Add(new VendedoresMap());
        //    modelBuilder.Configurations.Add(new VisitaLojasMap());

        //    base.OnModelCreating(modelBuilder);
        //}
    }
}
