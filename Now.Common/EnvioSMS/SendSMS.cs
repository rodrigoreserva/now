﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Now.Common.EnvioSMS
{
    public class SendSMS
    {
        public string destination { get; set; }
        public string messageText { get; set; }

        public void Initialize(string cellPhone, string message)
        {
            destination = cellPhone;
            messageText = message;
        }

        public async Task<bool> EnviaSMS(string chamada, SendSMS registro, string metodo, string tokenAcesso)
        {
            // retorno de execução da API em formato JSON
            string retornoJSON = null;

            // configura a chamada da API
            HttpWebRequest requisicao = (HttpWebRequest) WebRequest.Create(chamada);
            requisicao.ContentType = "application/json; charset=UTF-8";
            requisicao.KeepAlive = false;
            requisicao.ProtocolVersion = HttpVersion.Version10;
            requisicao.Method = metodo;

            // Verfica se token de acesso da Intelipost está preenchido
            if (!string.IsNullOrEmpty(tokenAcesso))
            {
                // Define variáveis de cabeçalho da requisição
                requisicao.Headers.Add("UserName", "reserva@movile.com");
                requisicao.Headers.Add("AuthenticationToken", tokenAcesso);
            }

            // Define configurações do pacote JSON
            //JsonSerializerSettings jsonSettings = new JsonSerializerSettings()
            //{
            //    DefaultValueHandling = DefaultValueHandling.Ignore,
            //    NullValueHandling = NullValueHandling.Ignore
            //};

            // serialia o parâmetro em formato JSON
            string parametroJSON = JsonConvert.SerializeObject(registro);

            // converte o parametro em array de bytes
            byte[] parametroBytes = Encoding.UTF8.GetBytes(parametroJSON);

            // configura o tamanho do parâmetro
            requisicao.ContentLength = parametroBytes.Length;

            // se o método utilizado for diferente de "GET"
            if (!metodo.Equals("GET"))
            {
                // instancia o escitor com a requisição da API para informar o parâmetro utilizado na chamada
                using (StreamWriter escritor = new StreamWriter(requisicao.GetRequestStream()))
                {
                    // escreve o parâmetro
                    await escritor.WriteAsync(parametroJSON);
                    escritor.Flush();
                }
            }

            // carrega a resposta da API
            HttpWebResponse resposta = (HttpWebResponse) await requisicao.GetResponseAsync();

            // se o status de resposta da API for "Ok"
            if (resposta.StatusCode == HttpStatusCode.OK)
            {
                // carrega o resultado da resposta da API
                Stream conteudoResposta = resposta.GetResponseStream();

                // transforma o resultado da resposta da API em string
                using (StreamReader leitor = new StreamReader(conteudoResposta))
                {
                    // carrega o JSON da resposta da API
                    retornoJSON = await leitor.ReadToEndAsync();
                    leitor.Close();
                }
            }

            // desserializa o JSON de resulatado no formato informado
            RetornoSMS resultado = (RetornoSMS)JsonConvert.DeserializeObject(
                              retornoJSON,
                              typeof(RetornoSMS)
                              //new JsonSerializerSettings
                              //{
                              //    NullValueHandling = NullValueHandling.Ignore,
                              //    DefaultValueHandling = DefaultValueHandling.Ignore,
                              //    MissingMemberHandling = MissingMemberHandling.Ignore
                              //}
                              );

            resposta.Close();

            // retorna true em caso de sucesso ou false em caso de não sucesso
            if (resultado != null)
                return true;
            else
                return false;
        }


        //public static String doPost(String strUrl, String request, String userName, String authenticationToken)
        //{
        //    HttpURLConnection conn = null;
        //    OutputStreamWriter wr = null;
        //    BufferedReader br = null;
        //    try
        //    {
        //        URL url = new URL(strUrl);

        //        conn = (HttpURLConnection)url.openConnection();
        //        conn.setRequestMethod("POST");
        //        conn.setDoOutput(true);
        //        conn.setUseCaches(false);
        //        conn.setInstanceFollowRedirects(true);
        //        conn.setConnectTimeout(30000);
        //        conn.setReadTimeout(30000);

        //        conn.setRequestProperty("Content-Type", "application/json");
        //        conn.setRequestProperty("UserName", userName);
        //        conn.setRequestProperty("AuthenticationToken", authenticationToken);

        //        // write the request
        //        wr = new OutputStreamWriter(conn.getOutputStream());
        //        wr.write(request);
        //        wr.close();

        //        // read the response
        //        br = new BufferedReader(new InputStreamReader(conn.getInputStream()));

        //        StringBuilder resp = new StringBuilder();
        //        String line;
        //        while ((line = br.readLine()) != null)
        //        {
        //            resp.append(line).append("\n");
        //        }
        //        return resp.toString();

        //    }
        //    catch (IOException e)
        //    {
        //       // e.printStackTrace();
        //    }
        //    finally
        //    {
        //        try
        //        {
        //            if (wr != null)
        //            {
        //                wr.close();
        //            }
        //            if (br != null)
        //            {
        //                br.close();
        //            }
        //            if (conn != null)
        //            {
        //                conn.disconnect();
        //            }
        //        }
        //        catch (IOException e)
        //        {
        //            //e.printStackTrace();
        //        }
        //    }
        //    return null;
        //}
    }

    public class RetornoSMS
    {
        public string id { get; set; }
        public string correlationId { get; set; }
    }
}

