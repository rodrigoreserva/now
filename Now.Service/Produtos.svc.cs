﻿using Now.Infrastructure.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Web.Script.Serialization;

namespace Now.Service
{
    // OBSERVAÇÃO: Você pode usar o comando "Renomear" no menu "Refatorar" para alterar o nome da classe "Produtos" no arquivo de código, svc e configuração ao mesmo tempo.
    // OBSERVAÇÃO: Para iniciar o cliente de teste do WCF para testar esse serviço, selecione Produtos.svc ou Produtos.svc.cs no Gerenciador de Soluções e inicie a depuração.
    public class Produtos : IProdutos
    {
        public string BaixarContato(string cliente)
        {
            throw new NotImplementedException();
        }

        public string ListarProdutos(string nome,string grupo)
        {
            var produto = new ProdutoListaVezRepository();
            var resultado = produto.ListarProdutos(nome, grupo);

            return new JavaScriptSerializer().Serialize(resultado);
        }

        public string ListarTodosProdutos()
        {
            var produto = new ProdutoListaVezRepository();
            var resultado = produto.ListarTodosProdutos();

            return new JavaScriptSerializer().Serialize(resultado);
        }
    }
}
