﻿using Now.Business.Services;
using Now.Domain.Contracts.Repositories;
using Now.Domain.Contracts.Services;
using Now.Domain.DTO;
using Now.Infrastructure.Repositories;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Json;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI.WebControls;
using System.Xml.Serialization;

namespace Now.Service
{
    /// <summary>
    /// Descrição resumida de WebService1
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // Para permitir que esse serviço da web seja chamado a partir do script, usando ASP.NET AJAX, remova os comentários da linha a seguir. 
    // [System.Web.Script.Services.ScriptService]
    public class ProdutoService : WebService
    {
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Xml)]
        public string ListarProdutos()
        {
            var produto = new ProdutoListaVezRepository();
            var resultado = produto.ListarProdutos("","");

            return new JavaScriptSerializer().Serialize(resultado);
        }

        [WebMethod]
        public string OlaMundo()
        {
            return "Olá Mundo!";
        }
    }
}
