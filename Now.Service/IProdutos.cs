﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace Now.Service
{
    // OBSERVAÇÃO: Você pode usar o comando "Renomear" no menu "Refatorar" para alterar o nome da interface "IProdutos" no arquivo de código e configuração ao mesmo tempo.
    [ServiceContract]
    public interface IProdutos
    {
        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = @"/produtos?nome={nome}&grupo={grupo}", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        string ListarProdutos(string nome, string grupo);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = @"/todos", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        string ListarTodosProdutos();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = @"/produtos?id={cliente}", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        string BaixarContato(string cliente);
    }
}
