﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Now.Service.DTO
{
    [Serializable]
    public class ProdutosDTO
    {
        //produto_id , p.produto, p.cor_produto, p.descricao_produto, p.grade
        public int produto_id { get; set; }
        public string produto { get; set; }
        public string cor_produto { get; set; }
        public string descricao_produto { get; set; }
        public string grade { get; set; }
        public string url { get; set; }
        public string griffe { get; set; }
        public DateTime atualizado_em { get; set; }
        public DateTime acessado_em { get; set; }

    }
}