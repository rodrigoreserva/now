﻿using System;
using System.ComponentModel;

namespace Now.Domain
{
    public class ReportRankingCornerEva
    {
        [Description(""), Category("")]
        public string ranking { get; set; }
        [Description("LOJA"), Category("")]
        public string loja { get; set; }
        //[Description("SUPERVISOR"), Category("")]
        //public string supervisor { get; set; }
        [Description("VENDEDOR"), Category("")]
        public string vendedor { get; set; }
        //[Description("VENDA"), Category("")]
        //public int venda { get; set; }
        //[Description("CLIENTE"), Category("")]
        //public int cliente { get; set; }
        //[Description("PECAS"), Category("")]
        //public int pecas { get; set; }
        [Description("VALOR"), Category("{0:C}")]
        public decimal valor { get; set; }
        [Description("VALOR TOTAL"), Category("{0:C}")]
        public decimal valor_total { get; set; }
        [Description("% CORNER MINI"), Category("{0:P}")]
        public decimal corner_eva { get; set; }
        [Description("VENDEU"), Category("{0:C}")]
        public decimal valor_venda_now { get; set; }
        [Description("GEROU"), Category("{0:C}")]
        public decimal valor_contato_now { get; set; }
    }
}