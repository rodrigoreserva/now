﻿using System;
namespace Now.Domain
{
    public class CatalogoProdutos
    {
        public int id { get; set; }
        public string produto { get; set; }
        public string descricao_produto { get; set; }
        public string cor_produto { get; set; }
        public string descricao_cor { get; set; }
        public string tamanho_1 { get; set; }
        public string tamanho_2 { get; set; }
        public string tamanho_3 { get; set; }
        public string tamanho_4 { get; set; }
        public string tamanho_5 { get; set; }
        public string tamanho_6 { get; set; }
        public string tamanho_7 { get; set; }
        public string tamanho_8 { get; set; }
        public string tamanho_9 { get; set; }
        public string tamanho_10 { get; set; }   
    }
}