﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Now.Domain
{
    public class RelatorioRankingOficina
    {
        public string filial { get; set; }
        public string vendedor { get; set; }
        public decimal peca_pronta { get; set; }
        public decimal personalizada { get; set; }     
    }
}

