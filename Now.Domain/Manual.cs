﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Now.Domain
{
    public class Manual
    {
        public int id { get; set; }                
        public string titulo { get; set; }
        public string conteudo { get; set; }
        public DateTime data_inclusao { get; set; }
        public DateTime? data_alteracao { get; set; }
        public DateTime? data_desativacao { get; set; }
        public int ordem { get; set; }
    }
}

