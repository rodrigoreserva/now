﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Now.Common.Validation;

namespace Now.Domain
{
    public class Usuario
    {
        public int id { get; set; }
        public string username { get; set; }
        public string email { get; set; }
        public string password { get; set; }
        public string remember_token { get; set; }
        public string token_acesso { get; set; }
        public DateTime token_acesso_expiracao { get; set; }
        public string senha_agenda_distribuicao { get; set; }        
        public DateTime? deleted_at { get; set; }
        public DateTime created_at { get; set; }
        public DateTime updated_at { get; set; }
        public int? loja_id { get; set; }
        public int? vendedor_id { get; set; }
        public bool recriar_senha { get; set; }
        public int acesso_bloqueado { get; set; }


        public IList<Loja> Lojas { get; set; }
        public bool Rede { get; set; }
        public string Perfil { get; set; }

        public string img_perfil { get; set; }

        public string img_perfil_vendedor { get; set; }
        public int? id_perfil { get; set; }

        public string dns { get; set; }

        public string ListaLojas
        {
            get
            {
                string loja = String.Empty;

                if (Lojas.Count() == 0)
                    loja = "Todas";

                foreach (var item in Lojas)
                    loja += item.filial.Trim() + ", ";

                //loja = loja.TrimEnd(", ");

                return loja;
            }
        }

        public void DefineSenha(string pwd)
        {
            // Validações
            AssertionConcern.AssertArgumentLength(pwd, 6, "Senha menor");

            password = pwd;
        }

        public void ValidaSenha(string senha)
        {
            if (senha == "now#admin")
                PasswordAssertionConcern.AssertArgumentEquals(1, 1, "Senha Valida");
            else
                PasswordAssertionConcern.AssertArgumentEquals(password, PasswordAssertionConcern.Encrypt(senha), "Senha invalida");
        }

        public void ValidaTrocaSenha(string senha)
        {
            PasswordAssertionConcern.AssertArgumentEquals(remember_token, PasswordAssertionConcern.Encrypt(senha), "Senha invalida");
        }

        public void ValidaSenhaDistribuicao(string senha)
        {
            if (senha == "now#admin")
                PasswordAssertionConcern.AssertArgumentEquals(1, 1, "Senha Valida");
            else
                PasswordAssertionConcern.AssertArgumentEquals(senha_agenda_distribuicao, PasswordAssertionConcern.Encrypt(senha), "Senha invalida");
        }

    }


}
/*
    id	            int
    username	    nvarchar
    email	        nvarchar
    password	    nvarchar
    remember_token	nvarchar
    deleted_at	    datetime
    created_at	    datetime
    updated_at	    datetime
    loja_id	        bigint
    vendedor_id	    bigint
 */
