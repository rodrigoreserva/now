﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Now.Domain.Contracts.Repositories
{
    public interface IVisitaLojasRepository
    {
        void SalvarVisitaLojas(VisitaLojas visitalojas);

        IList<VisitaLojas> ListarVisitaLojas(string idLoja);

        VisitaLojas BuscarVisitaLojas(int id);

        void DeleteVisitaLojas(int id);
    }
}
