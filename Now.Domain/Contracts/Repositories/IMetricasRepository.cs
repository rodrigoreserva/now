﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Now.Domain.Contracts.Repositories
{
    public interface IMetricasRepository
    {
        IList<Metricas> ListarMetricas(string nomeProcedure, string idLoja, string idVendedor, string dtIni, string dtFim);

        IList<Metricas> ListarMetricas(string idLoja, string idVendedor, string dtIni, string dtFim);  
    }
}
