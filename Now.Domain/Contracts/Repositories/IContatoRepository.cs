﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Now.Domain.Contracts.Repositories
{
    public interface IContatoRepository
    {
        IList<FraseContato> ContatosPorCliente(string idcliente);
        Contato AdicionarContato(Contato contato);
        IList<Produto> ListarProduto(string id);
        Contato BuscarContato(string idContato);

        ContatoCliente BuscarContatoCliente(string telefone);
        ContatoCliente BuscarCliente(string telefone);
        
        Contato AtualizarTermometroContato(string idContato, string termometro);
        Contato AdicionarAvaliacao(string idContato, string vlrAvaliacao, string observacao, int flgContatoFeito, string texto);
        ContatoCliente AdicionarCliente(string idContato, string nome, string cpf, string telefone,string email);
        IList<ContatoClienteAvaliacao> ReenvioAvaliacao();
    }
}
