﻿using Now.Domain.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Now.Domain.Contracts.Repositories
{
    public interface IProdutoListaVezRepository
    {
        IList<ProdutoDTO> ListarProdutos(string descricao,string grupo);
        IList<ProdutoDTO> ListarTodosProdutos();
    }
}
