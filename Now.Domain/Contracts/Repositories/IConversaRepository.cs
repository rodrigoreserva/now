﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Now.Domain.Contracts.Repositories
{
    public interface IConversaRepository
    {
        IList<Conversa> SalvarConversa(Conversa Conversa);
        IList<Conversa> ListarConversa();
        IList<Conversa> DeleteConversa(int id);
    }
}
