﻿using Now.Domain.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Now.Domain.Contracts.Repositories
{
    public interface IVendaRepository
    {
        IList<Venda> HistoricoVendas(string idcliente);

        IList<GrupoProdutoItem> GrupoProdutos(string idcliente);

        InfoVendasDTO ObterInfoVendas(string vendedor_id, string loja_id, string data);
    }
}
