﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Now.Domain.Contracts.Repositories
{
    public interface ILembreteRepository
    {
        void SalvarLembrete(Lembrete lembrete);

        IList<Lembrete> ListarLembretes(int idUsuario, string idLoja, string idLembrete);
    }
}
