﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Now.Domain.Contracts.Repositories
{
    public interface IUsuarioRepository
    {
        Usuario UsuarioPor(string cpf);

        Usuario Get(string email, string id, string vendedorId);

        Usuario UsuarioPorToken(string token);

        IList<UsuarioPerfil> ListarUsuarios(string IdsLojas);

        IList<Perfil> ListarPerfis();

        IList<SupervisorLojas> ListarSupervisores();

        void SalvarUsuario(Usuario usuario);

        void SalvaConfiguracoes(VendedoresConfiguracao configuracao);

        IList<Usuario> ListarUsuariosPorPerfil(string IdPerfil);

        void UpdateUsuario(Usuario usuario, bool alterarToken = true);

        void SalvarImgPerfil(string src, string idUsuario);

        void AdicionarErro(string metodo, string tela, string erro, int? vendedorId, int? clienteId, int? lojaId, int? userId);

        void AdicionarLogAcesso(string browser, string login, string senha, string ip, int? acesso);

        void AdicionarLogSistema(LogSistema logSistema);
    }
}
