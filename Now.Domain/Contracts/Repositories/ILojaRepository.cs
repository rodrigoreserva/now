﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Now.Domain.Contracts.Repositories
{
    public interface ILojaRepository
    {
        IList<Loja> ListarLojasVisiveisPorUsuario(int idUsuario, string idLoja);
        IList<Loja> ListarLojas(string idLoja);
        IList<Loja> ListarLojas();
        IList<Loja> ListarLojasFormatados();
        IList<Loja> ListarTodasLojas();
        IList<LojaDistribuicaoPerfil> ListarLojasDistribuicao();
        IList<DistribuicaoSupervisaoOrg> ListarSupervisaoDistribuicao();
        IList<LojaDistribuicaoOrigemDestino> ListarLojaDistribuicaoOrigemDestino();
        IList<LojaDistribuicaoOrigemDestino> SalvarLojaDistribuicaoOrigemDestino(LojaDistribuicaoOrigemDestino obj);

        void UpdateLoja(Loja loja);
        void UpdatePerfilSupervisao(string idUsuario, string idLoja, string idPerfil);
    }
}
