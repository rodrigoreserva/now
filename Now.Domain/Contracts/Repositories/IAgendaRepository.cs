﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Now.Domain.Contracts.Repositories
{
    public interface IAgendaRepository
    {
        IList<MenuAgendamento> TypeCalendar(Usuario UsuarioLogado, string idusuario = "", string idloja = "", string idvendedor = "", string param = "", bool _lembrete = false);

        IList<Agendamento> FindCalendars(string type, string strBusca, string idusuario = "", string idloja = "", string idvendedor = "", bool administrador = false, string idCliente = "");

        IList<MenuTipoAgendamento> ListarAgendas(string idloja);

        void AgendaNaoFalou(MenuAgendamentoNaoFalou naoFalou);
    }
}
