﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Now.Domain.Contracts.Repositories
{
    public interface ICatalogoProdutosRepository
    {
        IList<CatalogoProdutos> ListarCatalogoProdutos(string strPesqProduto, int pageIndex, ref int PageCount);
        IList<String> ListaGrupoProdutos(string griffe);
        IList<String> ListaGriffe();
    }
}
