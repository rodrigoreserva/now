﻿using Now.Domain.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Now.Domain.Contracts.Repositories
{
    public interface IDashboardRepository
    {
        IList<ReportRankingVendedores> ListarRankingVendedores(string nomeProcedure, string idLoja, string dtIni, string dtFim);

        IList<DashboardRankingVendedores> ListarDashboardRankingVendedores(string nomeProcedure, string idLoja, string dtIni, string dtFim);

        IList<DashboardRankingVendedores> ListarDashboardRankingVendedores(string idLoja, string dtIni, string dtFim);

        IList<DashboardGraficoEfetividade> ListarDashboardGraficoEfetividade(string nomeProcedure, string idLoja, string idVendedor, string idTodasLojas, string dtIni, string dtFim);

        IList<Dashboard> ListarMetricas(string idLoja, string idVendedor, string dtIni, string dtFim);

        IList<MetricasDTO> ListarNovoDashBoard(string nomeProcedure, string idLoja, string idVendedor, string dtIni, string dtFim);
    }
}
