﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Now.Domain.Contracts.Repositories
{
    public interface IFraseRepository
    {
        IList<Frase> FrasesMotivoLigacao(string idcliente);
        IEnumerable<IGrouping<string, Frase>> FraseClienteDetalhes(string idcliente);        
    }
}
