﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Now.Domain.Contracts.Repositories
{
    public interface IDistribuicaoAgendaRepository
    {
        //IList<ClientesDistribuicao> ListarClientesDistribuicao(int idLoja, string tipoAprovacao, string tipoDistribuicao);

        //void AtualizarTrocaVendedor(string lojaId, string vendedorId, string clienteId);

        void SalvarLogTransferencia(LogTransferenciaAgenda entity);
        IList<LogTransferenciaAgenda> ListarLogDistribuicao(string LojaId);
        LogTransferenciaAgenda ObterLogDistribuicao(string idLog);

    }
}
