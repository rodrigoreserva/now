﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Now.Domain.Contracts.Repositories
{
    public interface INowManutencaoRepository
    {
        void SalvarNowManutencao(NowManutencao NowManutencao);

        IList<NowControleCarga> ListarLogCarga();

        void DeleteNowManutencao(int id);
    }
}
 