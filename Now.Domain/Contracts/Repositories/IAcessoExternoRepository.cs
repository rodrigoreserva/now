﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Now.Domain.Contracts.Repositories
{
    public interface IAcessoExternoRepository
    {
        void SalvarVisitaLojas(VisitaLojas visitalojas);

        IList<VisitaLojas> ListarVisitaLojas(string idLoja);

        VisitaLojas BuscarVisitaLojas(int id);
       
        void DeleteVisitaLojas(int id); 

        IList<AbandonoCarrinhoClienteProduto> BuscarAbandonoCarrinhoClienteProduto(string cpf);

        void InserirProdutoAbandonoCarrinho(string cpf, string produto, string cor_produto, string codigo_filial, string grade, string sku, DateTime _dataControle, DateTime dataExperimente, string periodo, DateTime dataAbandono);

        IList<AbandonoCarrinhoNowLinxSolicitacao> BuscarAbandonoCarrinhoNowLinxSolicitacao(string cpf);

        IList<AbandonoCarrinhoClienteProdutoEstoque> BuscarProdutoClienteExperimenteEstoque(string produto);

        bool InsereClienteReservadoAgenda(string cpf, string lojaid, string telefone, string tipo, DateTime? datasolicitacao);

        IList<ClienteReservadoAgendada> BuscarClienteReservado(string cpf, string lojaid);
        
    }
} 
