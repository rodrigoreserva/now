﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Now.Domain.Contracts.Repositories
{
    public interface IClientesDistribuicaoRepository
    {
        IList<ClientesDistribuicao> ListarClientesDistribuicao(int idLoja, string tipoAprovacao, string tipoDistribuicao);

        void AtualizarTrocaVendedor(string lojaId, string vendedorId, string clienteId);
    }
}
