﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Now.Domain.Contracts.Repositories
{
    public interface IContatoAtgRepository
    {
        IList<ContatoAtg> BuscarContatosPorData(DateTime data);

        void AdicionarContatoAtg(ContatoAtg contato);
    }
}
