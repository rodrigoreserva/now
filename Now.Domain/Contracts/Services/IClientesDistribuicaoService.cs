﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Now.Domain.Contracts.Services
{
    public interface IClientesDistribuicaoService
    {
        IList<ClientesDistribuicao> ListarClientesDistribuicao(int idLoja, string tipoAprovacao, string tipoDistribuicao);

        void AtualizarTrocaVendedor(string lojaId, string vendedorId, string clienteId);
    }
}
