﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Now.Domain.Contracts.Services
{
    public interface IVisitaLojasService
    {
        void SalvarVisitaLojas(VisitaLojas visitalojas);

        IList<VisitaLojas> ListarVisitaLojas(string idLoja);

        void DeleteVisitaLojas(int id);
    }
}
