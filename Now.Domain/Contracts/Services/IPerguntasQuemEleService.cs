﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Now.Domain.Contracts.Services
{
    public interface IPerguntasQuemEleService
    {
        IList<PerguntasQuemEle> SalvarPerguntasQuemEle(PerguntasQuemEle perguntasQuemEle);
        IList<PerguntasQuemEle> ListarPerguntasQuemEle();
        IList<PerguntasQuemEle> DeletePerguntasQuemEle(int id);        
    }
}
