﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Now.Domain.Contracts.Services
{
    public interface IProdutoService
    {
        Produto ObterProduto(string produto, string cor_produto);
    }
}
