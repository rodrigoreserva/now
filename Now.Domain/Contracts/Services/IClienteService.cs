﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Now.Domain.Contracts.Services
{
    public interface IClienteService
    {
        Cliente FindClientById(string id, int idUsuario, string idLoja, string idVendedorSelecionado);

        IList<Cliente> BuscarClienteCpfOrigemLoja(string origemLoja, string cpf);

        void AtualizarPreferenciaContato(int id, string preferenciaContato);
        
        IList<Cliente> BuscarCliente(string origemLoja, string nome, string cpf, string telefone, string idLoja = "", string idVendedor = "", string filtroPesquisa = "");
        IList<ClienteDependente> BuscarDependentes(int idCliente);
        void SalvarDependenteCliente(ClienteDependente clienteDependente);
        IList<ClienteNovo> BuscarClienteNovo(string idLoja, string idVendedor, string idClienteNovo);
        void RemoverDependente(int idDependente);
        void SalvarClienteQuemEEle(int idcliente, string caracteristica);
        ClienteDependente ObterDependente(string IdDependente);
        void SalvarClienteObs(int idcliente, string caracteristica);
        void SalvarFavoritos(int idcliente, int idVendedor, string cliFavarito);

        void SalvarClienteNovo(ClienteNovo clienteNovo);
        void SalvarDependenteClienteNovo(ClienteNovoDependente clienteNovoDependente);
        void ExluirClienteNovo(string Id);
        int SalvarIndicacaoProdutos(int clienteId, int? lojaId, int? vendedorId, IList<Produto> prodSelecionados);
        IndicacaoProduto BuscarIndicacaoProdutosPorIndicacao(string id);
        void SalvarEnvioIndicacaoProdutos(int idIndicacao, IList<IndicacaoProdutoItens> indicacaoProdutoItens);
        IList<Cliente> ClientesAptosAgenda();
    } 
}
