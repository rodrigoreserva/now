﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Now.Domain.Contracts.Services
{
    public interface IContatoAtgService
    {
        IList<ContatoAtg> BuscarContatosPorData(DateTime data);

        void AdicionarContatoAtg(ContatoAtg contato);
    }
}
