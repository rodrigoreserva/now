﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Now.Domain.Contracts.Services
{
    public interface IManualService
    {
        void Salvar(Manual infoVenda);

        IList<Manual> ListarManual();

        Manual BuscarManual(int id);

        void Delete(int id);
    }
}
