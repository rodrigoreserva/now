﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Now.Domain.Contracts.Services
{
    public interface INotificacaoService
    {
        IList<Notificacao> Obter(string LojaId, string vendedorId);
        IList<Notificacao> ObterTodas(string LojaId, string vendedorId);
        void AtualizarVistoNotificacao(string Id);
    }
}
