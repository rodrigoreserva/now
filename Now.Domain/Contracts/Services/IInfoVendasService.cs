﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Now.Domain.Contracts.Services
{
    public interface IInfoVendasService
    {
        void SalvarInfoVenda(InfoVendas infoVenda);

        IList<InfoVendas> ListarInfoVendas(int idUsuario, string idLoja, string idVendedor);

        InfoVendas BuscarInfoVendas(int id);
        InfoVendas BuscarInfoVendasPorCliente(int idCliente, DateTime dataVenda, int LojaId);

        void DeleteInfoVenda(int id);
    }
}
