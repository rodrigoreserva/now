﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Now.Domain.Contracts.Services
{
    public interface IVendedoresLicenciadosService
    {
        void Salvar(VendedoresLicenciados VendedoresLicenciado);

        IList<VendedoresLicenciados> Listar(string idLoja, string idVendedor);

        void Deletar(int id);

        VendedoresLicenciados BuscarPorId(int id);

        IList<VendedoresLicenciadosTipo> ListarMotivos();
        IList<VendedoresAgendaDistribuicao> BuscarAgendaDistribuidas(int vendedorId);
        void DistribuirClientes(int vendedorId);
    }
}
