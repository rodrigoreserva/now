﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Now.Domain.Contracts.Services
{
    public interface IVendedorService
    {
        IList<Vendedor> GetVendedor(string idloja = "", string idvendedor = "", int idusuario = 0);

        IList<Vendedor> ListarVendedores(string idloja = "", string idvendedor = "", int idusuario = 0);

        void SalvarImgPerfilVendedor(string src, string id);

        #region "Controle Vendedor"

        IList<VendedoresControleTipo> ListarVendedoresControleTipo();

        IList<VendedoresControle> SalvarVendedoresControle(VendedoresControle vendedoresControle);

        #endregion "Controle Vendedor"
    }
}
