﻿using Now.Domain.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Now.Domain.Contracts.Services
{
    
    public interface IProdutoListaVez
    {
        IList<ProdutoDTO> ListarProdutos(string descricao, string grupo); 
    }
}
