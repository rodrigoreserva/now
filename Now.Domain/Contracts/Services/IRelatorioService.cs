﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Now.Domain.Contracts.Services
{
    public interface IRelatorioService
    {

        IList<ReportEfetividade> ListarEfetividadeLojas(string nomeProcedure, string idLoja, string dtIni, string dtFim);        

        IList<ReportEfetividade> ListarEfetividadeLojas(string idLoja, string dtIni, string dtFim);

        IList<ReportEfetividadeAgenda> ListarReportEfetividadeAgenda(string nomeProcedure, string idLoja, string dtIni, string dtFim);

        IList<ReportEfetividadeAgenda> ListarReportEfetividadeAgendaDashboard(string nomeProcedure, string idVendedor, string idLoja, string dtIni, string dtFim);

        IList<ReportEfetividadeAgenda> ListarReportEfetividadeAgendaDashboard(string idVendedor, string idLoja, string dtIni, string dtFim);

        IList<ReportDedoDuro> ListarReportDedoDuro(string nomeProcedure, string idLoja, string dtIni, string dtFim);

        IList<DashboardEfetividadesVendedores> ListarDashboardEfetividadesVendedores(string nomeProcedure, string idLoja, string dtIni, string dtFim);

        IList<DashboardEfetividadesVendedores> ListarDashboardEfetividadesVendedores(string idLoja, string dtIni, string dtFim);

        IList<RelatorioEfetividadePorMes> ListarEfetividadePorMes(string nomeProcedure, string idLoja, int ano);

        IList<RelatorioVisitasLojas> ListarVisitasLojas(string nomeProcedure, string idLoja, string dtIni, string dtFim);

        IList<ReportRankingVendedores> ListarRankVendedores(string idLoja, string dtIni, string dtFim, int? gerente);

        IList<ReportRankingCornerMini> ListarRankingCornerMini(string idLoja, string dtIni, string dtFim, int? gerente);
        IList<ReportDedoDuro> ListarDedoDuro(string idLoja, string dtIni, string dtFim, int? gerente);

        IList<ReportEfetividadeAgendaLojas> ListarEfetividadesPorAgendaLojaVendedor(string idLoja, string dtIni, string dtFim, int IdAgenda, int? gerente);

        IList<T> ListarRelatorios<T>(string nomeProcedure, string idLoja, string dtIni, string dtFim, int? gerente);
        IList<T> ListarRelatorioComVendedor<T>(string nomeProcedure, string idVendedor);

    }
}
