﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Now.Domain.Contracts.Services
{
    public interface IUsuarioService
    {
        Usuario Autentica(string email, string senha);

        Usuario AutenticaDistribuicao(string email, string senha);        

        IList<UsuarioPerfil> ListarUsuarios(string IdsLojas = "");

        Usuario ExiteUsuario(string email);

        Usuario BuscarUsuarioPorCPF(string email);
        

        IList<Perfil> ListarPerfis();

        IList<SupervisorLojas> ListarSupervisores();

        Usuario BuscarUsuario(string id, string vendedorId);

        Usuario BuscarUsuarioPor(string token);

        void SalvaConfiguracoes(VendedoresConfiguracao configuracao);

        IList<Usuario> ListarUsuariosPorPerfil(string IdPerfil);

        void SalvarUsuario(Usuario usuario);

        void UpdateUsuario(Usuario usuario, bool alterarToken = true);

        void SalvarImgPerfil(string src, string idUsuario);

        void TrocarSenhaAgenda(string login, string senhaRecebida);        
        void TrocarSenha(string login, string senhaRecebida, string senhaNova, string senhaConfirmar);

        void AdicionarErro(string metodo, string tela, string erro, int? vendedorId, int? clienteId, int? lojaId, int? userId);

        void AdicionarLogAcesso(string browser, string login, string senha, string ip, int? acesso);

        void AdicionarLogSistema(LogSistema logSistema);
    }
}
