﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Now.Domain.Contracts.Services
{
    public interface IPerguntasQuemEleRespostasService
    {
        IList<PerguntasQuemEleRespostas> SalvarPerguntasQuemEleRespostas(PerguntasQuemEleRespostas perguntasQuemEleRespostas);
        IList<PerguntasQuemEleRespostas> ListarPerguntasQuemEleRespostas(int idPergunta);
        IList<PerguntasQuemEleRespostas> ListarTodasPerguntasQuemEleRespostas();
        IList<PerguntasQuemEleRespostas> DeletePerguntasQuemEleRespostas(int id, int idPergunta);        
    }
}
