﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Now.Domain.Contracts.Services
{
    public interface IOmniAcompanhamentoService
    {
        IList<OmniAcompanhamento> Listar(string IdLojaSeleconado, string vendedorId);

        void AtualizarStatusOmni(string pedido);
    }
}
