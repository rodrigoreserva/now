﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Now.Domain.Contracts.Services
{
    public interface IFraseService
    {
        IList<Frase> FrasesMotivoLigacao(string idcliente);
        IEnumerable<IGrouping<string, Frase>> FraseClienteDetalhes(string idcliente);        
    }
}
