﻿using System;
using System.Collections.Generic;

namespace Now.Domain
{
    public class UsuarioLogado
    {
        public string origemLoja { get; set; }
        public string IdLojaSeleconado { get; set; }
        public string IdVendedorSeleconado { get; set; }
        public IList<string> Canais { get; set; }
        public string CodigoClima { get; set; }

        public bool PermissaoAcesso { get; set; }
        public bool AcessoNovoDashBoard { get; set; }

        public int VisualizarEndereco { get; set; }

        public int RegraPesquisa { get; set; }
        private IList<Loja> _lojasUsuario { get; set; }
        public IList<Loja> LojasUsuario
        {
            get
            {
                return (this._lojasUsuario.Count > 1 && LojasUsuarioCanais != null) ? LojasUsuarioCanais : _lojasUsuario;

            }
            set { _lojasUsuario = value; }
        }
        public IList<Loja> LojasUsuarioCanais { get; set; }

        public IList<Loja> TodasLojasCanaisUsuario { get; set; }

        public IList<Loja> LojasSupervisores { get; set; }

        public IList<SupervisorLojas> SupervisorLojas { get; set; }

        /// <summary>
        /// Retorna as lojas do usuario logado disponíveis ou selecionada
        /// </summary>                
        public string IdsLojasUsuario
        {
            get
            {
                if (String.IsNullOrEmpty(IdLojaSeleconado))
                {

                    string idsLojas = String.Empty;

                    foreach (var item in LojasUsuario)
                        idsLojas += item.id.ToString() + ",";

                    //if (LojasUsuario.Count > 0)
                    //{
                    //    foreach (var item in LojasUsuario)
                    //        idsLojas += item.id.ToString() + ",";
                    //}
                    //else
                    //{
                    //    foreach (var item in LojasCanais)
                    //        idsLojas += item.id.ToString() + ",";
                    //}

                    idsLojas = idsLojas.TrimEnd(',');

                    return idsLojas;
                }
                else
                    return IdLojaSeleconado;
            }
        }

        /// <summary>
        /// Retorna as lojas do usuario logado disponíveis ou selecionada (Por Canal selecionado)
        /// </summary>                
        public string IdsLojasUsuarioCanais
        {
            get
            {
                if (String.IsNullOrEmpty(IdLojaSeleconado))
                {

                    string idsLojas = String.Empty;

                    foreach (var item in LojasUsuario)
                        idsLojas += item.id.ToString() + ",";

                    //foreach (var item in LojasCanais)
                    //    idsLojas += item.id.ToString() + ",";

                    idsLojas = idsLojas.TrimEnd(',');

                    return idsLojas;
                }
                else
                    return IdLojaSeleconado;
            }
        }

        /// <summary>
        /// Retorna as lojas do canal do usuario
        /// </summary>                
        public string IdsTodasLojasUsuario
        {
            get
            {
                string idsLojas = String.Empty;

                foreach (var item in TodasLojasCanaisUsuario)
                    idsLojas += item.id.ToString() + ",";

                //foreach (var item in LojasCanais)
                //    idsLojas += item.id.ToString() + ",";

                idsLojas = idsLojas.TrimEnd(',');

                return idsLojas;
            }
        }

        public IList<Vendedor> VendedoresUsuario { get; set; }

        public IList<MenuTipoAgendamento> TiposAgendas { get; set; }
        public Usuario Usuario { get; set; }
        // PA - Peças Por Atendimento
        public decimal? Pa { get; set; }
        // TM - Ticket Médio
        public decimal? Tm { get; set; }

        public IList<ReportClientesFavoritos> ListaReportClientesFavoritos { get; set; }

        public IList<DashboardRankingVendedores> ListaRankingVendedores { get; set; }

        public IList<OmniAcompanhamento> ListaOmniAcompanhamento { get; set; }

        public Dashboard DashboardMes { get; set; }

        public IList<ReportEfetividadeAgenda> ListaEfetividadeAgenda { get; set; }

        public IList<ReportEfetividade> ListaEfetividadeLojas { get; set; }

        public IList<DashboardEfetividadesVendedores> ListaEfetividadeVendedores { get; set; }

        public IList<DashboardGraficoEfetividade> ListaDashboardGraficoEfetividade { get; set; }
    }
}
