﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Now.Domain
{
    public class Lembrete
    {
        public int id { get; set; }
        public int loja_id { get; set; }
        public int cliente_id { get; set; }
        public DateTime? data_inicio { get; set; }
        public DateTime? data_final { get; set; }
        public string dica { get; set; }
        public string motivo { get; set; }
        public DateTime created_at { get; set; }
        public DateTime updated_at { get; set; }
        public DateTime? deleted_at { get; set; }
        public int status { get; set; }
        public int vendedorId { get; set; }

        //[NotMapped]
        public string cpf { get; set; }
        //[NotMapped]
        public string filial { get; set; }
        //[NotMapped]
        public string nomeVendedor { get; set; }
        //[NotMapped]
        public string nome_cliente { get; set; }
        //[NotMapped]
        public string titulo { get; set; }

        public string prametroAgenda { get; set; }
        public string tituloAgenda { get; set; }
        public int idAgenda { get; set; }        
    }
}