﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Now.Domain
{
    public class NowManutencao
    {
        public int id { get; set; }        
        public Int64 loja_id { get; set; }        
        public DateTime data_visita { get; set; }
        public string observacao { get; set; }       
        public string filial { get; set; }
    }
}

