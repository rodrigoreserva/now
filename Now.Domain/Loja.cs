﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Now.Common.Validation;

namespace Now.Domain
{    
    public class Loja
    {
        public int id { get; set; }
        public string id_loja { get; set; }
        public string filial  { get; set; }
        public string origem { get; set; }
        public string supervisor { get; set; }
        public string pais { get; set; }
        public string regiao { get; set; }
        public string endereco { get; set; }
        public string cidade { get; set; }
        public string bairro { get; set; }
        public string cep { get; set; }
        public string uf { get; set; }
        public string numero { get; set; }
        public string tipo_loja { get; set; }
        public string email { get; set; }
        public int visivel { get; set; }        
        public DateTime created_at { get; set; }
        public DateTime updated_at { get; set; }

        public int codigo_clima { get; set; } 
        public IList<Usuario> Supervisores { get; set; }
        public IList<Usuario> Gerentes { get; set; }
    }
}