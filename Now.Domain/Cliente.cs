﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Now.Domain
{
    public class Cliente
    {
        public int id { get; set; }
        public string id_cliente { get; set; }
        public string cpf { get; set; }
        public string nome { get; set; }
        public string tel_ddd { get; set; }
        public string tel { get; set; }
        public string cel_ddd { get; set; }
        public string cel { get; set; }
        public string tipo_log { get; set; }
        public string logradouro { get; set; }
        public string numero { get; set; }
        public string complemento { get; set; }
        public string bairro { get; set; }
        public string cidade { get; set; }
        public string uf { get; set; }
        public string cep { get; set; }
        public DateTime dt_nasc { get; set; }
        public string email { get; set; }
        public string sexo { get; set; }
        public DateTime dt_cadastro { get; set; }
        public string estado_civil { get; set; }
        public string tipo_varejo { get; set; }
        public string sem_credito { get; set; }
        public string origem_cliente { get; set; }
        public DateTime data_para_transferencia { get; set; }
        public string caracteristicas { get; set; }
        public string vendedor { get; set; }
        public DateTime created_at { get; set; }
        public DateTime updated_at { get; set; }
        public int loja_id { get; set; }
        public int loja_id_mini { get; set; }
        public int loja_id_eva { get; set; }
        public int vendedor_id { get; set; }
        public int vendedor_id_mini { get; set; }
        public int vendedor_id_eva { get; set; }
        public int loja_id_original { get; set; }
        public string perfil_reservado { get; set; }
        public string perfil_reservado_eva { get; set; }
        public string flag_experimentador { get; set; }
        public string classificacao_cliente { get; set; }
        public decimal AcumuladoVendaGeral { get; set; }
        public decimal AcumuladoVenda12 { get; set; }
        public decimal acumulado_12_reserva { get; set; }
        public decimal acumulado_12_mini { get; set; }
        public decimal acumulado_12_eva { get; set; }
        public decimal acumulado_total_reserva { get; set; }
        public decimal acumulado_total_mini { get; set; }
        public decimal acumulado_total_eva { get; set; }
        public int? qtde_retirada_loja { get; set; }
        public string obs { get; set; }
        public int apto_reservado_reserva { get; set; }
        public int apto_reservado_mini { get; set; }
        public int apto_reservado_eva { get; set; }
        public int cliente_favorito { get; set; }

        public string preferencia_contato { get; set; }


        public DateTime ult_compra { get; set; }

        public DateTime ult_contato { get; set; }
        public int? dias_ult_contato { get; set; }
        public int? dias_ult_contato_reserva { get; set; }

        public int? dias_ult_contato_mini { get; set; }

        public int? dias_ult_contato_eva { get; set; }
        public string NomeLoja { get; set; }

        public string NomeVendedor { get; set; }

        public string recomendacao { get; set; }

        public int IdLoja { get; set; }

        public int IdVendedor { get; set; }

        public string tooltipTelefone
        {
            get
            {
                return TratarToolTelefone(tel_ddd, tel, cel_ddd, cel);
            }
        }

        public string telefone
        {
            get
            {
                return TratarTelefone(tel_ddd, tel, cel_ddd, cel);
            }
        }        

        private string TratarTelefone(string tel_ddd, string tel, string cel_ddd, string cel)
        {
            string telefones = String.Empty;

            if (!String.IsNullOrEmpty(tel) || !String.IsNullOrEmpty(cel))
            {
                telefones = (!String.IsNullOrEmpty(tel) ? "(" + tel_ddd + ") " + tel : "") + (!String.IsNullOrEmpty(tel) && !String.IsNullOrEmpty(cel) ? "<br>" : "") + (!String.IsNullOrEmpty(cel) ? "(" + cel_ddd + ") " + cel : "");
            }
            else
                telefones = "-";

            return telefones;
        }

        private string TratarToolTelefone(string tel_ddd, string tel, string cel_ddd, string cel)
        {
            string telefones = String.Empty;

            if (!String.IsNullOrEmpty(tel) || !String.IsNullOrEmpty(cel))
            {
                telefones = (!String.IsNullOrEmpty(tel) ? "(" + tel_ddd + ") " + tel : "") + (!String.IsNullOrEmpty(tel) && !String.IsNullOrEmpty(cel) ? "," : "-") + (!String.IsNullOrEmpty(cel) ? "(" + cel_ddd + ") " + cel : "");
            }
            else
                telefones = "Não Existe";

            return telefones;
        }
    }
}

/*
     id	bigint
    id_cliente	varchar
    cpf	varchar
    nome	varchar
    tel_ddd	varchar
    tel	varchar
    cel_ddd	varchar
    cel	varchar
    tipo_log	varchar
    logradouro	varchar
    numero	varchar
    complemento	varchar
    bairro	varchar
    cidade	varchar
    uf	varchar
    cep	varchar
    dt_nasc	datetime
    email	varchar
    sexo	varchar
    dt_cadastro	datetime
    estado_civil	varchar
    tipo_varejo	varchar
    sem_credito	bit
    origem_cliente	varchar
    data_para_transferencia	datetime
    caracteristicas	text
    vendedor	varchar
    created_at	datetime
    updated_at	datetime
    loja_id	bigint
    loja_id_mini	bigint
    loja_id_eva	bigint
    loja_id_original	bigint
    perfil_reservado	char
    flag_experimentador	char
    classificacao_cliente	char
 */