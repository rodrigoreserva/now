﻿using System;
namespace Now.Domain
{
    public class ClienteDependenteDetalhe
    {
        public int id { get; set; }
        public int loja_id { get; set; }
        public int vendedor_id { get; set; }
        public DateTime data_nascimento { get; set; }
        public string cpf { get; set; }
        public string nome_cliente { get; set; }
        public string grau_parentesco { get; set; }
        public string sexo { get; set; }
        public string nome_dependente { get; set; }
        public int idade { get; set; }
        public string observacao { get; set; }
    }
}