﻿using System;

namespace Now.Domain
{
    
    [Serializable]
    public class Notificacao
    {
        public int id { get; set; }
        public string icone { get; set; }        
        public string titulo { get; set; }
        public string descricao { get; set; }
        public DateTime data_notificacao { get; set; }
        public DateTime? data_visto { get; set; }
        public string controler { get; set; }
        public string action { get; set; }
        public int loja_id { get; set; }
        public int vendedor_id { get; set; }

        public string data_visto_formatado
        {
            get
            {
                return data_visto?.ToString("dd/MM/yyyy");
            }
        }

        public int visto
        {
            get
            {
                return (data_visto == null) ? 0 : 1;
            }
        }

        public string imgVisto
        {
            get
            {
                return (data_visto == null) ? "iconCheckOff.png" : "iconCheckOn.png";
            }
        }

        public string minutos
        {
            get
            {
                if ((DateTime.Now - data_notificacao).Hours > 0)                
                    return (DateTime.Now - data_notificacao).ToString("hh") + " hrs";
                else
                    return (DateTime.Now - data_notificacao).ToString("ss") + " min";                
            }
        }


    }
}
