﻿using System;

namespace Now.Domain
{
    
    [Serializable]
    public class Supervisores
    {    
        public int id_supervisor { get; set; }
        public string nome_supervisor { get; set; }
        public int flg_selecao { get; set; }
    }
}
