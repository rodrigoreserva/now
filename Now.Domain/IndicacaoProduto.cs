﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Now.Common.Validation;

namespace Now.Domain
{
    public class IndicacaoProduto
    {
        public int id { get; set; }
        public int cliente_id { get; set; }
        public string nome_cliente { get; set; }
        public int loja_id { get; set; }
        public string nome_loja { get; set; }
        public int vendedor_id { get; set; }
        public string nome_vendedor { get; set; }
        public int cliente_respondeu { get; set; }
        public DateTime cliente_respondeu_data { get; set; }
        public IList<IndicacaoProdutoItens> lista_produtos { get; set; }
    }
}

