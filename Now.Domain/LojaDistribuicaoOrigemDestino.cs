﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Now.Common.Validation;

namespace Now.Domain
{    
    public class LojaDistribuicaoOrigemDestino
    {
        public int id { get; set; }
        public int origem_loja_id { get; set; }
        public int destino_loja_id { get; set; }
        public int total_distribuicao { get; set; } 
        public DateTime data_inclusao { get; set; }
        public DateTime? data_alteracao { get; set; }
        public DateTime? data_exclusao { get; set; }
    }
}