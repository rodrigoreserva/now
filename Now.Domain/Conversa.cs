﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Now.Domain
{    
    public class Conversa
    {
        public int id { get; set; }
        public int usuario_rem_id { get; set; }
        public int usuario_dest_id { get; set; }
        public int loja_id { get; set; }
        public int mensagem_id { get; set; }
        public DateTime? data_leitura { get; set; }
        public DateTime data_envio { get; set; }
    }
}