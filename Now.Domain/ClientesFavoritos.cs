﻿using System;
namespace Now.Domain
{
    public class ClientesFavoritos
    {
        public int cliente_favorito_id { get; set; }
        public int cliente_id { get; set; }
        public DateTime data_favorito { get; set; }
        public int vendedor_id { get; set; }
        public DateTime data_sem_contato { get; set; }
        public DateTime data_desativacao { get; set; }
        public string obs { get; set; } 
    }
}