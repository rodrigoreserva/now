﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Now.Domain.DTO
{
    [DataContract]
    public class ListaProdutosComercioDigitalDTO
    {
        [DataMember]
        public string texto { get; set; }
        [DataMember]
        public string valor { get; set; }
        [DataMember]
        public int quantidade { get; set; }
        [DataMember]
        public Boolean Selected { get; set; }
        [DataMember]
        public string grupo { get; set; }
    }

    [DataContract]
    public class CupomDesconto
    {
        [DataMember]
        public string parametro { get; set; }
        [DataMember]
        public string data_inicio { get; set; }
        [DataMember]
        public string data_final { get; set; }
    }

    
}
