﻿using Now.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Now.Domain.DTO
{
    [Serializable]
    public class InfoVendasDTO
    {
        public int qtd_info_vendas_disponiveis { get; set; }
        public int qtd_info_vendas_feitos { get; set; }
    }
}