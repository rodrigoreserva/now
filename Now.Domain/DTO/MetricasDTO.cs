﻿using Now.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Now.Domain.DTO
{
    [Serializable]
    public class MetricasDTO
    {
        public double vendas_mes { get; set; }
        public double pa_mes { get; set; }
        public double vm_mes { get; set; }
        public double efetividade_gerou_mes { get; set; }
        public double efetividade_vendeu_mes { get; set; }
        public int ranking_efetividade { get; set; }
        public int Ligacao_disponivel_pos_venda { get; set; }
        public int Ligacao_feita_pos_venda { get; set; }
        public double valor_delivery { get; set; }
        public double valor_corner_mini { get; set; }
        public double valor_corner_oficina { get; set; }
        public double valor_venda_reservado { get; set; }
        public double pa_reservado { get; set; }
        public double vm_reservado { get; set; }
        public int qtd_info_vendas_disponiveis { get; set; }
        public int qtd_info_vendas_feitos { get; set; }
        public int qtd_cuecas_vendidas { get; set; }

        public int qtd_malas_vendidas { get; set; }
        public int qtd_malas_enviadas { get; set; }

        public double vendas_hoje { get; set; }
        public double pa_hoje { get; set; }
        public double vm_hoje { get; set; }

    }
}