﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Now.Domain.DTO
{
    [DataContract]
    public class ProdutoDTO
    {
        [DataMember]
        public int produto_id { get; set; }
        [DataMember]
        public string produto { get; set; }
        [DataMember]
        public string cor_produto { get; set; }
        [DataMember]
        public string descricao_produto { get; set; }
        [DataMember]
        public string grade { get; set; }
        [DataMember]
        public string url { get; set; }
        [DataMember]
        public string griffe { get; set; }
        [DataMember]
        public DateTime atualizado_em { get; set; }
        [DataMember]
        public DateTime acessado_em { get; set; }

    }
}
