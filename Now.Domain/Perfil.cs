﻿using System;

namespace Now.Domain
{
    
    [Serializable]
    public class Perfil
    {
        public int id_perfil { get; set; }
        public string nome { get; set; }        
        public string descricao { get; set; }        
        public DateTime created_at { get; set; }
        public DateTime updated_at { get; set; }
        public DateTime data_ativacao { get; set; }
        public DateTime data_desativacao { get; set; }
        public bool rede { get; set; }
    }
}
