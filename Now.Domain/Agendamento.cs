﻿using System;

namespace Now.Domain
{
    public class Agendamento
    {
        public int id { get; set; }
        public int cliente_id { get; set; }
        public string nome { get; set; }
        public string cpf { get; set; }
        public string logradouro { get; set; }
        public string numero { get; set; }
        public string bairro { get; set; }
        public string cidade { get; set; }
        public string uf { get; set; }
        public string cep { get; set; }
        public string agendamento { get; set; }
        public string titulo_tipo_agendamento { get; set; }        
        public int vendedor_id { get; set; }
        public int loja_id { get; set; }
        public string filial { get; set; }
        public DateTime data_venda { get; set; }
        public decimal valor_venda { get; set; }
        public string data_carga { get; set; }
        public string nome_vendedor { get; set; }
        public string ticket_ultima_compra { get; set; }
        public string Total { get; set; }
        public string Quantidade { get; set; }
        public string dica_lembrete { get; set; }
        public string titulo_dica { get; set; }
        public string sexo { get; set; }
        public int tipo_agendamento_id { get; set; }
        public string observacao { get; set; }
        public int pos_venda_delivery { get; set; }
        public int apto_reservado { get; set; }
    }
}

/*
    cliente_id	    bigint
    nome	        varchar
    cpf         	varchar
    bairro	        varchar
    cidade	        varchar
    agendamento	    varchar
    vendedor_id	    int
    loja_id	        int
    filial	        varchar
    data_venda	    datetime
    valor_venda 	numeric
    data_carga	    varchar
 */