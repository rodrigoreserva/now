﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Now.Domain
{
    public class GrupoProdutoItem
    {

        public int IdCliente { get; set; }
        public string GrupoProduto { get; set; }
        public string Grife { get; set; }
        public decimal VendaAcumulado { get; set; }
        public int Quantidade { get; set; }
        public int QuantidadeVendas { get; set; }
    }
}
/*
 * v_VendasProdutosAgrupados
IdCliente	        bigint
GrupoProduto	    varchar
Grife	            varchar
VendaAcumulado      numeric
QuantidadeAcumulada	int
QuantidadeVendas	int
 */