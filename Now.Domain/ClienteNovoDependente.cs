﻿using System;
namespace Now.Domain
{
    public class ClienteNovoDependente
    {
        public int id { get; set; }
        public DateTime data_nascimento { get; set; }
        public string sexo { get; set; }
        public string nome { get; set; }
        public string grau_parentesco { get; set; }
        public string observacao { get; set; }
        public int cliente_id { get; set; }
        public DateTime data_inclusao { get; set; }
        public DateTime? data_alteracao { get; set; }
        public DateTime? data_desativacao { get; set; }
        public int? loja_id { get; set; }
        public int? vendedor_id { get; set; }
        public string str_data_nascimento { get { return data_nascimento.ToString("dd/MM/yyyy"); } }
        public int idade { get { return DateTime.Now.Year - data_nascimento.Year; } }
    }
}