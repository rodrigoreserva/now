﻿using System;

namespace Now.Domain
{
    
    [Serializable]
    public class SupervisorLojas
    {
        public int id_loja { get; set; }
        public string filial { get; set; }
        public int id_supervisor { get; set; }
        public string nome_supervisor { get; set; }

    }
}
