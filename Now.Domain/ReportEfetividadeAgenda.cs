﻿using System;
using System.ComponentModel;

namespace Now.Domain
{
    public class ReportEfetividadeAgenda
    {
        public string nome { get; set; }
        public int ranking { get; set; }     
        public string agendamento { get; set; }        
        public int contatos_efetuados { get; set; }        
        public int ligacoes_disponiveis { get; set; }        
        public float produtividade { get; set; }        
        public decimal valor_venda_now { get; set; }
        public decimal percentual { get; set; }
        public string cor { get; set; }
    }
}