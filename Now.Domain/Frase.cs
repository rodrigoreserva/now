﻿namespace Now.Domain
{
    public class Frase
    {
        public int id { get; set; }
        public string frase { get; set; }
        public int tipo_frase_id { get; set; }
        public string descricao_tipo_frase { get; set; }
        public int cliente_id { get; set; }
        public string griffe { get; set; }
        public int ordenacao { get; set; }
    }
}

/*
 * frases_ordenacao
id	bigint
frase	varchar
tipo_frase_id	int
descricao_tipo_frase	varchar
cliente_id	bigint
griffe	varchar
ordenacao	int
 */