﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Now.Domain
{

    public class InfoVendas
    {
        public int id { get; set; }        
        public Int64 loja_id { get; set; }
        public Int64 vendedor_id { get; set; }
        public Int64 cliente_id { get; set; }
        public DateTime data_registro { get; set; }
        public string info { get; set; }
        public DateTime created_at { get; set; }
        public DateTime updated_at { get; set; }
        public DateTime? deleted_at { get; set; }
        public int status { get; set; }

        //[NotMapped]
        public string cpf { get; set; }
        //[NotMapped]
        public string filial { get; set; }
        //[NotMapped]
        public string vendedor { get; set; }
        //[NotMapped]
        public string nome_cliente { get; set; }

        public object ClienteVendas { get; set; }

        public string _data_registro { get { return data_registro.ToString("dd/MM/yyyy"); } }
    }
}

