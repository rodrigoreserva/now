﻿using System;
using System.ComponentModel;

namespace Now.Domain
{
    public class RelatorioEfetividadePorMes
    {
        public string loja { get; set; }        
        public decimal janeiro { get; set; }
        public decimal fevereiro { get; set; }
        public decimal marco { get; set; }
        public decimal abril { get; set; }
        public decimal maio { get; set; }
        public decimal junho { get; set; }
        public decimal julho { get; set; }
        public decimal agosto { get; set; }
        public decimal setembro { get; set; }
        public decimal outubro { get; set; }
        public decimal novembro { get; set; }
        public decimal dezembro { get; set; }
        public decimal media { get; set; }
    }
}