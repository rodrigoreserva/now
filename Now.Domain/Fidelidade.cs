﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Now.Domain
{
    /// <summary>
    /// Classe não vinculada ao banco, mas utilizada pra criar uma visão de venda
    /// </summary>
    public class Fidelidade
    {
        public DateTime DataVencimento { get; set; }

        public string Pontos { get; set; }

        public double Valor { get; set; }
    }
}
