﻿using System;

namespace Now.Domain
{
    public class ContatoClienteAvaliacao
    {
        public int id { get; set; }
        public DateTime created_at { get; set; }
        public DateTime? updated_at { get; set; }
        public string obs { get; set; }
        public int cliente_id { get; set; }
        public int vendedor_id { get; set; }
        public string status { get; set; }
        public string termometro { get; set; }
        public string modo { get; set; }
        public DateTime? deleted_at { get; set; }
        public string tipo { get; set; }
        public bool flagReservado { get; set; }
        public DateTime? dt_Reservado { get; set; }
        public string agendamento { get; set; }
        public string caracteristicas { get; set; }
        public int tipo_agendamento_id { get; set; }
        public int loja_id { get; set; }
        public DateTime? data_criacao { get; set; }
        
             public string img_perfil_intranet { get; set; }
        public string LojaNome { get; set; }        
        public string VendedorNome { get; set; }        
        public string AgendaNome { get; set; }
        public string tipo_loja { get; set; }
        public int avaliado { get; set; }
        public int flg_contato_feito { get; set; }        
        public string clienteNome { get; set; }
        public string clienteEmail { get; set; }
        public string url_avaliacao { get; set; }
    }
}

/*
 * Tabela - Contatos
    id	            bigint
    created_at	    datetime
    updated_at	    datetime
    obs	            nvarchar
    cliente_id	    bigint
    vendedor_id     bigint
    status	        nvarchar
    termometro	    nvarchar
    modo        	nvarchar
    deleted_at	    datetime
    tipo	        varchar
    flagReservado	bit
    dt_Reservado	datetime
    agendamento	    varchar
    caracteristicas	nvarchar
 */