﻿using System;

namespace Now.Domain
{
    public class ContatoAtg
    {
        public string cpf { get; set; }
        public string status { get; set; }
        public string modo { get; set; }
        public string termometro { get; set; }
        public string quem_ligou { get; set; }
        public string quem_ele { get; set; }
        public string obs_ligacao { get; set; }       
        public DateTime data_cadastro { get; set; }
    }
}

