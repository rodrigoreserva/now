﻿using System;
namespace Now.Domain
{
    public class VendedoresControle
    {
        public int id { get; set; }
        public int vendedor_id { get; set; }
        public int loja_id { get; set; }
        public DateTime vigencia_inicio { get; set; }
        public DateTime vigencia_fim { get; set; }        
        public int vendedores_controle_tipo_id { get; set; }
        public DateTime data_inclusao { get; set; }
        public DateTime data_alteracao { get; set; }
        public DateTime data_exclusao { get; set; }      
    }
}