﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Now.Common.Validation;

namespace Now.Domain
{
    public class AbandonoCarrinhoNowLinxSolicitacao
    {
        public DateTime data_experimente { get; set; }
        public string codigo_filial { get; set; }
        public string filial { get; set; }
        public string endereco { get; set; }
        public string periodo { get; set; }
        public DateTime data { get; set; }
        public IList<AbandonoCarrinhoClienteProduto> produtos_experimente { get; set; }       
    }
}

