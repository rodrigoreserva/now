﻿using System;
namespace Now.Domain
{
    public class VendedoresLicenciados
    {
        public int id { get; set; }
        public int vendedor_id { get; set; }
        public int loja_id { get; set; }
        public DateTime? data_inicio_bloqueio { get; set; }
        public DateTime? data_fim_bloqueio { get; set; }
        public int flg_bloqueio_indeterminado { get; set; }
        public DateTime data_inclusao { get; set; }
        public DateTime data_alteracao { get; set; }
        public DateTime? data_desativacao { get; set; }
        public int vendedor_licenciados_tipo_id { get; set; }
        public string vendedor_licenciados_tipo { get; set; }
        public string motivo { get; set; }
        public bool isExpirado
        {
            get
            {
                bool sExpirado = false;

                if (data_desativacao == null)
                {
                    if (data_inicio_bloqueio != null && data_fim_bloqueio != null)
                    {
                        if (data_inicio_bloqueio.Value <= DateTime.Today && data_fim_bloqueio.Value >= DateTime.Today)
                            sExpirado = true;
                        else if (data_inicio_bloqueio.Value >= DateTime.Today)
                            sExpirado = true;                        
                    }                        
                    else if (data_fim_bloqueio == null)
                        sExpirado = true;                                                                
                }

                return sExpirado;
            }
        }

        public string data_inicio_bloqueio_formatada
        {
            get
            {
                return data_inicio_bloqueio != null ? data_inicio_bloqueio.Value.ToString("dd/MM/yyyy") : "-";
            }
        }

        public string data_fim_bloqueio_formatada
        {
            get
            {
                return data_fim_bloqueio != null ? data_fim_bloqueio.Value.ToString("dd/MM/yyyy") : "-";
            }
        }       

        public string status
        {
            get
            {
                string _status;

                if (data_desativacao == null)
                {
                    if (data_inicio_bloqueio != null && data_fim_bloqueio != null)
                    {
                        if (data_inicio_bloqueio.Value <= DateTime.Today && data_fim_bloqueio.Value >= DateTime.Today)
                            _status = "<span class='label label-info'>Em Andamento</span>";
                        else if (data_inicio_bloqueio.Value >= DateTime.Today)
                            _status = "<span class='label label-warning'>Agendado</span>";
                        else _status = "<span class='label label-default'>Finalizado</span>";
                    }
                    else if (data_fim_bloqueio == null)
                    {
                        _status = "<span class='label label-info'>Em Andamento</span>";
                    }
                    else
                        _status = "<span class='label label-default'>Finalizado</span>";
                }
                else _status = "<span class='label label-danger'>Desativado</span>";

                return _status;
            }
        }

        public string tempo
        {
            get
            {
                string _tempo;

                if (flg_bloqueio_indeterminado == 0)                
                    _tempo = "<p class='text-green'>Por Período</p>";
                else
                    _tempo = "<p class='text-green'>Indetermidado</p>";

                return _tempo;
            }
        }
    }
}