﻿using System;

namespace Now.Domain
{   
    public class PedidosRetiradaLoja
    {
        public int id { get; set; }
        public string pedido { get; set; }
        public DateTime data_compra { get; set; }
        public string status_pedido { get; set; }
        public string recebido_por { get; set; }
        public DateTime? recebido_em { get; set; }
        public string entregue_por { get; set; }
        public DateTime? entregue_em { get; set; }
        public DateTime data_carga { get; set; }
        public int cliente_id { get; set; }       
        public int quantidade { get; set; }
        public int loja_id { get; set; }
        public string order_id { get; set; }
        public string filial { get; set; }
        public decimal valor_total { get; set; }
        public decimal valor_venda_retirada { get; set; }
    }
}
