﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Now.Domain
{   
    public class LogTransferenciaAgenda
    {
        public int id { get; set; }
        public int loja_id { get; set; }
        public string filial { get; set; }
        public int vendedor_id_origem { get; set; }
        public string nome_vendedor_origem { get; set; }
        public int vendedor_id_destino { get; set; }
        public string nome_vendedor_destino { get; set; }
        public string cpf { get; set; }
        public string cliente_nome { get; set; }        
        public int cliente_id { get; set; }
        public int tipo_agendamento_id { get; set; }
        public int vendedor_id_transferencia { get; set; }
        public string nome_vendedor_transferencia { get; set; }
        public string nome_agenda { get; set; }
        public DateTime data { get; set; }

        public string data_formatada { get {
                return data.ToString("dd/MM/yyyy HH:mm");
            } }
        
        public DateTime data_transferencia { get; set; }
        public DateTime? data_cancelamento { get; set; }
        public string status { get; set; }

      
    }
}
