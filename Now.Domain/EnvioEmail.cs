﻿using System;
using System.Collections.Generic;

namespace Now.Domain
{
    public class EnvioEmail
    {
        public int id { get; set; }

        public int id_user { get; set; }

        public string Nome { get; set; }
        public string email { get; set; }
        //[NotMapped]
        public IList<string> email_copia  { get; set; }
        //[NotMapped]
        public IList<int> lojas { get; set; }       
    }
}