﻿using System;
using System.ComponentModel;

namespace Now.Domain
{
    public class ReportRankingCornerOficina
    {
        public string ranking { get; set; }
        public string loja { get; set; }
        public string vendedor { get; set; }      
        public decimal valor { get; set; }
        public decimal valor_total { get; set; }
        public decimal corner_oficina { get; set; }
        public decimal valor_venda_now { get; set; }
        public decimal valor_contato_now { get; set; }
    }
}