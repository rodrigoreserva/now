﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Now.Domain
{
    public class NowControleCarga
    {
        public int id { get; set; }                
        public DateTime data_carga { get; set; }
        public string status { get; set; }       
        public string descricao { get; set; }
    }
}

