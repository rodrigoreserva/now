﻿using System;

namespace Now.Domain
{
    public class ContatoCliente
    {
        public int vendedor_id { get; set; }
        public DateTime created_at { get; set; }
        public DateTime? updated_at { get; set; }
        public string nome { get; set; }
        public string email { get; set; }
        public string cpf { get; set; }
        public string telefone { get; set; }
        public string flg_contato_enviado { get; set; }
    }
}
