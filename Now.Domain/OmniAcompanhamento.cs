﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Now.Domain
{

    public class OmniAcompanhamento
    {
        public int id { get; set; }        
        public Int64 loja_id { get; set; }
        public Int64 vendedor_id { get; set; }
        public Int64 cliente_id { get; set; }
        public string cpf { get; set; }
        public string pedido { get; set; }
        public DateTime pedido_data { get; set; }
        public int quatidade { get; set; }
        public decimal valor { get; set; }          
        public string status_pedido { get; set; }
        public int visto_flag { get; set; }
        public DateTime visto_data { get; set; }        
        public string filial { get; set; }        
        public string vendedor { get; set; }
        public string nome_cliente { get; set; }        
    }
}

