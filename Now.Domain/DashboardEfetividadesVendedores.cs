﻿using System;
using System.ComponentModel;

namespace Now.Domain
{
    public class DashboardEfetividadesVendedores
    {   
        public string ranking { get; set; }
        public Int32 loja_id { get; set; }
        public string loja { get; set; }      
        public Int32 vendedor_id { get; set; }
        public string vendedor { get; set; } 
                       
        public int lig_efetuadas { get; set; }        
        public int LigDisponiveis { get; set; }        
        public decimal efetividade { get; set; }
        public decimal produtividade { get; set; }       

    }
}