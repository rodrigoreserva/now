﻿using System;
using System.ComponentModel;

namespace Now.Domain
{
    public class RelatorioCompreRetireRankLojas
    {
        public string loja { get; set; }
        
        public string supervisor { get; set; }
        public decimal vlr_compra_retirada { get; set; }
        public int qtde_retire_loja { get; set; }
        public int qtde_retire_loja_compra { get; set; }
        public decimal aproveitamento { get; set; }        

    }
}