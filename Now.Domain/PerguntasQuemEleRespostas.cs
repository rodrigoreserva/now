﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Now.Domain
{    
    public class PerguntasQuemEleRespostas
    {
        public int id { get; set; }
        public string resposta { get; set; }        
        public int id_perguntas_quem_ele { get; set; }
        public int? id_perguntas_quem_ele_subsequente { get; set; }        
        public DateTime data_inclusao { get; set; }
        public DateTime? data_alteracao { get; set; }
        public DateTime? data_desativacao { get; set; }
        public string perg_subsequente { get; set; }
    }
}