﻿using System;
using System.ComponentModel;

namespace Now.Domain
{
    public class ReportEfetividade
    {
        [Description(""), Category("")]
        public int ranking { get; set; }
        public int loja_id { get; set; }
        public string loja { get; set; }
        public string supervisor { get; set; }
        [Description("EFETIVIDADE"), Category("{0:C}")]
        public decimal efetividade { get; set; }
        public string _strEfetividade { get { return String.Format("{0:C}", efetividade); } }
        public decimal efetividade_sssg { get; set; }
        public string _strEfetividade_sssg { get { return String.Format("{0:P}", efetividade_sssg); } }
        public decimal valor_linx { get; set; }
        public decimal valor_now { get; set; }
        public string _strValor_now { get { return String.Format("{0:C}", valor_now); } }
        public decimal contato_direto { get; set; }
        public string _strContato_direto { get { return String.Format("{0:C}", contato_direto); } }
        public decimal perc_efetividade { get; set; }
        public string _strPerc_efetividade { get { return String.Format("{0:P}", perc_efetividade); } }

        public string _perc_crescimento
        {
            get
            { return String.Format("{0:P}", (efetividade_sssg == 0 ? 0 : (efetividade / efetividade_sssg) - Convert.ToDecimal(1))); }
        }

        public decimal perc_crescimento
        {
            get
            { return (efetividade_sssg == 0 ? 0 : (efetividade / efetividade_sssg) - Convert.ToDecimal(1)); }
        }
    }
}