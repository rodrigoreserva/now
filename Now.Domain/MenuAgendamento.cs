﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Now.Domain
{
    public class MenuAgendamento
    {
        public int id { get; set; }
        public string parametro { get; set; }
        public string titulo { get; set; }
        public int qtdeTotal { get; set; }
        public int prioridade { get; set; }        
        public int qtdeDisponivel { get; set; }
        public int qtdeLigacoes { get; set; }
        public decimal percentual { get; set; }    
        public string percentualDesc
        {
            get
            {
                return String.Format("{0:P}", percentual);
            }
        }        
    }
}
