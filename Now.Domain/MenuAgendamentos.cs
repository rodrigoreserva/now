﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Now.Domain
{
    public class MenuAgendamentos
    {

        public int id { get; set; }
        public int cliente_id { get; set; }
        public string nome { get; set; }
        public string cpf { get; set; }
        public string bairro { get; set; }
        public string cidade { get; set; }
        public int vendedor_id { get; set; }
        public int loja_id { get; set; }
        public string filial { get; set; }
        public DateTime data_venda { get; set; }
        public decimal valor_venda { get; set; }
        public DateTime data_carga { get; set; }
        public string nome_vendedor { get; set; }
        public int tipo_agendamento_id { get; set; }
        public string dica_lembrete { get; set; }
    }
}
