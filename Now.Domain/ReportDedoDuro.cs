﻿using System;
using System.ComponentModel;

namespace Now.Domain
{
    public class ReportDedoDuro
    {   
        [Description("supervisor"), Category("")]
        public string supervisor { get; set; }
        [Description("loja"), Category("")]
        public string loja { get; set; }
        [Description("vendedor"), Category("")]
        public string vendedor { get; set; }
        [Description("agendamento"), Category("")]
        public string agendamento { get; set; }
        [Description("Ligações Disponíveis"), Category("")]
        public int ligacoes_disponiveis { get; set; }
        [Description("Telefone Inválido"), Category("")]
        public int tel_invalido { get; set; }
        [Description("Não conseguiu falar"), Category("")]
        public int nao_conseguiu_falar { get; set; }
        [Description("Contatos Efetuados"), Category("")]
        public int contatos_efetuados { get; set; }
        [Description("Baixa de outro vendedor"), Category("")]
        public int baixa_outro_vendedor { get; set; }
        [Description("Produtividade"), Category("")]
        public decimal produtividade { get; set; }        
        [Description("Total Supervisor"), Category("")]
        public Int32 total_supervisor { get; set; }
        [Description("Total Loja"), Category("")]
        public Int32 total_loja { get; set; }
        [Description("Total Contatos Vendedor"), Category("")]
        public Int32 total_contatos_vendedor { get; set; }
        [Description("Período"), Category("")]
        public string periodo { get; set; }
        [Description("valor_venda_now"), Category("")]
        public decimal valor_venda_now { get; set; }        
    }
}