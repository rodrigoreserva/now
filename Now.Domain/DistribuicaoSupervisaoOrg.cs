﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Now.Common.Validation;

namespace Now.Domain
{    
    public class DistribuicaoSupervisaoOrg
    {        
        public string filial  { get; set; }      
        public string supBrasil { get; set; }
        public string supRegional { get; set; }
        public string gerente { get; set; }        
    }
}