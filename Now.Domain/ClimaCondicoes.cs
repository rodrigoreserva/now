﻿using System;
namespace Now.Domain
{
    public class ClimaCondicoes
    {
        public string codigo { get; set; }
        public string titulo { get; set; }    
        public string descricao { get; set; }       
    }
}