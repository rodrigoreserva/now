﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Now.Domain
{
    /// <summary>
    /// Classe não vinculada ao banco, mas utilizada pra criar uma visão de venda
    /// </summary>
    public class Venda
    {

        /// <summary>
        /// Id da venda
        /// </summary>
        public int Ticket { get; set; }

        /// <summary>
        /// Nome da loja que efetuou a venda
        /// </summary>
        public string NomeLoja { get; set; }

        /// <summary>
        /// Id da loja que efetuou a venda
        /// </summary>
        public int IdLoja { get; set; }

        public string tipo_loja { get; set; }

        /// <summary>
        /// Nome do vendedor que efetuou a venda
        /// </summary>
        public string NomeVendedor { get; set; }

        public int IdVendedor { get; set; }

        /// <summary>
        /// Qual a "campanha" que gerou essa venda
        /// </summary>
        public string Operacao { get; set; }

        /// <summary>
        /// A data da venda
        /// </summary>
        public DateTime Data { get; set; }

        /// <summary>
        /// Valor total da venda
        /// </summary>
        public decimal Total { get; set; }

        /// <summary>
        /// Valor total da Desconto
        /// </summary>
        public decimal TotalDesconto { get; set; }

        /// <summary>
        /// Quantidade total da venda
        /// </summary>
        public int Quantidade { get; set; }

        /// <summary>
        /// Descrição compra retire na loja
        /// </summary>
        public string retirada_loja { get; set; }


        public VendedorGerou vendedorGerou { get; set; }
        /// <summary>
        /// Lista dos produtos dessa venda
        /// </summary>
        public IList<VendaItem> Itens { get; set; }


        public IList<InfoVendas> InfosVenda { get; set; }
    }
}
