﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Now.Domain
{    
    public class ConversaMensagem
    {
        public int id { get; set; }
        public string mensagem { get; set; }
        public DateTime? data_exclusao { get; set; }        
    }
}