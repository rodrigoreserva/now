﻿using System;
using System.ComponentModel;

namespace Now.Domain
{
    public class ReportRankingVendedores
    {
        [Description(""), Category("")]
        public string ranking { get; set; }
        [Description("LOJA"), Category("")]
        public string loja { get; set; }
        //[Description("SUPERVISOR"), Category("")]
        //public string supervisor { get; set; }
        [Description("VENDEDOR"), Category("")]
        public string vendedor { get; set; }
        public decimal pa { get; set; }
        public decimal vm { get; set; }
        public decimal total_vendas { get; set; }
        public decimal total_tickets { get; set; }
        public decimal valor_total_vendas { get; set; }

        [Description("VALOR"), Category("{0:C}")]
        public decimal valor { get; set; }
        [Description("VENDEU"), Category("{0:C}")]

        public int qtde_venda_clientes { get; set; }
        public int qtde_venda_pecas { get; set; }
        public decimal valor_venda_now { get; set; }

        public int qtde_contato_clientes { get; set; }
        public int qtde_contato_pecas { get; set; }
        public decimal valor_contato_now { get; set; }

        public int qtde_contato_venda_clientes { get; set; }
        public int qtde_contato_venda_pecas { get; set; }
        public decimal valor_contato_venda_now { get; set; }
    }
}