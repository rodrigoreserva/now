﻿using System;
using System.ComponentModel;

namespace Now.Domain
{
    public class ReportEcommerceRankingRitireLoja
    {
        [Description("Ranking"), Category("")]
        public string Ranking { get; set; }
        [Description("Loja"), Category("")]
        public string loja { get; set; }
        [Description("VENDEDOR"), Category("")]
        public string nome_vendedor { get; set; }
        
        [Description("Qtde Pedidos"), Category("")]
        public int qtde_pedidos { get; set; }
        [Description("VALOR"), Category("{0:C}")]
        public decimal Valor_Venda { get; set; }        
    }
}