﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Now.Domain
{
    public class FraseContato
    {
        public int id { get; set; }
        public int cliente_id { get; set; }
        public DateTime data_contato { get; set; }
        public string termometro { get; set; }
        public string status { get; set; }
        public string modo { get; set; }
        public string obs { get; set; }
        public string quem_ele { get; set; }
        public string modo_icone { get; set; }        
        public string loja { get; set; }
        public string agendamento { get; set; }
        public string vendedor_contato { get; set; }
        public string vendedor_venda { get; set; }
        public string tipo_loja { get; set; }
        public string tipo { get; set; }
        public string descricao { get; set; }
        public int avaliacao { get; set; }
        public string img_perfil_intranet { get; set; }
        public DateTime data_alteracao { get; set; }
        public string texto_automatizado { get; set; }
        public string cliente_nome { get; set; }
        public string cliente_sexo { get; set; }
        public int? flg_contato_feito { get; set; }
        public IndicacaoProduto ListaProdutos { get; set; }
    }
}

