﻿using System;
using System.ComponentModel;

namespace Now.Domain
{
    public class ReportDedoDuroEmail
    {

        [Description("loja"), Category("")]
        public string loja { get; set; }
        [Description("vendedor"), Category("")]
        public string vendedor { get; set; }
        [Description("agendamento"), Category("")]

        public int ligacoes_disponiveis { get; set; }
        [Description("Contatos Efetuados"), Category("")]
        public int contatos_efetuados { get; set; }
        [Description("Produtividade"), Category("")]
        public decimal produtividade { get; set; }

    }
}