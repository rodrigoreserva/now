﻿using System;

namespace Now.Domain
{
    public class Metricas
    {
        // PA - Peças Por Atendimento
        public decimal Pa { get; set; }
        // TM - Ticket Médio
        public decimal Tm { get; set; }
    }
}
