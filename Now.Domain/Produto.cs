﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Now.Common.Validation;

namespace Now.Domain
{
    public class Produto
    {
        public int id { get; set; }
        public string produto { get; set; }
        public string cor_produto { get; set; }
        public string descricao_produto { get; set; }
        public string grupo_produto { get; set; }
        public string imgSite
        {
            get
            {
                return "https://imagens.usereserva.com.br/images/853x1280/" + produto + cor_produto + "_{posicao}.jpg?ims={tamanho}";
            }

        }

    }


}
