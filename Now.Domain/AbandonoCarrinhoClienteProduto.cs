﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Now.Common.Validation;

namespace Now.Domain
{
    public class AbandonoCarrinhoClienteProduto
    {
        public int id { get; set; }        
        public string cpf { get; set; }
        public string cliente_nome { get; set; }
        public int produto_id { get; set; }
        public string produto { get; set; }
        public string cor_produto { get; set; }
        public string descricao_produto { get; set; }
        public string grade { get; set; }
        public string tamanho { get; set; }
        public string sku { get; set; }
        public decimal preco { get; set; }
        public int cliente_escolheu { get; set; }        
        public DateTime data_abandono { get; set; }
        public DateTime data_cliente { get; set; }        
        public IList<AbandonoCarrinhoClienteProdutoEstoque> listEstoque { get; set; }
        public string preco_format { get { return String.Format("{0:C}", preco); } }
        public string imgSite
        {
            get
            {
                return "https://imagens.usereserva.com.br/images/853x1280/" + produto + cor_produto + "_{posicao}.jpg?ims={tamanho}";
            }

        }
    }
}