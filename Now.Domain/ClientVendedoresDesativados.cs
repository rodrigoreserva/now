﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Now.Domain
{
    public class ClientVendedoresDesativados
    {
        public int cliente_id { get; set; }
        public string vendedor { get; set; }
        public string cliente { get; set; }
        public string cpf { get; set; }
        public string bairro { get; set; }        
        public decimal AcumuladoVenda12 { get; set; }        
    }
}
