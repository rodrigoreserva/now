﻿using System;
namespace Now.Domain
{
    public class VendedoresLicenciadosTipo
    {
        public int id { get; set; }
        public string nome { get; set; }        
        public DateTime data_inclusao { get; set; }
        public DateTime data_alteracao { get; set; }
        public DateTime? data_desativacao { get; set; }        
    }
}