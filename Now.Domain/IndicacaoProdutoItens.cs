﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Now.Common.Validation;

namespace Now.Domain
{
    public class IndicacaoProdutoItens
    {
        public int id { get; set; }
        public int cliente_indicacao_id { get; set; }
        public int produto_id { get; set; }
        public string _produto { get; set; }
        public string cor_produto { get; set; }
        public string grade { get; set; }
        public int cliente_escolheu { get; set; }
        public string observacao { get; set; }
        public string sku { get; set; }
        public Produto produto { get; set; }
        public DateTime data_cliente { get; set; }
    }
}

