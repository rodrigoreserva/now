﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Now.Common.Validation;

namespace Now.Domain
{    
    public class LojaDistribuicaoPerfil
    {
        public int id { get; set; }        
        public string filial  { get; set; }      
        public int idGerente { get; set; }
        public int idRegional { get; set; }
        public int idBrasil { get; set; }
        public int QtdeGerente { get; set; }
        public int QtdeRegional { get; set; }
        public int QtdeBrasil { get; set; }
        public string status { get; set; }
    }
}