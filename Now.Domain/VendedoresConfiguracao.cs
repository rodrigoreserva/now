﻿using System;
namespace Now.Domain
{
    public class VendedoresConfiguracao
    {
        public int id { get; set; }
        public int loja_id { get; set; }
        public int vendedor_id { get; set; }
        public int flg_gerente { get; set; }
        public int flg_vendedor { get; set; }
        public int flg_operador_caixa { get; set; }
        public int flg_bloqueado_now { get; set; }
        public DateTime data_inclusao { get; set; }
        public DateTime data_alteracao { get; set; }
        public DateTime data_desativacao { get; set; }        
    }
}