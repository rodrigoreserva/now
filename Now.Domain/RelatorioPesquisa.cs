﻿using System;
using System.ComponentModel;

namespace Now.Domain
{
    public class RelatorioPesquisa
    {
        public int loja_id { get; set; }

        public int[] lojaIds { get; set; }

        public string lojasId { get; set; }
        public int ano { get; set; }
        public int gerente { get; set; }
        public int vendedor_id { get; set; }
        public string data_inicial { get;  set; }
        public string data_final { get; set; }
        public string procedure { get; set; }        
    }
}