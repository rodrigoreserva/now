﻿using System;
using System.ComponentModel;

namespace Now.Domain
{
    public class RelatorioVisitasLojas
    {
        public string loja { get; set; }

        public DateTime data_visita { get; set; }
        public decimal mes_anterior { get; set; }
        public decimal mes_corrente { get; set; }
        public decimal mes_0 { get; set; }
        public decimal mes_1 { get; set; }
        public decimal mes_2 { get; set; }
        public decimal mes_3 { get; set; }
        public decimal mes_4 { get; set; }
        public decimal mes_5 { get; set; }
        public decimal mes_6 { get; set; }
        public decimal mes_7 { get; set; }
        public decimal mes_8 { get; set; }
        public decimal mes_9 { get; set; }
        public decimal mes_10 { get; set; }
        public decimal mes_11 { get; set; }
        public decimal mes_12 { get; set; }        
    }
}