﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Now.Common.Validation;

namespace Now.Domain
{
    public class ClienteReservadoAgendada
    {  
        public string cliente_nome { get; set; }
        public string cpf { get; set; }
        public string telefone { get; set; }
        public string cpf_alterado { get; set; }
        public string telefone_alterado { get; set; }
        public int loja_id { get; set; }
        public string id_loja { get; set; }
        public string endereco { get; set; }
        public string cidade { get; set; }
        public string bairro { get; set; }
        public string uf { get; set; }
        public int solicitou { get; set; }
        public string filial { get; set; }
        public string tipo_loja_adulto { get; set; }
        public string tipo_loja_mini { get; set; }
    }
}