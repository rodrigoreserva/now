﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Now.Common.Validation;

namespace Now.Domain
{    
    public class VendedorGerou
    {
        public int vendedor_id { get; set; }
        public string nome_vendedor { get; set; }
        public string ticket { get; set; }
        public DateTime data_ligacao { get; set; }
        public DateTime data_venda { get; set; }
        public string nome_agenda { get; set; }
        public decimal valor_gerou { get; set; }
        public string img_perfil_intranet { get; set; }       

    }
}