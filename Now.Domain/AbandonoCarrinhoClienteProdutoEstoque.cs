﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Now.Common.Validation;

namespace Now.Domain
{
    public class AbandonoCarrinhoClienteProdutoEstoque
    {
        public string skui { get; set; }        
        public string produto { get; set; }
        public string cor_produto { get; set; }
        public decimal preco { get; set; }
        public string grade { get; set; }
        public int disponivel { get; set; }

        public string preco_format { get { return String.Format("{0:C}", preco); } }

        public string imgCor
        {
            get
            {
                return "https://www.usereserva.com/file/general/" + cor_produto.TrimEnd() + ".jpg";
            }

        }
    }
}