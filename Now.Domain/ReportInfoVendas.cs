﻿using System;
using System.ComponentModel;

namespace Now.Domain
{
    public class ReportInfoVendas
    {
        [Description("LOJA"), Category("")]

        public int loja_id { get; set; }
        public string loja { get; set; }
        [Description("VENDEDOR"), Category("")]
        public int vendedor_id { get; set; }
        public string vendedor { get; set; }
        [Description("INFO VENDAS"), Category("")]
        public int total_info { get; set; }
        [Description("LIGAÇÕES PÓS-VENDAS DISPONÍVEIS"), Category("")]
        public int total_lig { get; set; }        
    }
}