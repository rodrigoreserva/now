﻿using System;
namespace Now.Domain
{
    public class Clima
    {
        public string cidade { get; set; }
        public string uf { get; set; }
        public DateTime dia { get; set; }
        public string tempo { get; set; }
        public string maxima { get; set; }
        public string minima { get; set; }
        public string iuv { get; set; }
        public string titulo { get; set; }
        public string descricao {get; set;}
        public string imagem { get { return string.Format("../../Content/img_tempo/{0}.png", tempo); } }
    }
}