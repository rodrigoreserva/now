﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Now.Domain
{
    public class VendaReservado
    {
        public IList<Venda> VendasReservadoRESERVA { get; set; }
        public IList<Venda> VendasReservadoMINI { get; set; }
        public IList<Venda> VendasReservadoEVA { get; set; }
    }
}
