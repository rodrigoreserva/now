﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Now.Common.Validation;

namespace Now.Domain
{
    public class AbandonoCarrinhoNowLinx
    {
        public int id { get; set; }
        public string cpf { get; set; }
        public string produto { get; set; }
        public string cor_produto { get; set; }
        public string codigo_filial { get; set; }
        public string grade { get; set; }
        public string solicitado { get; set; }
    }
}

