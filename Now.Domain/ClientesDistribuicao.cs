﻿using System;
namespace Now.Domain
{
    public class ClientesDistribuicao
    {
        public string cliente_id { get; set; }
        public string cpf { get; set; }
        public string nome { get; set; }        
        public string perfil_reservado { get; set; }
        public string filial { get; set; }
        public string griffe { get; set; }
        public string vendedor { get; set; }
        public string classificacao_cliente { get; set; }
        public decimal AcumuladoVenda12 { get; set; }
        public decimal AcumuladoVendaGeral { get; set; }
        public string aprovacao_gerente { get; set; }
        public string obs { get; set; }
        public string vendedor_gerente { get; set; }
        public string vendedor_antigo { get; set; }
        public int lig_total_vend_antigo { get; set; }
    }
}