﻿using System;

namespace Now.Domain
{
    public class DashboardGraficoEfetividade
    {
        public int dia { get; set; }
        public decimal media_perc_efet_vendedor { get; set; }
        public decimal media_perc_efet_loja { get; set; }
        public decimal media_perc_efet_empresa { get; set; }
    }
}
