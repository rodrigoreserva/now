﻿using System;
using System.ComponentModel;

namespace Now.Domain
{
    public class ReportClientesFavoritos
    {
        public int cliente_id { get; set; }
        public string nome { get; set; }       
        public string cpf { get; set; }        
        public string bairro { get; set; }
        public int acum12_qtde_compras { get; set; }
        public decimal acum12_valor_compras { get; set; }        
        public decimal acum12_tm { get; set; }
        public int acumvd_qtde_compras { get; set; }
        public decimal acumvd_valor_compras { get; set; }
        public decimal acumvd_tm { get; set; }
        public int vend_qtde_compras { get; set; }
        public DateTime data_ult_contato { get; set; }

    }
}