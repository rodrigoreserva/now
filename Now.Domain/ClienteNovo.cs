﻿using System;
namespace Now.Domain
{
    public class ClienteNovo
    {
        public int id { get; set; }
        public Int64 loja_id { get; set; }
        public Int64 vendedor_id { get; set; }
        public string info_vendas { get; set; }
        public string cpf { get; set; }
        public string quem_ele { get; set; }
        public DateTime data_venda { get; set; }
        public DateTime data_cadastro { get; set; }

        public string filial { get; set; }
        public string vendedor { get; set; }
        public string status { get; set; }

        public string str_data_venda { get { return data_venda.ToString("dd/MM/yyyy"); } }
    }
}