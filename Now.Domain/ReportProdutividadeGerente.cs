﻿using System;
using System.ComponentModel;

namespace Now.Domain
{
    public class ReportProdutividadeGerente    
    {
        [Description(""), Category("")]
        public string ranking { get; set; }
        [Description("loja"), Category("")]
        public string loja { get; set; }
        [Description("vendedor"), Category("")]
        public string vendedor { get; set; }
        [Description("Contatos Efetuados"), Category("")]
        public int contatos_efetuados { get; set; }
        [Description("GEROU"), Category("{0:C}")]
        public decimal valor_contato_now { get; set; }        
    }
}