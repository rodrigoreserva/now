﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Now.Domain
{
    public class MenuTipoAgendamento
    {
        public int id { get; set; }
        public string parametro { get; set; }
        public string titulo { get; set; }
        public string ordenacao { get; set; }
        public int prioridade { get; set; }
        public DateTime? data_carga { get; set; }
        public int Exibir { get; set; }
        public int validar_contato { get; set; }
        public int limite_agenda { get; set; }
        public string frase { get; set; }
        public string regra_agenda { get; set; }
        public DateTime? data_inicio_vigencia { get; set; }
        public DateTime? data_fim_vigencia { get; set; }
    }
}
