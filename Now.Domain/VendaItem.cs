﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Now.Domain
{
    public class VendaItem
    {
        public int Ticket { get; set; }
        public string Tamanho { get; set; }
        public string NomeProduto { get; set; }
        public string NomeCor { get; set; }
        public string IdProduto { get; set; }
        public string IdCor { get; set; }
        public int Unidade { get; set; }
        public decimal SubTotal { get; set; }
        public decimal Desconto { get; set; }
        public string Status { get; set; }
        public string descVenda { get; set; }        
    }
}
