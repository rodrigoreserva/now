﻿using System;
using System.ComponentModel;

namespace Now.Domain
{
    public class ReportEfetividadeAgendaLojas
    {
        public string ranking { get; set; }
        public string loja { get; set; }
        public string vendedor { get; set; }
        public int lig_efetuadas { get; set; }
        public decimal produtividade { get; set; }
        public int LigDisponiveis { get; set; }
        public decimal efetividade { get; set; }
    }
}