﻿using System;
using System.ComponentModel;

namespace Now.Domain
{
    public class DashboardRankingVendedores
    {
        [Description(""), Category("")]
        public string ranking { get; set; }
        [Description("LOJA"), Category("")]
        public string loja { get; set; }      
        [Description("VENDEDOR"), Category("")]

        public Int32 vendedor_id { get; set; }
        public string vendedor { get; set; }        
        [Description("VALOR"), Category("{0:C}")]
        public decimal valor { get; set; }
        [Description("VENDEU"), Category("{0:C}")]
        public decimal valor_venda_now { get; set; }
        [Description("GEROU"), Category("{0:C}")]
        public decimal valor_contato_now { get; set; }
        public int posicao { get; set; }
        public string img_perfil { get; set; }
        public int qtde_venda_clientes { get; set; }
        public int qtde_venda_pecas { get; set; }
        public int qtde_contato_clientes { get; set; }
        public int qtde_contato_pecas { get; set; }        
        public int qtde_contato_venda_clientes { get; set; }
        public int qtde_contato_venda_pecas { get; set; }
        public decimal valor_contato_venda_now { get; set; }
    }
}