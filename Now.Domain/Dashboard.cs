﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Now.Domain
{
    public class Dashboard
    {
        public int id { get; set; }
        public DateTime data { get; set; }
        public int vendedor_id { get; set; }
        public int loja_id { get; set; }
        public decimal taxa_conversao { get; set; }
        public decimal valor_efetividade { get; set; }
        public int qtde_ligacoes_dispon { get; set; }
        public int qtde_ligacoes_feitas { get; set; }
        public int qtde_ligacoes_direto { get; set; }
        public decimal valor_reservado { get; set; }
        public int total_vendas_reservado { get; set; }
        public decimal valor_delivery { get; set; }
        public int total_vendas_delivery { get; set; }

    }
}
