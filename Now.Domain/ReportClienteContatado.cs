﻿using System;
using System.ComponentModel;

namespace Now.Domain
{
    public class ReportClienteContatado
    {
        [Description("loja"), Category("")]
        public string loja { get; set; }
        [Description("Cpf"), Category("")]
        public string cpf { get; set; }        
        [Description("Cliente"), Category("")]
        public string cliente { get; set; }
        [Description("Agenda"), Category("")]
        public string motivo { get; set; }
        [Description("Data Contato"), Category("{0:d}")]
        public DateTime data_contato { get; set; }
        [Description("Vendedor que ligou"), Category("")]
        public string vendedor_ligou { get; set; }
        [Description("Status Ligação"), Category("")]
        public string status_ligacao { get; set; }
        [Description("Modo Contato"), Category("")]
        public string modo { get; set; }
        [Description("Termo metro"), Category("")]
        public string termometro { get; set; }        
        [Description("Observações"), Category("")]
        public string observacao { get; set; }        
    }
}