﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Now.Domain
{   
    public class LogAcesso
    {
        public int id { get; set; }
        public string browser { get; set; }
        public string ip { get; set; }
        public string login { get; set; }
        public string senha { get; set; }
        public DateTime data { get; set; }
        public int acesso { get; set; }
    }
}
