﻿using System;
using System.ComponentModel;

namespace Now.Domain
{
    public class ReportCadastroDependentes
    {
        public int loja_id { get; set; }
        public string loja { get; set; }
        public string vendedor_id { get; set; }
        public string vendedor { get; set; }
        public int qtde_ticket { get; set; }
        public int qtde_dependentes { get; set; }
        public int qtde_dependentes_ticket { get; set; }
        public decimal perc_percentual { get; set; }        
    }
}