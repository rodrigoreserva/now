﻿using System;
using System.ComponentModel;

namespace Now.Domain
{
    public class ReportEfetividadeGeral
    {
        [Description("SUPERVISÃO"), Category("")]
        public string nome_supervisor { get; set; }        
        [Description("LOJA"), Category("")]
        public string loja { get; set; }
        [Description("VENDEDOR"), Category("")]
        public string nome_vendedor { get; set; }
        [Description("AGENDA"), Category("")]
        public string agendamento { get; set; }
        [Description("<span title='TELEFONE INVÁLIDO'>TEL. INV.</span>"), Category("")]
        public int tel_invalido { get; set; }
        [Description("<span title='CLIENTES CONTATADOS'>C. CONT.</span>"), Category("")]
        public int clientes_contatado { get; set; }
        [Description("<span title='TOTAL LIGAÇÕES'>T</span>"), Category("")]
        public int total_ligacoes { get; set; }
        [Description("<span title='DIAS CORRIDOS'>DCO</span>"), Category("")]
        public int dias_corridos { get; set; }
        [Description("<span title='DIAS ÚTEIS'>DUT</span>"), Category("")]
        public int dias_uteis { get; set; }
        [Description("<span title='DIAS CONTATO'>DCT</span>"), Category("")]
        public int dias_contato { get; set; }
        [Description("<span title='CLIENTES COM COMPRAS'>CCC</span>"), Category("")]
        public int clientes_compra { get; set; }
        [Description("<span title='VALOR COMPRA'>VL COMPRA</span>"), Category("{0:C}")]
        public decimal valor_compra { get; set; }
        [Description("MÉDIA CONTATO ÚTEIS"), Category("")]
        public decimal media_contato_dia { get; set; }
        [Description("CONVERSÃO"), Category("{0:P}")]
        public decimal conversao { get; set; }
        [Description("TICKET MEDIO"), Category("{0:C}")]
        public decimal ticket_medio { get; set; }
    }
}