﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Now.Domain
{   
    public class LogErro
    {
        public int id { get; set; }
        public string metodo { get; set; }
        public string tela { get; set; }
        public string erro { get; set; }
        public DateTime data { get; set; }
        public int? vendedor_id { get; set; }
        public int? user_id { get; set; }        
        public int? cliente_id { get; set; }
        public int? loja_id { get; set; }
      
    }
}
