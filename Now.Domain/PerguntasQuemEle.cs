﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Now.Domain
{    
    public class PerguntasQuemEle
    {
        public int id { get; set; }
        public string pergunta { get; set; }
        public string tipo_pergunta { get; set; }
        public int ordem { get; set; }
        public DateTime data_inclusao { get; set; }
        public DateTime? data_alteracao { get; set; }
        public DateTime? data_desativacao { get; set; }
    }
}