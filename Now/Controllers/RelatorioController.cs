﻿using Now.Domain;
using Now.Models;
using Now.Domain.Contracts.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Text;
using System.IO;
using System.Web.UI;

namespace Now.Controllers
{
    public class RelatorioController : BaseController
    {
        public IRelatorioService _serviceRelatorio;
        public RelatorioController(IRelatorioService serviceRelatorio)
        {
            _serviceRelatorio = serviceRelatorio;
        }


        public ActionResult Index(RelatorioPesquisa _relatorioPesquisa)
        {
            // Log do sistema
            LogSistema(new LogSistema { acao = "Tela de Relatório", login = UsuarioLogado.Usuario.email, tipo_log = "NAVEGANDO", vendedor_id = (String.IsNullOrEmpty(UsuarioLogado.IdVendedorSeleconado) ? 0 : Convert.ToInt32(UsuarioLogado.IdVendedorSeleconado)), loja_id = (String.IsNullOrEmpty(UsuarioLogado.IdLojaSeleconado) ? 0 : Convert.ToInt32(UsuarioLogado.IdLojaSeleconado)), metodo = "Index - Relatório", tela = "Relatório", user_id = UsuarioLogado.Usuario.id, ip = HttpContext.Request.UserHostAddress.ToString(), data = DateTime.Now });

            ViewBag.Relatorios = TiposRelatoriosSelectList();

            ViewBag.ativarNovaTelaVendedor = VendedorDashboard != null ? true : false;

            return View(new RelatorioPesquisa() { data_inicial = "01/" + DateTime.Now.AddDays(-1).ToString("MM/yyyy"), data_final = DateTime.Now.AddDays(-1).ToString("dd/MM/yyyy") });
        }

        public ActionResult Pesquisar(RelatorioPesquisa dadosPesquisa)
        {
            string lojas = "";

            // Se não exister loja selecionada, pegar da pesquisa.
            if (String.IsNullOrEmpty(UsuarioLogado.IdLojaSeleconado))
            {
                foreach (var item in dadosPesquisa.lojaIds)
                    lojas += item.ToString() + ",";
            }
            else
            {
                lojas = UsuarioLogado.IdLojaSeleconado;
            }

            lojas = lojas.TrimEnd(',');


            switch (dadosPesquisa.procedure)
            {
                case "SP_RelatorioRankingVendedores":
                    {
                        DateTime TempoInicial = DateTime.Now;
                        var lista = _serviceRelatorio.ListarRelatorios<ReportRankingVendedores>(dadosPesquisa.procedure, lojas, dadosPesquisa.data_inicial, dadosPesquisa.data_final, dadosPesquisa.gerente);
                        //var lista = _serviceRelatorio.ListarRankVendedores(lojas, Convert.ToDateTime(dadosPesquisa.data_inicial).ToString("yyyy-MM-dd"), Convert.ToDateTime(dadosPesquisa.data_final).ToString("yyyy-MM-dd"), dadosPesquisa.gerente);
                        DateTime TempoFinal = DateTime.Now;
                        // Log do sistema
                        LogSistema(new LogSistema { processo_minutos = TempoProcesso(TempoInicial, TempoFinal), acao = "Pesquisando Relatório: ---- Dados Rel: " + dadosPesquisa.procedure + " # Loja: " + lojas + " # Data Inicio: " + Convert.ToDateTime(dadosPesquisa.data_inicial).ToString("yyyy-MM-dd") + " # Data Fim: " + Convert.ToDateTime(dadosPesquisa.data_final).ToString("yyyy-MM-dd"), login = UsuarioLogado.Usuario.email, tipo_log = "RELATÓRIO", vendedor_id = (String.IsNullOrEmpty(UsuarioLogado.IdVendedorSeleconado) ? 0 : Convert.ToInt32(UsuarioLogado.IdVendedorSeleconado)), loja_id = (String.IsNullOrEmpty(UsuarioLogado.IdLojaSeleconado) ? 0 : Convert.ToInt32(UsuarioLogado.IdLojaSeleconado)), metodo = "Pesquisar - Relatório", tela = "Relatório", user_id = UsuarioLogado.Usuario.id, ip = HttpContext.Request.UserHostAddress.ToString(), data = DateTime.Now });

                        return PartialView("_RelatorioRankingVendedores", lista);
                    }
                case "SP_RelatorioRankingCornerMini":
                    {
                        DateTime TempoInicial = DateTime.Now;
                        //var lista = _serviceRelatorio.ListarRelatorios<ReportRankingCornerMini>(dadosPesquisa.procedure, lojas, dadosPesquisa.data_inicial, dadosPesquisa.data_final, dadosPesquisa.gerente);
                        var lista = _serviceRelatorio.ListarRankingCornerMini(lojas, Convert.ToDateTime(dadosPesquisa.data_inicial).ToString("yyyy-MM-dd"), Convert.ToDateTime(dadosPesquisa.data_final).ToString("yyyy-MM-dd"), dadosPesquisa.gerente);
                        DateTime TempoFinal = DateTime.Now;
                        // Log do sistema
                        LogSistema(new LogSistema { processo_minutos = TempoProcesso(TempoInicial, TempoFinal), acao = "Pesquisando Relatório: ---- Dados Rel: " + dadosPesquisa.procedure + " # Loja: " + lojas + " # Data Inicio: " + Convert.ToDateTime(dadosPesquisa.data_inicial).ToString("yyyy-MM-dd") + " # Data Fim: " + Convert.ToDateTime(dadosPesquisa.data_final).ToString("yyyy-MM-dd"), login = UsuarioLogado.Usuario.email, tipo_log = "RELATÓRIO", vendedor_id = (String.IsNullOrEmpty(UsuarioLogado.IdVendedorSeleconado) ? 0 : Convert.ToInt32(UsuarioLogado.IdVendedorSeleconado)), loja_id = (String.IsNullOrEmpty(UsuarioLogado.IdLojaSeleconado) ? 0 : Convert.ToInt32(UsuarioLogado.IdLojaSeleconado)), metodo = "Pesquisar - Relatório", tela = "Relatório", user_id = UsuarioLogado.Usuario.id, ip = HttpContext.Request.UserHostAddress.ToString(), data = DateTime.Now });
                        return PartialView("_RelatorioRankingCornerMini", lista);
                    }
                case "SP_RelatorioDedoDuro#Vendedores#":
                    {
                        DateTime TempoInicial = DateTime.Now;
                        //var lista = _serviceRelatorio.ListarRelatorios<ReportDedoDuro>(dadosPesquisa.procedure.Replace("#Vendedores#", ""), lojas, dadosPesquisa.data_inicial, dadosPesquisa.data_final, dadosPesquisa.gerente);
                        var lista = _serviceRelatorio.ListarDedoDuro(lojas, Convert.ToDateTime(dadosPesquisa.data_inicial).ToString("yyyy-MM-dd"), Convert.ToDateTime(dadosPesquisa.data_final).ToString("yyyy-MM-dd"), dadosPesquisa.gerente);
                        DateTime TempoFinal = DateTime.Now;
                        // Log do sistema
                        LogSistema(new LogSistema { processo_minutos = TempoProcesso(TempoInicial, TempoFinal), acao = "Pesquisando Relatório: ---- Dados Rel: " + dadosPesquisa.procedure + " # Loja: " + lojas + " # Data Inicio: " + Convert.ToDateTime(dadosPesquisa.data_inicial).ToString("yyyy-MM-dd") + " # Data Fim: " + Convert.ToDateTime(dadosPesquisa.data_final).ToString("yyyy-MM-dd"), login = UsuarioLogado.Usuario.email, tipo_log = "RELATÓRIO", vendedor_id = (String.IsNullOrEmpty(UsuarioLogado.IdVendedorSeleconado) ? 0 : Convert.ToInt32(UsuarioLogado.IdVendedorSeleconado)), loja_id = (String.IsNullOrEmpty(UsuarioLogado.IdLojaSeleconado) ? 0 : Convert.ToInt32(UsuarioLogado.IdLojaSeleconado)), metodo = "Pesquisar - Relatório", tela = "Relatório", user_id = UsuarioLogado.Usuario.id, ip = HttpContext.Request.UserHostAddress.ToString(), data = DateTime.Now });
                        return PartialView("_RelatorioDedoDuroVendedores", MontarRelatorioEfetVendedores(lista.OrderBy(x => x.loja).ThenBy(x => x.vendedor).ToList()));
                    }
                case "SP_RelatorioEfetividadeMini":
                    {
                        DateTime TempoInicial = DateTime.Now;
                        var lista = _serviceRelatorio.ListarRelatorios<ReportEfetividade>(dadosPesquisa.procedure, lojas, dadosPesquisa.data_inicial, dadosPesquisa.data_final, dadosPesquisa.gerente);
                        DateTime TempoFinal = DateTime.Now;
                        // Log do sistema
                        LogSistema(new LogSistema { processo_minutos = TempoProcesso(TempoInicial, TempoFinal), acao = "Pesquisando Relatório: ---- Dados Rel: " + dadosPesquisa.procedure + " # Loja: " + lojas + " # Data Inicio: " + Convert.ToDateTime(dadosPesquisa.data_inicial).ToString("yyyy-MM-dd") + " # Data Fim: " + Convert.ToDateTime(dadosPesquisa.data_final).ToString("yyyy-MM-dd"), login = UsuarioLogado.Usuario.email, tipo_log = "RELATÓRIO", vendedor_id = (String.IsNullOrEmpty(UsuarioLogado.IdVendedorSeleconado) ? 0 : Convert.ToInt32(UsuarioLogado.IdVendedorSeleconado)), loja_id = (String.IsNullOrEmpty(UsuarioLogado.IdLojaSeleconado) ? 0 : Convert.ToInt32(UsuarioLogado.IdLojaSeleconado)), metodo = "Pesquisar - Relatório", tela = "Relatório", user_id = UsuarioLogado.Usuario.id, ip = HttpContext.Request.UserHostAddress.ToString(), data = DateTime.Now });
                        return PartialView("_RelatorioEfetividadeMini", lista);
                    }
                case "SP_RelatorioRankingCornerEva":
                    {
                        DateTime TempoInicial = DateTime.Now;
                        var lista = _serviceRelatorio.ListarRelatorios<ReportRankingCornerEva>(dadosPesquisa.procedure, lojas, dadosPesquisa.data_inicial, dadosPesquisa.data_final, dadosPesquisa.gerente);
                        DateTime TempoFinal = DateTime.Now;
                        // Log do sistema
                        LogSistema(new LogSistema { processo_minutos = TempoProcesso(TempoInicial, TempoFinal), acao = "Pesquisando Relatório: ---- Dados Rel: " + dadosPesquisa.procedure + " # Loja: " + lojas + " # Data Inicio: " + Convert.ToDateTime(dadosPesquisa.data_inicial).ToString("yyyy-MM-dd") + " # Data Fim: " + Convert.ToDateTime(dadosPesquisa.data_final).ToString("yyyy-MM-dd"), login = UsuarioLogado.Usuario.email, tipo_log = "RELATÓRIO", vendedor_id = (String.IsNullOrEmpty(UsuarioLogado.IdVendedorSeleconado) ? 0 : Convert.ToInt32(UsuarioLogado.IdVendedorSeleconado)), loja_id = (String.IsNullOrEmpty(UsuarioLogado.IdLojaSeleconado) ? 0 : Convert.ToInt32(UsuarioLogado.IdLojaSeleconado)), metodo = "Pesquisar - Relatório", tela = "Relatório", user_id = UsuarioLogado.Usuario.id, ip = HttpContext.Request.UserHostAddress.ToString(), data = DateTime.Now });
                        return PartialView("_RelatorioRankingCornerEva", lista);
                    }
                case "SP_RelatorioRankingCornerOficina":
                    {
                        DateTime TempoInicial = DateTime.Now;
                        var lista = _serviceRelatorio.ListarRelatorios<ReportRankingCornerOficina>(dadosPesquisa.procedure, lojas, dadosPesquisa.data_inicial, dadosPesquisa.data_final, dadosPesquisa.gerente);
                        DateTime TempoFinal = DateTime.Now;
                        // Log do sistema
                        LogSistema(new LogSistema { processo_minutos = TempoProcesso(TempoInicial, TempoFinal), acao = "Pesquisando Relatório: ---- Dados Rel: " + dadosPesquisa.procedure + " # Loja: " + lojas + " # Data Inicio: " + Convert.ToDateTime(dadosPesquisa.data_inicial).ToString("yyyy-MM-dd") + " # Data Fim: " + Convert.ToDateTime(dadosPesquisa.data_final).ToString("yyyy-MM-dd"), login = UsuarioLogado.Usuario.email, tipo_log = "RELATÓRIO", vendedor_id = (String.IsNullOrEmpty(UsuarioLogado.IdVendedorSeleconado) ? 0 : Convert.ToInt32(UsuarioLogado.IdVendedorSeleconado)), loja_id = (String.IsNullOrEmpty(UsuarioLogado.IdLojaSeleconado) ? 0 : Convert.ToInt32(UsuarioLogado.IdLojaSeleconado)), metodo = "Pesquisar - Relatório", tela = "Relatório", user_id = UsuarioLogado.Usuario.id, ip = HttpContext.Request.UserHostAddress.ToString(), data = DateTime.Now });
                        return PartialView("_RelatorioRankingCornerOficina", lista);
                    }
                case "SP_RelatorioProdutividadeGerente":
                    {
                        DateTime TempoInicial = DateTime.Now;
                        var lista = _serviceRelatorio.ListarRelatorios<ReportProdutividadeGerente>(dadosPesquisa.procedure, lojas, dadosPesquisa.data_inicial, dadosPesquisa.data_final, dadosPesquisa.gerente);
                        DateTime TempoFinal = DateTime.Now;
                        // Log do sistema
                        LogSistema(new LogSistema { processo_minutos = TempoProcesso(TempoInicial, TempoFinal), acao = "Pesquisando Relatório: ---- Dados Rel: " + dadosPesquisa.procedure + " # Loja: " + lojas + " # Data Inicio: " + Convert.ToDateTime(dadosPesquisa.data_inicial).ToString("yyyy-MM-dd") + " # Data Fim: " + Convert.ToDateTime(dadosPesquisa.data_final).ToString("yyyy-MM-dd"), login = UsuarioLogado.Usuario.email, tipo_log = "RELATÓRIO", vendedor_id = (String.IsNullOrEmpty(UsuarioLogado.IdVendedorSeleconado) ? 0 : Convert.ToInt32(UsuarioLogado.IdVendedorSeleconado)), loja_id = (String.IsNullOrEmpty(UsuarioLogado.IdLojaSeleconado) ? 0 : Convert.ToInt32(UsuarioLogado.IdLojaSeleconado)), metodo = "Pesquisar - Relatório", tela = "Relatório", user_id = UsuarioLogado.Usuario.id, ip = HttpContext.Request.UserHostAddress.ToString(), data = DateTime.Now });

                        return PartialView("_ReportProdutividadeGerente", lista);
                    }
                case "SP_RelatorioEfetividadeGeral":
                    {
                        DateTime TempoInicial = DateTime.Now;
                        var lista = _serviceRelatorio.ListarRelatorios<ReportEfetividadeGeral>(dadosPesquisa.procedure, lojas, dadosPesquisa.data_inicial, dadosPesquisa.data_final, dadosPesquisa.gerente);
                        lista = lista.OrderBy(x => x.nome_supervisor).ThenBy(x => x.nome_supervisor).ThenBy(x => x.loja).ThenBy(x => x.nome_vendedor).ToList();
                        DateTime TempoFinal = DateTime.Now;
                        // Log do sistema
                        LogSistema(new LogSistema { processo_minutos = TempoProcesso(TempoInicial, TempoFinal), acao = "Pesquisando Relatório: ---- Dados Rel: " + dadosPesquisa.procedure + " # Loja: " + lojas + " # Data Inicio: " + Convert.ToDateTime(dadosPesquisa.data_inicial).ToString("yyyy-MM-dd") + " # Data Fim: " + Convert.ToDateTime(dadosPesquisa.data_final).ToString("yyyy-MM-dd"), login = UsuarioLogado.Usuario.email, tipo_log = "RELATÓRIO", vendedor_id = (String.IsNullOrEmpty(UsuarioLogado.IdVendedorSeleconado) ? 0 : Convert.ToInt32(UsuarioLogado.IdVendedorSeleconado)), loja_id = (String.IsNullOrEmpty(UsuarioLogado.IdLojaSeleconado) ? 0 : Convert.ToInt32(UsuarioLogado.IdLojaSeleconado)), metodo = "Pesquisar - Relatório", tela = "Relatório", user_id = UsuarioLogado.Usuario.id, ip = HttpContext.Request.UserHostAddress.ToString(), data = DateTime.Now });

                        return PartialView("_RelatorioEfetividadeGeral", MontarEfetividadeGeral(lista));
                    }
                case "SP_RelatorioEfetividadeAgenda":
                    {
                        DateTime TempoInicial = DateTime.Now;
                        var lista = _serviceRelatorio.ListarRelatorios<ReportEfetividadeAgenda>(dadosPesquisa.procedure, lojas, dadosPesquisa.data_inicial, dadosPesquisa.data_final, dadosPesquisa.gerente);
                        DateTime TempoFinal = DateTime.Now;
                        // Log do sistema
                        LogSistema(new LogSistema { processo_minutos = TempoProcesso(TempoInicial, TempoFinal), acao = "Pesquisando Relatório: ---- Dados Rel: " + dadosPesquisa.procedure + " # Loja: " + lojas + " # Data Inicio: " + Convert.ToDateTime(dadosPesquisa.data_inicial).ToString("yyyy-MM-dd") + " # Data Fim: " + Convert.ToDateTime(dadosPesquisa.data_final).ToString("yyyy-MM-dd"), login = UsuarioLogado.Usuario.email, tipo_log = "RELATÓRIO", vendedor_id = (String.IsNullOrEmpty(UsuarioLogado.IdVendedorSeleconado) ? 0 : Convert.ToInt32(UsuarioLogado.IdVendedorSeleconado)), loja_id = (String.IsNullOrEmpty(UsuarioLogado.IdLojaSeleconado) ? 0 : Convert.ToInt32(UsuarioLogado.IdLojaSeleconado)), metodo = "Pesquisar - Relatório", tela = "Relatório", user_id = UsuarioLogado.Usuario.id, ip = HttpContext.Request.UserHostAddress.ToString(), data = DateTime.Now });

                        return PartialView("_RelatorioEfetividadeAgenda", lista);
                    }
                case "SP_RelatorioEfetividadeAgenda_por_vendedor":
                    {
                        DateTime TempoInicial = DateTime.Now;
                        var lista = _serviceRelatorio.ListarRelatorios<ReportEfetividadeAgenda>(dadosPesquisa.procedure, lojas, dadosPesquisa.data_inicial, dadosPesquisa.data_final, dadosPesquisa.gerente);
                        DateTime TempoFinal = DateTime.Now;
                        // Log do sistema
                        LogSistema(new LogSistema { processo_minutos = TempoProcesso(TempoInicial, TempoFinal), acao = "Pesquisando Relatório: ---- Dados Rel: " + dadosPesquisa.procedure + " # Loja: " + lojas + " # Data Inicio: " + Convert.ToDateTime(dadosPesquisa.data_inicial).ToString("yyyy-MM-dd") + " # Data Fim: " + Convert.ToDateTime(dadosPesquisa.data_final).ToString("yyyy-MM-dd"), login = UsuarioLogado.Usuario.email, tipo_log = "RELATÓRIO", vendedor_id = (String.IsNullOrEmpty(UsuarioLogado.IdVendedorSeleconado) ? 0 : Convert.ToInt32(UsuarioLogado.IdVendedorSeleconado)), loja_id = (String.IsNullOrEmpty(UsuarioLogado.IdLojaSeleconado) ? 0 : Convert.ToInt32(UsuarioLogado.IdLojaSeleconado)), metodo = "Pesquisar - Relatório", tela = "Relatório", user_id = UsuarioLogado.Usuario.id, ip = HttpContext.Request.UserHostAddress.ToString(), data = DateTime.Now });

                        return PartialView("_RelatorioEfetividadeAgendaVendedor", lista);
                    }
                case "SP_RelatorioEfetividadeLojas":
                    {
                        DateTime TempoInicial = DateTime.Now;
                        var lista = _serviceRelatorio.ListarRelatorios<ReportEfetividade>(dadosPesquisa.procedure, lojas, dadosPesquisa.data_inicial, dadosPesquisa.data_final, dadosPesquisa.gerente);

                        @ViewBag.listaSupervisor = _serviceRelatorio.ListarRelatorios<ReportEfetividade>("SP_RelatorioEfetividadeLojasSupervisao", lojas, dadosPesquisa.data_inicial, dadosPesquisa.data_final, dadosPesquisa.gerente);

                        DateTime TempoFinal = DateTime.Now;
                        // Log do sistema
                        LogSistema(new LogSistema { processo_minutos = TempoProcesso(TempoInicial, TempoFinal), acao = "Pesquisando Relatório: ---- Dados Rel: " + dadosPesquisa.procedure + " # Loja: " + lojas + " # Data Inicio: " + Convert.ToDateTime(dadosPesquisa.data_inicial).ToString("yyyy-MM-dd") + " # Data Fim: " + Convert.ToDateTime(dadosPesquisa.data_final).ToString("yyyy-MM-dd"), login = UsuarioLogado.Usuario.email, tipo_log = "RELATÓRIO", vendedor_id = (String.IsNullOrEmpty(UsuarioLogado.IdVendedorSeleconado) ? 0 : Convert.ToInt32(UsuarioLogado.IdVendedorSeleconado)), loja_id = (String.IsNullOrEmpty(UsuarioLogado.IdLojaSeleconado) ? 0 : Convert.ToInt32(UsuarioLogado.IdLojaSeleconado)), metodo = "Pesquisar - Relatório", tela = "Relatório", user_id = UsuarioLogado.Usuario.id, ip = HttpContext.Request.UserHostAddress.ToString(), data = DateTime.Now });

                        return PartialView("_RelatorioEfetividadeLojas", lista);
                    }
                case "SP_RelatorioClienteContatado":
                    {
                        DateTime TempoInicial = DateTime.Now;
                        var lista = _serviceRelatorio.ListarRelatorios<ReportClienteContatado>(dadosPesquisa.procedure, lojas, dadosPesquisa.data_inicial, dadosPesquisa.data_final, dadosPesquisa.gerente);
                        DateTime TempoFinal = DateTime.Now;
                        // Log do sistema
                        LogSistema(new LogSistema { processo_minutos = TempoProcesso(TempoInicial, TempoFinal), acao = "Pesquisando Relatório: ---- Dados Rel: " + dadosPesquisa.procedure + " # Loja: " + lojas + " # Data Inicio: " + Convert.ToDateTime(dadosPesquisa.data_inicial).ToString("yyyy-MM-dd") + " # Data Fim: " + Convert.ToDateTime(dadosPesquisa.data_final).ToString("yyyy-MM-dd"), login = UsuarioLogado.Usuario.email, tipo_log = "RELATÓRIO", vendedor_id = (String.IsNullOrEmpty(UsuarioLogado.IdVendedorSeleconado) ? 0 : Convert.ToInt32(UsuarioLogado.IdVendedorSeleconado)), loja_id = (String.IsNullOrEmpty(UsuarioLogado.IdLojaSeleconado) ? 0 : Convert.ToInt32(UsuarioLogado.IdLojaSeleconado)), metodo = "Pesquisar - Relatório", tela = "Relatório", user_id = UsuarioLogado.Usuario.id, ip = HttpContext.Request.UserHostAddress.ToString(), data = DateTime.Now });

                        return PartialView("_RelatorioClienteContatado", lista);
                    }

                case "SP_RelatorioAcompanhamentoOmni":
                    {
                        DateTime TempoInicial = DateTime.Now;
                        var lista = _serviceRelatorio.ListarRelatorios<OmniAcompanhamento>(dadosPesquisa.procedure, lojas, dadosPesquisa.data_inicial, dadosPesquisa.data_final, dadosPesquisa.gerente);
                        DateTime TempoFinal = DateTime.Now;
                        // Log do sistema
                        LogSistema(new LogSistema { processo_minutos = TempoProcesso(TempoInicial, TempoFinal), acao = "Pesquisando Relatório: ---- Dados Rel: " + dadosPesquisa.procedure + " # Loja: " + lojas + " # Data Inicio: " + Convert.ToDateTime(dadosPesquisa.data_inicial).ToString("yyyy-MM-dd") + " # Data Fim: " + Convert.ToDateTime(dadosPesquisa.data_final).ToString("yyyy-MM-dd"), login = UsuarioLogado.Usuario.email, tipo_log = "RELATÓRIO", vendedor_id = (String.IsNullOrEmpty(UsuarioLogado.IdVendedorSeleconado) ? 0 : Convert.ToInt32(UsuarioLogado.IdVendedorSeleconado)), loja_id = (String.IsNullOrEmpty(UsuarioLogado.IdLojaSeleconado) ? 0 : Convert.ToInt32(UsuarioLogado.IdLojaSeleconado)), metodo = "Pesquisar - Relatório", tela = "Relatório", user_id = UsuarioLogado.Usuario.id, ip = HttpContext.Request.UserHostAddress.ToString(), data = DateTime.Now });

                        return PartialView("_RelatorioAcompanhamentoOmni", lista);
                    }
                case "SP_RelatorioRankingOficina":
                    {
                        DateTime TempoInicial = DateTime.Now;
                        var lista = _serviceRelatorio.ListarRelatorios<RelatorioRankingOficina>(dadosPesquisa.procedure, lojas, dadosPesquisa.data_inicial, dadosPesquisa.data_final, dadosPesquisa.gerente);
                        DateTime TempoFinal = DateTime.Now;
                        // Log do sistema
                        LogSistema(new LogSistema { processo_minutos = TempoProcesso(TempoInicial, TempoFinal), acao = "Pesquisando Relatório: ---- Dados Rel: " + dadosPesquisa.procedure + " # Loja: " + lojas + " # Data Inicio: " + Convert.ToDateTime(dadosPesquisa.data_inicial).ToString("yyyy-MM-dd") + " # Data Fim: " + Convert.ToDateTime(dadosPesquisa.data_final).ToString("yyyy-MM-dd"), login = UsuarioLogado.Usuario.email, tipo_log = "RELATÓRIO", vendedor_id = (String.IsNullOrEmpty(UsuarioLogado.IdVendedorSeleconado) ? 0 : Convert.ToInt32(UsuarioLogado.IdVendedorSeleconado)), loja_id = (String.IsNullOrEmpty(UsuarioLogado.IdLojaSeleconado) ? 0 : Convert.ToInt32(UsuarioLogado.IdLojaSeleconado)), metodo = "Pesquisar - Relatório", tela = "Relatório", user_id = UsuarioLogado.Usuario.id, ip = HttpContext.Request.UserHostAddress.ToString(), data = DateTime.Now });

                        return PartialView("_RelatorioRankingOficina", lista.OrderByDescending(x => x.personalizada).ToList());
                    }

                case "SP_RelatorioDedoDuro":
                    {
                        DateTime TempoInicial = DateTime.Now;
                        var lista = _serviceRelatorio.ListarRelatorios<ReportDedoDuro>(dadosPesquisa.procedure, lojas, dadosPesquisa.data_inicial, dadosPesquisa.data_final, dadosPesquisa.gerente);
                        DateTime TempoFinal = DateTime.Now;
                        // Log do sistema
                        LogSistema(new LogSistema { processo_minutos = TempoProcesso(TempoInicial, TempoFinal), acao = "Pesquisando Relatório: ---- Dados Rel: " + dadosPesquisa.procedure + " # Loja: " + lojas + " # Data Inicio: " + Convert.ToDateTime(dadosPesquisa.data_inicial).ToString("yyyy-MM-dd") + " # Data Fim: " + Convert.ToDateTime(dadosPesquisa.data_final).ToString("yyyy-MM-dd"), login = UsuarioLogado.Usuario.email, tipo_log = "RELATÓRIO", vendedor_id = (String.IsNullOrEmpty(UsuarioLogado.IdVendedorSeleconado) ? 0 : Convert.ToInt32(UsuarioLogado.IdVendedorSeleconado)), loja_id = (String.IsNullOrEmpty(UsuarioLogado.IdLojaSeleconado) ? 0 : Convert.ToInt32(UsuarioLogado.IdLojaSeleconado)), metodo = "Pesquisar - Relatório", tela = "Relatório", user_id = UsuarioLogado.Usuario.id, ip = HttpContext.Request.UserHostAddress.ToString(), data = DateTime.Now });

                        return PartialView("_RelatorioDedoDuro", MontarDedoDuro(lista.OrderBy(x => x.supervisor).ThenBy(x => x.supervisor).ThenBy(x => x.loja).ThenBy(x => x.vendedor).ToList()));
                    }

                case "SP_RelatorioInfoVendas":
                    {
                        DateTime TempoInicial = DateTime.Now;
                        var lista = _serviceRelatorio.ListarRelatorios<ReportInfoVendas>(dadosPesquisa.procedure, lojas, dadosPesquisa.data_inicial, dadosPesquisa.data_final, dadosPesquisa.gerente);
                        DateTime TempoFinal = DateTime.Now;
                        // Log do sistema
                        LogSistema(new LogSistema { processo_minutos = TempoProcesso(TempoInicial, TempoFinal), acao = "Pesquisando Relatório: ---- Dados Rel: " + dadosPesquisa.procedure + " # Loja: " + lojas + " # Data Inicio: " + Convert.ToDateTime(dadosPesquisa.data_inicial).ToString("yyyy-MM-dd") + " # Data Fim: " + Convert.ToDateTime(dadosPesquisa.data_final).ToString("yyyy-MM-dd"), login = UsuarioLogado.Usuario.email, tipo_log = "RELATÓRIO", vendedor_id = (String.IsNullOrEmpty(UsuarioLogado.IdVendedorSeleconado) ? 0 : Convert.ToInt32(UsuarioLogado.IdVendedorSeleconado)), loja_id = (String.IsNullOrEmpty(UsuarioLogado.IdLojaSeleconado) ? 0 : Convert.ToInt32(UsuarioLogado.IdLojaSeleconado)), metodo = "Pesquisar - Relatório", tela = "Relatório", user_id = UsuarioLogado.Usuario.id, ip = HttpContext.Request.UserHostAddress.ToString(), data = DateTime.Now });

                        return PartialView("_RelatorioInfoVendas", lista);
                    }
                case "SP_RelatorioCadastroDependentes":
                    {
                        DateTime TempoInicial = DateTime.Now;
                        var lista = _serviceRelatorio.ListarRelatorios<ReportCadastroDependentes>(dadosPesquisa.procedure, lojas, dadosPesquisa.data_inicial, dadosPesquisa.data_final, dadosPesquisa.gerente);
                        DateTime TempoFinal = DateTime.Now;
                        // Log do sistema
                        LogSistema(new LogSistema { processo_minutos = TempoProcesso(TempoInicial, TempoFinal), acao = "Pesquisando Relatório: ---- Dados Rel: " + dadosPesquisa.procedure + " # Loja: " + lojas + " # Data Inicio: " + Convert.ToDateTime(dadosPesquisa.data_inicial).ToString("yyyy-MM-dd") + " # Data Fim: " + Convert.ToDateTime(dadosPesquisa.data_final).ToString("yyyy-MM-dd"), login = UsuarioLogado.Usuario.email, tipo_log = "RELATÓRIO", vendedor_id = (String.IsNullOrEmpty(UsuarioLogado.IdVendedorSeleconado) ? 0 : Convert.ToInt32(UsuarioLogado.IdVendedorSeleconado)), loja_id = (String.IsNullOrEmpty(UsuarioLogado.IdLojaSeleconado) ? 0 : Convert.ToInt32(UsuarioLogado.IdLojaSeleconado)), metodo = "Pesquisar - Relatório", tela = "Relatório", user_id = UsuarioLogado.Usuario.id, ip = HttpContext.Request.UserHostAddress.ToString(), data = DateTime.Now });

                        return PartialView("_RelatorioCadastroDependentes", lista);
                    }
                case "SP_Email_EfetividadesPorAgendaLojaVendedor#47":
                case "SP_Email_EfetividadesPorAgendaLojaVendedor#3":
                case "SP_Email_EfetividadesPorAgendaLojaVendedor#49":
                case "SP_Email_EfetividadesPorAgendaLojaVendedor#1":
                    {
                        var idAgenda = Convert.ToInt32(dadosPesquisa.procedure.Split('#')[1]);

                        var lista = _serviceRelatorio.ListarEfetividadesPorAgendaLojaVendedor(lojas, Convert.ToDateTime(dadosPesquisa.data_inicial).ToString("yyyy-MM-dd"), Convert.ToDateTime(dadosPesquisa.data_final).ToString("yyyy-MM-dd"), idAgenda, dadosPesquisa.gerente);

                        return PartialView("_EfetividadesPorAgendaLojaVendedo", lista);
                    }

                default: return PartialView("_RelatorioRankingVendedores", new List<ReportRankingVendedores>());


            }
        }

        private string MontarEfetividadeGeral(IList<ReportEfetividadeGeral> lista)
        {
            String relatorio = String.Empty;
            string periodo = String.Empty;

            //if (lista.Count() > 0)
            //    periodo = lista[0].periodo;

            relatorio += "<table class='table table-striped table-bordered table-hover tbPesquisa'>";
            relatorio += "<thead>";
            relatorio += "<tr>";
            relatorio += "<th style='background: #DDD none repeat scroll 0% 0%;' >Supervisor</th>";
            relatorio += "<th style='background: #DDD none repeat scroll 0% 0%;' >Loja</th>";
            relatorio += "<th style='background: #DDD none repeat scroll 0% 0%;' >Vendedor</th>";
            relatorio += "<th style='background: #DDD none repeat scroll 0% 0%;' >Agenda</th>";

            relatorio += "<th style='background: #DDD none repeat scroll 0% 0%;'  title='TELEFONE INVÁLIDO' >TEL.INV.</th>";
            relatorio += "<th style='background: #DDD none repeat scroll 0% 0%;'  title='CLIENTES CONTATADOS'>C.CONT.</th>";
            relatorio += "<th style='background: #DDD none repeat scroll 0% 0%;'  title='TOTAL LIGAÇÕES'>T</th>";
            relatorio += "<th style='background: #DDD none repeat scroll 0% 0%;'  title='DIAS CORRIDOS'> DCO </th>";
            relatorio += "<th style='background: #DDD none repeat scroll 0% 0%;'  title='DIAS CONTATO'>DCT</th>";
            relatorio += "<th style='background: #DDD none repeat scroll 0% 0%;'  title='CLIENTES COM COMPRAS'>CCC</th>";
            relatorio += "<th style='background: #DDD none repeat scroll 0% 0%;'  title='VALOR COMPRA'>VALOR COMPRA</th>";
            relatorio += "<th style='background: #DDD none repeat scroll 0% 0%;' >CONTATO / DIA</th>";
            relatorio += "<th style='background: #DDD none repeat scroll 0% 0%;' >" + Server.HtmlEncode("CONVERSÃO") + "</th>";
            relatorio += "<th style='background: #DDD none repeat scroll 0% 0%;' >TICKET MEDIO</th>";
            relatorio += "</tr>";
            relatorio += "</thead>";
            relatorio += "<tbody>";

            String vendedor = String.Empty;
            String loja = String.Empty;
            String supervisor = String.Empty;
            int totalSupervisor1 = 0;
            int totalSupervisor2 = 0;
            int totalSupervisor3 = 0;
            int totalSupervisor4 = 0;
            int totalSupervisor5 = 0;
            int totalSupervisor6 = 0;
            decimal totalSupervisor7 = 0;
            decimal totalSupervisor10 = 0;

            int totalContatosVendedor1 = 0;
            int totalContatosVendedor2 = 0;
            int totalContatosVendedor3 = 0;
            int totalContatosVendedor4 = 0;
            int totalContatosVendedor5 = 0;
            int totalContatosVendedor6 = 0;
            decimal totalContatosVendedor7 = 0;
            decimal totalContatosVendedor10 = 0;

            int totalLoja1 = 0;
            int totalLoja2 = 0;
            int totalLoja3 = 0;
            int totalLoja4 = 0;
            int totalLoja5 = 0;
            int totalLoja6 = 0;
            decimal totalLoja7 = 0;
            decimal totalLoja10 = 0;

            int totalGeral1 = 0;
            int totalGeral2 = 0;
            int totalGeral3 = 0;
            int totalGeral4 = 0;
            int totalGeral5 = 0;
            int totalGeral6 = 0;
            decimal totalGeral7 = 0;
            decimal totalGeral10 = 0;

            int countRowSpanVendedor = 0;
            int countRowSpanLoja = 0;
            int countRowSpanSupervisor = 0;
            int countLista = lista.Count();
            int countLista2 = 0;

            foreach (ReportEfetividadeGeral item in lista)
            {
                countLista2++;

                // Inserindo o Total por Vendedor
                if ((vendedor != String.Empty && vendedor != item.nome_vendedor))// || countLista2 == countLista)
                {
                    relatorio += "<tr  class='subTotal " + TransformarTexto(supervisor) + "'>";
                    relatorio += "<td style='border: 1px solid #DDD; vertical-align: top;'colspan='2' class='subTot" + TransformarTexto(vendedor) + " " + TransformarTexto(loja) + "' >Sub Total</td>";
                    relatorio += "<td style='border: 1px solid #DDD; vertical-align: top;'class='" + TransformarTexto(loja) + "'>" + totalContatosVendedor1 + "</td>";
                    relatorio += "<td style='border: 1px solid #DDD; vertical-align: top;'class='" + TransformarTexto(loja) + "'>" + totalContatosVendedor2 + "</td>";
                    relatorio += "<td style='border: 1px solid #DDD; vertical-align: top;'class='" + TransformarTexto(loja) + "'>" + totalContatosVendedor3 + "</td>";
                    relatorio += "<td style='border: 1px solid #DDD; vertical-align: top;'class='" + TransformarTexto(loja) + "'>" + totalContatosVendedor4 + "</td>";
                    relatorio += "<td style='border: 1px solid #DDD; vertical-align: top;'class='" + TransformarTexto(loja) + "'>" + totalContatosVendedor5 + "</td>";
                    relatorio += "<td style='border: 1px solid #DDD; vertical-align: top;'class='" + TransformarTexto(loja) + "'>" + totalContatosVendedor6 + "</td>";
                    relatorio += "<td style='border: 1px solid #DDD; vertical-align: top;'class='" + TransformarTexto(loja) + "'>" + String.Format("{0:C}", totalContatosVendedor7) + "</td>";
                    relatorio += "<td style='border: 1px solid #DDD; vertical-align: top;'class='" + TransformarTexto(loja) + "'>" + (totalContatosVendedor5 == 0 ? "0" : ((totalContatosVendedor3 - totalContatosVendedor1) / totalContatosVendedor5).ToString()) + "</td>";
                    relatorio += "<td style='border: 1px solid #DDD; vertical-align: top;'class='" + TransformarTexto(loja) + "'>" + String.Format("{0:P}", totalContatosVendedor5 == 0 ? 0 : totalContatosVendedor6 / totalContatosVendedor5) + "</td>";
                    relatorio += "<td style='border: 1px solid #DDD; vertical-align: top;'class='" + TransformarTexto(loja) + "'>" + String.Format("{0:C}", totalContatosVendedor6 == 0 ? 0 : totalContatosVendedor7 / totalContatosVendedor6) + "</td>";
                    relatorio += "</tr>";

                    relatorio = relatorio.Replace("#row" + vendedor, countRowSpanVendedor.ToString());

                    totalContatosVendedor1 = 0;
                    totalContatosVendedor2 = 0;
                    totalContatosVendedor3 = 0;
                    totalContatosVendedor4 = 0;
                    totalContatosVendedor5 = 0;
                    totalContatosVendedor6 = 0;
                    totalContatosVendedor7 = 0;
                    totalContatosVendedor10 = 0;

                    countRowSpanVendedor = 0;
                }

                // Inserindo o Total por Loja
                if ((loja != String.Empty && loja != item.loja))// || countLista2 == countLista)
                {
                    relatorio += "<tr  class='subTotal " + TransformarTexto(supervisor) + "'>";
                    relatorio += "<td style='border: 1px solid #DDD; vertical-align: top;'colspan='3' class='subTot" + TransformarTexto(loja) + "'>Sub Total</td>";
                    relatorio += "<td style='border: 1px solid #DDD; vertical-align: top;'>" + totalLoja1 + "</td>";
                    relatorio += "<td style='border: 1px solid #DDD; vertical-align: top;'>" + totalLoja2 + "</td>";
                    relatorio += "<td style='border: 1px solid #DDD; vertical-align: top;'>" + totalLoja3 + "</td>";
                    relatorio += "<td style='border: 1px solid #DDD; vertical-align: top;'>" + totalLoja4 + "</td>";
                    relatorio += "<td style='border: 1px solid #DDD; vertical-align: top;'>" + totalLoja5 + "</td>";
                    relatorio += "<td style='border: 1px solid #DDD; vertical-align: top;'>" + totalLoja6 + "</td>";
                    relatorio += "<td style='border: 1px solid #DDD; vertical-align: top;'>" + String.Format("{0:C}", totalLoja7) + "</td>";
                    relatorio += "<td style='border: 1px solid #DDD; vertical-align: top;'>" + (totalLoja5 == 0 ? "0" : ((totalLoja3 - totalLoja1) / totalLoja5).ToString()) + "</td>";
                    relatorio += "<td style='border: 1px solid #DDD; vertical-align: top;'>" + String.Format("{0:P}", totalLoja5 == 0 ? 0 : totalLoja6 / totalLoja5) + "</td>";
                    relatorio += "<td style='border: 1px solid #DDD; vertical-align: top;'>" + String.Format("{0:C}", totalLoja6 == 0 ? 0 : totalLoja7 / totalLoja6) + "</td>";
                    relatorio += "</tr>";

                    totalLoja1 = 0;
                    totalLoja2 = 0;
                    totalLoja3 = 0;
                    totalLoja4 = 0;
                    totalLoja5 = 0;
                    totalLoja6 = 0;
                    totalLoja7 = 0;
                    totalLoja10 = 0;

                    // Se existir mais de um vendedor parab mesma loja adiciona mais rowspan                    
                    countRowSpanLoja += lista.Where(x => x.loja == loja).ToList().GroupBy(x => x.nome_vendedor).Count();

                    relatorio = relatorio.Replace("#row" + loja, countRowSpanLoja.ToString());
                    countRowSpanLoja = 0;
                }

                // Inserindo o Total por Supervisor
                if ((supervisor != String.Empty && supervisor != item.nome_supervisor && loja != item.loja))// || countLista2 == countLista)
                {
                    relatorio += "<tr  class='subTotal'>";
                    relatorio += "<td style='border: 1px solid #DDD; vertical-align: top;'colspan='4' class='subTot" + TransformarTexto(supervisor) + "'>Sub Total</td>";
                    relatorio += "<td style='border: 1px solid #DDD; vertical-align: top;'>" + totalSupervisor1 + "</td>";
                    relatorio += "<td style='border: 1px solid #DDD; vertical-align: top;'>" + totalSupervisor2 + "</td>";
                    relatorio += "<td style='border: 1px solid #DDD; vertical-align: top;'>" + totalSupervisor3 + "</td>";
                    relatorio += "<td style='border: 1px solid #DDD; vertical-align: top;'>" + totalSupervisor4 + "</td>";
                    relatorio += "<td style='border: 1px solid #DDD; vertical-align: top;'>" + totalSupervisor5 + "</td>";
                    relatorio += "<td style='border: 1px solid #DDD; vertical-align: top;'>" + totalSupervisor6 + "</td>";
                    relatorio += "<td style='border: 1px solid #DDD; vertical-align: top;'>" + String.Format("{0:C}", totalSupervisor7) + "</td>";
                    relatorio += "<td style='border: 1px solid #DDD; vertical-align: top;'>" + (totalSupervisor5 == 0 ? "0" : ((totalSupervisor3 - totalSupervisor1) / totalSupervisor5).ToString()) + "</td>";
                    relatorio += "<td style='border: 1px solid #DDD; vertical-align: top;'>" + String.Format("{0:P}", totalSupervisor5 == 0 ? 0 : totalSupervisor6 / totalSupervisor5) + "</td>";
                    relatorio += "<td style='border: 1px solid #DDD; vertical-align: top;'>" + String.Format("{0:C}", totalSupervisor6 == 0 ? 0 : totalSupervisor7 / totalSupervisor6) + "</td>";
                    relatorio += "</tr>";

                    totalSupervisor1 = 0;
                    totalSupervisor2 = 0;
                    totalSupervisor3 = 0;
                    totalSupervisor4 = 0;
                    totalSupervisor5 = 0;
                    totalSupervisor6 = 0;
                    totalSupervisor7 = 0;
                    totalSupervisor10 = 0;

                    // Se existir mais de um vendedor parab mesma loja adiciona mais rowspan                    
                    countRowSpanSupervisor += lista.Where(x => x.nome_supervisor == supervisor).ToList().GroupBy(x => x.nome_vendedor).Count() + lista.Where(x => x.nome_supervisor == supervisor).ToList().GroupBy(x => x.loja).Count();

                    relatorio = relatorio.Replace("#row" + supervisor, countRowSpanSupervisor.ToString());
                    countRowSpanSupervisor = 0;
                }

                relatorio += "<tr  class='" + item.nome_supervisor + "'>";

                if (countRowSpanSupervisor == 0)
                    relatorio += "<td style='border: 1px solid #DDD; vertical-align: top;'rowspan='#row" + item.nome_supervisor + "' class='supervisor" + TransformarTexto(item.nome_supervisor) + "' ><a href='javascript:DetalheGrid(\"minus\", \"" + TransformarTexto(item.nome_supervisor) + "\", 4, \"supervisor\", \"\");' class='glyphicon glyphicon-minus' style='text-decoration: none !important;'></a><span class='nmsupervisor" + TransformarTexto(item.nome_supervisor) + "'>&nbsp;&nbsp;" + Server.HtmlEncode(item.nome_supervisor) + "</span></td>";

                if (countRowSpanLoja == 0)
                    relatorio += "<td style='border: 1px solid #DDD; vertical-align: top;'rowspan='#row" + item.loja + "' class='loja" + TransformarTexto(item.loja) + " " + TransformarTexto(item.loja) + "' ><a href='javascript:DetalheGrid(\"minus\", \"" + TransformarTexto(item.loja) + "\", 3, \"loja\", \"\");' class='glyphicon glyphicon-minus' style='text-decoration: none !important;'></a><span class='nmloja" + TransformarTexto(item.loja) + "'>&nbsp;&nbsp;" + Server.HtmlEncode(item.loja) + "</span></td>";

                if (countRowSpanVendedor == 0)
                    relatorio += "<td style='border: 1px solid #DDD; vertical-align: top;'rowspan='#row" + item.nome_vendedor + "' class='vendedor" + TransformarTexto(item.nome_vendedor) + " " + TransformarTexto(item.loja) + " " + TransformarTexto(item.nome_vendedor) + "' ><a href='javascript:DetalheGrid(\"minus\", \"" + TransformarTexto(item.nome_vendedor) + "\", 2, \"vendedor\", \"" + TransformarTexto(item.loja) + "\");' class='glyphicon glyphicon-minus' style='text-decoration: none !important;'></a><span class='nmvendedor" + TransformarTexto(item.nome_vendedor) + "'>&nbsp;&nbsp;" + Server.HtmlEncode(item.nome_vendedor) + "</span></td>";

                relatorio += "<td style='border: 1px solid #DDD; vertical-align: top;'class='" + TransformarTexto(item.loja) + " " + TransformarTexto(item.nome_vendedor) + "'>" + item.agendamento + "</td>";
                relatorio += "<td style='border: 1px solid #DDD; vertical-align: top;'class='" + TransformarTexto(item.loja) + " " + TransformarTexto(item.nome_vendedor) + "'>" + item.tel_invalido.ToString() + "</td>";
                relatorio += "<td style='border: 1px solid #DDD; vertical-align: top;'class='" + TransformarTexto(item.loja) + " " + TransformarTexto(item.nome_vendedor) + "'>" + item.clientes_contatado + "</td>";
                relatorio += "<td style='border: 1px solid #DDD; vertical-align: top;'class='" + TransformarTexto(item.loja) + " " + TransformarTexto(item.nome_vendedor) + "'>" + item.total_ligacoes + "</td>";
                relatorio += "<td style='border: 1px solid #DDD; vertical-align: top;'class='" + TransformarTexto(item.loja) + " " + TransformarTexto(item.nome_vendedor) + "'>" + item.dias_corridos + "</td>";
                relatorio += "<td style='border: 1px solid #DDD; vertical-align: top;'class='" + TransformarTexto(item.loja) + " " + TransformarTexto(item.nome_vendedor) + "'>" + item.dias_contato + "</td>";
                relatorio += "<td style='border: 1px solid #DDD; vertical-align: top;'class='" + TransformarTexto(item.loja) + " " + TransformarTexto(item.nome_vendedor) + "'>" + item.clientes_compra + "</td>";
                relatorio += "<td style='border: 1px solid #DDD; vertical-align: top;'class='" + TransformarTexto(item.loja) + " " + TransformarTexto(item.nome_vendedor) + "'>" + String.Format("{0:C}", item.valor_compra) + "</td>";
                relatorio += "<td style='border: 1px solid #DDD; vertical-align: top;'class='" + TransformarTexto(item.loja) + " " + TransformarTexto(item.nome_vendedor) + "'>" + item.media_contato_dia + "</td>";
                relatorio += "<td style='border: 1px solid #DDD; vertical-align: top;'class='" + TransformarTexto(item.loja) + " " + TransformarTexto(item.nome_vendedor) + "'>" + String.Format("{0:P}", item.conversao) + "</td>";
                relatorio += "<td style='border: 1px solid #DDD; vertical-align: top;'class='" + TransformarTexto(item.loja) + " " + TransformarTexto(item.nome_vendedor) + "'>" + String.Format("{0:C}", item.ticket_medio) + "</td>";

                relatorio += "</tr>";

                countRowSpanVendedor++;
                countRowSpanLoja++;
                countRowSpanSupervisor++;

                vendedor = item.nome_vendedor;
                loja = item.loja;
                supervisor = item.nome_supervisor;

                totalSupervisor1 += item.tel_invalido;
                totalSupervisor2 += item.clientes_contatado;
                totalSupervisor3 += item.total_ligacoes;
                totalSupervisor4 += item.dias_corridos;
                totalSupervisor5 += item.dias_contato;
                totalSupervisor6 += item.clientes_compra;
                totalSupervisor7 += item.valor_compra;
                totalSupervisor10 += item.ticket_medio;

                totalLoja1 += item.tel_invalido;
                totalLoja2 += item.clientes_contatado;
                totalLoja3 += item.total_ligacoes;
                totalLoja4 += item.dias_corridos;
                totalLoja5 += item.dias_contato;
                totalLoja6 += item.clientes_compra;
                totalLoja7 += item.valor_compra;
                totalLoja10 += item.ticket_medio;

                totalContatosVendedor1 += item.tel_invalido;
                totalContatosVendedor2 += item.clientes_contatado;
                totalContatosVendedor3 += item.total_ligacoes;
                totalContatosVendedor4 += item.dias_corridos;
                totalContatosVendedor5 += item.dias_contato;
                totalContatosVendedor6 += item.clientes_compra;
                totalContatosVendedor7 += item.valor_compra;
                totalContatosVendedor10 += item.ticket_medio;

                totalGeral1 += item.tel_invalido;
                totalGeral2 += item.clientes_contatado;
                totalGeral3 += item.total_ligacoes;
                totalGeral4 += item.dias_corridos;
                totalGeral5 += item.dias_contato;
                totalGeral6 += item.clientes_compra;
                totalGeral7 += item.valor_compra;
                totalGeral10 += item.ticket_medio;
            }

            // Só faz o total final se existir dados
            if (lista.Count > 0)
            {
                #region "Ultima LINHA Inserindo o Total por Vendedor"

                relatorio += "<tr  class='subTotal " + TransformarTexto(supervisor) + "'>";
                relatorio += "<td style='border: 1px solid #DDD; vertical-align: top;'colspan='2' class='subTot" + TransformarTexto(vendedor) + " " + TransformarTexto(loja) + "'>Sub Total</td>";
                relatorio += "<td style='border: 1px solid #DDD; vertical-align: top;'class='" + TransformarTexto(loja) + "'>" + totalContatosVendedor1 + "</td>";
                relatorio += "<td style='border: 1px solid #DDD; vertical-align: top;'class='" + TransformarTexto(loja) + "'>" + totalContatosVendedor2 + "</td>";
                relatorio += "<td style='border: 1px solid #DDD; vertical-align: top;'class='" + TransformarTexto(loja) + "'>" + totalContatosVendedor3 + "</td>";
                relatorio += "<td style='border: 1px solid #DDD; vertical-align: top;'class='" + TransformarTexto(loja) + "'>" + totalContatosVendedor4 + "</td>";
                relatorio += "<td style='border: 1px solid #DDD; vertical-align: top;'class='" + TransformarTexto(loja) + "'>" + totalContatosVendedor5 + "</td>";
                relatorio += "<td style='border: 1px solid #DDD; vertical-align: top;'class='" + TransformarTexto(loja) + "'>" + totalContatosVendedor6 + "</td>";
                relatorio += "<td style='border: 1px solid #DDD; vertical-align: top;'class='" + TransformarTexto(loja) + "'>" + String.Format("{0:C}", totalContatosVendedor7) + "</td>";
                relatorio += "<td style='border: 1px solid #DDD; vertical-align: top;'class='" + TransformarTexto(loja) + "'>" + (totalContatosVendedor5 == 0 ? "0" : ((totalContatosVendedor3 - totalContatosVendedor1) / totalContatosVendedor5).ToString()) + "</td>";
                relatorio += "<td style='border: 1px solid #DDD; vertical-align: top;'class='" + TransformarTexto(loja) + "'>" + String.Format("{0:P}", totalContatosVendedor5 == 0 ? 0 : totalContatosVendedor6 / totalContatosVendedor5) + "</td>";
                relatorio += "<td style='border: 1px solid #DDD; vertical-align: top;' class='" + TransformarTexto(loja) + "'>" + String.Format("{0:C}", totalContatosVendedor6 == 0 ? 0 : totalContatosVendedor7 / totalContatosVendedor6) + "</td>";
                relatorio += "</tr>";

                relatorio = relatorio.Replace("#row" + vendedor, countRowSpanVendedor.ToString());

                #endregion "Ultima LINHA Inserindo o Total por Vendedor"

                #region "Ultima LINHA Inserindo o Total por Loja"

                relatorio += "<tr  class='subTotal " + TransformarTexto(supervisor) + "'>";
                relatorio += "<td style='border: 1px solid #DDD; vertical-align: top;' colspan='3' class='subTot" + TransformarTexto(loja) + "'>Sub Total</td>";
                relatorio += "<td style='border: 1px solid #DDD; vertical-align: top;'>" + totalLoja1 + "</td>";
                relatorio += "<td style='border: 1px solid #DDD; vertical-align: top;'>" + totalLoja2 + "</td>";
                relatorio += "<td style='border: 1px solid #DDD; vertical-align: top;'>" + totalLoja3 + "</td>";
                relatorio += "<td style='border: 1px solid #DDD; vertical-align: top;'>" + totalLoja4 + "</td>";
                relatorio += "<td style='border: 1px solid #DDD; vertical-align: top;'>" + totalLoja5 + "</td>";
                relatorio += "<td style='border: 1px solid #DDD; vertical-align: top;'>" + totalLoja6 + "</td>";
                relatorio += "<td style='border: 1px solid #DDD; vertical-align: top;'>" + String.Format("{0:C}", totalLoja7) + "</td>";
                relatorio += "<td style='border: 1px solid #DDD; vertical-align: top;'>" + (totalLoja5 == 0 ? "0" : ((totalLoja3 - totalLoja1) / totalLoja5).ToString()) + "</td>";
                relatorio += "<td style='border: 1px solid #DDD; vertical-align: top;'>" + String.Format("{0:P}", totalLoja5 == 0 ? 0 : totalLoja6 / totalLoja5) + "</td>";
                relatorio += "<td style='border: 1px solid #DDD; vertical-align: top;'>" + String.Format("{0:C}", totalLoja6 == 0 ? 0 : totalLoja7 / totalLoja6) + "</td>";
                relatorio += "</tr>";

                // Se existir mais de um vendedor parab mesma loja adiciona mais rowspan            
                countRowSpanLoja += lista.Where(x => x.loja == loja).ToList().GroupBy(x => x.nome_vendedor).Count();

                relatorio = relatorio.Replace("#row" + loja, countRowSpanLoja.ToString());

                #endregion "Ultima LINHA Inserindo o Total por Loja"

                #region "Ultima LINHA Inserindo o Total por Supervisor"

                relatorio += "<tr  class='subTotal'>";
                relatorio += "<td style='border: 1px solid #DDD; vertical-align: top;' colspan='4' class='subTot" + TransformarTexto(supervisor) + "'>Sub Total</td>";
                relatorio += "<td style='border: 1px solid #DDD; vertical-align: top;'>" + totalSupervisor1 + "</td>";
                relatorio += "<td style='border: 1px solid #DDD; vertical-align: top;'>" + totalSupervisor2 + "</td>";
                relatorio += "<td style='border: 1px solid #DDD; vertical-align: top;'>" + totalSupervisor3 + "</td>";
                relatorio += "<td style='border: 1px solid #DDD; vertical-align: top;'>" + totalSupervisor4 + "</td>";
                relatorio += "<td style='border: 1px solid #DDD; vertical-align: top;'>" + totalSupervisor5 + "</td>";
                relatorio += "<td style='border: 1px solid #DDD; vertical-align: top;'>" + totalSupervisor6 + "</td>";
                relatorio += "<td style='border: 1px solid #DDD; vertical-align: top;'>" + String.Format("{0:C}", totalSupervisor7) + "</td>";
                relatorio += "<td style='border: 1px solid #DDD; vertical-align: top;'>" + (totalSupervisor5 == 0 ? "0" : ((totalSupervisor3 - totalSupervisor1) / totalSupervisor5).ToString()) + "</td>";
                relatorio += "<td style='border: 1px solid #DDD; vertical-align: top;'>" + String.Format("{0:P}", totalSupervisor5 == 0 ? 0 : totalSupervisor6 / totalSupervisor5) + "</td>";
                relatorio += "<td style='border: 1px solid #DDD; vertical-align: top;'>" + String.Format("{0:C}", totalSupervisor6 == 0 ? 0 : totalSupervisor7 / totalSupervisor6) + "</td>";
                relatorio += "</tr>";

                // Se existir mais de um vendedor parab mesma loja adiciona mais rowspan            
                countRowSpanSupervisor += lista.Where(x => x.nome_supervisor == supervisor).ToList().GroupBy(x => x.nome_vendedor).Count() + lista.Where(x => x.nome_supervisor == supervisor).ToList().GroupBy(x => x.loja).Count();

                relatorio = relatorio.Replace("#row" + supervisor, countRowSpanSupervisor.ToString());

                #endregion "Ultima LINHA Inserindo o Total por Supervisor"
            }
            else
            {
                relatorio += "<tr  class='subTotal'>";
                relatorio += "<td style='border: 1px solid #DDD; vertical-align: top;' colspan='14' style='text-align:center'>Nenhum registro encontrado!</td>";
                relatorio += "</tr>";
            }

            relatorio += "</tbody>";


            if (lista.Count > 0)
            {
                // TOTAL GERAL
                relatorio += "<tfoot>";
                relatorio += "<tr  class='subTotal'>";
                relatorio += "<td style='border: 1px solid #DDD; vertical-align: top;' colspan='4'>Total Geral</td>";
                relatorio += "<td style='border: 1px solid #DDD; vertical-align: top;'>" + totalGeral1 + "</td>";
                relatorio += "<td style='border: 1px solid #DDD; vertical-align: top;'>" + totalGeral2 + "</td>";
                relatorio += "<td style='border: 1px solid #DDD; vertical-align: top;'>" + totalGeral3 + "</td>";
                relatorio += "<td style='border: 1px solid #DDD; vertical-align: top;'>" + totalGeral4 + "</td>";
                relatorio += "<td style='border: 1px solid #DDD; vertical-align: top;'>" + totalGeral5 + "</td>";
                relatorio += "<td style='border: 1px solid #DDD; vertical-align: top;'>" + totalGeral6 + "</td>";
                relatorio += "<td style='border: 1px solid #DDD; vertical-align: top;'>" + String.Format("{0:C}", totalGeral7) + "</td>";
                relatorio += "<td style='border: 1px solid #DDD; vertical-align: top;'>" + (totalGeral5 == 0 ? "0" : ((totalGeral3 - totalGeral1) / totalGeral5).ToString()) + "</td>";
                relatorio += "<td style='border: 1px solid #DDD; vertical-align: top;'>" + String.Format("{0:P}", totalGeral5 == 0 ? 0 : totalGeral6 / totalGeral5) + "</td>";
                relatorio += "<td style='border: 1px solid #DDD; vertical-align: top;'>" + String.Format("{0:C}", totalGeral6 == 0 ? 0 : totalGeral7 / totalGeral6) + "</td>";
                relatorio += "</tr>";
                relatorio += "<tfoot>";
            }

            relatorio += "</table>";

            return relatorio;

        }

        private string MontarDedoDuro(IList<ReportDedoDuro> lista)
        {
            String relatorio = String.Empty;
            string periodo = String.Empty;

            if (lista.Count() > 0)
                periodo = lista[0].periodo;

            relatorio += "<table class='table table-striped table-bordered table-hover tbPesquisa'>";
            relatorio += "<thead>";
            relatorio += "<tr>";
            //relatorio += "<th style='background: #DDD none repeat scroll 0% 0%;' >Supervisor</th>";
            relatorio += "<th style='background: #DDD none repeat scroll 0% 0%;' >Loja</th>";
            relatorio += "<th style='background: #DDD none repeat scroll 0% 0%;text-align:center;' >Vendedor</th>";
            relatorio += "<th style='background: #DDD none repeat scroll 0% 0%;text-align:center;' >Agenda</th>";
            relatorio += "<th style='background: #DDD none repeat scroll 0% 0%;text-align:center;' >Contatos </br>Efetuados</th>";
            relatorio += "<th style='background: #DDD none repeat scroll 0% 0%;text-align:center;'  title='Contatos Disponíveis no Período'>Nº de <br> Ligações</th>";
            relatorio += "<th style='background: #DDD none repeat scroll 0% 0%;text-align:center;' >Telefone </br>Inválido</th>";
            relatorio += "<th style='background: #DDD none repeat scroll 0% 0%;text-align:center;' >Não conseguiu </br>falar</th>";
            relatorio += "<th style='background: #DDD none repeat scroll 0% 0%;text-align:center;' >Baixa por </br>outro vendedor</th>";

            //relatorio += "<th style='background: #DDD none repeat scroll 0% 0%;' >Percentual de produtividade</th>";
            relatorio += "</tr>";
            relatorio += "</thead>";
            relatorio += "<tbody>";

            String vendedor = String.Empty;
            String loja = String.Empty;
            String supervisor = String.Empty;

            decimal totalContatosVendedor1 = 0;
            decimal totalContatosVendedor2 = 0;
            decimal totalContatosVendedor3 = 0;
            decimal totalContatosVendedor4 = 0;
            decimal totalContatosVendedor5 = 0;

            decimal totalSupervisor1 = 0;
            decimal totalSupervisor2 = 0;
            decimal totalSupervisor3 = 0;
            decimal totalSupervisor4 = 0;
            decimal totalSupervisor5 = 0;

            decimal totalLoja1 = 0;
            decimal totalLoja2 = 0;
            decimal totalLoja3 = 0;
            decimal totalLoja4 = 0;
            decimal totalLoja5 = 0;

            decimal totalGeral1 = 0;
            decimal totalGeral2 = 0;
            decimal totalGeral3 = 0;
            decimal totalGeral4 = 0;
            decimal totalGeral5 = 0;

            int countRowSpanVendedor = 0;
            int countRowSpanLoja = 0;
            int countRowSpanSupervisor = 0;
            int countLista = lista.Count();
            int countLista2 = 0;
            int corAlteranada = 0;

            foreach (ReportDedoDuro item in lista)
            {
                countLista2++;
                //< a href = 'javascript:DetalheGrid("plus", "DIOGO_VITAL_DA_SILVA", 2,  "vendedor", "ALPHAVILLE");' class="glyphicon glyphicon-plus" style="text-decoration: none !important;"></a>
                // Inserindo o Total por Vendedor
                if ((vendedor != String.Empty && vendedor != item.vendedor))// || countLista2 == countLista)
                {
                    relatorio += "<tr class='subTotal " + TransformarTexto(supervisor) + "'>";
                    relatorio += "<td colspan='2' class='subTot" + TransformarTexto(vendedor) + " " + TransformarTexto(loja) + "' ><a href='javascript:DetalheGrid(\"plus\", \"" + TransformarTexto(vendedor) + "\", 2,  \"" + "vendedor" + "\", \"" + TransformarTexto(vendedor) + "\");' class='glyphicon glyphicon-plus' style='text-decoration: none !important;'></a>" + TransformarTexto(vendedor) + " </td>";

                    //relatorio += "<td style='border: 1px solid #DDD; vertical-align: top; " + (corAlteranada == 0 ? "" : "#cor#") + "' colspan='2' class='subTot" + TransformarTexto(vendedor) + " " + TransformarTexto(loja) + "' >Sub Total</td>";

                    relatorio += "<td style='border: 1px solid #DDD; vertical-align: top; text-align:center; " + (corAlteranada == 0 ? "" : "#cor#") + "' class='" + TransformarTexto(loja) + "' >" + totalContatosVendedor5 + "</td>"; //CONTATO EFETUADO
                    relatorio += "<td style='border: 1px solid #DDD; vertical-align: top; text-align:center; " + (corAlteranada == 0 ? "" : "#cor#") + "' class='" + TransformarTexto(loja) + "' >" + totalContatosVendedor1 + "</td>";
                    relatorio += "<td style='border: 1px solid #DDD; vertical-align: top; text-align:center; " + (corAlteranada == 0 ? "" : "#cor#") + "' class='" + TransformarTexto(loja) + "' >" + totalContatosVendedor2 + "</td>";
                    relatorio += "<td style='border: 1px solid #DDD; vertical-align: top; text-align:center; " + (corAlteranada == 0 ? "" : "#cor#") + "' class='" + TransformarTexto(loja) + "' >" + totalContatosVendedor3 + "</td>";
                    relatorio += "<td style='border: 1px solid #DDD; vertical-align: top; text-align:center; " + (corAlteranada == 0 ? "" : "#cor#") + "' class='" + TransformarTexto(loja) + "' >" + totalContatosVendedor4 + "</td>";
                    //relatorio += "<td style='border: 1px solid #DDD; vertical-align: top; text-align:center; " + (corAlteranada == 0 ? "" : "#cor#") + "' class='" + TransformarTexto(loja) + "'>" + String.Format("{0:P}", totalContatosVendedor2 == 0 ? 0 : totalContatosVendedor1 / totalContatosVendedor2) + "</td>";
                    relatorio += "</tr>";

                    totalContatosVendedor1 = 0;
                    totalContatosVendedor2 = 0;
                    totalContatosVendedor3 = 0;
                    totalContatosVendedor4 = 0;
                    totalContatosVendedor5 = 0;

                    relatorio = relatorio.Replace("#row" + vendedor, countRowSpanVendedor.ToString());

                    countRowSpanVendedor = 0;
                }

                // Inserindo o Total por Loja
                if ((loja != String.Empty && loja != item.loja))// || countLista2 == countLista)
                {
                    relatorio += "<tr  class='subTotal " + TransformarTexto(supervisor) + "'>";

                    relatorio += "<td style='border: 1px solid #DDD; vertical-align: top; " + (corAlteranada == 0 ? "" : "#cor#") + "' colspan='3' class='subTot" + TransformarTexto(loja) + "'>Total por Loja: " + TransformarTexto(loja) + "</td>";
                    relatorio += "<td style='border: 1px solid #DDD; vertical-align: top; text-align:center; " + (corAlteranada == 0 ? "" : "#cor#") + "'>" + totalLoja5 + "</td>"; //CONTATO EFETUADO
                    relatorio += "<td style='border: 1px solid #DDD; vertical-align: top; text-align:center; " + (corAlteranada == 0 ? "" : "#cor#") + "'>" + totalLoja1 + "</td>"; //CONTATO DISPONIVEL
                    relatorio += "<td style='border: 1px solid #DDD; vertical-align: top; text-align:center; " + (corAlteranada == 0 ? "" : "#cor#") + "'>" + totalLoja2 + "</td>";
                    relatorio += "<td style='border: 1px solid #DDD; vertical-align: top; text-align:center; " + (corAlteranada == 0 ? "" : "#cor#") + "'>" + totalLoja3 + "</td>";
                    relatorio += "<td style='border: 1px solid #DDD; vertical-align: top; text-align:center; " + (corAlteranada == 0 ? "" : "#cor#") + "'>" + totalLoja4 + "</td>";

                    //relatorio += "<td style='border: 1px solid #DDD; vertical-align: top; text-align:center; " + (corAlteranada == 0 ? "" : "#cor#") + "'>" + String.Format("{0:P}", totalLoja2 == 0 ? 0 : totalLoja1 / totalLoja2) + "</td>";
                    relatorio += "</tr>";

                    totalLoja1 = 0;
                    totalLoja2 = 0;
                    totalLoja3 = 0;
                    totalLoja4 = 0;
                    totalLoja5 = 0;

                    // Se existir mais de um vendedor parab mesma loja adiciona mais rowspan                    
                    countRowSpanLoja += lista.Where(x => x.loja == loja).ToList().GroupBy(x => x.vendedor).Count();

                    relatorio = relatorio.Replace("#row" + loja, countRowSpanLoja.ToString());
                    countRowSpanLoja = 0;
                }

                // Inserindo o Total por Supervisor
                if ((supervisor != String.Empty && supervisor != item.supervisor && loja != item.loja))// || countLista2 == countLista)
                {
                    relatorio += "<tr  class='subTotal'>";
                    relatorio += "<td style='border: 1px solid #DDD; vertical-align: top; " + (corAlteranada == 0 ? "" : "#cor#") + "'colspan='3' class='subTot" + TransformarTexto(supervisor) + "'>Total por Supervisão: " + TransformarTexto(supervisor) + "</td>";
                    relatorio += "<td style='border: 1px solid #DDD; vertical-align: top; text-align:center;" + (corAlteranada == 0 ? "" : "#cor#") + "'>" + totalSupervisor5 + "</td>";
                    relatorio += "<td style='border: 1px solid #DDD; vertical-align: top; text-align:center;" + (corAlteranada == 0 ? "" : "#cor#") + "'>" + totalSupervisor1 + "</td>";
                    relatorio += "<td style='border: 1px solid #DDD; vertical-align: top; text-align:center;" + (corAlteranada == 0 ? "" : "#cor#") + "'>" + totalSupervisor2 + "</td>";
                    relatorio += "<td style='border: 1px solid #DDD; vertical-align: top; text-align:center;" + (corAlteranada == 0 ? "" : "#cor#") + "'>" + totalSupervisor3 + "</td>";
                    relatorio += "<td style='border: 1px solid #DDD; vertical-align: top; text-align:center;" + (corAlteranada == 0 ? "" : "#cor#") + "'>" + totalSupervisor4 + "</td>";
                    
                    //relatorio += "<td style='border: 1px solid #DDD; vertical-align: top; text-align:center;" + (corAlteranada == 0 ? "" : "#cor#") + "'>" + String.Format("{0:P}", totalSupervisor2 == 0 ? 0 : totalSupervisor1 / totalSupervisor2) + "</td>";

                    relatorio += "</tr>";

                    totalSupervisor1 = 0;
                    totalSupervisor2 = 0;
                    totalSupervisor3 = 0;
                    totalSupervisor4 = 0;
                    totalSupervisor5 = 0;

                    // Se existir mais de um vendedor parab mesma loja adiciona mais rowspan                    
                    countRowSpanSupervisor += lista.Where(x => x.supervisor == supervisor).ToList().GroupBy(x => x.vendedor).Count() + lista.Where(x => x.supervisor == supervisor).ToList().GroupBy(x => x.loja).Count();

                    relatorio = relatorio.Replace("#row" + supervisor, countRowSpanSupervisor.ToString());
                    countRowSpanSupervisor = 0;
                }

                relatorio += "<tr  class='" + item.supervisor + "'>";

                //if (countRowSpanSupervisor == 0)
                //    relatorio += "<td style='border: 1px solid #DDD; vertical-align: top; " + (corAlteranada == 0 ? "" : "#cor#") + "'rowspan='#row" + item.supervisor + "' class='supervisor" + TransformarTexto(item.supervisor) + " superSub' ><a href='javascript:DetalheGrid(\"minus\", \"" + TransformarTexto(item.supervisor) + "\", 4, \"supervisor\", \"\");' class='glyphicon glyphicon-minus' style='text-decoration: none !important;'></a><span class='nmsupervisor" + TransformarTexto(item.supervisor) + "'>&nbsp;&nbsp;" + Server.HtmlEncode(item.supervisor) + "</span></td>";

                if (countRowSpanLoja == 0)
                    relatorio += "<td style='border: 1px solid #DDD; vertical-align: top; " + (corAlteranada == 0 ? "" : "#cor#") + "'rowspan='#row" + item.loja + "' class='loja" + TransformarTexto(item.loja) + " " + TransformarTexto(item.loja) + "  vendeLoja' ><a href='javascript:DetalheGrid(\"minus\", \"" + TransformarTexto(item.loja) + "\", 3, \"loja\", \"\");' class='glyphicon glyphicon-minus' style='text-decoration: none !important;'></a><span class='nmloja" + TransformarTexto(item.loja) + "'>&nbsp;&nbsp;" + Server.HtmlEncode(item.loja) + "</span></td>";

                if (countRowSpanVendedor == 0)
                    relatorio += "<td style='border: 1px solid rgb(221, 221, 221); vertical - align: top;display: none;  " + (corAlteranada == 0 ? "" : "#cor#") + "'rowspan='#row" + item.vendedor + "' id='vendedor" + TransformarTexto(item.vendedor) + "' class='vendedor" + TransformarTexto(item.vendedor) + " " + TransformarTexto(item.loja) + " " + TransformarTexto(item.vendedor) + " vendeSub' ><a href='javascript:DetalheGrid(\"minus\", \"" + TransformarTexto(item.vendedor) + "\", 2, \"vendedor\", \"" + TransformarTexto(item.loja) + "\");' class='glyphicon glyphicon-minus' style='text-decoration: none !important;'></a><span class='nmvendedor" + TransformarTexto(item.vendedor) + "'>&nbsp;&nbsp;" + Server.HtmlEncode(item.vendedor) + "</span></td>";

                relatorio += "<td style='border: 1px solid #DDD; vertical-align: top;display: none; " + (corAlteranada == 0 ? "" : "#cor#") + "'class='" + TransformarTexto(item.loja) + " " + TransformarTexto(item.vendedor) + "'>" + Server.HtmlEncode(item.agendamento) + "</td>";
                relatorio += "<td style='border: 1px solid #DDD; vertical-align: top;display: none; text-align:center; " + (corAlteranada == 0 ? "" : "#cor#") + "'class='" + TransformarTexto(item.loja) + " " + TransformarTexto(item.vendedor) + "'>" + item.contatos_efetuados + "</td>";
                relatorio += "<td style='border: 1px solid #DDD; vertical-align: top;display: none; text-align:center; " + (corAlteranada == 0 ? "" : "#cor#") + "'class='" + TransformarTexto(item.loja) + " " + TransformarTexto(item.vendedor) + "'>" + item.ligacoes_disponiveis + "</td>";
                relatorio += "<td style='border: 1px solid #DDD; vertical-align: top;display: none; text-align:center; " + (corAlteranada == 0 ? "" : "#cor#") + "'class='" + TransformarTexto(item.loja) + " " + TransformarTexto(item.vendedor) + "'>" + item.tel_invalido + "</td>";
                relatorio += "<td style='border: 1px solid #DDD; vertical-align: top;display: none; text-align:center; " + (corAlteranada == 0 ? "" : "#cor#") + "'class='" + TransformarTexto(item.loja) + " " + TransformarTexto(item.vendedor) + "'>" + item.nao_conseguiu_falar + "</td>";
                relatorio += "<td style='border: 1px solid #DDD; vertical-align: top;display: none; text-align:center; " + (corAlteranada == 0 ? "" : "#cor#") + "'class='" + TransformarTexto(item.loja) + " " + TransformarTexto(item.vendedor) + "'>" + item.baixa_outro_vendedor + "</td>";
                //relatorio += "<td style='border: 1px solid #DDD; vertical-align: top;display: none; text-align:center; " + (corAlteranada == 0 ? "" : "#cor#") + "'class='" + TransformarTexto(item.loja) + " " + TransformarTexto(item.vendedor) + "'>" + String.Format("{0:P}", item.ligacoes_disponiveis == 0 ? 0 : Convert.ToDecimal(item.contatos_efetuados) / Convert.ToDecimal(item.ligacoes_disponiveis)) + "</td>";
                relatorio += "</tr>";

                countRowSpanVendedor++;
                countRowSpanLoja++;
                countRowSpanSupervisor++;

                vendedor = item.vendedor;
                loja = item.loja;
                supervisor = item.supervisor;

                totalContatosVendedor1 += item.ligacoes_disponiveis;
                totalContatosVendedor2 += item.tel_invalido;
                totalContatosVendedor3 += item.nao_conseguiu_falar;

                totalContatosVendedor4 += item.baixa_outro_vendedor;
                totalContatosVendedor5 += item.contatos_efetuados;

                totalLoja1 += item.ligacoes_disponiveis;
                totalLoja2 += item.tel_invalido;
                totalLoja3 += item.nao_conseguiu_falar;
                totalLoja4 += item.baixa_outro_vendedor;
                totalLoja5 += item.contatos_efetuados;

                totalSupervisor1 += item.ligacoes_disponiveis;
                totalSupervisor2 += item.tel_invalido;
                totalSupervisor3 += item.nao_conseguiu_falar;
                totalSupervisor4 += item.baixa_outro_vendedor;
                totalSupervisor5 += item.contatos_efetuados;

                totalGeral1 += item.ligacoes_disponiveis;
                totalGeral2 += item.tel_invalido;
                totalGeral3 += item.nao_conseguiu_falar;
                totalGeral4 += item.baixa_outro_vendedor;
                totalGeral5 += item.contatos_efetuados;

                if (corAlteranada == 0)
                    corAlteranada = 1;
                else
                    corAlteranada = 0;
            }

            // Só faz o total final se existir dados
            if (lista.Count > 0)
            {
                #region "Ultima LINHA Inserindo o Total por Vendedor"

                relatorio += "<tr class='subTotal " + TransformarTexto(supervisor) + "'>";
                relatorio += "<td colspan='2' class='subTot" + TransformarTexto(vendedor) + " " + TransformarTexto(loja) + "' ><a href='javascript:DetalheGrid(\"plus\", \"" + TransformarTexto(vendedor) + "\", 2,  \"" + "vendedor" + "\", \"" + TransformarTexto(vendedor) + "\");' class='glyphicon glyphicon-plus' style='text-decoration: none !important;'></a>" + TransformarTexto(vendedor) + " </td>";

                //relatorio += "<td style='border: 1px solid #DDD; vertical-align: top; " + (corAlteranada == 0 ? "" : "#cor#") + "' colspan='2' class='subTot" + TransformarTexto(vendedor) + " " + TransformarTexto(loja) + "' >Sub Total</td>";

                relatorio += "<td style='border: 1px solid #DDD; vertical-align: top; text-align:center; " + (corAlteranada == 0 ? "" : "#cor#") + "' class='" + TransformarTexto(loja) + "' >" + totalContatosVendedor5 + "</td>"; //CONTATOS EFETUADOS
                relatorio += "<td style='border: 1px solid #DDD; vertical-align: top; text-align:center; " + (corAlteranada == 0 ? "" : "#cor#") + "' class='" + TransformarTexto(loja) + "' >" + totalContatosVendedor1 + "</td>";
                relatorio += "<td style='border: 1px solid #DDD; vertical-align: top; text-align:center; " + (corAlteranada == 0 ? "" : "#cor#") + "' class='" + TransformarTexto(loja) + "' >" + totalContatosVendedor2 + "</td>";
                relatorio += "<td style='border: 1px solid #DDD; vertical-align: top; text-align:center; " + (corAlteranada == 0 ? "" : "#cor#") + "' class='" + TransformarTexto(loja) + "' >" + totalContatosVendedor3 + "</td>";
                relatorio += "<td style='border: 1px solid #DDD; vertical-align: top; text-align:center; " + (corAlteranada == 0 ? "" : "#cor#") + "' class='" + TransformarTexto(loja) + "' >" + totalContatosVendedor4 + "</td>";
                //relatorio += "<td style='border: 1px solid #DDD; vertical-align: top; text-align:center; " + (corAlteranada == 0 ? "" : "#cor#") + "' class='" + TransformarTexto(loja) + "'>" + String.Format("{0:P}", totalContatosVendedor2 == 0 ? 0 : totalContatosVendedor1 / totalContatosVendedor2) + "</td>";
                relatorio += "</tr>";

                relatorio = relatorio.Replace("#row" + vendedor, countRowSpanVendedor.ToString());

                #endregion "Ultima LINHA Inserindo o Total por Vendedor"

                #region "Ultima LINHA Inserindo o Total por Loja"

                relatorio += "<tr  class='subTotal " + TransformarTexto(supervisor) + "'>";
                relatorio += "<td style='border: 1px solid #DDD; vertical-align: top; " + (corAlteranada == 0 ? "" : "#cor#") + "'colspan='3' class='subTot" + TransformarTexto(loja) + "'>Total por Loja: " + TransformarTexto(loja) + "</td>";
                relatorio += "<td style='border: 1px solid #DDD; vertical-align: top; text-align:center; " + (corAlteranada == 0 ? "" : "#cor#") + "'>" + totalLoja5 + "</td>"; //CONTATOS EFETUADOS
                relatorio += "<td style='border: 1px solid #DDD; vertical-align: top; text-align:center; " + (corAlteranada == 0 ? "" : "#cor#") + "'>" + totalLoja1 + "</td>";
                relatorio += "<td style='border: 1px solid #DDD; vertical-align: top; text-align:center; " + (corAlteranada == 0 ? "" : "#cor#") + "'>" + totalLoja2 + "</td>";
                relatorio += "<td style='border: 1px solid #DDD; vertical-align: top; text-align:center; " + (corAlteranada == 0 ? "" : "#cor#") + "'>" + totalLoja3 + "</td>";
                relatorio += "<td style='border: 1px solid #DDD; vertical-align: top; text-align:center; " + (corAlteranada == 0 ? "" : "#cor#") + "'>" + totalLoja4 + "</td>";
                //relatorio += "<td style='border: 1px solid #DDD; vertical-align: top; text-align:center; " + (corAlteranada == 0 ? "" : "#cor#") + "'>" + String.Format("{0:P}", totalLoja2 == 0 ? 0 : totalLoja1 / totalLoja2) + "</td>";
                relatorio += "</tr>";

                // Se existir mais de um vendedor parab mesma loja adiciona mais rowspan            
                countRowSpanLoja += lista.Where(x => x.loja == loja).ToList().GroupBy(x => x.vendedor).Count();

                relatorio = relatorio.Replace("#row" + loja, countRowSpanLoja.ToString());

                #endregion "Ultima LINHA Inserindo o Total por Loja"

                #region "Ultima LINHA Inserindo o Total por Supervisor"

                //relatorio += "<tr  class='subTotal'>";                
                //relatorio += "<td style='border: 1px solid #DDD; vertical-align: top; " + (corAlteranada == 0 ? "" : "#cor#") + "'colspan='2' class='subTot" + TransformarTexto(supervisor) + "'>Sub Total</td>";
                //relatorio += "<td style='border: 1px solid #DDD; vertical-align: top; text-align:center; " + (corAlteranada == 0 ? "" : "#cor#") + "'>" + totalSupervisor1 + "</td>";
                //relatorio += "<td style='border: 1px solid #DDD; vertical-align: top; text-align:center; " + (corAlteranada == 0 ? "" : "#cor#") + "'>" + totalSupervisor2 + "</td>";
                //relatorio += "<td style='border: 1px solid #DDD; vertical-align: top; text-align:center; " + (corAlteranada == 0 ? "" : "#cor#") + "'>" + totalSupervisor3 + "</td>";
                //relatorio += "<td style='border: 1px solid #DDD; vertical-align: top; text-align:center; " + (corAlteranada == 0 ? "" : "#cor#") + "'>" + totalSupervisor4 + "</td>";
                ////relatorio += "<td style='border: 1px solid #DDD; vertical-align: top; text-align:center; " + (corAlteranada == 0 ? "" : "#cor#") + "'>" + String.Format("{0:P}", totalSupervisor2 == 0 ? 0 : totalSupervisor1 / totalSupervisor2) + "</td>";
                //relatorio += "</tr>";

                //// Se existir mais de um vendedor parab mesma loja adiciona mais rowspan            
                //countRowSpanSupervisor += lista.Where(x => x.supervisor == supervisor).ToList().GroupBy(x => x.vendedor).Count() + lista.Where(x => x.supervisor == supervisor).ToList().GroupBy(x => x.loja).Count();

                //relatorio = relatorio.Replace("#row" + supervisor, countRowSpanSupervisor.ToString());

                #endregion "Ultima LINHA Inserindo o Total por Supervisor"
            }
            else
            {
                relatorio += "<tr  class='subTotal'>";
                relatorio += "<td style='border: 1px solid #DDD; vertical-align: top; " + (corAlteranada == 0 ? "" : "#cor#") + "'colspan='3' style='text-align:center'>Nenhum registro encontrado!</td>";
                relatorio += "</tr>";
            }

            relatorio += "</tbody>";

            if (lista.Count > 0)
            {
                // TOTAL GERAL
                relatorio += "<tfoot>";
                relatorio += "<tr  class='subTotal'>";
                relatorio += "<td style='border: 1px solid #DDD; vertical-align: top; " + (corAlteranada == 0 ? "" : "#cor#") + "'colspan='3'>Total Geral</td>";
                relatorio += "<td style='border: 1px solid #DDD; vertical-align: top; text-align:center; " + (corAlteranada == 0 ? "" : "#cor#") + "'>" + totalGeral5 + "</td>";
                relatorio += "<td style='border: 1px solid #DDD; vertical-align: top; text-align:center; " + (corAlteranada == 0 ? "" : "#cor#") + "'>" + totalGeral1 + "</td>";
                relatorio += "<td style='border: 1px solid #DDD; vertical-align: top; text-align:center; " + (corAlteranada == 0 ? "" : "#cor#") + "'>" + totalGeral2 + "</td>";
                relatorio += "<td style='border: 1px solid #DDD; vertical-align: top; text-align:center; " + (corAlteranada == 0 ? "" : "#cor#") + "'>" + totalGeral3 + "</td>";
                relatorio += "<td style='border: 1px solid #DDD; vertical-align: top; text-align:center; " + (corAlteranada == 0 ? "" : "#cor#") + "'>" + totalGeral4 + "</td>";
                
                //relatorio += "<td style='border: 1px solid #DDD; vertical-align: top; text-align:center; " + (corAlteranada == 0 ? "" : "#cor#") + "'>" + String.Format("{0:P}", totalGeral2 == 0 ? 0 : totalGeral1 / totalGeral2) + "</td>";
                relatorio += "</tr>";
                relatorio += "<tfoot>";
            }

            relatorio += "</table>";

            return relatorio;
        }

        private string MontarRelatorioEfetVendedores(IList<ReportDedoDuro> lista)
        {
            String relatorio = String.Empty;

            relatorio += "<table class='table table-striped table-bordered table-hover tbPesquisa'>";
            relatorio += "<thead>";
            relatorio += "<tr>";
            relatorio += "<th style='background: #FFC000 none repeat scroll 0% 0%; font-weight: bold; text-align:center !important;' >Loja</th>";
            relatorio += "<th style='background: #FFC000 none repeat scroll 0% 0%; font-weight: bold; text-align:center !important;' >Vendedor</th>";
            relatorio += "<th style='background: #FFC000 none repeat scroll 0% 0%; font-weight: bold; text-align:center !important;' >Agendamento</th>";
            relatorio += "<th style='background: #FFC000 none repeat scroll 0% 0%; font-weight: bold; text-align:center !important;' >Contatos Efetuados</th>";
            relatorio += "<th style='background: #FFC000 none repeat scroll 0% 0%; font-weight: bold; text-align:center !important;'  title='Contatos Disponíveis no Período'>" + Server.HtmlEncode("Nº de Ligações") + "</th>";
            relatorio += "<th style='background: #FFC000 none repeat scroll 0% 0%; font-weight: bold; text-align:center !important;' >Percentual de produtividade</th>";
            relatorio += "<th style='background: #FFC000 none repeat scroll 0% 0%; font-weight: bold; text-align:center !important;' >Efetividade</th>";
            relatorio += "</tr>";
            relatorio += "</thead>";
            relatorio += "<tbody>";

            String vendedor = String.Empty;
            String loja = String.Empty;

            decimal totalContatosVendedor1 = 0;
            decimal totalContatosVendedor2 = 0;
            Decimal totalContatosVendedor3 = 0;

            decimal totalLoja1 = 0;
            decimal totalLoja2 = 0;
            decimal totalLoja3 = 0;

            decimal totalGeral1 = 0;
            decimal totalGeral2 = 0;
            decimal totalGeral3 = 0;

            int countRowSpanVendedor = 0;
            int countRowSpanLoja = 0;

            int countLista = lista.Count();
            int countLista2 = 0;
            int corAlteranada = 0;

            foreach (ReportDedoDuro item in lista)
            {
                countLista2++;

                // Inserindo o Total por Vendedor
                if ((vendedor != String.Empty && vendedor != item.vendedor))// || countLista2 == countLista)
                {
                    relatorio += "<tr class='subTotal " + TransformarTexto(loja) + "'>";
                    relatorio += "<td style='border: 1px solid #DDD; background: #B4C6E7 !important; vertical-align: top; text-align:center !important;" + (corAlteranada == 0 ? "" : "#cor#") + "' colspan='3' class='subTot" + TransformarTexto(vendedor) + " " + TransformarTexto(loja) + "' >Sub Total</td>";
                    relatorio += "<td style='border: 1px solid #DDD; background: #B4C6E7 !important; vertical-align: top; text-align:center; " + (corAlteranada == 0 ? "" : "#cor#") + "' class='" + TransformarTexto(loja) + "' >" + totalContatosVendedor1 + "</td>";
                    relatorio += "<td style='border: 1px solid #DDD; background: #B4C6E7 !important; vertical-align: top; text-align:center; " + (corAlteranada == 0 ? "" : "#cor#") + "' class='" + TransformarTexto(loja) + "' >" + totalContatosVendedor2 + "</td>";

                    relatorio += "<td style='border: 1px solid #DDD; background: #B4C6E7 !important; vertical-align: top; text-align:center; " + (corAlteranada == 0 ? "" : "#cor#") + "' class='" + TransformarTexto(loja) + "'>" + String.Format("{0:P}", (totalContatosVendedor2 == 0 || totalContatosVendedor2 == 0) ? 0 : totalContatosVendedor1 / totalContatosVendedor2) + "</td>";
                    relatorio += "<td style='border: 1px solid #DDD; background: #B4C6E7 !important; vertical-align: top; text-align:center; " + (corAlteranada == 0 ? "" : "#cor#") + "' class='" + TransformarTexto(loja) + "' >" + String.Format("{0:C}", totalContatosVendedor3) + "</td>";
                    relatorio += "</tr>";

                    totalContatosVendedor1 = 0;
                    totalContatosVendedor2 = 0;
                    totalContatosVendedor3 = 0;

                    relatorio = relatorio.Replace("#row" + vendedor, countRowSpanVendedor.ToString());

                    countRowSpanVendedor = 0;
                }

                // Inserindo o Total por Loja
                if ((loja != String.Empty && loja != item.loja))// || countLista2 == countLista)
                {
                    relatorio += "<tr  class='subTotal'>";
                    relatorio += "<td style='border: 1px solid #DDD; background: #B4C6E7 !important; vertical-align: top;" + (corAlteranada == 0 ? "" : "#cor#") + "' colspan='3' class='subTot" + TransformarTexto(loja) + "'>Sub Total</td>";
                    relatorio += "<td style='border: 1px solid #DDD; background: #B4C6E7 !important; vertical-align: top; text-align:center; " + (corAlteranada == 0 ? "" : "#cor#") + "'>" + totalLoja1 + "</td>";
                    relatorio += "<td style='border: 1px solid #DDD; background: #B4C6E7 !important; vertical-align: top; text-align:center; " + (corAlteranada == 0 ? "" : "#cor#") + "'>" + totalLoja2 + "</td>";
                    relatorio += "<td style='border: 1px solid #DDD; background: #B4C6E7 !important; vertical-align: top; text-align:center; " + (corAlteranada == 0 ? "" : "#cor#") + "'>" + String.Format("{0:P}", (totalLoja2 == 0 || totalLoja1 == 0) ? 0 : totalLoja1 / totalLoja2) + "</td>";
                    relatorio += "<td style='border: 1px solid #DDD; background: #B4C6E7 !important; vertical-align: top; text-align:center; " + (corAlteranada == 0 ? "" : "#cor#") + "'>" + String.Format("{0:C}", totalLoja3) + "</td>";
                    relatorio += "</tr>";

                    totalLoja1 = 0;
                    totalLoja2 = 0;
                    totalLoja3 = 0;

                    // Se existir mais de um vendedor parab mesma loja adiciona mais rowspan                    
                    countRowSpanLoja += lista.Where(x => x.loja == loja).ToList().GroupBy(x => x.vendedor).Count();

                    relatorio = relatorio.Replace("#row" + loja, countRowSpanLoja.ToString());
                    countRowSpanLoja = 0;
                }

                relatorio += "<tr  class='" + item.loja + "'>";

                if (countRowSpanLoja == 0)
                    relatorio += "<td style='border: 1px solid #DDD; vertical-align: top; " + (corAlteranada == 0 ? "" : "#cor#") + "'rowspan='#row" + item.loja + "' class='loja" + TransformarTexto(item.loja) + " " + TransformarTexto(item.loja) + "  vendeLoja' ><a href='javascript:DetalheGrid(\"minus\", \"" + TransformarTexto(item.loja) + "\", 3, \"loja\", \"\");' class='glyphicon glyphicon-minus' style='text-decoration: none !important;'></a><span class='nmloja" + TransformarTexto(item.loja) + "'>&nbsp;&nbsp;" + Server.HtmlEncode(item.loja) + "</span></td>";

                if (countRowSpanVendedor == 0)
                    relatorio += "<td style='border: 1px solid #DDD; vertical-align: top; " + (corAlteranada == 0 ? "" : "#cor#") + "'rowspan='#row" + item.vendedor + "' class='vendedor" + TransformarTexto(item.vendedor) + " " + TransformarTexto(item.loja) + " " + TransformarTexto(item.vendedor) + " vendeSub' ><a href='javascript:DetalheGrid(\"minus\", \"" + TransformarTexto(item.vendedor) + "\", 2, \"vendedor\", \"" + TransformarTexto(item.loja) + "\");' class='glyphicon glyphicon-minus' style='text-decoration: none !important;'></a><span class='nmvendedor" + TransformarTexto(item.vendedor) + "'>&nbsp;&nbsp;" + Server.HtmlEncode(AjustarNomeVendedor(item.vendedor)) + "</span>" + VendedorDesativado(item.vendedor) + "</td>";

                relatorio += "<td style='border: 1px solid #DDD; vertical-align: top; text-align:center !important;" + (corAlteranada == 0 ? "" : "#cor#") + "'class='" + TransformarTexto(item.loja) + " " + TransformarTexto(item.vendedor) + "'>" + Server.HtmlEncode(item.agendamento) + "</td>";
                relatorio += "<td style='border: 1px solid #DDD; vertical-align: top; text-align:center; " + (corAlteranada == 0 ? "" : "#cor#") + "'class='" + TransformarTexto(item.loja) + " " + TransformarTexto(item.vendedor) + "'>" + item.contatos_efetuados + "</td>";
                relatorio += "<td style='border: 1px solid #DDD; vertical-align: top; text-align:center; " + (corAlteranada == 0 ? "" : "#cor#") + "'class='" + TransformarTexto(item.loja) + " " + TransformarTexto(item.vendedor) + "'>" + item.ligacoes_disponiveis + "</td>";
                relatorio += "<td style='border: 1px solid #DDD; vertical-align: top; text-align:center; " + (corAlteranada == 0 ? "" : "#cor#") + "'class='" + TransformarTexto(item.loja) + " " + TransformarTexto(item.vendedor) + "'>" + String.Format("{0:P}", (item.ligacoes_disponiveis == 0 || item.contatos_efetuados == 0) ? 0 : Convert.ToDecimal(item.contatos_efetuados) / Convert.ToDecimal(item.ligacoes_disponiveis)) + "</td>";
                relatorio += "<td style='border: 1px solid #DDD; vertical-align: top; text-align:center; " + (corAlteranada == 0 ? "" : "#cor#") + "'class='" + TransformarTexto(item.loja) + " " + TransformarTexto(item.vendedor) + "'>" + String.Format("{0:C}", item.valor_venda_now) + "</td>";
                relatorio += "</tr>";

                countRowSpanVendedor++;
                countRowSpanLoja++;

                vendedor = item.vendedor;
                loja = item.loja;

                totalContatosVendedor1 += item.contatos_efetuados;
                totalContatosVendedor2 += item.ligacoes_disponiveis;
                totalContatosVendedor3 += item.valor_venda_now;

                totalLoja1 += item.contatos_efetuados;
                totalLoja2 += item.ligacoes_disponiveis;
                totalLoja3 += item.valor_venda_now;

                totalGeral1 += item.contatos_efetuados;
                totalGeral2 += item.ligacoes_disponiveis;
                totalGeral3 += item.valor_venda_now;

                if (corAlteranada == 0)
                    corAlteranada = 1;
                else
                    corAlteranada = 0;
            }

            // Só faz o total final se existir dados
            if (lista.Count > 0)
            {
                #region "Ultima LINHA Inserindo o Total por Vendedor"

                relatorio += "<tr  class='subTotal " + TransformarTexto(loja) + "'>";
                relatorio += "<td style='border: 1px solid #DDD; background: #B4C6E7 !important; vertical-align: top; " + (corAlteranada == 0 ? "" : "#cor#") + "'colspan='2' class='subTot" + TransformarTexto(vendedor) + " " + TransformarTexto(loja) + "'>Sub Total</td>";
                relatorio += "<td style='border: 1px solid #DDD; background: #B4C6E7 !important; vertical-align: top; text-align:center; text-align:center; " + (corAlteranada == 0 ? "" : "#cor#") + "'class='" + TransformarTexto(loja) + "' style='text-align:center'>" + totalContatosVendedor1 + "</td>";
                relatorio += "<td style='border: 1px solid #DDD; background: #B4C6E7 !important; vertical-align: top; text-align:center; text-align:center; " + (corAlteranada == 0 ? "" : "#cor#") + "'class='" + TransformarTexto(loja) + "' style='text-align:center'>" + totalContatosVendedor2 + "</td>";
                relatorio += "<td style='border: 1px solid #DDD; background: #B4C6E7 !important; vertical-align: top; text-align:center; text-align:center; " + (corAlteranada == 0 ? "" : "#cor#") + "'class='" + TransformarTexto(loja) + "'>" + String.Format("{0:P}", (totalContatosVendedor2 == 0 || totalContatosVendedor1 == 0) ? 0 : totalContatosVendedor1 / totalContatosVendedor2) + "</td>";
                relatorio += "<td style='border: 1px solid #DDD; background: #B4C6E7 !important; vertical-align: top; text-align:center; text-align:center; " + (corAlteranada == 0 ? "" : "#cor#") + "'class='" + TransformarTexto(loja) + "' style='text-align:center'>" + String.Format("{0:C}", totalContatosVendedor3) + "</td>";
                relatorio += "</tr>";

                relatorio = relatorio.Replace("#row" + vendedor, countRowSpanVendedor.ToString());

                #endregion "Ultima LINHA Inserindo o Total por Vendedor"

                #region "Ultima LINHA Inserindo o Total por Loja"

                relatorio += "<tr  class='subTotal'>";
                relatorio += "<td style='border: 1px solid #DDD; background: #B4C6E7 !important; vertical-align: top; " + (corAlteranada == 0 ? "" : "#cor#") + "'colspan='3' class='subTot" + TransformarTexto(loja) + "'>Sub Total</td>";
                relatorio += "<td style='border: 1px solid #DDD; background: #B4C6E7 !important; vertical-align: top; text-align:center; " + (corAlteranada == 0 ? "" : "#cor#") + "'>" + totalLoja1 + "</td>";
                relatorio += "<td style='border: 1px solid #DDD; background: #B4C6E7 !important; vertical-align: top; text-align:center; " + (corAlteranada == 0 ? "" : "#cor#") + "'>" + totalLoja2 + "</td>";
                relatorio += "<td style='border: 1px solid #DDD; background: #B4C6E7 !important; vertical-align: top; text-align:center; " + (corAlteranada == 0 ? "" : "#cor#") + "'>" + String.Format("{0:P}", (totalLoja1 == 0 || totalLoja2 == 0) ? 0 : totalLoja1 / totalLoja2) + "</td>";
                relatorio += "<td style='border: 1px solid #DDD; background: #B4C6E7 !important; vertical-align: top; text-align:center; " + (corAlteranada == 0 ? "" : "#cor#") + "'>" + String.Format("{0:C}", totalLoja3) + "</td>";
                relatorio += "</tr>";

                // Se existir mais de um vendedor parab mesma loja adiciona mais rowspan            
                countRowSpanLoja += lista.Where(x => x.loja == loja).ToList().GroupBy(x => x.vendedor).Count();

                relatorio = relatorio.Replace("#row" + loja, countRowSpanLoja.ToString());

                #endregion "Ultima LINHA Inserindo o Total por Loja"
            }
            else
            {
                relatorio += "<tr  class='subTotal'>";
                relatorio += "<td style='border: 1px solid #DDD; vertical-align: top; " + (corAlteranada == 0 ? "" : "#cor#") + "'colspan='5' style='text-align:center'>Nenhum registro encontrado!</td>";
                relatorio += "</tr>";
            }

            relatorio += "</tbody>";

            if (lista.Count > 0)
            {
                // TOTAL GERAL
                relatorio += "<tfoot>";
                relatorio += "<tr  class='subTotal'>";
                relatorio += "<td style='border: 1px solid #DDD; background: #B4C6E7 !important; vertical-align: top; " + (corAlteranada == 0 ? "" : "#cor#") + "'colspan='3'>Total Geral</td>";
                relatorio += "<td style='border: 1px solid #DDD; background: #B4C6E7 !important; vertical-align: top; text-align:center; " + (corAlteranada == 0 ? "" : "#cor#") + "'>" + totalGeral1 + "</td>";
                relatorio += "<td style='border: 1px solid #DDD; background: #B4C6E7 !important; vertical-align: top; text-align:center; " + (corAlteranada == 0 ? "" : "#cor#") + "'>" + totalGeral2 + "</td>";
                relatorio += "<td style='border: 1px solid #DDD; background: #B4C6E7 !important; vertical-align: top; text-align:center; " + (corAlteranada == 0 ? "" : "#cor#") + "'>" + String.Format("{0:P}", totalGeral2 == 0 ? 0 : totalGeral1 / totalGeral2) + "</td>";
                relatorio += "<td style='border: 1px solid #DDD; background: #B4C6E7 !important; vertical-align: top; text-align:center; " + (corAlteranada == 0 ? "" : "#cor#") + "'>" + String.Format("{0:C}", totalGeral3) + "</td>";
                relatorio += "</tr>";
                relatorio += "<tfoot>";
            }

            relatorio += "</table>";

            return relatorio;
        }

        private string TransformarTexto(string p)
        {
            if (p.IndexOf('<') > -1)
                return p.Substring(0, p.IndexOf('<')).Trim().Replace(" ", "_").Replace(".", "");
            else
                return p.Trim().Replace(" ", "_").Replace(".", "");
        }

        private string AjustarNomeVendedor(string p)
        {
            if (p.IndexOf('<') > -1)
                return p.Substring(0, p.IndexOf('<'));
            else
                return p;
        }

        private string VendedorDesativado(string p)
        {
            if (p.IndexOf('<') > -1)
                return p.Substring(p.IndexOf('<'));
            else
                return "";
        }

        public JsonResult ExportarExcel(string html, string nomeRelatorio)
        {
            LiteralControl ltlRelatorio = new LiteralControl();
            ltlRelatorio.Text = html.Replace("=!", "<").Replace("!=", ">");

            Response.Clear();
            Response.Buffer = true;

            Response.AddHeader("content-disposition",
             "attachment;filename=" + nomeRelatorio.Replace(" ", "_") + "_" + DateTime.Now.ToString("dd-MM-yyyy_HHmmss") + ".xls");
            Response.Charset = Encoding.UTF8.WebName;
            Response.ContentType = "application/vnd.ms-excel";

            StringWriter sw = new StringWriter();
            HtmlTextWriter hw = new HtmlTextWriter(sw);

            ltlRelatorio.RenderControl(hw);

            string style = @"<style> .textmode { mso-number-format:\@; } </style>";
            Response.Write(style);
            Response.Output.Write(sw.ToString());
            Response.Flush();
            Response.End();

            return JsonRetono(true);
        }

    }
}
