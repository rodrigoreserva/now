﻿using Now.Domain;
using Now.Models;
using Now.Domain.Contracts.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.IO;
using System.Net;
using System.Web.Mvc;

namespace Now.Controllers
{
    public class ImageController : BaseController
    {
        private IUsuarioService _serviceUsuario;
        private IVendedorService _serviceVendedor;
        public ImageController(IUsuarioService Usuarioservice, IVendedorService Vendedorservice)
        {
            _serviceUsuario = Usuarioservice;
            _serviceVendedor = Vendedorservice;
        }

        [HttpPost]
        public virtual ActionResult CropImage(
            string imagePath,
            int? cropPointX,
            int? cropPointY,
            int? imageCropWidth,
            int? imageCropHeight,
            string id 
            )
        {
            if (string.IsNullOrEmpty(imagePath)
                || !cropPointX.HasValue
                || !cropPointY.HasValue
                || !imageCropWidth.HasValue
                || !imageCropHeight.HasValue)
            {
                return new HttpStatusCodeResult((int)HttpStatusCode.BadRequest);
            }

            byte[] imageBytes = System.IO.File.ReadAllBytes(Server.MapPath("~/" + imagePath));
            byte[] croppedImage = ImageHelper.CropImage(imageBytes, cropPointX.Value, cropPointY.Value, imageCropWidth.Value, imageCropHeight.Value);

            string tempFolderName = Server.MapPath("~/" + ConfigurationManager.AppSettings["Image.Fotos"] + "/Temp");
            string fileName = (String.IsNullOrEmpty(id) ? UsuarioLogado.Usuario.id.ToString() : id) + "_" + DateTime.Now.ToString("HHmmss") + Path.GetExtension(imagePath);//Path.GetFileName(imagePath);

            try
            {
                FileHelper.SaveFile(croppedImage, Path.Combine(tempFolderName, fileName));
            }
            catch (Exception ex)
            {
                //Log an error     
                return new HttpStatusCodeResult((int)HttpStatusCode.InternalServerError);
            }

            string photoPath = string.Concat("/", ConfigurationManager.AppSettings["Image.Fotos"], "/Temp/", fileName);
            ImgTemp = photoPath;
            return JsonRetono(new { photoPath = photoPath });
        }

        public FilePathResult Image()
        {
            string filename = Request.Url.AbsolutePath.Replace("/home/image", "");
            string contentType = "";
            var filePath = new FileInfo(Server.MapPath("~/App_Data") + filename);

            var index = filename.LastIndexOf(".") + 1;
            var extension = filename.Substring(index).ToUpperInvariant();

            // Fix for IE not handling jpg image types
            contentType = string.Compare(extension, "JPG") == 0 ? "image/jpeg" : string.Format("image/{0}", extension);

            return File(filePath.FullName, contentType);
        }

        [HttpPost]
        public ContentResult UploadFiles()
        {
            var r = new List<UploadFilesResult>();

            foreach (string file in Request.Files)
            {
                HttpPostedFileBase hpf = Request.Files[file] as HttpPostedFileBase;
                if (hpf.ContentLength == 0)
                    continue;

                string savedFileName = Path.Combine(Server.MapPath("~/" + ConfigurationManager.AppSettings["Image.Fotos"] + "/Temp"),
                                                        Path.GetFileName(hpf.FileName));

                ImgTempOrig = savedFileName;             

                hpf.SaveAs(savedFileName);

                ResizeImage(savedFileName, savedFileName, 600, 400, true);

                r.Add(new UploadFilesResult()
                {
                    Name = hpf.FileName,
                    Length = hpf.ContentLength,
                    Type = hpf.ContentType,
                    Src = Path.Combine(ConfigurationManager.AppSettings["Image.Fotos"] + "/Temp", Path.GetFileName(hpf.FileName))
                });
            }
            return Content("{\"src\":\"" + r[0].Src.Replace("\\", "#") + "\"}", "application/json");
        }

        public void ResizeImage(string OriginalFile, string NewFile, int NewWidth, int MaxHeight, bool OnlyResizeIfWider)
        {
            try
            {
                System.Drawing.Image FullsizeImage = System.Drawing.Image.FromFile(OriginalFile);

                // Prevent using images internal thumbnail
                FullsizeImage.RotateFlip(System.Drawing.RotateFlipType.Rotate180FlipNone);
                FullsizeImage.RotateFlip(System.Drawing.RotateFlipType.Rotate180FlipNone);

                if (OnlyResizeIfWider)
                {
                    if (FullsizeImage.Width <= NewWidth)
                    {
                        NewWidth = FullsizeImage.Width;
                    }
                }

                int NewHeight = FullsizeImage.Height * NewWidth / FullsizeImage.Width;
                if (NewHeight > MaxHeight)
                {
                    // Resize with height instead
                    NewWidth = FullsizeImage.Width * MaxHeight / FullsizeImage.Height;
                    NewHeight = MaxHeight;
                }

                System.Drawing.Image NewImage = FullsizeImage.GetThumbnailImage(NewWidth, NewHeight, null, IntPtr.Zero);

                // Clear handle to original file so that we can overwrite it if necessary
                FullsizeImage.Dispose();

                // Save resized picture
                NewImage.Save(NewFile);

            }
            catch (Exception ex)
            {

            }
        }

        public JsonResult CarregarFoto(string tipo, string idVendedor)
        {
            string reload = "YESRELOAD";

            if (!String.IsNullOrEmpty(ImgTemp))
            {
                byte[] imageBytes = System.IO.File.ReadAllBytes(Server.MapPath(ImgTemp));

                FileHelper.SaveFile(imageBytes, ImgTemp.Replace("/Temp", ""));

                if (tipo == "U")//Usuario
                {
                    UsuarioLogado.Usuario.img_perfil = ImgTemp.Replace("/Temp", "");

                    @ViewBag.ImagemPerfil = ImgTemp.Replace("/Temp", "");

                    ModificaSessao(UsuarioLogado);

                    _serviceUsuario.SalvarImgPerfil(UsuarioLogado.Usuario.img_perfil, UsuarioLogado.Usuario.id.ToString());
                }
                else if (tipo == "V")//Vendedores
                {
                    reload = "NORELOAD#" + ImgTemp.Replace("/Temp", "");

                    IList<Vendedor> vendedoresAlt = new List<Vendedor>();
                    foreach (var item in UsuarioLogado.VendedoresUsuario)
                    {
                        if (item.id.ToString() == idVendedor)
                        {
                            item.img_perfil = ImgTemp.Replace("/Temp", "");
                        }

                        vendedoresAlt.Add(item);
                    }

                    //Salva no Banco
                    _serviceVendedor.SalvarImgPerfilVendedor(ImgTemp.Replace("/Temp", ""), idVendedor);

                    UsuarioLogado.VendedoresUsuario = vendedoresAlt;

                    ModificaSessao(UsuarioLogado);
                }

                System.IO.File.Delete(ImgTempOrig);
                System.IO.File.Delete(Server.MapPath(ImgTemp));

                ImgTemp = String.Empty;
            }
            return JsonRetono(new { reload = reload });
        }

    }
}
