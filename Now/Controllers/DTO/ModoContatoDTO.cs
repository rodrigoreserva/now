﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Now.Controllers.DTO
{
    public class ModoContatoDTO
    {
        public string Text { get; set; }
        public string Value { get; set; }
        public string Icon { get; set; }

    }
}
