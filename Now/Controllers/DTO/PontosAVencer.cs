﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Now.Controllers.DTO
{
    public class PontosAVencer
    {
        public string Mensagem { get; set; }
        public object[] PontosVencer { get; set; }
        public string Result { get; set; }

    }
}
