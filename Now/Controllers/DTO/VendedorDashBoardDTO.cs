﻿using Now.Domain;
using Now.Domain.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Now.Controllers.DTO
{
    [Serializable]
    public class VendedorDashBoardDTO
    {

        public int id { get; set; }
        public string id_vendedor { get; set; }
        public int loja_id { get; set; }
        public string apelido { get; set; }
        public string nome { get; set; }
        public string cpf { get; set; }
        public string regiao { get; set; }
        public DateTime created_at { get; set; }
        public DateTime updated_at { get; set; }
        public DateTime data_ativacao { get; set; }
        public DateTime data_desativacao { get; set; }
        public string img_perfil { get; set; }
        public string perfil { get; set; }
        public string img_perfil_intranet { get; set; }
        public DateTime? desativado_linx_data { get; set; }
        public string status_linx { get; set; }
        public string status_controle { get; set; }
        public string status_login { get; set; }
        public string login { get; set; }

        public int QtdLigacaoesDisponiveis { get; set; }
        public int QtdLigacaoesFeitas { get; set; }

        public IList<MenuAgendamento> agendamentos { get; set; }
        public MetricasDTO MetricasVendedor { get; set; }

    }
}