﻿using Now.Domain.Contracts.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Now.Controllers
{
    public class ChatController : BaseController
    {
        public IPerguntasQuemEleService _servicePerguntasQuemEle;
        public IPerguntasQuemEleRespostasService _servicePerguntasQuemEleRespostas;
        public ChatController(IPerguntasQuemEleService servicePerguntasQuemEle, IPerguntasQuemEleRespostasService servicePerguntasQuemEleRespostas)
        {
            _servicePerguntasQuemEle = servicePerguntasQuemEle;
            _servicePerguntasQuemEleRespostas = servicePerguntasQuemEleRespostas;
        }
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Cadastro()
        {
            return View();
        }

        public JsonResult SalvarMensagem(string mensagem)
        {
            try
            {
             

                return JsonRetono(new { sucesso = true });
            }
            catch (Exception ex)
            {
                return JsonRetono(new { sucesso = false, msg = "Erro ao carregar pergunta extra! " + ex.Message });
            }
        }

    }
}
