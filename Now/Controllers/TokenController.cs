﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Now.Controllers
{
    public class TokenController : Controller
    {
        //
        // GET: /Token/

        public ActionResult Index()
        {
            return View();
        }

        private string GenerateToken()
        {
            Random rnd = new Random();
            return rnd.Next(100000, 999999).ToString();
        }

        public JsonResult RetornaToken()
        {
            return JsonRetono(GenerateToken());
        }

        public JsonResult JsonRetono(object data)
        {
            JsonResult jsonResult = Json(data, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = int.MaxValue;
            return jsonResult;
        }

    }
}
