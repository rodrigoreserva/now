﻿using Now.Domain;
using Now.Models;
using Now.Domain.Contracts.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Uber.SDK;
using System.Web.Script.Serialization;
using System.Net.Mail;
using System.Configuration;
using System.Threading.Tasks;
using Now.Common.EnvioSMS;
using RestSharp;
using Newtonsoft.Json;
using Now.Controllers.DTO;

namespace Now.Controllers
{
    public class ClienteController : BaseController
    {
        private IClienteService _serviceCliente;
        private IAgendaService _serviceAgenda;
        private IVendaService _serviceVenda;
        private ILojaService _serviceLoja;
        private IContatoService _serviceContato;
        private IRetiradaLojaService _serviceRetiradaLoja;
        private IFraseService _serviceFrase;
        private IVendedorService _serviceVendedor;
        private IUsuarioService _serviceUsuario;
        private ILembreteService _serviceLembrete;
        public IRelatorioService _serviceRelatorio;
        public IProdutoService _serviceProduto;
        public ClienteController(IClienteService serviceCliente, IVendaService serviceVenda, IContatoService serviceContato, IRetiradaLojaService serviceRetiradaLoja, IFraseService serviceFrase, IVendedorService serviceVendedor, IUsuarioService serviceUsuario, ILembreteService serviceLembrete, IAgendaService serviceAgenda, IRelatorioService serviceRelatorio, IProdutoService serviceProduto, ILojaService serviceLoja)
        {
            _serviceCliente = serviceCliente;
            _serviceVenda = serviceVenda;
            _serviceLoja = serviceLoja;
            _serviceContato = serviceContato;
            _serviceRetiradaLoja = serviceRetiradaLoja;
            _serviceFrase = serviceFrase;
            _serviceVendedor = serviceVendedor;
            _serviceUsuario = serviceUsuario;
            _serviceAgenda = serviceAgenda;
            _serviceLembrete = serviceLembrete;
            _serviceRelatorio = serviceRelatorio;
            _serviceProduto = serviceProduto;
        }

        public ActionResult Index(Cliente cliente)
        {
            try
            {
                // Log do sistema
                LogSistema(new LogSistema { acao = "Acesso Tela de Clientes", login = UsuarioLogado.Usuario.email, tipo_log = "NAVEGACAO", vendedor_id = (String.IsNullOrEmpty(UsuarioLogado.IdVendedorSeleconado) ? 0 : Convert.ToInt32(UsuarioLogado.IdVendedorSeleconado)), loja_id = (String.IsNullOrEmpty(UsuarioLogado.IdLojaSeleconado) ? 0 : Convert.ToInt32(UsuarioLogado.IdLojaSeleconado)), metodo = "Index - Cliente", tela = "Cliente", user_id = UsuarioLogado.Usuario.id, ip = HttpContext.Request.UserHostAddress.ToString(), data = DateTime.Now });

                var busca = Request.QueryString["busca"];
                var buscafone = Request.QueryString["buscafone"];

                @ViewBag.ativarNovaTelaVendedor = Session["ativarNovaTelaVendedor"] == null ? false : Session["ativarNovaTelaVendedor"];

                if (!string.IsNullOrEmpty(busca))
                {
                    DateTime TempoInicial = DateTime.Now;

                    var pesquisa = busca;

                    // Validar se o login for gerente ou Vendedor
                    //if (UsuarioLogado.RegraPesquisa == 1)
                    //{
                    //    ViewBag.Clientes = _serviceCliente.BuscarCliente(OrigemLoja, pesquisa, pesquisa, pesquisa, UsuarioLogado.IdLojaSeleconado, UsuarioLogado.IdVendedorSeleconado, "1");
                    //}
                    //else
                    //{
                    ViewBag.Clientes = _serviceCliente.BuscarCliente(OrigemLoja, pesquisa, pesquisa, pesquisa);
                    //}

                    DateTime TempoFinal = DateTime.Now;

                    // Log do sistema
                    LogSistema(new LogSistema { processo_minutos = TempoProcesso(TempoInicial, TempoFinal), acao = "Tela de Clientes Pesquisando: " + pesquisa, login = UsuarioLogado.Usuario.email, tipo_log = "NAVEGACAO", vendedor_id = (String.IsNullOrEmpty(UsuarioLogado.IdVendedorSeleconado) ? 0 : Convert.ToInt32(UsuarioLogado.IdVendedorSeleconado)), loja_id = (String.IsNullOrEmpty(UsuarioLogado.IdLojaSeleconado) ? 0 : Convert.ToInt32(UsuarioLogado.IdLojaSeleconado)), metodo = "Index - Pesquisa", tela = "Cliente", user_id = UsuarioLogado.Usuario.id, ip = HttpContext.Request.UserHostAddress.ToString(), data = DateTime.Now });
                }
                else if (!string.IsNullOrEmpty(buscafone))
                {
                    DateTime TempoInicial = DateTime.Now;

                    var pesquisa = buscafone;

                    ViewBag.Clientes = _serviceCliente.BuscarCliente(OrigemLoja, pesquisa, pesquisa, pesquisa);

                    DateTime TempoFinal = DateTime.Now;
                    // Log do sistema
                    LogSistema(new LogSistema { processo_minutos = TempoProcesso(TempoInicial, TempoFinal), acao = "Tela de Clientes Pesquisando: " + pesquisa, login = UsuarioLogado.Usuario.email, tipo_log = "NAVEGACAO", vendedor_id = (String.IsNullOrEmpty(UsuarioLogado.IdVendedorSeleconado) ? 0 : Convert.ToInt32(UsuarioLogado.IdVendedorSeleconado)), loja_id = (String.IsNullOrEmpty(UsuarioLogado.IdLojaSeleconado) ? 0 : Convert.ToInt32(UsuarioLogado.IdLojaSeleconado)), metodo = "Index - Pesquisa", tela = "Cliente", user_id = UsuarioLogado.Usuario.id, ip = HttpContext.Request.UserHostAddress.ToString(), data = DateTime.Now });
                }
                else
                    ViewBag.Clientes = new List<Cliente>();
            }
            catch (Exception ex)
            {
                MsgErro();
            }

            return View();
        }

        public ActionResult Perfil(string ClienteId)
        {
            // Log do sistema
            LogSistema(new LogSistema { acao = "Acesso Tela do perfil Cliente", login = UsuarioLogado.Usuario.email, tipo_log = "NAVEGACAO", vendedor_id = (String.IsNullOrEmpty(UsuarioLogado.IdVendedorSeleconado) ? 0 : Convert.ToInt32(UsuarioLogado.IdVendedorSeleconado)), loja_id = (String.IsNullOrEmpty(UsuarioLogado.IdLojaSeleconado) ? 0 : Convert.ToInt32(UsuarioLogado.IdLojaSeleconado)), metodo = "Cliente - Perfil", tela = "Cliente", cliente_id = (String.IsNullOrEmpty(ClienteId) ? 0 : Convert.ToInt32(ClienteId)), user_id = UsuarioLogado.Usuario.id, ip = HttpContext.Request.UserHostAddress.ToString(), data = DateTime.Now });

            @ViewBag.ativarNovaTelaVendedor = Session["ativarNovaTelaVendedor"] == null ? false : Session["ativarNovaTelaVendedor"];

            if (!String.IsNullOrEmpty(ClienteId))
            {
                Session["ClienteDependente"] = null;
                Session["ClienteIndicacaoProduto"] = null;
                Session["ProdutosIndicados"] = null;
                // Tipo do Cliente Dependente
                ViewBag.tipoCliente = "EXISTENTE";

                DateTime TempoInicial = DateTime.Now;
                string _idAgenda = Request.QueryString["idAgenda"];
                string _agenda = Request.QueryString["param"];
                string _agendaTitulo = Request.QueryString["tituloAgenda"];

                var cliente = BuscarCliente(ClienteId);
                ///  var recomendacao = cliente.recomendacao.ToArray(); 

                //if (cliente.recomendacao != null)
                //{
                //    JavaScriptSerializer serializer = new JavaScriptSerializer();
                //    var deserializedResult = serializer.Deserialize<List<Produto>>(cliente.recomendacao);
                //    IList<Produto> listProdutos = new List<Produto>();
                //    foreach (Produto item in deserializedResult)
                //    {
                //        listProdutos.Add(_serviceProduto.ObterProduto(item.produto, item.cor_produto));
                //    }
                //    ViewBag.Produtos = Session["ProdutosIndicados"] = listProdutos;
                //}

                // Mapa loja só aparece pra quem é de reservado
                if (cliente.apto_reservado_reserva == 1 || cliente.apto_reservado_mini == 1 || cliente.apto_reservado_eva == 1)
                {
                    var _loja = lojaUsuario();
                    ViewBag.EnderecoCliente = _loja.endereco + ", " + _loja.numero + " " + _loja.bairro + " " + _loja.cidade + " " + _loja.uf + " " + _loja.cep;
                }

                //TODO - VERIFICAR QUANDO FOR EVA, MINI OU OFICINA PARA QUAL ID DE LOJA QUE VAI BUSCAR - EX.: 1232942
                //var loja = cliente.loja_id == 0 ? (cliente.loja_id_original) : cliente.loja_id;
                //ObterPontosFidelidade(BuscarLoja(loja).id_loja, cliente.cpf);

                // Lista os dependentes
                var listDependente = ListarDependentes(ClienteId);
                var vendas = BuscarVendas(ClienteId);

                //var dados = String.IsNullOrEmpty(UsuarioLogado.VendedoresUsuario[0].img_perfil_intranet) ? Url.Content("~/dist/img/user2-160x160.jpg") : "http://usereserva.dyndns.org:82/Intranet/images/fotosVendedores/" + UsuarioLogado.VendedoresUsuario[0].img_perfil_intranet;
                ViewBag.ListaVendedoresContato = UsuarioLogado.VendedoresUsuario == null ? new List<Vendedor>() : UsuarioLogado.VendedoresUsuario;
                ViewBag.Termometro = ObterTemometroList();
                ViewBag.ModoContato = ObterModoContatoList();
                ViewBag.ModoContatoComercioDigital = ObterModoContatoComercioDigitalList();
                ViewBag.ContatoCliente = "55" + cliente.cel_ddd + cliente.cel;

                var dados_tel_cliente = "";

                if ((cliente.cel_ddd != "") || (cliente.tel_ddd != ""))
                    dados_tel_cliente = cliente.cel != "" ? Convert.ToDecimal(cliente.cel_ddd).ToString("000") + ValidaTelefone(cliente.cel) : Convert.ToDecimal(cliente.tel_ddd).ToString("000") + ValidaTelefone(cliente.tel);

                ViewBag.LigarContatoCliente = dados_tel_cliente;

                //hddModoContatoComercioDigital
                //ViewBag.MensagemWhatsapp = mensagem;

                // Detalhar Reservado Por GRIFFE
                VendaReservado vendaReservado = new VendaReservado();
                // RESERVADO RESERVA
                vendaReservado.VendasReservadoRESERVA = vendas.Where(x => x.tipo_loja == "RESERVA" && x.Operacao.Contains("RESERVADO")).ToList();
                vendaReservado.VendasReservadoMINI = vendas.Where(x => x.tipo_loja == "MINI" && x.Operacao.Contains("RESERVADO")).ToList();
                vendaReservado.VendasReservadoEVA = vendas.Where(x => x.tipo_loja == "EVA" && x.Operacao.Contains("RESERVADO")).ToList();

                ViewBag.VendaReservado = vendaReservado;

                var contatos = ContatosPorCliente(ClienteId);

                ViewBag.TemContatoOdiou = contatos.Where(x => x.termometro == "Odiou").Count() > 0 ? true : false;

                var dataUltimoContatoReserva = contatos.Where(x => x.tipo_loja == "RESERVA").OrderByDescending(x => x.data_contato).ToList().FirstOrDefault();
                var dataUltimoContatoMini = contatos.Where(x => x.tipo_loja == "MINI").OrderByDescending(x => x.data_contato).ToList().FirstOrDefault();
                var dataUltimoContatoEva = contatos.Where(x => x.tipo_loja == "EVA").OrderByDescending(x => x.data_contato).ToList().FirstOrDefault();

                // Verifica se o cliente está fazendo aniversário hoje
                var agendaCliente = _serviceAgenda.FindCalendars(_idAgenda, "", UsuarioLogado.Usuario.id.ToString(), UsuarioLogado.IdsLojasUsuario, UsuarioLogado.IdVendedorSeleconado, false, ClienteId).FirstOrDefault();

                ViewBag.ContatoDiretoNaAgenda = false;



                // Verifica de o cliente está em uma agenda, se estiver ele não pode receber o contato por contato direto.
                if (_idAgenda == null)
                    if (agendaCliente != null)
                    {
                        string bodyAlerta = "<div class=\"modal-body\" style=\"background: url(../app/img/fundo_alerta.jpg) top center; height: 600px; padding-top:30px\"><div class=\"col-md-12 text-center\"><img src=\"../Images/exclamation.png\" alt=\"Atenção\" class=\"img-responsive\" style=\"display:inline-block!important\" /></div><div class=\"col-md-12 text-center\" style=\"padding-top:20px\"><p><h1>O cliente está na agenda " + agendaCliente.titulo_tipo_agendamento + " na loja " + agendaCliente.filial + "! <br><br>Não é possível registrar o contato DIRETO.</h1></p>#BOTAO#</div></div>";
                        ViewBag.ContatoDiretoNaAgenda = true;
                        string botaoAgenda = "<a href=\"?clienteId=" + agendaCliente.cliente_id + "&idAgenda=" + agendaCliente.tipo_agendamento_id + "&param=" + agendaCliente.agendamento + "&tituloAgenda=" + agendaCliente.titulo_tipo_agendamento + "&tipoPagina=Agenda\" class=\"btn2-gradient red text-uppercase text-bold text-nowrap\">IR PARA AGENDA <i class=\"glyphicon glyphicon-share\"></i></a>";

                        if (agendaCliente.loja_id.ToString() == UsuarioLogado.IdLojaSeleconado && ((UsuarioLogado.Usuario.Perfil.ToUpper() == "Administrador Sistema".ToUpper() || UsuarioLogado.Usuario.Perfil.ToUpper() == "Gerente".ToUpper()) || UsuarioLogado.IdVendedorSeleconado == agendaCliente.vendedor_id.ToString()))
                        {
                            bodyAlerta = bodyAlerta.Replace("#BOTAO#", botaoAgenda);
                        }
                        else
                            bodyAlerta = bodyAlerta.Replace("#BOTAO#", "");

                        ViewBag.MsgContatoDiretoNaAgenda = bodyAlerta;
                    }

                string _alertAniversario = String.Empty;
                string _aniversario = "false";
                DateTime dataUtimContato = Convert.ToDateTime(DateTime.Now.AddDays(-1).ToString("dd/MM/yyyy"));

                if (contatos != null)
                    if (contatos.Count() > 0)
                        dataUtimContato = Convert.ToDateTime(contatos.FirstOrDefault().data_contato.ToString("dd/MM/yyyy"));

                if (dataUtimContato < Convert.ToDateTime(DateTime.Now.ToString("dd/MM/yyyy")))
                {
                    if (agendaCliente != null)
                    {
                        if (!String.IsNullOrEmpty(agendaCliente.observacao))
                        {
                            _alertAniversario = agendaCliente.observacao;
                            _aniversario = "true";
                        }
                    }
                }

                var data_atual = DateTime.Today.Day.ToString("00") + "/" + DateTime.Today.Month.ToString("00") + "/" + DateTime.Today.Year;
                var localizar_cupom = ListaCupomDesconto().Where(x => x.data_inicio == data_atual);
                var cupom = localizar_cupom.Count() > 0 ? localizar_cupom.First().parametro : "";


                var _data_aniversario = false;
                if (cliente.dt_nasc.Day.ToString("00") == DateTime.Today.Day.ToString("00") && (cliente.dt_nasc.Month.ToString("00") == DateTime.Today.Month.ToString("00")))
                {
                    _data_aniversario = true;
                }

                @ViewBag.TelefoneCliente = "55" + (cliente.cel == "" ? cliente.tel_ddd + cliente.tel : cliente.cel_ddd + cliente.cel);

                ViewBag.MensagemWhatsapp00 = " Olá " + FirstCharToUpper(cliente.nome.Split(' ')[0]) + ", \r";
                ViewBag.MensagemWhatsapp01 = " Meu nome é " + FirstCharToUpper(UsuarioLogado.Usuario.username.Split(' ')[0]) + " e sou do ecommerce da Reserva.".Replace(" ", "%20");

                if (_data_aniversario)
                {
                    ViewBag.MensagemWhatsapp02 = " Passando para te desejar um Feliz Aniversário!!!".Replace(" ", "%20");
                    ViewBag.MensagemWhatsapp03 = " E de presente, te envio um link que te dará 20% de desconto e frete grátis!!!".Replace(" ", "%20");
                }
                else
                {
                    ViewBag.MensagemWhatsapp02 = " Conforme consta em nosso cadastro, já tem um tempo que não nos prestigia e estou aqui para mudar isso =)".Replace(" ", "%20");
                    ViewBag.MensagemWhatsapp03 = " Para acelerar o seu retorno te envio um link que te dará 20% de desconto e frete grátis.".Replace(" ", "%20");
                }

                //ViewBag.MensagemWhatsapp031 = " Cupom: " + cupom.Replace(" ", "%20");
                ViewBag.MensagemWhatsapp04 = " Como você não tem meu número gravado na agenda, basta responder Oi que o link é ativado ou copiar e colar o endereço do link no seu navegador.".Replace(" ", "%20");
                ViewBag.MensagemWhatsapp05 = " Você cairá na página de produtos que mais gosta.".Replace(" ", "%20");
                ViewBag.MensagemWhatsapp06 = " Esse link é válido por 24h e serve para todo o site, exceto produtos já com desconto.".Replace(" ", "%20");
                ViewBag.MensagemWhatsapp08 = " Esse é o meu WhatsApp e estou 100% à sua disposição para o que necessitar.".Replace(" ", "%20");
                ViewBag.MensagemWhatsapp09 = " Grande abraço";



                ViewBag.Alerta = _alertAniversario;
                ViewBag.ExibirAlerta = _aniversario;

                // Se o Cliente não for de preferencia do vendedor exibir a mensagem
                // QUANDO UM VENDEDOR ENTRAR EM UM CLIENTE QUE NÃO SEJA SEU DE PREFERÊNCIA SERÁ EXIBIDO UMA MENSAGEM QUE PROIBIÇÃO DO REGISTRO DE CONTATO.
                if (!String.IsNullOrEmpty(UsuarioLogado.IdVendedorSeleconado))
                {
                    // Se o cliente for preferencial do ecommerce ele não terá bloqueio
                    if (cliente.loja_id != 87)
                    {
                        if (!ViewBag.ContatoDiretoNaAgenda)
                        {
                            bool eVendedor = false;
                            if (cliente.vendedor_id.ToString() == UsuarioLogado.IdVendedorSeleconado)
                                eVendedor = true;
                            if (cliente.vendedor_id_mini.ToString() == UsuarioLogado.IdVendedorSeleconado)
                                eVendedor = true;
                            if (cliente.vendedor_id_eva.ToString() == UsuarioLogado.IdVendedorSeleconado)
                                eVendedor = true;


                            // verifica se esta na agenda de pós venda
                            var agendaClientePos = _serviceAgenda.FindCalendars("47", "", UsuarioLogado.Usuario.id.ToString(), UsuarioLogado.IdsLojasUsuario, UsuarioLogado.IdVendedorSeleconado, false, ClienteId).FirstOrDefault();

                            if (agendaClientePos != null)
                                if (agendaClientePos.vendedor_id.ToString() == UsuarioLogado.IdVendedorSeleconado)
                                {
                                    eVendedor = true;
                                }

                            if (!eVendedor)
                            {
                                ViewBag.ExibirAlerta = "true";
                                ViewBag.Alerta = "<div class=\"modal-body\" style=\"background: url(../images/prob_contato.jpg) top center; height: 600px; padding-top:30px\"><div class=\"col-md-12 text-center\" style=\"padding-top:110px\"><p><h3>O Vendedor de Preferência é " + cliente.NomeVendedor + "!</h3></p></div></div>";
                            }
                        }
                    }
                }

                ViewBag.Venda = vendas;

                // Última Compra
                if (vendas.Count > 0)
                {
                    cliente.ult_compra = vendas.FirstOrDefault().Data;
                    ViewBag.ticket = vendas.FirstOrDefault().Ticket;
                }

                // Último Contato
                if (contatos.Count > 0)
                {
                    cliente.ult_contato = contatos.FirstOrDefault().data_contato;

                    //•	Bloquear o registro do cliente que foi contatado nos últimos 3 dias
                    //•	Criar uma pop up em formato de alerta, para informar ao vendedor que o cliente foi contatado nos últimos 30 dias

                    if (String.IsNullOrEmpty(_idAgenda))
                    {
                        if (dataUltimoContatoReserva != null)
                        {
                            if ((TipoLoja() == "RESERVA") && (DateTime.Now - dataUltimoContatoReserva.data_contato).Days < 30)
                                cliente.dias_ult_contato = (DateTime.Now - dataUltimoContatoReserva.data_contato).Days;
                        }

                        if (dataUltimoContatoMini != null)
                        {
                            if ((TipoLoja() == "MINI") && (DateTime.Now - dataUltimoContatoMini.data_contato).Days < 30)
                                cliente.dias_ult_contato = (DateTime.Now - dataUltimoContatoMini.data_contato).Days;
                        }

                        if (dataUltimoContatoEva != null)
                        {
                            if ((TipoLoja() == "EVA") && (DateTime.Now - dataUltimoContatoEva.data_contato).Days < 30)
                                cliente.dias_ult_contato = (DateTime.Now - dataUltimoContatoEva.data_contato).Days;
                        }
                    }
                }

                int _vendedor_id = String.IsNullOrEmpty(UsuarioLogado.IdVendedorSeleconado) ? 0 : Convert.ToInt32(UsuarioLogado.IdVendedorSeleconado);

                ViewBag.VendedorPref = _vendedor_id == 0 ? false : (cliente.vendedor_id == _vendedor_id || cliente.vendedor_id_mini == _vendedor_id || cliente.vendedor_id_eva == _vendedor_id);

                ViewBag.Cliente = cliente;
                ViewBag.Contatos = contatos;
                ViewBag.RetiradaLoja = ListarRetiradas6Meses(ClienteId);
                ViewBag.CestaProduto = GrupoProdutos(ClienteId);
                //ViewBag.Frases = ListarFrases(ClienteId);
                ViewBag.FrasesMotivoLigacao = ListarFrasesMotivoLigacao(ClienteId);




                ViewBag.TipoAgendas = TipoAgendaSelectList(_agenda);

                DateTime TempoFinal = DateTime.Now;
                // Log do sistema
                LogSistema(new LogSistema { processo_minutos = TempoProcesso(TempoInicial, TempoFinal), acao = "Carregando Perfil do Cliente: " + cliente.nome + " # Cpf: " + cliente.cpf, login = UsuarioLogado.Usuario.email, tipo_log = "NAVEGACAO", vendedor_id = (String.IsNullOrEmpty(UsuarioLogado.IdVendedorSeleconado) ? 0 : Convert.ToInt32(UsuarioLogado.IdVendedorSeleconado)), loja_id = (String.IsNullOrEmpty(UsuarioLogado.IdLojaSeleconado) ? 0 : Convert.ToInt32(UsuarioLogado.IdLojaSeleconado)), metodo = "Cliente - Perfil", tela = "Cliente", cliente_id = (String.IsNullOrEmpty(ClienteId) ? 0 : Convert.ToInt32(ClienteId)), user_id = UsuarioLogado.Usuario.id, ip = HttpContext.Request.UserHostAddress.ToString(), data = DateTime.Now });

            }
            return View("Perfil.min");
        }

        public async Task<JsonResult> SalvarContatoComercioDigitalDireto(string clienteId, string id_agenda, string parametro, string enviar_avaliacao, string status, string modo, string vendedor_id, string tipoPagina, string ticket)
        {
            var cliente = BuscarCliente(clienteId);

            var dados_tel_cliente = "";

            if ((cliente.cel_ddd != "") || (cliente.tel_ddd != ""))
                dados_tel_cliente = cliente.cel != "" ? "55" + Convert.ToDecimal(cliente.cel_ddd).ToString("000") + ValidaTelefone(cliente.cel) : "55" + Convert.ToDecimal(cliente.tel_ddd).ToString("000") + ValidaTelefone(cliente.tel);

            ViewBag.ContatoCliente = dados_tel_cliente;

            string _idAgenda = Request.QueryString["idAgenda"] == null ? id_agenda : Request.QueryString["idAgenda"];
            var data_atual = DateTime.Today.Day.ToString("00") + "/" + DateTime.Today.Month.ToString("00") + "/" + DateTime.Today.Year;
            var localizar_cupom = ListaCupomDesconto().Where(x => x.data_inicio == data_atual);
            var cupom = localizar_cupom.Count() > 0 ? localizar_cupom.First().parametro : "";

            // Verifica se o cliente está fazendo aniversário hoje
            var _aniversario = false;
            if (cliente.dt_nasc.Day.ToString("00") == DateTime.Today.Day.ToString("00") && (cliente.dt_nasc.Month.ToString("00") == DateTime.Today.Month.ToString("00")))
            {
                _aniversario = true;
            }

            var mensagem = "Olá " + FirstCharToUpper(cliente.nome.Split(' ')[0]) + ", " +
                           "\r\n\r\n" + "Sou " + FirstCharToUpper(UsuarioLogado.Usuario.username.Split(' ')[0]) + " da " + FirstCharToUpper(UsuarioLogado.Usuario.Lojas.FirstOrDefault().filial) + " tudo bem ? " +
                           "\r\n\r\n";

            ViewBag.Whatsapp = mensagem;
            ViewBag.MensagemWhatsapp = "https://wa.me/" + dados_tel_cliente + "/?text=";

            return JsonRetono(new { sucesso = true, msg = "Whatsapp enviado com sucesso", mensagem = ViewBag.MensagemWhatsapp, whatsapp = ViewBag.Whatsapp });
        }


        private static string ValidaTelefone(string telefone)
        {
            return telefone != null ? telefone.Length == 8 ? "9" + telefone : telefone : string.Empty;
        }

        public ActionResult Favoritos()
        {
            @ViewBag.ativarNovaTelaVendedor = Session["ativarNovaTelaVendedor"] == null ? false : Session["ativarNovaTelaVendedor"];

            // Log do sistema
            //LogSistema(new LogSistema { acao = "Acesso Tela Clientes Favoritos", login = UsuarioLogado.Usuario.email, tipo_log = "CARREGAMENTO", vendedor_id = (String.IsNullOrEmpty(UsuarioLogado.IdVendedorSeleconado) ? 0 : Convert.ToInt32(UsuarioLogado.IdVendedorSeleconado)), loja_id = (String.IsNullOrEmpty(UsuarioLogado.IdLojaSeleconado) ? 0 : Convert.ToInt32(UsuarioLogado.IdLojaSeleconado)), metodo = "Cliente - Perfil", tela = "Cliente", cliente_id = (String.IsNullOrEmpty(ClienteId) ? 0 : Convert.ToInt32(ClienteId)), user_id = UsuarioLogado.Usuario.id, ip = HttpContext.Request.UserHostAddress.ToString(), data = DateTime.Now });
            //DateTime TempoInicial = DateTime.Now;

            IList<ReportClientesFavoritos> list = _serviceRelatorio.ListarRelatorioComVendedor<ReportClientesFavoritos>("SP_RelatorioClientesFavoritos", UsuarioLogado.IdVendedorSeleconado);


            //DateTime TempoFinal = DateTime.Now;
            //LogSistema(new LogSistema { processo_minutos = TempoProcesso(TempoInicial, TempoFinal), acao = "Carregando Perfil do Cliente: " + cliente.nome + " # Cpf: " + cliente.cpf, login = UsuarioLogado.Usuario.email, tipo_log = "CARREGAMENTO", vendedor_id = (String.IsNullOrEmpty(UsuarioLogado.IdVendedorSeleconado) ? 0 : Convert.ToInt32(UsuarioLogado.IdVendedorSeleconado)), loja_id = (String.IsNullOrEmpty(UsuarioLogado.IdLojaSeleconado) ? 0 : Convert.ToInt32(UsuarioLogado.IdLojaSeleconado)), metodo = "Cliente - Perfil", tela = "Cliente", cliente_id = (String.IsNullOrEmpty(ClienteId) ? 0 : Convert.ToInt32(ClienteId)), user_id = UsuarioLogado.Usuario.id, ip = HttpContext.Request.UserHostAddress.ToString(), data = DateTime.Now });
            return View(list);
        }

        public ActionResult QuemeEle()
        {
            @ViewBag.ativarNovaTelaVendedor = Session["ativarNovaTelaVendedor"] == null ? false : Session["ativarNovaTelaVendedor"];

            // Log do sistema
            //LogSistema(new LogSistema { acao = "Acesso Tela Clientes Favoritos", login = UsuarioLogado.Usuario.email, tipo_log = "CARREGAMENTO", vendedor_id = (String.IsNullOrEmpty(UsuarioLogado.IdVendedorSeleconado) ? 0 : Convert.ToInt32(UsuarioLogado.IdVendedorSeleconado)), loja_id = (String.IsNullOrEmpty(UsuarioLogado.IdLojaSeleconado) ? 0 : Convert.ToInt32(UsuarioLogado.IdLojaSeleconado)), metodo = "Cliente - Perfil", tela = "Cliente", cliente_id = (String.IsNullOrEmpty(ClienteId) ? 0 : Convert.ToInt32(ClienteId)), user_id = UsuarioLogado.Usuario.id, ip = HttpContext.Request.UserHostAddress.ToString(), data = DateTime.Now });
            //DateTime TempoInicial = DateTime.Now;

            //IList<ReportClientesFavoritos> list = _serviceRelatorio.ListarRelatorioComVendedor<ReportClientesFavoritos>("SP_RelatorioClientesFavoritos", UsuarioLogado.IdVendedorSeleconado);


            //DateTime TempoFinal = DateTime.Now;
            //LogSistema(new LogSistema { processo_minutos = TempoProcesso(TempoInicial, TempoFinal), acao = "Carregando Perfil do Cliente: " + cliente.nome + " # Cpf: " + cliente.cpf, login = UsuarioLogado.Usuario.email, tipo_log = "CARREGAMENTO", vendedor_id = (String.IsNullOrEmpty(UsuarioLogado.IdVendedorSeleconado) ? 0 : Convert.ToInt32(UsuarioLogado.IdVendedorSeleconado)), loja_id = (String.IsNullOrEmpty(UsuarioLogado.IdLojaSeleconado) ? 0 : Convert.ToInt32(UsuarioLogado.IdLojaSeleconado)), metodo = "Cliente - Perfil", tela = "Cliente", cliente_id = (String.IsNullOrEmpty(ClienteId) ? 0 : Convert.ToInt32(ClienteId)), user_id = UsuarioLogado.Usuario.id, ip = HttpContext.Request.UserHostAddress.ToString(), data = DateTime.Now });
            return View("QuemeEle.min");
        }

        #region "Método para Busca de Clientes"
        private Cliente BuscarCliente(string clienteID)
        {
            try
            {
                @ViewBag.ativarNovaTelaVendedor = Session["ativarNovaTelaVendedor"] == null ? false : Session["ativarNovaTelaVendedor"];

                return _serviceCliente.FindClientById(clienteID, UsuarioLogado.Usuario.id, "", UsuarioLogado.IdVendedorSeleconado);

            }
            catch (Exception)
            {
                MsgErro();
            }

            return new Cliente();
        }

        private Loja BuscarLoja(int lojaId)
        {
            try
            {
                return _serviceLoja.ListarLojas(lojaId.ToString()).FirstOrDefault();

            }
            catch (Exception)
            {
                MsgErro();
            }

            return new Loja();
        }



        #endregion "Método para Busca de Clientes"

        #region "Métodos Perfil do Cliente"


        private IList<GrupoProdutoItem> GrupoProdutos(string clienteID)
        {

            try
            {
                return _serviceVenda.GrupoProdutos(clienteID);
            }
            catch (Exception)
            {
                MsgErro();
            }

            return new List<GrupoProdutoItem>();
        }

        private IList<FraseContato> ContatosPorCliente(string clienteID)
        {
            try
            {
                return _serviceContato.ContatosPorCliente(clienteID);
            }
            catch (Exception)
            {
                MsgErro();
            }

            return new List<FraseContato>();
        }

        private IList<PedidosRetiradaLoja> ListarRetiradas6Meses(string clienteID)
        {
            try
            {
                return _serviceRetiradaLoja.ListarRetiradas6Meses(clienteID);
            }
            catch (Exception)
            {
                MsgErro();
            }

            return new List<PedidosRetiradaLoja>();
        }

        private IList<Venda> BuscarVendas(string clienteID)
        {
            try
            {
                return _serviceVenda.HistoricoVendas(clienteID);
            }
            catch (Exception)
            {
                MsgErro();
            }

            return new List<Venda>();
        }

        private IEnumerable<IGrouping<string, Frase>> ListarFrases(string clienteID)
        {
            try
            {
                return _serviceFrase.FraseClienteDetalhes(clienteID);
            }
            catch (Exception)
            {
                MsgErro();
            }

            return null;
        }

        private IList<Frase> ListarFrasesMotivoLigacao(string clienteID)
        {
            try
            {
                return _serviceFrase.FrasesMotivoLigacao(clienteID);
            }
            catch (Exception)
            {
                MsgErro();
            }

            return new List<Frase>();
        }

        public JsonResult SalvarClienteQuemEEle(string clienteId, string caracteristica)
        {
            try
            {
                @ViewBag.ativarNovaTelaVendedor = Session["ativarNovaTelaVendedor"] == null ? false : Session["ativarNovaTelaVendedor"];

                if (!String.IsNullOrEmpty(clienteId))
                {
                    int _id = Convert.ToInt32(clienteId);

                    DateTime TempoInicial = DateTime.Now;

                    if (_id == 0)
                        if (Session["cliente"] != null)
                        {
                            _id = ((Cliente)Session["cliente"]).id;
                        }

                    _serviceCliente.SalvarClienteQuemEEle(_id, caracteristica);

                    DateTime TempoFinal = DateTime.Now;
                    // Log do sistema
                    LogSistema(new LogSistema { processo_minutos = TempoProcesso(TempoInicial, TempoFinal), acao = "Salvando Observação Cliente Info Vendas: Observação: " + caracteristica, cliente_id = _id, login = UsuarioLogado.Usuario.email, tipo_log = "CADASTRANDO", vendedor_id = (String.IsNullOrEmpty(UsuarioLogado.IdVendedorSeleconado) ? 0 : Convert.ToInt32(UsuarioLogado.IdVendedorSeleconado)), loja_id = (String.IsNullOrEmpty(UsuarioLogado.IdLojaSeleconado) ? 0 : Convert.ToInt32(UsuarioLogado.IdLojaSeleconado)), metodo = "Info Vendas - Cadastro", tela = "Info Vendas", user_id = UsuarioLogado.Usuario.id, ip = HttpContext.Request.UserHostAddress.ToString(), data = DateTime.Now });

                    return JsonRetono(new { sucesso = true, msg = MsgSucesso() });
                }
                else
                {
                    return JsonRetono(new { sucesso = false, msg = "O Cliente não foi informado" });
                }

            }
            catch (Exception ex)
            {
                _serviceUsuario.AdicionarErro("SalvarClienteQuemEEle", "Perfil do Cliente", ex.Message + "Dado: " + caracteristica, (String.IsNullOrEmpty(UsuarioLogado.IdVendedorSeleconado) ? 0 : Convert.ToInt32(UsuarioLogado.IdVendedorSeleconado)), (String.IsNullOrEmpty(clienteId.ToString()) ? 0 : Convert.ToInt32(clienteId)), (String.IsNullOrEmpty(UsuarioLogado.IdLojaSeleconado) ? 0 : Convert.ToInt32(UsuarioLogado.IdLojaSeleconado)), UsuarioLogado.Usuario.id);

                return JsonRetono(new { sucesso = false, msg = MsgErro() });
            }
        }

        public JsonResult SalvarClienteObs(string clienteId, string observacao)
        {
            try
            {
                @ViewBag.ativarNovaTelaVendedor = Session["ativarNovaTelaVendedor"] == null ? false : Session["ativarNovaTelaVendedor"];
                if (!String.IsNullOrEmpty(clienteId))
                {
                    int _id = Convert.ToInt32(clienteId);
                    DateTime TempoInicial = DateTime.Now;

                    _serviceCliente.SalvarClienteObs(_id, observacao);

                    DateTime TempoFinal = DateTime.Now;
                    // Log do sistema
                    LogSistema(new LogSistema { processo_minutos = TempoProcesso(TempoInicial, TempoFinal), acao = "Salvando Observação Cliente Info Vendas: Observação: " + observacao, cliente_id = _id, login = UsuarioLogado.Usuario.email, tipo_log = "CADASTRANDO", vendedor_id = (String.IsNullOrEmpty(UsuarioLogado.IdVendedorSeleconado) ? 0 : Convert.ToInt32(UsuarioLogado.IdVendedorSeleconado)), loja_id = (String.IsNullOrEmpty(UsuarioLogado.IdLojaSeleconado) ? 0 : Convert.ToInt32(UsuarioLogado.IdLojaSeleconado)), metodo = "Info Vendas - Cadastro", tela = "Info Vendas", user_id = UsuarioLogado.Usuario.id, ip = HttpContext.Request.UserHostAddress.ToString(), data = DateTime.Now });

                    return JsonRetono(new { sucesso = true, msg = MsgSucesso() });
                }
                else
                {
                    return JsonRetono(new { sucesso = true, msg = "O Cliente não foi informado" });
                }

            }
            catch (Exception ex)
            {
                _serviceUsuario.AdicionarErro("SalvarClienteObs", "Perfil do Cliente", ex.Message + "Dado: " + observacao, (String.IsNullOrEmpty(UsuarioLogado.IdVendedorSeleconado) ? 0 : Convert.ToInt32(UsuarioLogado.IdVendedorSeleconado)), (String.IsNullOrEmpty(clienteId.ToString()) ? 0 : Convert.ToInt32(clienteId)), (String.IsNullOrEmpty(UsuarioLogado.IdLojaSeleconado) ? 0 : Convert.ToInt32(UsuarioLogado.IdLojaSeleconado)), UsuarioLogado.Usuario.id);

                return JsonRetono(new { sucesso = true, msg = MsgErro() });
            }
        }

        public JsonResult SalvarFavoritos(string clienteId, string cliFavarito)
        {
            try
            {
                @ViewBag.ativarNovaTelaVendedor = Session["ativarNovaTelaVendedor"] == null ? false : Session["ativarNovaTelaVendedor"];
                if (!String.IsNullOrEmpty(clienteId))
                {
                    int _id = Convert.ToInt32(clienteId);
                    if (!String.IsNullOrEmpty(UsuarioLogado.IdVendedorSeleconado))
                    {
                        DateTime TempoInicial = DateTime.Now;
                        _serviceCliente.SalvarFavoritos(_id, Convert.ToInt32(UsuarioLogado.IdVendedorSeleconado), cliFavarito);

                        @ViewBag.ListaReportClientesFavoritos = UsuarioLogado.ListaReportClientesFavoritos = _serviceRelatorio.ListarRelatorioComVendedor<ReportClientesFavoritos>("SP_RelatorioClientesFavoritos", UsuarioLogado.IdVendedorSeleconado);

                        DateTime TempoFinal = DateTime.Now;
                        // Log do sistema
                        LogSistema(new LogSistema { processo_minutos = TempoProcesso(TempoInicial, TempoFinal), acao = "Salvando Favoritos Cliente: Observação: ", cliente_id = _id, login = UsuarioLogado.Usuario.email, tipo_log = "SALVANDO", vendedor_id = (String.IsNullOrEmpty(UsuarioLogado.IdVendedorSeleconado) ? 0 : Convert.ToInt32(UsuarioLogado.IdVendedorSeleconado)), loja_id = (String.IsNullOrEmpty(UsuarioLogado.IdLojaSeleconado) ? 0 : Convert.ToInt32(UsuarioLogado.IdLojaSeleconado)), metodo = "Perfil - Cliente", tela = "Perfil do Cliente", user_id = UsuarioLogado.Usuario.id, ip = HttpContext.Request.UserHostAddress.ToString(), data = DateTime.Now });
                    }
                    return JsonRetono(new { sucesso = true, msg = MsgSucesso(), totalFavoritos = UsuarioLogado.ListaReportClientesFavoritos.Count().ToString() });
                }
                else
                {
                    return JsonRetono(new { sucesso = true, msg = "O Cliente não foi informado", totalFavoritos = UsuarioLogado.ListaReportClientesFavoritos.Count().ToString() });
                }

            }
            catch (Exception ex)
            {
                _serviceUsuario.AdicionarErro("SalvarFavoritos", "Perfil do Cliente", ex.Message + "Dado: " + clienteId, (String.IsNullOrEmpty(UsuarioLogado.IdVendedorSeleconado) ? 0 : Convert.ToInt32(UsuarioLogado.IdVendedorSeleconado)), (String.IsNullOrEmpty(clienteId.ToString()) ? 0 : Convert.ToInt32(clienteId)), (String.IsNullOrEmpty(UsuarioLogado.IdLojaSeleconado) ? 0 : Convert.ToInt32(UsuarioLogado.IdLojaSeleconado)), UsuarioLogado.Usuario.id);

                return JsonRetono(new { sucesso = true, msg = MsgErro(), totalFavoritos = UsuarioLogado.ListaReportClientesFavoritos.Count().ToString() });
            }
        }

        public async Task<JsonResult> SalvarContatoComercioDigital(string clienteId, string id_agenda, string parametro, string enviar_avaliacao, string status, string modo, string vendedor_id, string tipoPagina, string ticket)
        {
            var cliente = BuscarCliente(clienteId);

            var dados_tel_cliente = cliente.cel != "" ? "55" + cliente.cel_ddd + cliente.cel : "55" + cliente.tel_ddd + cliente.tel;
            ViewBag.ContatoCliente = dados_tel_cliente;

            var listagem = "";
            var listagemController = listagemProdutos();

            for (int j = 0; j < modo.Split(';').Length; j++)
            {
                for (int i = 0; i < listagemController.Count(); i++)
                {
                    if (listagemController[i].texto == modo.Split(';')[j])
                    {
                        listagem += listagemController[i].valor + ",";
                    }
                }
            }

            var data_atual = DateTime.Today.Day.ToString("00") + "/" + DateTime.Today.Month.ToString("00") + "/" + DateTime.Today.Year;
            var localizar_cupom = ListaCupomDesconto().Where(x => x.data_inicio == data_atual);
            var cupom = localizar_cupom.Count() > 0 ? localizar_cupom.First().parametro : "";

            // Verifica se o cliente está fazendo aniversário hoje
            var _aniversario = false;
            if (cliente.dt_nasc.Day.ToString("00") == DateTime.Today.Day.ToString("00") && (cliente.dt_nasc.Month.ToString("00") == DateTime.Today.Month.ToString("00")))
            {
                _aniversario = true;
            }

            var mensagem_aniversario = "Olá " + FirstCharToUpper(cliente.nome.Split(' ')[0]) + ", " +
                           "\r\n\r\n" + "Meu nome é " + FirstCharToUpper(UsuarioLogado.Usuario.username.Split(' ')[0]) + " e sou do ecommerce da Reserva." +
                           "\r\n\r\n" + "Passando para te desejar um Feliz Aniversário!!!" +
                           "\r\n\r\n" + "E de presente, te envio um link que te dará *20% de desconto* e frete grátis!!!" +
                           //"\r\n\r\n" + "Cupom: " + cupom +
                           "\r\n\r\n" + "Como você não tem meu número gravado na sua agenda, basta responder *Oi* que o link é ativado  ou *copiar e colar* o endereço do link no seu navegador ." +
                           "\r\n\r\n" + "Você cairá na página de produtos que mais gosta." +
                           "\r\n\r\n" + "O link é válido por 24h e serve para todo o site, exceto produtos já com desconto." +
                           //"\r\n\r\n" +
                           //listagem.Replace(",", "\r\n\r\n") +
                           "\r\n\r\n" + "Esse é o meu WhatsApp e estou 100 % à sua disposição para o que necessitar." +
                           "\r\n\r\n" + "Grande abraço" +
                           "\r\n\r\n" +
                            listagem.Replace(",", "\r\n\r\n");


            var mensagem = "Olá " + FirstCharToUpper(cliente.nome.Split(' ')[0]) + ", " +
                           "\r\n\r\n" + "Meu nome é " + FirstCharToUpper(UsuarioLogado.Usuario.username.Split(' ')[0]) + " e sou do ecommerce da Reserva." +
                           "\r\n\r\n" + "Conforme consta em nosso cadastro, já tem um tempo que não nos prestigia e estou aqui para mudar isso =)" +
                           "\r\n\r\n" + "Para acelerar o seu retorno te envio um link que te dará *20% de desconto* e frete grátis." +
                           //"\r\n\r\n" + "Cupom: " + cupom +
                           "\r\n\r\n" + "Como você não tem meu número gravado na sua agenda, basta responder *Oi* que o link é ativado ou *copiar e colar* o endereço do link no seu navegador ." +
                           "\r\n\r\n" + "Você cairá na página de produtos que mais gosta." +
                           "\r\n\r\n" + "O link é válido por 24h e serve para todo o site, exceto produtos já com desconto." +

                           "\r\n\r\n" + "Esse é o meu WhatsApp e estou 100% à sua disposição para o que necessitar." +
                           "\r\n\r\n" + "Grande abraço" +
                           "\r\n\r\n" +
                            listagem.Replace(",", "\r\n\r\n");

            ViewBag.Whatsapp = (_aniversario ? mensagem_aniversario : mensagem);
            ViewBag.MensagemWhatsapp = "https://wa.me/" + dados_tel_cliente + "/?text=";


            //int _id;
            //DateTime TempoInicial;
            //Contato contato;
            //salvarDadosNovoContato(clienteId, id_agenda, parametro, enviar_avaliacao, status, modo, "Gostou", vendedor_id, "", "", ticket, out _id, out TempoInicial, out contato);


            return JsonRetono(new { sucesso = true, msg = "Whatsapp enviado com sucesso", mensagem = ViewBag.MensagemWhatsapp, whatsapp = ViewBag.Whatsapp }); ;
            //return JsonRetono(new { sucesso = true, responseText = "Agenda" });
        }


        public string FirstCharToUpper(string input)
        {
            if (String.IsNullOrEmpty(input))
                return "";

            var nome = input.ToLower();
            return nome.Length > 1 ? char.ToUpper(nome[0]) + nome.Substring(1) : nome.ToUpper();
        }

        public async Task<JsonResult> SalvarContato(string clienteId, string id_agenda, string parametro, string enviar_avaliacao, string status, string modo, string termometro, string vendedor_id, string caracteristicas, string obs, string tipoPagina, string ticket)
        {
            try
            {
                @ViewBag.ativarNovaTelaVendedor = Session["ativarNovaTelaVendedor"] == null ? false : Session["ativarNovaTelaVendedor"];

                if (!String.IsNullOrEmpty(clienteId))
                {
                    int _id;
                    DateTime TempoInicial;
                    Contato contato;
                    salvarDadosNovoContato(clienteId, id_agenda, parametro, enviar_avaliacao, status, modo, termometro, vendedor_id, caracteristicas, obs, ticket, out _id, out TempoInicial, out contato);

                    var _clie = _serviceCliente.FindClientById(_id.ToString(), UsuarioLogado.Usuario.id, UsuarioLogado.IdLojaSeleconado, UsuarioLogado.IdVendedorSeleconado);
                    var _vendedor = _serviceVendedor.GetVendedor("", vendedor_id, 0);
                    var _loja = UsuarioLogado.TodasLojasCanaisUsuario.Where(x => x.id.ToString() == UsuarioLogado.IdLojaSeleconado).FirstOrDefault();

                    string telefone = "55";

                    switch (_clie.cel)
                    {
                        case "6":
                        case "7":
                        case "8":
                            telefone += _clie.cel_ddd + _clie.cel;
                            break;
                    }

                    switch (_clie.tel)
                    {
                        case "6":
                        case "7":
                        case "8":
                            telefone += _clie.tel_ddd + _clie.tel;
                            break;
                    }

                    string tipoLoja = _loja.tipo_loja;
                    switch (_loja.tipo_loja)
                    {
                        case "RESERVA":
                        case "RESERVA+MINI":
                            tipoLoja = "RESERVA";
                            break;
                    }

                    //EnviarEmailTrocaSenha(_contato.id.ToString(), _vendedor.FirstOrDefault().nome, _clie, _contato.url_avaliacao, tipoLoja, _vendedor.FirstOrDefault().img_perfil_intranet, _contato.modo);

                    //
                    //try
                    //{
                    //    if ((Ambiente == "PRODUCAO") && (tipoLoja != "EVA"))
                    //    {
                    //        // Telefone Invalido e Não consegui falar, com contato direto não entra no envio de e-mail.
                    //        if (!status.Contains("Telefone inválido") && !status.Contains("Não consegui falar") && contato.tipo_agendamento_id != 1)
                    //        {
                    //            //RETIRADO ESSA ROTINA POIS AGORA O EMAIL SERÁ ENVIADO A CADA 6 HORAS
                    //            //Enviar E-mail de Avaliação do Contato
                    //            //EnviarEmailTrocaSenha(_contato.id.ToString(), _vendedor.FirstOrDefault().nome, _clie, _contato.url_avaliacao, tipoLoja, _vendedor.FirstOrDefault().img_perfil_intranet);

                    //            // Log do sistema
                    //            LogSistema(new LogSistema { processo_minutos = TempoProcesso(TempoInicial, DateTime.Now), acao = "Envio Email Avaliação Contato: " + "---Dados Inserido: " + _id + " # Email: " + _clie.email + " # Status: " + contato.status + " # Vendedor: " + contato.vendedor_id + " # CheckReservado: " + contato.flagReservado + " # Data Reservado: " + contato.dt_Reservado + " # Modo: " + contato.modo + " # Termometro: " + contato.termometro + " # Caracteristicas: " + contato.caracteristicas + " # Obs: " + contato.obs + " # Agenda: " + parametro, login = UsuarioLogado.Usuario.email, tipo_log = "CADASTRANDO", vendedor_id = (String.IsNullOrEmpty(UsuarioLogado.IdVendedorSeleconado) ? 0 : Convert.ToInt32(UsuarioLogado.IdVendedorSeleconado)), loja_id = (String.IsNullOrEmpty(UsuarioLogado.IdLojaSeleconado) ? 0 : Convert.ToInt32(UsuarioLogado.IdLojaSeleconado)), metodo = "Cliente - Contato", tela = "Contato", cliente_id = _id, user_id = UsuarioLogado.Usuario.id, ip = HttpContext.Request.UserHostAddress.ToString(), data = DateTime.Now });
                    //        }
                    //    }
                    //}
                    //catch (Exception ex)
                    //{
                    //    string dados = " --- ERRO Envio de E-MAIL: ";
                    //    _serviceUsuario.AdicionarErro("SalvarContato", "Perfil do Cliente", ex.Message + dados, (String.IsNullOrEmpty(UsuarioLogado.IdVendedorSeleconado) ? 0 : Convert.ToInt32(UsuarioLogado.IdVendedorSeleconado)), null, (String.IsNullOrEmpty(UsuarioLogado.IdLojaSeleconado) ? 0 : Convert.ToInt32(UsuarioLogado.IdLojaSeleconado)), UsuarioLogado.Usuario.id);
                    //}


                    //Enviar SMS
                    // por sugestão MKT - foi desativado o envio de SMS
                    /*
                    String _bodySms = @_vendedor.FirstOrDefault().nome + " entrou contato com você. Gostariamos de saber a sua opinião, por favor clique no link e avalie o contato.: http://www.usenow.com.br/AcessoExterno/AvaliacaoContato?id=";
                    _bodySms = _bodySms + _contato.id;
                    try
                    {
                        var _serviceSMS = new SendSMS();
                        _serviceSMS.Initialize(telefone, _bodySms);
                        await _serviceSMS.EnviaSMS(ConfigurationManager.AppSettings["Servico_SMS"].ToString(), _serviceSMS, "POST", ConfigurationManager.AppSettings["Chave_Servico_SMS"].ToString());
                    }
                    catch (Exception ex)
                    {
                        string dados = " --- ERRO Envio SMS: " + _bodySms;
                        _serviceUsuario.AdicionarErro("SalvarContato", "Perfil do Cliente", ex.Message + dados, (String.IsNullOrEmpty(UsuarioLogado.IdVendedorSeleconado) ? 0 : Convert.ToInt32(UsuarioLogado.IdVendedorSeleconado)), null, (String.IsNullOrEmpty(UsuarioLogado.IdLojaSeleconado) ? 0 : Convert.ToInt32(UsuarioLogado.IdLojaSeleconado)), UsuarioLogado.Usuario.id);
                    }
                    */

                    DateTime TempoFinal = DateTime.Now;
                    // Log do sistema
                    LogSistema(new LogSistema { processo_minutos = TempoProcesso(TempoInicial, TempoFinal), acao = "Salvando contato do Cliente: " + "---Dados Inserido: " + _id + " # Status: " + contato.status + " # Vendedor: " + contato.vendedor_id + " # CheckReservado: " + contato.flagReservado + " # Data Reservado: " + contato.dt_Reservado + " # Modo: " + contato.modo + " # Termometro: " + contato.termometro + " # Caracteristicas: " + contato.caracteristicas + " # Obs: " + contato.obs + " # Agenda: " + parametro, login = UsuarioLogado.Usuario.email, tipo_log = "CADASTRANDO", vendedor_id = (String.IsNullOrEmpty(UsuarioLogado.IdVendedorSeleconado) ? 0 : Convert.ToInt32(UsuarioLogado.IdVendedorSeleconado)), loja_id = (String.IsNullOrEmpty(UsuarioLogado.IdLojaSeleconado) ? 0 : Convert.ToInt32(UsuarioLogado.IdLojaSeleconado)), metodo = "Cliente - Contato", tela = "Contato", cliente_id = _id, user_id = UsuarioLogado.Usuario.id, ip = HttpContext.Request.UserHostAddress.ToString(), data = DateTime.Now });

                    // Se o Parametro estiver vazio retorna para a busca de clientes
                    if (string.IsNullOrEmpty(tipoPagina))
                    {
                        return JsonRetono(new { sucesso = true, responseText = "Cliente" });
                    }
                    else
                    {
                        return JsonRetono(new { sucesso = true, responseText = "Agenda" });
                    }
                }
                else
                {
                    return JsonRetono(new { sucesso = true, msg = "O Cliente não foi informado", totalFavoritos = UsuarioLogado.ListaReportClientesFavoritos.Count().ToString() });
                }
            }
            catch (Exception ex)
            {
                string dados = " --- Dados Inserido: " + clienteId + " # Status: " + status + " # Vendedor: " + vendedor_id + " # Modo: " + modo + " # Termometro: " + termometro + " # Caracteristicas: " + caracteristicas + " # Obs: " + obs + " # Agenda: " + parametro;

                _serviceUsuario.AdicionarErro("SalvarContato", "Perfil do Cliente", ex.Message + dados, (String.IsNullOrEmpty(UsuarioLogado.IdVendedorSeleconado) ? 0 : Convert.ToInt32(UsuarioLogado.IdVendedorSeleconado)), null, (String.IsNullOrEmpty(UsuarioLogado.IdLojaSeleconado) ? 0 : Convert.ToInt32(UsuarioLogado.IdLojaSeleconado)), UsuarioLogado.Usuario.id);

                MensagemRetorno("", TypeMensage.error);

                return JsonRetono(new { sucesso = false, responseText = "", msg = "Ocorreu um erro contate o adiministrador do sistema!" });
            }
        }

        private void salvarDadosNovoContato(string clienteId, string id_agenda, string parametro, string enviar_avaliacao, string status, string modo, string termometro, string vendedor_id, string caracteristicas, string obs, string ticket, out int _id, out DateTime TempoInicial, out Contato contato)
        {
            _id = Convert.ToInt32(clienteId);
            TempoInicial = DateTime.Now;
            contato = new Contato();
            contato.loja_id = Convert.ToInt32(UsuarioLogado.IdLojaSeleconado);
            contato.agendamento = parametro;
            contato.enviar_avaliacao = Convert.ToInt32(enviar_avaliacao);
            contato.status = status;
            contato.modo = modo;
            contato.termometro = termometro;
            contato.vendedor_id = vendedor_id == "" ? Convert.ToInt32(UsuarioLogado.IdVendedorSeleconado) : Convert.ToInt32(vendedor_id);
            contato.caracteristicas = caracteristicas;
            contato.obs = obs;
            contato.cliente_id = _id;
            contato.ticket = ticket;

            // Para garantir  o contato direto
            if (string.IsNullOrEmpty(id_agenda))
            {
                contato.agendamento = "clientes_contato_vendedor";
                contato.tipo_agendamento_id = 1;
            }
            else
            {
                contato.agendamento = parametro;
                contato.tipo_agendamento_id = Convert.ToInt32(id_agenda);
            }

            if (!status.Contains("Telefone inválido") && !status.Contains("Não consegui falar") && contato.tipo_agendamento_id != 3 && contato.tipo_agendamento_id != 44 && contato.tipo_agendamento_id != 1)
                contato.data_envio_avaliacao = DateTime.Now;

            var _contato = _serviceContato.AdicionarContato(contato);

            MensagemRetorno("Contato salvo com sucesso!", TypeMensage.success);
        }

        private void EnviarEmailTrocaSenha(string idcontato, string NomeVendedor, Cliente clie, string logo, string tipoLoja, string urlImgVendedor, string modoContato)
        {
            //MailAddress from = new MailAddress(clie.email);
            MailAddress to = new MailAddress(clie.email);
            MailAddress from = new MailAddress("sac@usereserva.com");
            //MailAddress to = new MailAddress("peterson.andrade@usereserva.com");
            MailMessage message = new MailMessage(from, to);

            string _urlImg = "";
            if (!String.IsNullOrEmpty(urlImgVendedor))
            {
                _urlImg = @"<img border='0' vspace='0' hspace='0'
                                     src='http://usereserva.dyndns.org:82/Intranet/images/fotosVendedores/" + urlImgVendedor + @"'
                                     width='120' height='120'
                                     alt='Logo' title='Logo' style='border-radius: 50%;' />";
            }

            message.Subject = tipoLoja + " - Avaliação do Contato (" + DateTime.Now.ToString("HH:mm") + "h)";

            var qtd_itens = NomeVendedor.Split(' ').Length - 1;

            var _body = CorpoEmailAvaliacaoNovoContato(logo, _urlImg).Replace("#VENDEDOR#", NomeVendedor.Split(' ')[0] + ' ' + NomeVendedor.Split(' ')[qtd_itens]);
            _body = _body.Replace("#nomecliente#", clie.nome);
            _body = _body.Replace("#idcontato#", idcontato);
            _body = _body.Replace("#formacontato#", modoContato);
            _body = _body.Replace("#imgurl#", urlImgVendedor);

            message.Body = _body;

            message.IsBodyHtml = true;

            SmtpClient client = new SmtpClient();

            client.Send(message);
        }

        public string CorpoEmailAvaliacaoNovoContato(string logo, string urlImgVendedor)
        {
            string corpoEmail = @"<html xmlns='http://www.w3.org/1999/xhtml'>
<head>

    <meta http-equiv='content-type' content='text/html; charset=utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1.0;'>
    <meta name='format-detection' content='telephone=no' />
    <style>
        /* Reset styles */
        body {
            margin: 0;
            padding: 0;
            min-width: 100%;
            width: 100% !important;
            height: 100% !important;
            font-family: Helvetica,sans-serif;
        }

        body, table, td, div, p, a {
            -webkit-font-smoothing: antialiased;
            text-size-adjust: 100%;
            -ms-text-size-adjust: 100%;
            -webkit-text-size-adjust: 100%;
            line-height: 100%;
        }

        table, td {
            mso-table-lspace: 0pt;
            mso-table-rspace: 0pt;
            border-collapse: collapse !important;
            border-spacing: 0;
        }

        img {
            border: 0;
            line-height: 100%;
            outline: none;
            text-decoration: none;
            -ms-interpolation-mode: bicubic;
        }

        #outlook a {
            padding: 0;
        }

        .ReadMsgBody {
            width: 100%;
        }

        .ExternalClass {
            width: 100%;
        }

            .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {
                line-height: 100%;
            }

        /* Rounded corners for advanced mail clients only */


        /* Set color for auto links (addresses, dates, etc.) */
        a, a:hover {
            color: #127DB3;
        }

        .footer a, .footer a:hover {
            color: #999999;
        }



        .list-type1_hover {
            background: #93C775;
            text-decoration: none;
            transform: scale(1.1);
        }

        .corpo-email {
            border-collapse: collapse;
            border-spacing: 0;
            margin: 0;
            padding: 0;
            width: 100%;
            height: 100%;
            -webkit-font-smoothing: antialiased;
            text-size-adjust: 100%;
            -ms-text-size-adjust: 100%;
            -webkit-text-size-adjust: 100%;
            line-height: 100%;
            background-color: #F0F0F0;
            color: #000000;
        }

        .saudacao-cliente {
            border-collapse: collapse;
            border-spacing: 0;
            margin: 0;
            padding: 0;
            padding-left: 6.25%;
            padding-right: 6.25%;
            width: 87.5%;
            font-size: 24px;
            font-weight: bold;
            line-height: 130%;
            padding-top: 25px;
            color: #000000;
            font-family: sans-serif;
        }

        .img-vendedor {
            border-collapse: collapse;
            border-spacing: 0;
            margin: 0;
            padding: 0;
            padding-bottom: 3px;
            padding-left: 6.25%;
            padding-right: 6.25%;
            width: 87.5%;
            font-size: 18px;
            font-weight: 300;
            line-height: 150%;
            padding-top: 5px;
            color: #000000;
            font-family: sans-serif;
        }

        .btn-enviar-avaliacao {
            border-collapse: collapse;
            border-spacing: 0;
            margin: 0;
            padding: 0;
            padding-left: 6.25%;
            padding-right: 6.25%;
            width: 87.5%;
            padding-top: 25px;
            padding-bottom: 5px;
        }

        .nps-icon {
            text-decoration: none;
            padding: 1rem;
            background-color: #e2e6ea;
            border-color: #dae0e5;
            color: #a0a1a1;
            padding-left: 17px;
            padding-right: 17px;
            font-size: 13pt;
            font-weight: 600;
            margin-right: 4px;
        }

            .nps-icon:hover {
                background-color: #f1c40f;
                color: #505050;
                border: solid 1px #505050;
            }

        .nps-pouco {
            position: absolute;
            float: left;
            margin-top: 28px;
            margin-right: -91px;
            font-family: Helvetica,sans-serif;
            font-size: 11px;
            font-weight: bold;
            text-transform: uppercase;
            padding-left: 0px;
            margin-left: 22px;
            color: #E0E0E0;
        }

        .nps-muito-feliz {
            float: right;
            font-family: Helvetica,sans-serif;
            font-size: 11px;
            font-weight: bold;
            text-transform: uppercase;
            margin-top: 28px;
            padding-right: 30px;
            color: #E0E0E0;
        }

        .link-final {
            line-height: 20px;
            text-decoration: none;
            color: #333333;
            font-family: Helvetica,sans-serif;
            font-size: 11px;
            font-weight: bold;
            text-transform: uppercase;
        }

        .textos-arquivo {
            line-height: 20px;
            text-decoration: none;
            color: #333333;
            font-family: sans-serif;
            font-size: 11px;
            font-weight: bold;
        }

        .texto-complemento-avaliacao {
            border-collapse: collapse;
            border-spacing: 0;
            margin: 0;
            padding: 0;
            padding-left: 6.25%;
            padding-right: 6.25%;
            width: 87.5%;
            font-size: 17px;
            font-weight: 400;
            /*line-height: 160%;*/
            padding-top: 5px;
            color: #000000;
            font-family: Helvetica,sans-serif;
            line-height: 30px
        }
    </style>
    <script src='https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js'></script>
    <link rel='stylesheet' href='https://use.fontawesome.com/releases/v5.8.1/css/all.css' integrity='sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf' crossorigin='anonymous'>
    
    <!-- MESSAGE SUBJECT -->
    <title>Avalie o contato realizado</title>
</head>


<body topmargin='0' rightmargin='0' bottommargin='0' leftmargin='0' marginwidth='0' marginheight='0' width='100%' class='corpo-email textos-arquivo' bgcolor='#F0F0F0' text='#000000'>
    <input type='hidden' value='' id='hddAvaliacao' />
    <!-- SECTION / BACKGROUND -->
    <!-- Set message background color one again -->
    <table width='100%' align='center' border='0' cellpadding='0' cellspacing='0' style='border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0; width: 100%;' class='background'>
        <tr>
            <td align='center' valign='top' style='border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0;'
                bgcolor='#F0F0F0'>
                <!-- WRAPPER -->
                <!-- Set wrapper width (twice) -->
                <table border='0' cellpadding='0' cellspacing='0' align='center'
                       width='600' style='border-collapse: collapse; border-spacing: 0; padding: 0; width: inherit;	max-width: 600px;' class='wrapper'>
                    <tr>
                        <td align='center' valign='top' style='border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0; padding-left: 6.25%; padding-right: 6.25%; width: 87.5%; padding-top: 20px;			padding-bottom: 20px;'>
                            <!-- PREHEADER -->
                            <!-- Set text color to background color -->
                            <div style='display: none; visibility: hidden; overflow: hidden; opacity: 0; font-size: 1px; line-height: 1px; height: 0; max-height: 0; max-width: 0;color: #F0F0F0;' class='preheader'>
                                #VENDEDOR#<br />
                                entrou em contato com você por <span style = 'color:red'> #formacontato# </span>. Agora é sua vez de nos contar como foi!
                            </div>
                            <!-- LOGO -->
                            <!-- Image text color should be opposite to background color. Set your url, image src, alt and title. Alt text should fit the image size. Real image size should be x2. URL format: http://domain.com/?utm_source={{Campaign-Source}}&utm_medium=email&utm_content=logo&utm_campaign={{Campaign-Name}} -->
                            <a target='_blank' style='text-decoration: none;'
                               href='http://www.usereserva.com.br'>
                                <img border='0' vspace='0' hspace='0'
                                     src='" + logo + @"' 
                                     width='100' class='logo'
                                     alt='Logo' title='Logo' style='color: #000000;font-size: 10px; margin: 0; padding: 0; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; border: none; display: block;' />
                            </a>
                        </td>
                    </tr>
                    <!-- End of WRAPPER -->
                </table>
                <!-- WRAPPER / CONTEINER -->
                <!-- Set conteiner background color -->
                <table border='0' cellpadding='0' cellspacing='0' align='center' bgcolor='#FFFFFF' width='600' style='border-collapse: collapse; border-spacing: 0; padding: 0; width: inherit;	max-width: 600px;' class='container'>
                    <!-- HEADER -->
                    <!-- Set text color and font family ('sans-serif' or 'Georgia, serif') -->
                    <tr>
                        <td align='center' valign='top' class='saudacao-cliente header'>
                            Olá, #nomecliente#!
                        </td>
                    </tr>

                    <!-- SUBHEADER -->
                    <!-- Set text color and font family ('sans-serif' or 'Georgia, serif') -->
                    <tr>
                        <td align='center' valign='top' class='img-vendedor subheader'>
                            <br />
                            " + urlImgVendedor + @"<br> 
                            #VENDEDOR#<br />
                            entrou em contato com você por <span style = 'color:red'> #formacontato# </span>. Agora é sua vez de nos contar como foi!
                          </td>
                    </tr>
                    <tr class='avaliacao'>
                        <td align='center' valign='top' style='border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0; width: 100%;padding-top: 25px;padding-bottom: 25px;' class='line'>
                            <hr color='#E0E0E0' align='center' width='100%' size='1' noshade style='margin: 0; padding: 0;' />
                        </td>
                    </tr>
                    <tr class='textMensagem avaliacao'>
                        <td align='center' valign='top' style='border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0; padding-left: 6.25%; padding-right: 6.25%; width: 87.5%; font-size: 18px; font-weight: 400; line-height: 160%;
			padding-top: 25px;padding-bottom: 25px;' class='paragraph  textos-arquivo'>
                            O quanto você gostou do atendimento do #VENDEDOR#
                        </td>
                    </tr>
                    <tr class='avaliacao'>
                        <td align='center' valign='top' style='border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0; width: 100%;
			padding-top: 15px;padding-bottom: 30px;' class='button'>

                            <table border='0' cellpadding='0' cellspacing='0' align='center' style='max-width: 100%; min-width: 120px; border-collapse: collapse; border-spacing: 0; padding: 0;'>
                                <tr>
                                    <td align='center' valign='middle' style='margin: 0; border-collapse: collapse; border-spacing: 0; border-radius: 4px; -webkit-border-radius: 4px; -moz-border-radius: 4px; -khtml-border-radius: 4px;'>
                                        <div style='color: #FFFFFF; font-family: sans-serif; font-size: 17px; font-weight: 400; line-height: 120%;'>
                                            
                                            <a href='http://localhost:11625/AcessoExterno/AvaliacaoContato?id=#idcontato#&avaliacao=1&imgurl=#imgurl#' onclick='Avaliar(1)' class='nps-icon'>
                                                <span>1</span>
                                            </a>
                                            <a href='http://www.usenow.com.br/AcessoExterno/AvaliacaoContato?id=#idcontato#&avaliacao=2&imgurl=#imgurl#' onclick='Avaliar(2)' class='nps-icon'>
                                                <span>2</span>
                                            </a>
                                            <a href='http://www.usenow.com.br/AcessoExterno/AvaliacaoContato?id=#idcontato#&avaliacao=3&imgurl=#imgurl#' onclick='Avaliar(3)' class='nps-icon'>
                                                <span>3</span>
                                            </a>
                                            <a href='http://www.usenow.com.br/AcessoExterno/AvaliacaoContato?id=#idcontato#&avaliacao=4&imgurl=#imgurl#' onclick='Avaliar(4)' class='nps-icon'>
                                                <span>4</span>
                                            </a>
                                            <a href='http://www.usenow.com.br/AcessoExterno/AvaliacaoContato?id=#idcontato#&avaliacao=5&imgurl=#imgurl#' onclick='Avaliar(5)' class='nps-icon'>
                                                <span>5</span>
                                            </a>
                                            <a href='http://www.usenow.com.br/AcessoExterno/AvaliacaoContato?id=#idcontato#&avaliacao=6&imgurl=#imgurl#' onclick='Avaliar(6)' class='nps-icon'>
                                                <span>6</span>
                                            </a>
                                            <a href='http://www.usenow.com.br/AcessoExterno/AvaliacaoContato?id=#idcontato#&avaliacao=7&imgurl=#imgurl#' onclick='Avaliar(7)' class='nps-icon'>
                                                <span>7</span>
                                            </a>
                                            <a href='http://www.usenow.com.br/AcessoExterno/AvaliacaoContato?id=#idcontato#&avaliacao=8&imgurl=#imgurl#' onclick='Avaliar(8)' class='nps-icon'>
                                                <span>8</span>
                                            </a>
                                            <a href='http://www.usenow.com.br/AcessoExterno/AvaliacaoContato?id=#idcontato#&avaliacao=9&imgurl=#imgurl#' onclick='Avaliar(9)' class='nps-icon'>
                                                <span>9</span>
                                            </a>
                                            <a href='http://www.usenow.com.br/AcessoExterno/AvaliacaoContato?id=#idcontato#&avaliacao=10&imgurl=#imgurl#' onclick='Avaliar(10)' class='nps-icon'>
                                                <span>10</span>
                                            </a>
                                        </div>
                                        <div class='nps-pouco'>
                                            Pouco feliz 
                                        </div>
                                        <div class='nps-muito-feliz'>
                                            Muito feliz
                                        </div>
                                        
                                    </td>
                                </tr>
                            </table>


                        </td>
                    </tr>
                    
                    <tr>
                        <td align='center' valign='top' style='border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0;  width: 100%;padding-top: 25px;padding-bottom: 25px;' class='line'>
                            <hr color='#E0E0E0' align='center' width='100%' size='1' noshade style='margin: 0; padding: 0;' />
                        </td>
                    </tr>
                    <tr class='tdNaoRecebiContato avaliacao'>
                        <td align='center' valign='top' style='border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0; padding-left: 6.25%; padding-right: 6.25%; width: 87.5%;padding-bottom:40px;' class='button'>
                            <a href='#' style='text-decoration:none; display:none' onclick='EnviarNaoRecebiContato(0)'>
                                <table border='0' cellpadding='0' cellspacing='0' align='center' style='max-width: 240px; min-width: 120px; border-collapse: collapse; border-spacing: 0; padding: 0;'>
                                    <tr>
                                        <td align='center' valign='middle' style='padding: 7px 10px; margin: 0; border-collapse: collapse; border-spacing: 0; border-radius: 4px; -webkit-border-radius: 4px; -moz-border-radius: 4px; -khtml-border-radius: 4px;'
                                            bgcolor='red'>
                                            <div style='color: #FFFFFF; font-family: sans-serif; font-size: 12px; font-weight: 400; line-height: 120%;'>

                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </a>

                    <div id='opcaoRespNaoRecebi'>
                        <br />
                        <span style='padding-top: 50px; font-size: 18px; font-weight: 300; line-height: 150%; padding-top: 5px; color: #000000; font-family: sans-serif;' class='subheader'>
                            Se você <b style='color:crimson'>não recebeu o contato</b>, por favor, selecione o motivo:
                        </span>
                        <table border='0' cellpadding='0' cellspacing='0' align='center' style='max-width: 600px; min-width: 120px; border-collapse: collapse;margin-top:30px; border-spacing: 0; padding: 0;'>
                            <tr>
                                <td class='m_-4791612877786818603pb-30' style='padding:0 0 43px'>
                                    <table align='center' style='margin:0 auto' cellpadding='0' cellspacing='0'>
                                        <tbody>
                                            <tr>
                                                <td align='center' style='min-width: 240px;background:#00d058;font:bold 16px/20px Arial,Helvetica,sans-serif,Apercu;border-radius:5px'>
                                                    <a style='color:#fff;text-decoration:none;display:block;padding:14px 34px' class='link-final' href='http://localhost:11625/AcessoExterno/AvaliacaoContato?id=#idcontato#&naorecebi=0&texto=Não%20sou%20o%20cliente%20informado!'>Não sou o cliente informado!</a>
                                                </td>
                                                <td align='center' style='padding: 18px;font:bold 16px/20px Arial,Helvetica,sans-serif,Apercu;border-radius:5px'></td>
                                                <td align='center' style='min-width: 240px;background:#00d058;font:bold 16px/20px Arial,Helvetica,sans-serif,Apercu;border-radius:5px'>
                                                   
                                                    <a style='color:#fff;text-decoration:none;display:block;padding:14px 34px' class='link-final' href='http://localhost:11625/AcessoExterno/AvaliacaoContato?id=#idcontato#&naorecebi=0&texto=Nenhum%20VENDEDOR%20entrou%20em%20contato!'><b>Nenhum VENDEDOR entrou em contato!</b></a>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        </table>
                       
                    </div>
                    <div id='opcaoSairListagem'>
                        <br />
                        <span style='padding-top: 50px; font-size: 18px; font-weight: 300; line-height: 150%; padding-top: 5px; color: #000000; font-family: sans-serif;' class='subheader'>
                            Jamais queremos ser inconvenientes com você. <br /><br />
                            Caso <b style='color:crimson'>não</b> deseje receber ligações: <br />
                        </span>

                        <div style='margin-bottom:20px'> 
                            <a  class='link-final' href='http://localhost:11625/AcessoExterno/AvaliacaoContato?id=#idcontato#&naorecebi=-2&texto=Contato%20apenas%20via%20WhatsApp!&imgurl=#imgurl#'>
                                <div style='max-width: 150px;border: solid 1px;padding: 1rem;display: block;    margin: 30px;float: left;'>
                                    <div style='font-size: 25pt;margin-bottom: 10px;'><i class='fab fa-whatsapp'></i></div>
                                    Contato apenas via whatsapp/SMS!
                                </div>
                            </a>
                            <a class='link-final'  href='http://localhost:11625/AcessoExterno/AvaliacaoContato?id=#idcontato#&naorecebi=-1&texto=Não%20quero%20receber%20ligacao!&imgurl=#imgurl#'>
                                <div style='max-width: 150px;border: solid 1px;padding: 1rem;display: block; margin: 30px;float: right;'>
                                    <div style='font-size: 25pt;margin-bottom: 10px;'><i class='fas fa-phone-volume'></i></div>
                                        Não quero receber ligação!
                                    </div>
                                </div>
                            </a>
                        <a href='#' style='text-decoration:none; display:none' onclick='$('#opcaoRespNaoRecebi').hide('show');$('#opcaoSairListagem').hide('show'); $('.avaliacao').show('show'); $('.textMensagem').show('show');'>
                            <table border='0' cellpadding='0' cellspacing='0' align='center' style='max-width: 240px; min-width: 120px; border-collapse: collapse; border-spacing: 0; padding: 0;'>
                                <tr>
                                    <td align='center' valign='middle' style='padding: 7px 10px; margin: 0; border-collapse: collapse; border-spacing: 0; border-radius: 4px; -webkit-border-radius: 4px; -moz-border-radius: 4px; -khtml-border-radius: 4px;'
                                        bgcolor='gray'>
                                        <div style='color: #FFFFFF; font-family: sans-serif; font-size: 12px; font-weight: 400; line-height: 120%;'>
                                            FECHAR
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </a>
                    </div>


                    </td>
                    </tr>
                    
                </table>

                <!-- AREA DE MARKETING -->
                <table align='center' bgcolor='#FFF' cellpadding='0' cellspacing='0' role='presentation' width='600' style='border-bottom:1px dashed #cccccc;background-color:#fff;min-width:600px'>
                    <tbody>
                        <tr align='center'>
                            <td style='padding:20px 0'>
                                <table align='center' cellpadding='0' cellspacing='0' role='presentation' width='600'>
                                    <tbody>
                                        <tr align='center'>
                                           
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <table align='center' cellpadding='0' cellspacing='0' role='presentation' width='600' bgcolor='#FFF' style='background-color:#fff;min-width:600px'>
                    <tbody>
                        <tr>
                            <td align='center' style='font-family:Helvetica,sans-serif;font-size:11px;line-height:11px;color:#333333;padding:30px 30px 10px 30px'>Sigam-me os bons:</td>
                        </tr>
                    </tbody>
                </table>
                <table align='center' cellpadding='0' cellspacing='0' role='presentation' width='600' bgcolor='#FFF' style='background-color:#fff;min-width:600px'>
                    <tbody>
                        <tr>
                            <td align='center' width='600' style='padding:0 30px 20px 30px'>
                                <a href='http://news.usereserva.com/pub/cc?_ri_=X0Gzc2X%3DYQpglLjHJlTQGgYBBRwdYzeDItn6loyDgCyUJBfWzazdoze6p0uwmEp55igTCza8CIIzaVXtpKX%3DUCDSCRRT&amp;_ei_=EkLIVj8x9EsyUELCtX-jUiJ7Rup0MmWF5RKin5Eq9OJeaKdy4CTrCCRwSZvQeDsrA2m3lzinv9i7g8r1S3L5H4EeD7hD8s3_t8kl5bnnSi31Q5iuSJAo-RhYj3deU5ySCdnLZ3vxjQ58q9vXPdB3ZQHqxxIB3otFiZV9e6BLrCKDy_mgjCH9XqcNPMfKDhBs-VU-U4jljDERrzp8stB54QHZGxQgA0.' style='text-decoration:none' target='_blank' data-saferedirecturl='https://www.google.com/url?q=http://news.usereserva.com/pub/cc?_ri_%3DX0Gzc2X%253DYQpglLjHJlTQGgYBBRwdYzeDItn6loyDgCyUJBfWzazdoze6p0uwmEp55igTCza8CIIzaVXtpKX%253DUCDSCRRT%26_ei_%3DEkLIVj8x9EsyUELCtX-jUiJ7Rup0MmWF5RKin5Eq9OJeaKdy4CTrCCRwSZvQeDsrA2m3lzinv9i7g8r1S3L5H4EeD7hD8s3_t8kl5bnnSi31Q5iuSJAo-RhYj3deU5ySCdnLZ3vxjQ58q9vXPdB3ZQHqxxIB3otFiZV9e6BLrCKDy_mgjCH9XqcNPMfKDhBs-VU-U4jljDERrzp8stB54QHZGxQgA0.&amp;source=gmail&amp;ust=1555608106301000&amp;usg=AFQjCNEej_fWU650siCjN3BfLasF3t23Ig'>
                                    <img src='https://ci5.googleusercontent.com/proxy/hRZkkTwUYP2ciT1iGyxWuTe0GVWRBGe6szGCsr5keDO_0k7o7ysY2gUDh2-YtL9XSnZVKUUYk3yFU5LZMIdrO5mNC5XAoLThaagCoW7Svlx3ZQGZ3dRkRA8=s0-d-e1-ft#https://imagens.usereserva.com.br/usereserva/news/template/icon-fb.png' width='22' alt='Facebook' style='border:0;display:inline-block;padding-right:5px' class='CToWUd'>
                                </a>
                                <a href='http://news.usereserva.com/pub/cc?_ri_=X0Gzc2X%3DYQpglLjHJlTQGgYBBRwdYzeDItn6loyDgCyUJBfWzazdoze6p0uwmEp55igTCza8CIIzaVXtpKX%3DUADSDUCT&amp;_ei_=EkLIVj8x9EsyUELCtX-jUiJ7Rup0MmWF5RKin5Eq9OJeaKdy4CTrCCRwSZvQeDsrA2m3lzinv9i7g8r1S3L5H4EeD7hD8s3_t8kl5bnnSi31Q5iuSJAo-RhYj3deU5ySCdnLZ3vxjQ58q9vXPdB3ZQHqxxIB3otFiZV9e6BLrCKDy_mgjCH9XqcNPMfKDhBs-VU-U4jljDERrzp8stB54QHZGxQgA0.' style='text-decoration:none' target='_blank' data-saferedirecturl='https://www.google.com/url?q=http://news.usereserva.com/pub/cc?_ri_%3DX0Gzc2X%253DYQpglLjHJlTQGgYBBRwdYzeDItn6loyDgCyUJBfWzazdoze6p0uwmEp55igTCza8CIIzaVXtpKX%253DUADSDUCT%26_ei_%3DEkLIVj8x9EsyUELCtX-jUiJ7Rup0MmWF5RKin5Eq9OJeaKdy4CTrCCRwSZvQeDsrA2m3lzinv9i7g8r1S3L5H4EeD7hD8s3_t8kl5bnnSi31Q5iuSJAo-RhYj3deU5ySCdnLZ3vxjQ58q9vXPdB3ZQHqxxIB3otFiZV9e6BLrCKDy_mgjCH9XqcNPMfKDhBs-VU-U4jljDERrzp8stB54QHZGxQgA0.&amp;source=gmail&amp;ust=1555608106301000&amp;usg=AFQjCNEbtOapSk6CBqIrLy1IiALsxQpS6Q'>
                                    <img src='https://ci4.googleusercontent.com/proxy/KJCEUwVZqMLZMNXk3Hfx5oGV_w1M0O1bRLGgLEzK-Pa-amJjsdp2JF-aVT1CMssPXUCqH2gbgslks_L2ZbbDs64LXB18bHDdfnvIIAdvpSawKWtVTrMKg94=s0-d-e1-ft#https://imagens.usereserva.com.br/usereserva/news/template/icon-tt.png' width='22' alt='Twitter' style='border:0;display:inline-block;padding-right:7px' class='CToWUd'>
                                </a>
                                <a href='http://news.usereserva.com/pub/cc?_ri_=X0Gzc2X%3DYQpglLjHJlTQGgYBBRwdYzeDItn6loyDgCyUJBfWzazdoze6p0uwmEp55igTCza8CIIzaVXtpKX%3DUCDSCRTT&amp;_ei_=EkLIVj8x9EsyUELCtX-jUiJ7Rup0MmWF5RKin5Eq9OJeaKdy4CTrCCRwSZvQeDsrA2m3lzinv9i7g8r1S3L5H4EeD7hD8s3_t8kl5bnnSi31Q5iuSJAo-RhYj3deU5ySCdnLZ3vxjQ58q9vXPdB3ZQHqxxIB3otFiZV9e6BLrCKDy_mgjCH9XqcNPMfKDhBs-VU-U4jljDERrzp8stB54QHZGxQgA0.' style='text-decoration:none' target='_blank' data-saferedirecturl='https://www.google.com/url?q=http://news.usereserva.com/pub/cc?_ri_%3DX0Gzc2X%253DYQpglLjHJlTQGgYBBRwdYzeDItn6loyDgCyUJBfWzazdoze6p0uwmEp55igTCza8CIIzaVXtpKX%253DUCDSCRTT%26_ei_%3DEkLIVj8x9EsyUELCtX-jUiJ7Rup0MmWF5RKin5Eq9OJeaKdy4CTrCCRwSZvQeDsrA2m3lzinv9i7g8r1S3L5H4EeD7hD8s3_t8kl5bnnSi31Q5iuSJAo-RhYj3deU5ySCdnLZ3vxjQ58q9vXPdB3ZQHqxxIB3otFiZV9e6BLrCKDy_mgjCH9XqcNPMfKDhBs-VU-U4jljDERrzp8stB54QHZGxQgA0.&amp;source=gmail&amp;ust=1555608106302000&amp;usg=AFQjCNH8M30WnoiSJHxWkM27QXNbpeafSw'>
                                    <img src='https://ci6.googleusercontent.com/proxy/9PrFY08Qtb9rzzHzz5UyXU3FUuLGU7MdepzGZg991vwjIBRELFVkxiRpF1TqG64HquSiFnZiwXdg075lTwGm-z59OwXIm7bdLoGmxwySgL_sYrRR4J6OEHI=s0-d-e1-ft#https://imagens.usereserva.com.br/usereserva/news/template/icon-ig.png' width='22' alt='Instagram' style='border:0;display:inline-block;padding-right:8px' class='CToWUd'>
                                </a>
                                <a href='http://news.usereserva.com/pub/cc?_ri_=X0Gzc2X%3DYQpglLjHJlTQGgYBBRwdYzeDItn6loyDgCyUJBfWzazdoze6p0uwmEp55igTCza8CIIzaVXtpKX%3DUCDSCRWT&amp;_ei_=EkLIVj8x9EsyUELCtX-jUiJ7Rup0MmWF5RKin5Eq9OJeaKdy4CTrCCRwSZvQeDsrA2m3lzinv9i7g8r1S3L5H4EeD7hD8s3_t8kl5bnnSi31Q5iuSJAo-RhYj3deU5ySCdnLZ3vxjQ58q9vXPdB3ZQHqxxIB3otFiZV9e6BLrCKDy_mgjCH9XqcNPMfKDhBs-VU-U4jljDERrzp8stB54QHZGxQgA0.' style='text-decoration:none' target='_blank' data-saferedirecturl='https://www.google.com/url?q=http://news.usereserva.com/pub/cc?_ri_%3DX0Gzc2X%253DYQpglLjHJlTQGgYBBRwdYzeDItn6loyDgCyUJBfWzazdoze6p0uwmEp55igTCza8CIIzaVXtpKX%253DUCDSCRWT%26_ei_%3DEkLIVj8x9EsyUELCtX-jUiJ7Rup0MmWF5RKin5Eq9OJeaKdy4CTrCCRwSZvQeDsrA2m3lzinv9i7g8r1S3L5H4EeD7hD8s3_t8kl5bnnSi31Q5iuSJAo-RhYj3deU5ySCdnLZ3vxjQ58q9vXPdB3ZQHqxxIB3otFiZV9e6BLrCKDy_mgjCH9XqcNPMfKDhBs-VU-U4jljDERrzp8stB54QHZGxQgA0.&amp;source=gmail&amp;ust=1555608106302000&amp;usg=AFQjCNFIo1w5tEd62vi9eEh6n4GyLlRFSw'>
                                    <img src='https://ci4.googleusercontent.com/proxy/gSmNGbIwh0H8dmZKNqp0Ikpm1iJVolBwJhk5y0DpQwZ2Xm03s9mURsI9Iw3mH_ThE0ii4zSPAFtjy-OcAVU5WEYQ_FZhla84COmLxuvtemTSkmXuEyosIiY=s0-d-e1-ft#https://imagens.usereserva.com.br/usereserva/news/template/icon-yt.png' width='22' alt='Youtube' style='border:0;display:inline-block' class='CToWUd'>
                                </a>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <!-- AREA DE MARKETING -->

                <!-- WRAPPER -->
                <!-- Set wrapper width (twice) -->
                <table border='0' cellpadding='0' cellspacing='0' align='center' width='560' style='border-collapse: collapse; border-spacing: 0; padding: 0; width: inherit;	max-width: 560px;' class='wrapper'>

                    <!-- Set text color and font family ('sans-serif' or 'Georgia, serif'). Duplicate all text styles in links, including line-height -->
                    <tr>
                        <td align='center' valign='top' style='border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0; padding-left: 6.25%; padding-right: 6.25%; width: 87.5%; font-size: 13px; font-weight: 400; line-height: 150%;
			            padding-top: 20px;padding-bottom: 20px;color: #999999;font-family: sans-serif;' class='footer'>
                           
                            <!-- ANALYTICS -->
                            <!-- http://www.google-analytics.com/collect?v=1&tid={{UA-Tracking-ID}}&cid={{Client-ID}}&t=event&ec=email&ea=open&cs={{Campaign-Source}}&cm=email&cn={{Campaign-Name}} -->

                            <img width='1' height='1' border='0' vspace='0' hspace='0' style='margin: 0; padding: 0; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; border: none; display: block;'
                                 src='https://raw.githubusercontent.com/konsav/email-templates/master/images/tracker.png' />
                        </td>
                    </tr>
                    <!-- End of WRAPPER -->
                </table>
                <!-- End of SECTION / BACKGROUND -->
            </td>
        </tr>
    </table>
</body>

</html>
";


            return corpoEmail.Replace("#SENHA#", "");
        }

        public string CorpoEmailAvaliacaoContato(string logo, string urlImgVendedor)
        {
            string corpoEmail = @" 
<html xmlns='http://www.w3.org/1999/xhtml'>
<head>
    <meta http-equiv='content-type' content='text/html; charset=utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1.0;'>
    <meta name='format-detection' content='telephone=no' />   
    <style>
        /* Reset styles */
        body {
            margin: 0;
            padding: 0;
            min-width: 100%;
            width: 100% !important;
            height: 100% !important;
        }

        body, table, td, div, p, a {
            -webkit-font-smoothing: antialiased;
            text-size-adjust: 100%;
            -ms-text-size-adjust: 100%;
            -webkit-text-size-adjust: 100%;
            line-height: 100%;
        }

        table, td {
            mso-table-lspace: 0pt;
            mso-table-rspace: 0pt;
            border-collapse: collapse !important;
            border-spacing: 0;
        }

        img {
            border: 0;
            line-height: 100%;
            outline: none;
            text-decoration: none;
            -ms-interpolation-mode: bicubic;
        }

        #outlook a {
            padding: 0;
        }

        .ReadMsgBody {
            width: 100%;
        }

        .ExternalClass {
            width: 100%;
        }

            .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {
                line-height: 100%;
            }

        /* Rounded corners for advanced mail clients only */
        @media all and (min-width: 560px) {
            .container {
                border-radius: 8px;
                -webkit-border-radius: 8px;
                -moz-border-radius: 8px;
                -khtml-border-radius: 8px;
            }
        }

        /* Set color for auto links (addresses, dates, etc.) */
        a, a:hover {
            color: #127DB3;
        }

        .footer a, .footer a:hover {
            color: #999999;
        }

 .list-type1 {
            /* width: 400px;*/
            margin: 0 auto;
        }

            .list-type1 ol {
                counter-reset: li;
                list-style: none;
                *list-style: decimal;
                font-size: 15px;
                font-family: 'Raleway', sans-serif;
                padding: 0;
                margin-bottom: 4em;
            }

                .list-type1 ol ol {
                    margin: 0 0 0 2em;
                }

            .list-type1 a {
                position: relative;
                display: block;
                padding: .4em .4em .4em 2em;
                *padding: .4em;
                margin: .5em 0;
                background: #93C775;
                color: #000;
                text-decoration: none;
                -moz-border-radius: .3em;
                -webkit-border-radius: .3em;
                border-radius: 10em;
                transition: all .2s ease-in-out;
            }

                .list-type1 a:hover {
                    background: #d6d4d4;
                    text-decoration: none;
                    transform: scale(1.1);
                }

                .list-type1 a:before {
                    content: counter(li);
                    counter-increment: li;
                    position: absolute;
                    left: -1.3em;
                    top: 50%;
                    margin-top: -1.3em;
                    background: #93C775;
                    height: 2em;
                    width: 2em;
                    line-height: 2em;
                    border: .3em solid #fff;
                    text-align: center;
                    font-weight: bold;
                    -moz-border-radius: 2em;
                    -webkit-border-radius: 2em;
                    border-radius: 2em;
                    color: #FFF;
                }

        .list-type1_hover {
            background: #93C775;
            text-decoration: none;
            transform: scale(1.1);
        }
    </style>
    <!-- MESSAGE SUBJECT -->
    <title>Solicitação de troca de senha NOW</title>
</head>
<!-- BODY -->
<!-- Set message background color (twice) and text color (twice) -->
<body topmargin='0' rightmargin='0' bottommargin='0' leftmargin='0' marginwidth='0' marginheight='0' width='100%' style='border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0; width: 100%; height: 100%; -webkit-font-smoothing: antialiased; text-size-adjust: 100%; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; line-height: 100%;
	background-color: #F0F0F0;
	color: #000000;'
      bgcolor='#F0F0F0'
      text='#000000'>
    <!-- SECTION / BACKGROUND -->
    <!-- Set message background color one again -->
    <table width='100%' align='center' border='0' cellpadding='0' cellspacing='0' style='border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0; width: 100%;' class='background'>
        <tr>
            <td align='center' valign='top' style='border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0;'
                bgcolor='#F0F0F0'>
                <!-- WRAPPER -->
                <!-- Set wrapper width (twice) -->
                <table border='0' cellpadding='0' cellspacing='0' align='center'
                       width='560' style='border-collapse: collapse; border-spacing: 0; padding: 0; width: inherit;
	max-width: 560px;' class='wrapper'>
                    <tr>
                        <td align='center' valign='top' style='border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0; padding-left: 6.25%; padding-right: 6.25%; width: 87.5%;
			padding-top: 20px;
			padding-bottom: 20px;'>
                            <!-- PREHEADER -->
                            <!-- Set text color to background color -->
                            <div style='display: none; visibility: hidden; overflow: hidden; opacity: 0; font-size: 1px; line-height: 1px; height: 0; max-height: 0; max-width: 0;
			color: #F0F0F0;' class='preheader'>
                                Available on&nbsp;GitHub and&nbsp;CodePen. Highly compatible. Designer friendly. More than 50%&nbsp;of&nbsp;total email opens occurred on&nbsp;a&nbsp;mobile device&nbsp;� a&nbsp;mobile-friendly design is&nbsp;a&nbsp;must for&nbsp;email campaigns.
                            </div>
                            <!-- LOGO -->
                            <!-- Image text color should be opposite to background color. Set your url, image src, alt and title. Alt text should fit the image size. Real image size should be x2. URL format: http://domain.com/?utm_source={{Campaign-Source}}&utm_medium=email&utm_content=logo&utm_campaign={{Campaign-Name}} -->
                            <a target='_blank' style='text-decoration: none;'
                               href='http://www.usereserva.com.br'>
                                <img border='0' vspace='0' hspace='0'
                                     src='" + logo + @"' width='120'
                                     alt='Logo' title='Logo' style='
				color: #000000;
				font-size: 10px; margin: 0; padding: 0; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; border: none; display: block;' />
                            </a>
                        </td>
                    </tr>
                    <!-- End of WRAPPER -->
                </table>
                <!-- WRAPPER / CONTEINER -->
                <!-- Set conteiner background color -->
                <table border='0' cellpadding='0' cellspacing='0' align='center'
                       bgcolor='#FFFFFF'
                       width='560' style='border-collapse: collapse; border-spacing: 0; padding: 0; width: inherit;
	max-width: 560px;' class='container'>
                    <!-- HEADER -->
                    <!-- Set text color and font family ('sans-serif' or 'Georgia, serif') -->
                    <tr>
                        <td align='center' valign='top' style='border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0; padding-left: 6.25%; padding-right: 6.25%; width: 87.5%; font-size: 24px; font-weight: bold; line-height: 130%;
			padding-top: 25px;
			color: #000000;
			font-family: sans-serif;' class='header'>
                            Olá, #nomecliente#!
                        </td>
                    </tr>

                    <!-- SUBHEADER -->
                    <!-- Set text color and font family ('sans-serif' or 'Georgia, serif') -->
                    <tr>
                        <td align='center' valign='top' style='border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0; padding-bottom: 3px; padding-left: 6.25%; padding-right: 6.25%; width: 87.5%; font-size: 18px; font-weight: 300; line-height: 150%;
			padding-top: 5px;
			color: #000000;
			font-family: sans-serif;' class='subheader'>    
<br>
<br>
" + urlImgVendedor + @"<br> 
                            #VENDEDOR# entrou em contato com você. Agora é sua vez de nos contar como foi esta ligação!
                        </td>
                    </tr>
                    <!-- HERO IMAGE -->
                    <!-- Image text color should be opposite to background color. Set your url, image src, alt and title. Alt text should fit the image size. Real image size should be x2 (wrapper x2). Do not set height for flexible images (including 'auto'). URL format: http://domain.com/?utm_source={{Campaign-Source}}&utm_medium=email&utm_content={{�mage-Name}}&utm_campaign={{Campaign-Name}} -->
                    <!-- PARAGRAPH -->
                    <!-- Set text color and font family ('sans-serif' or 'Georgia, serif'). Duplicate all text styles in links, including line-height -->
                    <!-- Set line color -->
                    <tr>
                        <td align='center' valign='top' style='border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0; padding-left: 6.25%; padding-right: 6.25%; width: 87.5%;
			padding-top: 25px;' class='line'>
                            <hr color='#E0E0E0' align='center' width='100%' size='1' noshade style='margin: 0; padding: 0;' />
                        </td>
                    </tr>
                    <tr>
                        <td align='center' valign='top' style='border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0; padding-left: 6.25%; padding-right: 6.25%; width: 87.5%; font-size: 17px; font-weight: 400; line-height: 160%;
			padding-top: 25px;
			color: #000000;
			font-family: sans-serif;' class='paragraph'>
                            Avalie clicando abaixo no número de estrelas. Quanto mais estrelas, mais satisfeito você ficou!
                        </td>
                    </tr>
                    <!-- BUTTON -->
                    <!-- Set button background color at TD, link/text color at A and TD, font family ('sans-serif' or 'Georgia, serif') at TD. For verification codes add 'letter-spacing: 5px;'. Link format: http://domain.com/?utm_source={{Campaign-Source}}&utm_medium=email&utm_content={{Button-Name}}&utm_campaign={{Campaign-Name}} -->
                    <tr>
                        <td align='center' valign='top' style='border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0; padding-left: 6.25%; padding-right: 6.25%; width: 87.5%;
			padding-top: 25px;
			padding-bottom: 5px;' class='button'>
                            
                                <table border='0' cellpadding='0' cellspacing='0' align='center' style='max-width: 240px; min-width: 120px; border-collapse: collapse; border-spacing: 0; padding: 0;'>
                                    <tr>
                                        <td align='center' valign='middle' style='padding: 12px 24px; margin: 0; border-collapse: collapse; border-spacing: 0; border-radius: 4px; -webkit-border-radius: 4px; -moz-border-radius: 4px; -khtml-border-radius: 4px;' >
                                            <div style='color: #FFFFFF; font-family: sans-serif; font-size: 17px; font-weight: 400; line-height: 120%;'>
                                                <a href='http://www.usenow.com.br/AcessoExterno/AvaliacaoContato?id=#idcontato#&avaliacao=1&imgurl=#imgurl#' onclick='Avaliar(1)' style='text-decoration: none'>
                                                    <img src='http://www.usenow.com.br/Images/star0.png' id='s1'>
                                                </a>
                                                <a href='http://www.usenow.com.br/AcessoExterno/AvaliacaoContato?id=#idcontato#&avaliacao=2&imgurl=#imgurl#' onclick='Avaliar(2)' style='text-decoration: none'>
                                                    <img src='http://www.usenow.com.br/Images/star0.png' id='s2'>
                                                </a>
                                                <a href='http://www.usenow.com.br/AcessoExterno/AvaliacaoContato?id=#idcontato#&avaliacao=3&imgurl=#imgurl#' onclick='Avaliar(3)' style='text-decoration: none'>
                                                    <img src='http://www.usenow.com.br/Images/star0.png' id='s3'>
                                                </a>
                                                <a href='http://www.usenow.com.br/AcessoExterno/AvaliacaoContato?id=#idcontato#&avaliacao=4&imgurl=#imgurl#' onclick='Avaliar(4)' style='text-decoration: none'>
                                                    <img src='http://www.usenow.com.br/Images/star0.png' id='s4'>
                                                </a>
                                                <a href='http://www.usenow.com.br/AcessoExterno/AvaliacaoContato?id=#idcontato#&avaliacao=5&imgurl=#imgurl#' onclick='Avaliar(5)' style='text-decoration: none'>
                                                    <img src='http://www.usenow.com.br/Images/star0.png' id='s5'>
                                                </a>                                                
                                            </div>
                                        </td>
                                    </tr>
                                </table>                            
                        </td>
                    </tr> 
                    <tr class='avaliacao'>
                        <td align='center' valign='top' style='border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0; padding-left: 6.25%; padding-right: 6.25%; width: 87.5%; padding-top: 25px; adding-bottom: 5px;' class='button'>
                            
                             <div id='opcaoRespNaoRecebi'>
                                <br />
                                <span style='padding-top: 50px; font-size: 18px; font-weight: 300; line-height: 150%; padding-top: 5px; color: #000000; font-family: sans-serif;' class='subheader'>
                                    <b style='color:crimson'>NÃO RECEBI CONTATO</b> Selecione o motivo!
                                </span>

                                <div class='list-type1'>
                                    <ol>
                                        <li><a href='http://www.usenow.com.br/AcessoExterno/AvaliacaoContato?id=#idcontato#&naorecebi=0&texto=Não%20sou%20o%20cliente%20informado!'>Não sou o cliente informado!</a></li>
                                        <li><a href='http://www.usenow.com.br/AcessoExterno/AvaliacaoContato?id=#idcontato#&naorecebi=0&texto=Conheço%20o%20cliente,%20mas%20o%20e-mail%20não%20é%20dele!'>Conheço <span class='spnCliente'>#nomecliente#</span>, mas o e-mail não é dele!</a></li>
                                        <li><a href='http://www.usenow.com.br/AcessoExterno/AvaliacaoContato?id=#idcontato#&naorecebi=0&texto=Nenhum%20VENDEDOR%20entrou%20em%20contato!'><b>Sou <span class='spnCliente'>#nomecliente#</span>, mas nenhum VENDEDOR entrou em contato!</b></a></li>
                                    </ol>
                                </div>
                                <a href='#' style='text-decoration:none; display:none' onclick='$('#opcaoRespNaoRecebi').hide('show'); $('.avaliacao').show('show'); $('.textMensagem').show('show');'>
                                    <table border='0' cellpadding='0' cellspacing='0' align='center' style='max-width: 240px; min-width: 120px; border-collapse: collapse; border-spacing: 0; padding: 0;'>
                                        <tr>
                                            <td align='center' valign='middle' style='padding: 7px 10px; margin: 0; border-collapse: collapse; border-spacing: 0; border-radius: 4px; -webkit-border-radius: 4px; -moz-border-radius: 4px; -khtml-border-radius: 4px;'
                                                bgcolor='gray'>
                                                <div style='color: #FFFFFF; font-family: sans-serif; font-size: 12px; font-weight: 400; line-height: 120%;'>
                                                    FECHAR
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </a>
                            </div>



                            <!--<a href='http://www.usenow.com.br/AcessoExterno/AvaliacaoContato?id=#idcontato#&naorecebi=0' style='text-decoration:none'>
                                <table border='0' cellpadding='0' cellspacing='0' align='center' style='max-width: 240px; min-width: 120px; border-collapse: collapse; border-spacing: 0; padding: 0;'>
                                    <tr>
                                        <td align='center' valign='middle' style='padding: 12px 24px; margin: 0; border-collapse: collapse; border-spacing: 0; border-radius: 4px; -webkit-border-radius: 4px; -moz-border-radius: 4px; -khtml-border-radius: 4px;'
                                            bgcolor='#E9703E'>
                                            <div style='color: #FFFFFF; font-family: sans-serif; font-size: 17px; font-weight: 400; line-height: 120%;'>
                                                NÃO RECEBI CONTATO
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </a>-->

                        </td>
                    </tr>
                    <tr>
                        <td align='center' valign='top' style='border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0; padding-left: 6.25%; padding-right: 6.25%; width: 87.5%;
			padding-top: 25px;' class='line'>
                            <hr color='#E0E0E0' align='center' width='100%' size='1' noshade style='margin: 0; padding: 0;' />
                        </td>
                    </tr>
                    <!-- PARAGRAPH -->
                    <!-- Set text color and font family ('sans-serif' or 'Georgia, serif'). Duplicate all text styles in links, including line-height -->
                    
                    <!-- End of WRAPPER -->
                </table>
                <!-- WRAPPER -->
                <!-- Set wrapper width (twice) -->
                <table border='0' cellpadding='0' cellspacing='0' align='center'
                       width='560' style='border-collapse: collapse; border-spacing: 0; padding: 0; width: inherit;
	max-width: 560px;' class='wrapper'>

                    <!-- Set text color and font family ('sans-serif' or 'Georgia, serif'). Duplicate all text styles in links, including line-height -->
                    <tr>
                        <td align='center' valign='top' style='border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0; padding-left: 6.25%; padding-right: 6.25%; width: 87.5%; font-size: 13px; font-weight: 400; line-height: 150%;
			padding-top: 20px;
			padding-bottom: 20px;
			color: #999999;
			font-family: sans-serif;' class='footer'>
                             Reserva <a href='https://www.usenow.com.br' target='_blank' style='text-decoration: underline; color: #999999; font-family: sans-serif; font-size: 13px; font-weight: 400; line-height: 150%;'>usereserva.com</a>.

                            <!-- ANALYTICS -->
                            <!-- http://www.google-analytics.com/collect?v=1&tid={{UA-Tracking-ID}}&cid={{Client-ID}}&t=event&ec=email&ea=open&cs={{Campaign-Source}}&cm=email&cn={{Campaign-Name}} -->
                            <img width='1' height='1' border='0' vspace='0' hspace='0' style='margin: 0; padding: 0; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; border: none; display: block;'
                                 src='https://raw.githubusercontent.com/konsav/email-templates/master/images/tracker.png' />
                        </td>
                    </tr>
                    <!-- End of WRAPPER -->
                </table>
                <!-- End of SECTION / BACKGROUND -->
            </td>
        </tr>
    </table>
</body>
</html>
            ";

            return corpoEmail.Replace("#SENHA#", "");
        }

        public JsonResult SalvarLembrete(string clienteId, string cpf, string dica, string idAgenda, string dataInicio, string lojaId, string vendedorId, string tipoPagina)
        {
            try
            {
                @ViewBag.ativarNovaTelaVendedor = Session["ativarNovaTelaVendedor"] == null ? false : Session["ativarNovaTelaVendedor"];
                if (!String.IsNullOrEmpty(clienteId))
                {
                    Lembrete lembrete = new Lembrete();

                    int _id = Convert.ToInt32(clienteId);
                    DateTime TempoInicial = DateTime.Now;
                    lembrete.cliente_id = _id;

                    if (_id == 0)
                    {
                        var cliente = _serviceCliente.BuscarClienteCpfOrigemLoja(OrigemLoja, cpf).FirstOrDefault();
                        lembrete.cliente_id = _id;
                    }
                    lembrete.data_inicio = Convert.ToDateTime(dataInicio);
                    lembrete.data_final = Convert.ToDateTime(dataInicio).AddDays(6);
                    lembrete.dica = dica;
                    lembrete.motivo = idAgenda;
                    lembrete.loja_id = Convert.ToInt32(lojaId);
                    lembrete.vendedorId = Convert.ToInt32(vendedorId);

                    _serviceLembrete.SalvarLembrete(lembrete);

                    MensagemRetorno("Agendamento salvo com sucesso!", TypeMensage.success);

                    DateTime TempoFinal = DateTime.Now;
                    // Log do sistema
                    LogSistema(new LogSistema { processo_minutos = TempoProcesso(TempoInicial, TempoFinal), acao = "Salvando agendamento do Cliente: " + " --- Dados Inserido: " + lembrete.cliente_id + " # motivo: " + lembrete.motivo + " # Vendedor: " + lembrete.vendedorId + " # Loja: " + lembrete.loja_id + " # Dica: " + lembrete.dica + " # data_inicio: " + lembrete.data_inicio + " # data_final: " + lembrete.data_final, login = UsuarioLogado.Usuario.email, tipo_log = "CADASTRANDO", vendedor_id = (String.IsNullOrEmpty(UsuarioLogado.IdVendedorSeleconado) ? 0 : Convert.ToInt32(UsuarioLogado.IdVendedorSeleconado)), loja_id = (String.IsNullOrEmpty(UsuarioLogado.IdLojaSeleconado) ? 0 : Convert.ToInt32(UsuarioLogado.IdLojaSeleconado)), metodo = "Cliente - Agendamento", tela = "Agendamento", cliente_id = lembrete.cliente_id, user_id = UsuarioLogado.Usuario.id, ip = HttpContext.Request.UserHostAddress.ToString(), data = DateTime.Now });

                    // Se o Parametro estiver vazio retorna para a busca de clientes
                    if (string.IsNullOrEmpty(tipoPagina))
                    {
                        return JsonRetono(new { sucesso = true, responseText = "Cliente" });
                    }
                    else
                    {
                        return JsonRetono(new { sucesso = true, responseText = "Agenda" });
                    }
                }
                else
                {
                    return JsonRetono(new { sucesso = true, msg = "O Cliente não foi informado", totalFavoritos = UsuarioLogado.ListaReportClientesFavoritos.Count().ToString() });
                }
            }
            catch (Exception ex)
            {
                string dados = " --- Dados Inserido: " + clienteId + " # motivo: " + idAgenda + " # Vendedor: " + vendedorId + " # Loja: " + lojaId + " # Dica: " + dica + " # data_inicio: " + dataInicio;

                _serviceUsuario.AdicionarErro("SalvarAgendamento", "Perfil do Cliente", ex.Message + dados, (String.IsNullOrEmpty(UsuarioLogado.IdVendedorSeleconado) ? 0 : Convert.ToInt32(UsuarioLogado.IdVendedorSeleconado)), Convert.ToInt32(clienteId), (String.IsNullOrEmpty(UsuarioLogado.IdLojaSeleconado) ? 0 : Convert.ToInt32(UsuarioLogado.IdLojaSeleconado)), UsuarioLogado.Usuario.id);

                MensagemRetorno("", TypeMensage.error);

                return JsonRetono(new { sucesso = false, responseText = "", msg = "Ocorreu um erro contate o adiministrador do sistema!" });
            }
        }

        public JsonResult ObterEstimativaUber(string inicioLatitude, string inicioLongitude, string fimLatitude, string fimLongitude)
        {
            try
            {
                @ViewBag.ativarNovaTelaVendedor = Session["ativarNovaTelaVendedor"] == null ? false : Session["ativarNovaTelaVendedor"];
                var estimativa = ObterPrecoUber(inicioLatitude, inicioLongitude, fimLatitude, fimLongitude);

                return Json(new { sucesso = true, estimativa = estimativa });

            }
            catch (Exception ex)
            {
                return Json(new { sucesso = false, msg = MsgErro() });
            }
        }

        #endregion "Métodos Perfil do Cliente"

        #region "Métodos Dependentes"

        public JsonResult AddDepentende(string clienteId, string grauParentesco, string sexo, string dataNascimento, string nome, string observacao, string tipoCliente, string vendedorId, string lojaId)
        {
            @ViewBag.ativarNovaTelaVendedor = Session["ativarNovaTelaVendedor"] == null ? false : Session["ativarNovaTelaVendedor"];
            if (string.IsNullOrEmpty(clienteId))
            {
                if (Session["cliente"] != null)
                {
                    // Salvar o dependente pelo info vendas
                    clienteId = ((Cliente)Session["cliente"]).id.ToString();
                }
            }

            ClienteNovoDependente clienteNovoDependente = new ClienteNovoDependente()
            {
                data_inclusao = DateTime.Now,
                data_nascimento = Convert.ToDateTime(dataNascimento),
                grau_parentesco = grauParentesco,
                nome = nome,
                observacao = observacao,
                sexo = sexo,
                loja_id = (String.IsNullOrEmpty(UsuarioLogado.IdLojaSeleconado) ? (String.IsNullOrEmpty(lojaId) ? 0 : Convert.ToInt32(lojaId)) : Convert.ToInt32(UsuarioLogado.IdLojaSeleconado)),
                vendedor_id = (String.IsNullOrEmpty(UsuarioLogado.IdVendedorSeleconado) ? (String.IsNullOrEmpty(vendedorId) ? 0 : Convert.ToInt32(vendedorId)) : Convert.ToInt32(UsuarioLogado.IdVendedorSeleconado))
            };

            var lista = InserirDependenteClienteNovo(Convert.ToInt32(clienteId), clienteNovoDependente, "INSERIR", tipoCliente);

            return JsonRetono(new { success = true, dados = lista });
        }

        public JsonResult RemoveDepentende(string clienteId, string nomeDependente, string tipoCliente)
        {
            @ViewBag.ativarNovaTelaVendedor = Session["ativarNovaTelaVendedor"] == null ? false : Session["ativarNovaTelaVendedor"];
            ClienteNovoDependente clienteNovoDependente = new ClienteNovoDependente()
            {
                nome = nomeDependente
            };
            var lista = InserirDependenteClienteNovo(Convert.ToInt32(clienteId), clienteNovoDependente, "REMOVER", tipoCliente);

            return JsonRetono(new { success = true, dados = lista });
        }

        private IList<ClienteNovoDependente> InserirDependenteClienteNovo(int clienteId, ClienteNovoDependente clienteNovoDependente, string tipoInsercao, string tipoCliente)
        {
            IList<ClienteNovoDependente> lista = new List<ClienteNovoDependente>();

            if (tipoCliente == "NOVO")
            {
                if (Session["ClienteNovoDependente"] != null)
                    lista = Session["ClienteNovoDependente"] as List<ClienteNovoDependente>;

                if (tipoInsercao == "INSERIR")
                {
                    lista.Add(clienteNovoDependente);
                }
                else
                {
                    lista = lista.Where(x => x.nome != clienteNovoDependente.nome).ToList();
                }

                Session["ClienteNovoDependente"] = lista;
            }
            else
            {
                int idDep = 0;
                if (Session["idDependente"] != null)
                {
                    idDep = Convert.ToInt32(Session["idDependente"].ToString());
                    Session["idDependente"] = null;
                }

                if (Session["ClienteDependente"] != null)
                    lista = Session["ClienteDependente"] as List<ClienteNovoDependente>;

                if (tipoInsercao == "INSERIR")
                {
                    bool update = false;
                    foreach (var item in lista)
                    {
                        if (item.id == idDep)
                        {
                            item.cliente_id = clienteId;
                            item.data_inclusao = clienteNovoDependente.data_inclusao;
                            item.data_nascimento = clienteNovoDependente.data_nascimento;
                            item.grau_parentesco = clienteNovoDependente.grau_parentesco;
                            item.data_alteracao = DateTime.Now;
                            item.nome = clienteNovoDependente.nome;
                            item.observacao = clienteNovoDependente.observacao;
                            item.loja_id = clienteNovoDependente.loja_id;
                            item.vendedor_id = clienteNovoDependente.vendedor_id;
                            item.sexo = clienteNovoDependente.sexo;
                            update = true;
                        }
                    }

                    ClienteDependente _dep = new ClienteDependente()
                    {
                        cliente_id = clienteId,
                        data_inclusao = clienteNovoDependente.data_inclusao,
                        data_nascimento = clienteNovoDependente.data_nascimento,
                        grau_parentesco = clienteNovoDependente.grau_parentesco,
                        nome = clienteNovoDependente.nome,
                        observacao = clienteNovoDependente.observacao,
                        loja_id = clienteNovoDependente.loja_id,
                        vendedor_id = clienteNovoDependente.vendedor_id,
                        sexo = clienteNovoDependente.sexo,
                        id = idDep
                    };

                    _serviceCliente.SalvarDependenteCliente(_dep);

                    clienteNovoDependente.id = _dep.id;

                    if (!update)
                        lista.Add(clienteNovoDependente);
                }
                else
                {
                    lista = lista.Where(x => x.nome != clienteNovoDependente.nome).ToList();
                }

                Session["ClienteDependente"] = lista;
            }

            return lista.OrderByDescending(x => x.data_inclusao).ToList();
        }

        public JsonResult ObterDependente(string idDependente)
        {
            try
            {
                @ViewBag.ativarNovaTelaVendedor = Session["ativarNovaTelaVendedor"] == null ? false : Session["ativarNovaTelaVendedor"];
                Session["idDependente"] = idDependente;

                var dependente = _serviceCliente.ObterDependente(idDependente);

                return JsonRetono(new { Sucesso = true, dependente = dependente });
            }
            catch (Exception ex)
            {
                return JsonRetono(new { Sucesso = false, Mensagem = ex.Message });
            }
        }

        public JsonResult RemoverDependente(string idDependente)
        {
            try
            {
                @ViewBag.ativarNovaTelaVendedor = Session["ativarNovaTelaVendedor"] == null ? false : Session["ativarNovaTelaVendedor"];
                _serviceCliente.RemoverDependente(Convert.ToInt32(idDependente));

                IList<ClienteNovoDependente> lista = new List<ClienteNovoDependente>();
                if (Session["ClienteDependente"] != null)
                {
                    lista = Session["ClienteDependente"] as List<ClienteNovoDependente>;

                    Session["ClienteDependente"] = lista.Where(x => x.id != Convert.ToInt32(idDependente)).ToList();
                }

                return JsonRetono(new { Sucesso = true, Mensagem = "Operação Realizada com Sucesso!" });
            }
            catch (Exception ex)
            {
                return JsonRetono(new { Sucesso = false, Mensagem = ex.Message });
            }
        }

        public JsonResult ListaDependentesDetalhe(string lojaId, string vendedorId, string dataInicio, string dataFim)
        {
            try
            {
                @ViewBag.ativarNovaTelaVendedor = Session["ativarNovaTelaVendedor"] == null ? false : Session["ativarNovaTelaVendedor"];
                var lista = _serviceRelatorio.ListarRelatorios<ClienteDependenteDetalhe>("SP_CadastroDependentesDetalhe", lojaId, dataInicio, dataFim, null);

                if (!String.IsNullOrEmpty(vendedorId))
                    lista = lista.Where(x => x.vendedor_id == Convert.ToInt32(vendedorId)).ToList();

                return JsonRetono(new { success = true, lista = lista });
            }
            catch (Exception ex)
            {
                MsgErro();
                return JsonRetono(new { success = false });
            }


        }

        public JsonResult ObterDependentesPorClientes(string clienteId)
        {
            @ViewBag.ativarNovaTelaVendedor = Session["ativarNovaTelaVendedor"] == null ? false : Session["ativarNovaTelaVendedor"];
            return JsonRetono(true);
            //try
            //{
            //    Session["idDependente"] = idDependente;

            //    var dependente = _serviceCliente.ObterDependente(idDependente);

            //    return JsonRetono(new { success = true, dados = lista });
            //}
            //catch (Exception ex)
            //{
            //    return JsonRetono(new { Sucesso = false, msg = ex.Message });
            //}
        }

        private void ObterPontosFidelidade(string loja, string cpf_cliente)
        {
            var resultado = GetAuthToken();

            var resultado2 = ObterPontosAVencer("26618449880", "TLVLM", resultado.token_type + " " + resultado.access_token);

            var listagem = new List<Fidelidade>();
            listagem.Add(new Fidelidade { DataVencimento = DateTime.Today.AddDays(100), Pontos = "500", Valor = 850.00 });
            listagem.Add(new Fidelidade { DataVencimento = DateTime.Today.AddDays(50), Pontos = "700", Valor = 550.00 });
            listagem.Add(new Fidelidade { DataVencimento = DateTime.Today, Pontos = "700", Valor = 550.00 });
            ViewBag.Fidelidade = listagem.OrderBy(x => x.DataVencimento).ToList();

            ViewBag.TotalFidelidade = listagem.Sum(x => x.Valor);

        }

        public TokenDTO GetAuthToken()
        {
            @ViewBag.ativarNovaTelaVendedor = Session["ativarNovaTelaVendedor"] == null ? false : Session["ativarNovaTelaVendedor"];
            var client = new RestClient(ConfigurationManager.AppSettings["url_token"].ToString());
            var request = new RestRequest(Method.POST);
            string encodedBody = string.Format("grant_type=password&username={0}&password={1}", ConfigurationManager.AppSettings["user_token"].ToString(), ConfigurationManager.AppSettings["password_token"].ToString());
            request.AddParameter("application/x-www-form-urlencoded", encodedBody, ParameterType.RequestBody);
            request.AddParameter("Content-Type", "application/x-www-form-urlencoded", ParameterType.HttpHeader);
            // execute the request


            IRestResponse response = client.Execute(request);
            var content = response.Content;

            return JsonConvert.DeserializeObject<TokenDTO>(content);
        }

        public PontosAVencer ObterPontosAVencer(string cliente_cpf, string codigo_loja, string autenticacao)
        {
            @ViewBag.ativarNovaTelaVendedor = Session["ativarNovaTelaVendedor"] == null ? false : Session["ativarNovaTelaVendedor"];
            var client = new RestClient("https://www.unicosistemas.com.br/api/fidelidade/ConsultaPontosVencer?documentoFidelidade=" + cliente_cpf + "&codigoLoja=" + codigo_loja);
            var request = new RestRequest(Method.GET);
            //string encodedBody = string.Format("documentoFidelidade={0}&codigoLoja={1}", cliente_cpf, codigo_loja);
            //request.AddParameter(encodedBody, ParameterType.RequestBody);
            request.AddHeader("Authorization", autenticacao);
            // execute the request


            IRestResponse response = client.Execute(request);
            var content = response.Content;

            return JsonConvert.DeserializeObject<PontosAVencer>(content);
        }

        private IList<ClienteNovoDependente> ListarDependentes(string ClienteId)
        {
            #region Carga de Dependentes

            IList<ClienteDependente> listaDep = new List<ClienteDependente>();

            listaDep = ViewBag.Dependentes = _serviceCliente.BuscarDependentes(Convert.ToInt32(ClienteId));

            IList<ClienteNovoDependente> lista = new List<ClienteNovoDependente>();

            foreach (var item in listaDep)
            {
                lista.Add(new ClienteNovoDependente()
                {
                    cliente_id = item.cliente_id,
                    data_inclusao = item.data_inclusao,
                    data_nascimento = item.data_nascimento,
                    grau_parentesco = item.grau_parentesco,
                    nome = item.nome,
                    observacao = item.observacao,
                    id = item.id
                });
            }


            Session["ClienteDependente"] = lista;
            return lista;

            #endregion Carga de Dependentes
        }

        #endregion "Métodos Dependentes"

        #region "Métodos Info Vendas"
        public JsonResult VendasCliente(string cpf, string IdLoja)
        {
            @ViewBag.ativarNovaTelaVendedor = Session["ativarNovaTelaVendedor"] == null ? false : Session["ativarNovaTelaVendedor"];
            var list = ClienteVendas(cpf, IdLoja, _serviceCliente, _serviceVenda);

            var tipoCliente = "NOVO";
            if (Session["cliente"] != null)
            {
                // Tipo do Cliente Dependente ANTIGO
                tipoCliente = "EXISTENTE";
            }

            return JsonRetono(new { tipoCliente = tipoCliente, dados = list });
        }

        public JsonResult DadosClienteInfoVendas(string cpf)
        {
            @ViewBag.ativarNovaTelaVendedor = Session["ativarNovaTelaVendedor"] == null ? false : Session["ativarNovaTelaVendedor"];
            // Log do sistema
            LogSistema(new LogSistema { acao = "Buscando Cliente Info Vendas: CPF: " + cpf, login = UsuarioLogado.Usuario.email, tipo_log = "NAVEGACAO", vendedor_id = (String.IsNullOrEmpty(UsuarioLogado.IdVendedorSeleconado) ? 0 : Convert.ToInt32(UsuarioLogado.IdVendedorSeleconado)), loja_id = (String.IsNullOrEmpty(UsuarioLogado.IdLojaSeleconado) ? 0 : Convert.ToInt32(UsuarioLogado.IdLojaSeleconado)), metodo = "Info Vendas - Cadastro", tela = "Info Vendas", user_id = UsuarioLogado.Usuario.id, ip = HttpContext.Request.UserHostAddress.ToString(), data = DateTime.Now });

            var cliente = Session["cliente"] == null ? new Cliente() : (Cliente)Session["cliente"];

            if (!String.IsNullOrEmpty(cpf.Trim()))
            {
                if (cliente.id == 0)
                {
                    // Se o cliente não existir colocar o novo cliente            
                    cliente.nome = "Cliente Novo";
                }
            }
            else
            {
                cliente.nome = "Cpf não Informado!";
                cliente.id = 99;
            }

            return JsonRetono(cliente);
        }
        public JsonResult NovoCliente(string lojaId, string vendedorId, string quemEle, string cpf, string infoVendas, string dataCadastro)
        {
            @ViewBag.ativarNovaTelaVendedor = Session["ativarNovaTelaVendedor"] == null ? false : Session["ativarNovaTelaVendedor"];

            DateTime TempoInicial = DateTime.Now;
            ClienteNovo clienteNovo = new ClienteNovo()
            {
                cpf = cpf.Trim(),
                data_venda = Convert.ToDateTime(dataCadastro),
                info_vendas = infoVendas,
                loja_id = String.IsNullOrEmpty(lojaId) || lojaId == "0" ? Convert.ToInt64(UsuarioLogado.IdLojaSeleconado) : Convert.ToInt64(lojaId),
                quem_ele = quemEle,
                vendedor_id = String.IsNullOrEmpty(vendedorId) || vendedorId == "0" ? Convert.ToInt64(UsuarioLogado.IdVendedorSeleconado) : Convert.ToInt64(vendedorId),
                data_cadastro = DateTime.Now,
                status = "<span class='label label-warning'>Pendente</span>"
            };

            _serviceCliente.SalvarClienteNovo(clienteNovo);

            if (Session["ClienteNovoDependente"] != null)
            {
                foreach (var item in Session["ClienteNovoDependente"] as List<ClienteNovoDependente>)
                {
                    item.cliente_id = clienteNovo.id;
                    _serviceCliente.SalvarDependenteClienteNovo(item);
                }
            }

            DateTime TempoFinal = DateTime.Now;
            // Log do sistema
            LogSistema(new LogSistema { processo_minutos = TempoProcesso(TempoInicial, TempoFinal), acao = "Cadastrando Novo Cliente:  -- - Dados Inserido: " + cpf + " # data_venda: " + dataCadastro + " # infoVendas: " + infoVendas + " # lojaId: " + lojaId + " # quemEle: " + quemEle + " # vendedorId: " + vendedorId + " # data_cadastro: " + DateTime.Now.ToString("dd/MM/yyyy"), login = UsuarioLogado.Usuario.email, tipo_log = "CADASTRANDO", vendedor_id = (String.IsNullOrEmpty(UsuarioLogado.IdVendedorSeleconado) ? 0 : Convert.ToInt32(UsuarioLogado.IdVendedorSeleconado)), loja_id = (String.IsNullOrEmpty(UsuarioLogado.IdLojaSeleconado) ? 0 : Convert.ToInt32(UsuarioLogado.IdLojaSeleconado)), metodo = "Info Vendas - Cadastro", tela = "Info Vendas", user_id = UsuarioLogado.Usuario.id, ip = HttpContext.Request.UserHostAddress.ToString(), data = DateTime.Now });

            return JsonRetono(true);
        }
        public JsonResult ExcluirNovoCliente(string Id)
        {
            @ViewBag.ativarNovaTelaVendedor = Session["ativarNovaTelaVendedor"] == null ? false : Session["ativarNovaTelaVendedor"];
            DateTime TempoInicial = DateTime.Now;

            _serviceCliente.ExluirClienteNovo(Id);

            DateTime TempoFinal = DateTime.Now;

            // Log do sistema
            LogSistema(new LogSistema { processo_minutos = TempoProcesso(TempoInicial, TempoFinal), acao = "Excluindo Novo Cliente Cadastrado: Cliente ID Novo: " + Id, login = UsuarioLogado.Usuario.email, tipo_log = "EXCLUINDO", vendedor_id = (String.IsNullOrEmpty(UsuarioLogado.IdVendedorSeleconado) ? 0 : Convert.ToInt32(UsuarioLogado.IdVendedorSeleconado)), loja_id = (String.IsNullOrEmpty(UsuarioLogado.IdLojaSeleconado) ? 0 : Convert.ToInt32(UsuarioLogado.IdLojaSeleconado)), metodo = "Info Vendas - Cadastro", tela = "Info Vendas", user_id = UsuarioLogado.Usuario.id, ip = HttpContext.Request.UserHostAddress.ToString(), data = DateTime.Now });

            return JsonRetono(true);
        }
        public JsonResult EditarNovoCliente(string Id)
        {
            @ViewBag.ativarNovaTelaVendedor = Session["ativarNovaTelaVendedor"] == null ? false : Session["ativarNovaTelaVendedor"];
            DateTime TempoInicial = DateTime.Now;

            var cliente = _serviceCliente.BuscarClienteNovo("", "", Id).FirstOrDefault();

            DateTime TempoFinal = DateTime.Now;
            // Log do sistema
            LogSistema(new LogSistema { processo_minutos = TempoProcesso(TempoInicial, TempoFinal), acao = "Editando Novo Cliente Cadastrado: Cliente ID Novo: " + Id, login = UsuarioLogado.Usuario.email, tipo_log = "EDITANDO", vendedor_id = (String.IsNullOrEmpty(UsuarioLogado.IdVendedorSeleconado) ? 0 : Convert.ToInt32(UsuarioLogado.IdVendedorSeleconado)), loja_id = (String.IsNullOrEmpty(UsuarioLogado.IdLojaSeleconado) ? 0 : Convert.ToInt32(UsuarioLogado.IdLojaSeleconado)), metodo = "Info Vendas - Cadastro", tela = "Info Vendas", user_id = UsuarioLogado.Usuario.id, ip = HttpContext.Request.UserHostAddress.ToString(), data = DateTime.Now });

            return JsonRetono(cliente);
        }

        #endregion "Métodos Info Vendas"    

        #region "Métodos Indicar Produtos"

        public JsonResult Addproduto(string produto, string produto_cor, string clienteId)
        {
            if (string.IsNullOrEmpty(clienteId))
            {
                if (Session["cliente"] != null)
                {
                    // Salvar o dependente pelo info vendas
                    clienteId = ((Cliente)Session["cliente"]).id.ToString();
                }
            }

            Produto _produto = new Produto()
            {
                cor_produto = produto_cor,
                produto = produto
            };

            IList<Produto> listaProd = new List<Produto>();

            if (Session["ProdutosIndicados"] != null)
            {
                listaProd = ((IList<Produto>)Session["ProdutosIndicados"]);
            }

            IList<Produto> lista = new List<Produto>();
            if (Session["ClienteIndicacaoProduto"] != null)
                lista = Session["ClienteIndicacaoProduto"] as List<Produto>;

            if (lista.Where(x => x.produto == produto && x.cor_produto == produto_cor).ToList().Count() == 0)
            {
                lista.Add(listaProd.Where(x => x.produto == produto && x.cor_produto == produto_cor).FirstOrDefault());
            }

            Session["ClienteIndicacaoProduto"] = lista;

            return JsonRetono(new { success = true, dados = lista });
        }

        public JsonResult Delproduto(string produto, string produto_cor, string clienteId)
        {
            IList<Produto> lista = new List<Produto>();
            if (Session["ClienteIndicacaoProduto"] != null)
                lista = Session["ClienteIndicacaoProduto"] as List<Produto>;

            lista = lista.Where(x => x.produto + x.cor_produto != produto + produto_cor).ToList();

            Session["ClienteIndicacaoProduto"] = lista;

            return JsonRetono(new { success = true, dados = lista });
        }

        public JsonResult EnviaEmailIndicacao(string clienteId)
        {
            if (string.IsNullOrEmpty(clienteId))
            {
                if (Session["cliente"] != null)
                {
                    // Salvar o dependente pelo info vendas
                    clienteId = ((Cliente)Session["cliente"]).id.ToString();
                }
            }
            int _clienteId = Convert.ToInt32(clienteId);

            IList<Produto> lista = new List<Produto>();
            if (Session["ClienteIndicacaoProduto"] != null)
                lista = Session["ClienteIndicacaoProduto"] as List<Produto>;

            int? _lojaId = String.IsNullOrEmpty(UsuarioLogado.IdLojaSeleconado) ? 0 : Convert.ToInt32(UsuarioLogado.IdLojaSeleconado);
            int? _vendedorId = String.IsNullOrEmpty(UsuarioLogado.IdVendedorSeleconado) ? 0 : Convert.ToInt32(UsuarioLogado.IdVendedorSeleconado);

            var vendedor = _serviceVendedor.GetVendedor(_lojaId.ToString(), _vendedorId.ToString(), 0).FirstOrDefault();
            var loja = UsuarioLogado.LojasUsuario.Where(x => x.id == _lojaId.Value).FirstOrDefault();
            var cliente = BuscarCliente(clienteId);

            // Salva dos produtos indicados
            int id = _serviceCliente.SalvarIndicacaoProdutos(_clienteId, _lojaId, _vendedorId, lista);

            // Enviar Email para cliente.         
            //MailAddress to = new MailAddress("peterson.andrade@usereserva.com");
            //MailAddress from = new MailAddress("peterson.andrade@usereserva.com");
            MailAddress to = new MailAddress("now@usereserva.com");
            MailAddress from = new MailAddress(cliente.email);

            MailMessage message = new MailMessage(from, to);

            message.Subject = "RESERVA - Validação Indicação de Produtos (" + DateTime.Now.ToString("HH:mm") + "h)";
            message.Body = CorpoEmailIndicacao(cliente.nome, loja.filial, vendedor.nome, id.ToString());
            message.IsBodyHtml = true;

            SmtpClient client = new SmtpClient();

            client.Send(message);

            return JsonRetono(new { success = true, dados = lista });
        }


        public string CorpoEmailIndicacao(string cliente, string loja, string vendedor, string id)
        {
            string corpoEmail = @" 

   <html xmlns='http://www.w3.org/1999/xhtml'>
<head>
    <meta http-equiv='content-type' content='text/html; charset=utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1.0;'>
    <meta name='format-detection' content='telephone=no' />
    <!-- Responsive Mobile-First Email Template by Konstantin Savchenko, 2015.
    https://github.com/konsav/email-templates/  -->
    <style>
        /* Reset styles */
        body {
            margin: 0;
            padding: 0;
            min-width: 100%;
            width: 100% !important;
            height: 100% !important;
        }

        body, table, td, div, p, a {
            -webkit-font-smoothing: antialiased;
            text-size-adjust: 100%;
            -ms-text-size-adjust: 100%;
            -webkit-text-size-adjust: 100%;
            line-height: 100%;
        }

        table, td {
            mso-table-lspace: 0pt;
            mso-table-rspace: 0pt;
            border-collapse: collapse !important;
            border-spacing: 0;
        }

        img {
            border: 0;
            line-height: 100%;
            outline: none;
            text-decoration: none;
            -ms-interpolation-mode: bicubic;
        }

        #outlook a {
            padding: 0;
        }

        .ReadMsgBody {
            width: 100%;
        }

        .ExternalClass {
            width: 100%;
        }

            .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {
                line-height: 100%;
            }

        /* Rounded corners for advanced mail clients only */
        @media all and (min-width: 560px) {
            .container {
                border-radius: 8px;
                -webkit-border-radius: 8px;
                -moz-border-radius: 8px;
                -khtml-border-radius: 8px;
            }
        }

        /* Set color for auto links (addresses, dates, etc.) */
        a, a:hover {
            color: #127DB3;
        }

        .footer a, .footer a:hover {
            color: #999999;
        }
    </style>
    <!-- MESSAGE SUBJECT -->
    <title>Solicitação de troca de senha NOW</title>
</head>
<!-- BODY -->
<!-- Set message background color (twice) and text color (twice) -->
<body topmargin='0' rightmargin='0' bottommargin='0' leftmargin='0' marginwidth='0' marginheight='0' width='100%' style='border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0; width: 100%; height: 100%; -webkit-font-smoothing: antialiased; text-size-adjust: 100%; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; line-height: 100%;
	background-color: #F0F0F0;
	color: #000000;'
      bgcolor='#F0F0F0'
      text='#000000'>
    <!-- SECTION / BACKGROUND -->
    <!-- Set message background color one again -->
    <table width='100%' align='center' border='0' cellpadding='0' cellspacing='0' style='border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0; width: 100%;' class='background'>
        <tr>
            <td align='center' valign='top' style='border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0;'
                bgcolor='#F0F0F0'>
                <!-- WRAPPER -->
                <!-- Set wrapper width (twice) -->
                <table border='0' cellpadding='0' cellspacing='0' align='center'
                       width='560' style='border-collapse: collapse; border-spacing: 0; padding: 0; width: inherit;
	max-width: 560px;' class='wrapper'>
                    <tr>
                        <td align='center' valign='top' style='border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0; padding-left: 6.25%; padding-right: 6.25%; width: 87.5%;
			padding-top: 20px;
			padding-bottom: 20px;'>
                            <!-- PREHEADER -->
                            <!-- Set text color to background color -->
                            <div style='display: none; visibility: hidden; overflow: hidden; opacity: 0; font-size: 1px; line-height: 1px; height: 0; max-height: 0; max-width: 0;
			color: #F0F0F0;' class='preheader'>
                                Available on&nbsp;GitHub and&nbsp;CodePen. Highly compatible. Designer friendly. More than 50%&nbsp;of&nbsp;total email opens occurred on&nbsp;a&nbsp;mobile device&nbsp;— a&nbsp;mobile-friendly design is&nbsp;a&nbsp;must for&nbsp;email campaigns.
                            </div>
                            <!-- LOGO -->
                            <!-- Image text color should be opposite to background color. Set your url, image src, alt and title. Alt text should fit the image size. Real image size should be x2. URL format: http://domain.com/?utm_source={{Campaign-Source}}&utm_medium=email&utm_content=logo&utm_campaign={{Campaign-Name}} -->
                            
                            
                        </td>
                    </tr>
                    <!-- End of WRAPPER -->
                </table>
                <!-- WRAPPER / CONTEINER -->
                <!-- Set conteiner background color -->
                <table border='0' cellpadding='0' cellspacing='0' align='center'
                       bgcolor='#FFFFFF'
                       width='560' style='border-collapse: collapse; border-spacing: 0; padding: 0; width: inherit;
	max-width: 560px;' class='container'>
                    <!-- HEADER -->
                    <!-- Set text color and font family ('sans-serif' or 'Georgia, serif') -->
                    <tr>
                        <td align='center' valign='top' style='border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0; padding-left: 6.25%; padding-right: 6.25%; width: 87.5%; font-size: 24px; font-weight: bold; line-height: 130%;
			padding-top: 25px;
			color: #000000;
			font-family: sans-serif;' class='header'>
			<img src='http://www2.usenow.com.br/Images/fovorito_pica.png'                                     
                                     alt='Logo' title='Logo' /><br>
                            <h3>SUGESTÃO DE PRODUTOS</h2>
                    <h4>
                        <strong>#cliente#</strong> <br />
                    </h3>
                        </td>
                    </tr>

                    <!-- SUBHEADER -->
                    <!-- Set text color and font family ('sans-serif' or 'Georgia, serif') -->
                    <tr>
                        <td align='center' valign='top' style='border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0; padding-bottom: 3px; padding-left: 6.25%; padding-right: 6.25%; width: 87.5%; font-size: 18px; font-weight: 300; line-height: 150%;
			padding-top: 5px;
			color: #000000;
			font-family: sans-serif;' class='subheader'>
                            
                            Separamos os melhores produtos dentro do seu perfil! <br><br>Sou o Vendedor <b>#vendedor#</b> da loja <b>#loja#</b> selecione o que você mais gosta para eu possa dar uma caprichada especial na bolsa.
                        </td>
                    </tr>
                    <!-- HERO IMAGE -->
                    <!-- Image text color should be opposite to background color. Set your url, image src, alt and title. Alt text should fit the image size. Real image size should be x2 (wrapper x2). Do not set height for flexible images (including 'auto'). URL format: http://domain.com/?utm_source={{Campaign-Source}}&utm_medium=email&utm_content={{Ìmage-Name}}&utm_campaign={{Campaign-Name}} -->
                    <!-- PARAGRAPH -->
                    <!-- Set text color and font family ('sans-serif' or 'Georgia, serif'). Duplicate all text styles in links, including line-height -->
                    <!-- Set line color -->
                    <tr>
                        <td align='center' valign='top' style='border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0; padding-left: 6.25%; padding-right: 6.25%; width: 87.5%;
			padding-top: 25px;' class='line'>
                            <hr color='#E0E0E0' align='center' width='100%' size='1' noshade style='margin: 0; padding: 0;' />
                        </td>
                    </tr>
                    <tr>
                        <td align='center' valign='top' style='border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0; padding-left: 6.25%; padding-right: 6.25%; width: 87.5%; font-size: 17px; font-weight: 400; line-height: 160%;
			padding-top: 25px;
			color: #000000;
			font-family: sans-serif;' class='paragraph'>
                            Clique no link abaixo para começar a selecionar os produtos.
                        </td>
                    </tr>
                    <!-- BUTTON -->
                    <!-- Set button background color at TD, link/text color at A and TD, font family ('sans-serif' or 'Georgia, serif') at TD. For verification codes add 'letter-spacing: 5px;'. Link format: http://domain.com/?utm_source={{Campaign-Source}}&utm_medium=email&utm_content={{Button-Name}}&utm_campaign={{Campaign-Name}} -->
                    <tr>
                        <td align='center' valign='top' style='border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0; padding-left: 6.25%; padding-right: 6.25%; width: 87.5%;
			padding-top: 25px;
			padding-bottom: 5px;' class='button'>
                            
                                <table border='0' cellpadding='0' cellspacing='0' align='center' style='max-width: 240px; min-width: 120px; border-collapse: collapse; border-spacing: 0; padding: 0;'>
                                    <tr>
                                        <td align='center' valign='middle' style='padding: 12px 24px; margin: 0; border-collapse: collapse; border-spacing: 0; border-radius: 4px; -webkit-border-radius: 4px; -moz-border-radius: 4px; -khtml-border-radius: 4px;'
                                            bgcolor='#E9703E'>
                                            <div style='
					color: #FFFFFF; font-family: sans-serif; font-size: 17px; font-weight: 400; line-height: 120%;'
                                               >
                                                <!-- <a hre='http://www2.usenow.com.br/AcessoExterno/IndicacaoProdutos?id=#idSelecao#'>Selecionar</a>-->
												<a href='http://localhost:63249/AcessoExterno/IndicacaoProdutos?id=#idSelecao#' style='color:white'>Selecionar</a>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            
                        </td>
                    </tr>
                    <!-- LINE -->
                    <!-- LINE -->
                    <!-- Set line color -->
                    <tr>
                        <td align='center' valign='top' style='border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0; padding-left: 6.25%; padding-right: 6.25%; width: 87.5%;
			padding-top: 25px;' class='line'>
                            <hr color='#E0E0E0' align='center' width='100%' size='1' noshade style='margin: 0; padding: 0;' />
                        </td>
                    </tr>                 
                </table>
                <!-- WRAPPER -->
                <!-- Set wrapper width (twice) -->
                <table border='0' cellpadding='0' cellspacing='0' align='center'
                       width='560' style='border-collapse: collapse; border-spacing: 0; padding: 0; width: inherit;
	max-width: 560px;' class='wrapper'>

                    <!-- Set text color and font family ('sans-serif' or 'Georgia, serif'). Duplicate all text styles in links, including line-height -->
                    <tr>
                        <td align='center' valign='top' style='border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0; padding-left: 6.25%; padding-right: 6.25%; width: 87.5%; font-size: 13px; font-weight: 400; line-height: 150%;
			padding-top: 20px;
			padding-bottom: 20px;
			color: #999999;
			font-family: sans-serif;' class='footer'>
                            ® Reserva 2018 <a href='https://www.usereserva.com/' target='_blank' style='text-decoration: underline; color: #999999; font-family: sans-serif; font-size: 13px; font-weight: 400; line-height: 150%;'>UseReserva</a>.

                            <!-- ANALYTICS -->
                            <!-- http://www.google-analytics.com/collect?v=1&tid={{UA-Tracking-ID}}&cid={{Client-ID}}&t=event&ec=email&ea=open&cs={{Campaign-Source}}&cm=email&cn={{Campaign-Name}} -->
                            <img width='1' height='1' border='0' vspace='0' hspace='0' style='margin: 0; padding: 0; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; border: none; display: block;'
                                 src='https://raw.githubusercontent.com/konsav/email-templates/master/images/tracker.png' />
                        </td>
                    </tr>
                    <!-- End of WRAPPER -->
                </table>
                <!-- End of SECTION / BACKGROUND -->
            </td>
        </tr>
    </table>
</body>
</html>

         ";

            corpoEmail = corpoEmail.Replace("#vendedor#", vendedor);
            corpoEmail = corpoEmail.Replace("#loja#", loja);
            corpoEmail = corpoEmail.Replace("#idSelecao#", id);
            corpoEmail = corpoEmail.Replace("#cliente#", cliente);

            return corpoEmail;
        }



        //public JsonResult RemoveDepentende(string clienteId, string nomeDependente, string tipoCliente)
        //{
        //    ClienteNovoDependente clienteNovoDependente = new ClienteNovoDependente()
        //    {
        //        nome = nomeDependente
        //    };
        //    var lista = InserirDependenteClienteNovo(Convert.ToInt32(clienteId), clienteNovoDependente, "REMOVER", tipoCliente);

        //    return JsonRetono(new { success = true, dados = lista });
        //}

        //private IList<ClienteNovoDependente> InserirDependenteClienteNovo(int clienteId, ClienteNovoDependente clienteNovoDependente, string tipoInsercao, string tipoCliente)
        //{
        //    IList<ClienteNovoDependente> lista = new List<ClienteNovoDependente>();

        //    if (tipoCliente == "NOVO")
        //    {
        //        if (Session["ClienteNovoDependente"] != null)
        //            lista = Session["ClienteNovoDependente"] as List<ClienteNovoDependente>;

        //        if (tipoInsercao == "INSERIR")
        //        {
        //            lista.Add(clienteNovoDependente);
        //        }
        //        else
        //        {
        //            lista = lista.Where(x => x.nome != clienteNovoDependente.nome).ToList();
        //        }

        //        Session["ClienteNovoDependente"] = lista;
        //    }
        //    else
        //    {
        //        int idDep = 0;
        //        if (Session["idDependente"] != null)
        //        {
        //            idDep = Convert.ToInt32(Session["idDependente"].ToString());
        //            Session["idDependente"] = null;
        //        }

        //        if (Session["ClienteDependente"] != null)
        //            lista = Session["ClienteDependente"] as List<ClienteNovoDependente>;

        //        if (tipoInsercao == "INSERIR")
        //        {
        //            bool update = false;
        //            foreach (var item in lista)
        //            {
        //                if (item.id == idDep)
        //                {
        //                    item.cliente_id = clienteId;
        //                    item.data_inclusao = clienteNovoDependente.data_inclusao;
        //                    item.data_nascimento = clienteNovoDependente.data_nascimento;
        //                    item.grau_parentesco = clienteNovoDependente.grau_parentesco;
        //                    item.data_alteracao = DateTime.Now;
        //                    item.nome = clienteNovoDependente.nome;
        //                    item.observacao = clienteNovoDependente.observacao;
        //                    item.loja_id = clienteNovoDependente.loja_id;
        //                    item.vendedor_id = clienteNovoDependente.vendedor_id;
        //                    item.sexo = clienteNovoDependente.sexo;
        //                    update = true;
        //                }
        //            }

        //            ClienteDependente _dep = new ClienteDependente()
        //            {
        //                cliente_id = clienteId,
        //                data_inclusao = clienteNovoDependente.data_inclusao,
        //                data_nascimento = clienteNovoDependente.data_nascimento,
        //                grau_parentesco = clienteNovoDependente.grau_parentesco,
        //                nome = clienteNovoDependente.nome,
        //                observacao = clienteNovoDependente.observacao,
        //                loja_id = clienteNovoDependente.loja_id,
        //                vendedor_id = clienteNovoDependente.vendedor_id,
        //                sexo = clienteNovoDependente.sexo,
        //                id = idDep
        //            };

        //            _serviceCliente.SalvarDependenteCliente(_dep);

        //            clienteNovoDependente.id = _dep.id;

        //            if (!update)
        //                lista.Add(clienteNovoDependente);
        //        }
        //        else
        //        {
        //            lista = lista.Where(x => x.nome != clienteNovoDependente.nome).ToList();
        //        }

        //        Session["ClienteDependente"] = lista;
        //    }

        //    return lista.OrderByDescending(x => x.data_inclusao).ToList();
        //}

        //public JsonResult ObterDependente(string idDependente)
        //{
        //    try
        //    {
        //        Session["idDependente"] = idDependente;

        //        var dependente = _serviceCliente.ObterDependente(idDependente);

        //        return JsonRetono(new { Sucesso = true, dependente = dependente });
        //    }
        //    catch (Exception ex)
        //    {
        //        return JsonRetono(new { Sucesso = false, Mensagem = ex.Message });
        //    }
        //}

        //public JsonResult RemoverDependente(string idDependente)
        //{
        //    try
        //    {
        //        _serviceCliente.RemoverDependente(Convert.ToInt32(idDependente));

        //        IList<ClienteNovoDependente> lista = new List<ClienteNovoDependente>();
        //        if (Session["ClienteDependente"] != null)
        //        {
        //            lista = Session["ClienteDependente"] as List<ClienteNovoDependente>;

        //            Session["ClienteDependente"] = lista.Where(x => x.id != Convert.ToInt32(idDependente)).ToList();
        //        }

        //        return JsonRetono(new { Sucesso = true, Mensagem = "Operação Realizada com Sucesso!" });
        //    }
        //    catch (Exception ex)
        //    {
        //        return JsonRetono(new { Sucesso = false, Mensagem = ex.Message });
        //    }
        //}

        //public JsonResult ListaDependentesDetalhe(string lojaId, string vendedorId, string dataInicio, string dataFim)
        //{
        //    try
        //    {
        //        var lista = _serviceRelatorio.ListarRelatorios<ClienteDependenteDetalhe>("SP_CadastroDependentesDetalhe", lojaId, dataInicio, dataFim, null);

        //        if (!String.IsNullOrEmpty(vendedorId))
        //            lista = lista.Where(x => x.vendedor_id == Convert.ToInt32(vendedorId)).ToList();

        //        return JsonRetono(new { success = true, lista = lista });
        //    }
        //    catch (Exception ex)
        //    {
        //        MsgErro();
        //        return JsonRetono(new { success = false });
        //    }


        //}

        //public JsonResult ObterDependentesPorClientes(string clienteId)
        //{
        //    return JsonRetono(true);
        //    //try
        //    //{
        //    //    Session["idDependente"] = idDependente;

        //    //    var dependente = _serviceCliente.ObterDependente(idDependente);

        //    //    return JsonRetono(new { success = true, dados = lista });
        //    //}
        //    //catch (Exception ex)
        //    //{
        //    //    return JsonRetono(new { Sucesso = false, msg = ex.Message });
        //    //}
        //}

        #endregion "Métodos Dependentes"

        #region Registro de Contato em LOTE"

        public ActionResult ContatoLote()
        {
            @ViewBag.ativarNovaTelaVendedor = Session["ativarNovaTelaVendedor"] == null ? false : Session["ativarNovaTelaVendedor"];
            return View();
        }

        public JsonResult SalvarContatoLote(string cpf, string id_agenda, string status, string modo, string termometro, string vendedor_id, string caracteristicas, string obs, string tipoPagina)
        {
            @ViewBag.ativarNovaTelaVendedor = Session["ativarNovaTelaVendedor"] == null ? false : Session["ativarNovaTelaVendedor"];
            //return JsonRetono(true);
            try
            {
                if (!String.IsNullOrEmpty(cpf))
                {

                    var cliente = _serviceCliente.BuscarClienteCpfOrigemLoja(OrigemLoja, cpf).FirstOrDefault();

                    if (cliente != null)
                    {
                        var agenda = UsuarioLogado.TiposAgendas.Where(x => x.id.ToString() == id_agenda).FirstOrDefault();

                        DateTime TempoInicial = DateTime.Now;
                        Contato contato = new Contato();
                        contato.loja_id = Convert.ToInt32(UsuarioLogado.IdLojaSeleconado);
                        contato.agendamento = agenda.parametro;
                        contato.status = status;
                        contato.modo = modo;
                        contato.termometro = termometro;
                        contato.vendedor_id = Convert.ToInt32(vendedor_id);
                        contato.caracteristicas = caracteristicas;
                        contato.obs = obs;
                        contato.cliente_id = cliente.id;

                        // Para garantir  o contato direto
                        if (string.IsNullOrEmpty(id_agenda))
                        {
                            contato.agendamento = "clientes_contato_vendedor";
                            contato.tipo_agendamento_id = 1;
                        }
                        else
                        {
                            contato.agendamento = agenda.parametro;
                            contato.tipo_agendamento_id = Convert.ToInt32(agenda.id);
                        }

                        _serviceContato.AdicionarContato(contato);

                        MensagemRetorno("Contato salvo com sucesso!", TypeMensage.success);

                        DateTime TempoFinal = DateTime.Now;
                        // Log do sistema
                        LogSistema(new LogSistema { processo_minutos = TempoProcesso(TempoInicial, TempoFinal), acao = "Salvando contato do Cliente em Lote: " + "---Dados Inserido: " + cliente.id + " # Status: " + contato.status + " # Vendedor: " + contato.vendedor_id + " # CheckReservado: " + contato.flagReservado + " # Data Reservado: " + contato.dt_Reservado + " # Modo: " + contato.modo + " # Termometro: " + contato.termometro + " # Caracteristicas: " + contato.caracteristicas + " # Obs: " + contato.obs + " # Agenda: " + agenda.parametro, login = UsuarioLogado.Usuario.email, tipo_log = "CADASTRANDO", vendedor_id = (String.IsNullOrEmpty(UsuarioLogado.IdVendedorSeleconado) ? 0 : Convert.ToInt32(UsuarioLogado.IdVendedorSeleconado)), loja_id = (String.IsNullOrEmpty(UsuarioLogado.IdLojaSeleconado) ? 0 : Convert.ToInt32(UsuarioLogado.IdLojaSeleconado)), metodo = "Cliente - Contato", tela = "Contato", cliente_id = cliente.id, user_id = UsuarioLogado.Usuario.id, ip = HttpContext.Request.UserHostAddress.ToString(), data = DateTime.Now });


                        // Se o Parametro estiver vazio retorna para a busca de clientes
                        if (string.IsNullOrEmpty(tipoPagina))
                        {
                            return JsonRetono(new { sucesso = true, _cpf = cpf, responseText = "Cliente" });
                        }
                        else
                        {
                            return JsonRetono(new { sucesso = true, _cpf = cpf, responseText = "Agenda" });
                        }
                    }
                    else
                        return JsonRetono(new { sucesso = false, _cpf = cpf, responseText = "Cliente não localizado para o cpf informado!" });

                }
                else
                {
                    return JsonRetono(new { sucesso = true, _cpf = "", msg = "O Cliente não foi informado", totalFavoritos = UsuarioLogado.ListaReportClientesFavoritos.Count().ToString() });
                }
            }
            catch (Exception ex)
            {
                string dados = " --- Dados Inserido: " + cpf + " # Status: " + status + " # Vendedor: " + vendedor_id + " # Modo: " + modo + " # Termometro: " + termometro + " # Caracteristicas: " + caracteristicas + " # Obs: " + obs + " # Agenda: " + id_agenda;

                _serviceUsuario.AdicionarErro("SalvarContatoLote", "Perfil do Cliente", ex.Message + dados, (String.IsNullOrEmpty(UsuarioLogado.IdVendedorSeleconado) ? 0 : Convert.ToInt32(UsuarioLogado.IdVendedorSeleconado)), null, (String.IsNullOrEmpty(UsuarioLogado.IdLojaSeleconado) ? 0 : Convert.ToInt32(UsuarioLogado.IdLojaSeleconado)), UsuarioLogado.Usuario.id);

                MensagemRetorno("", TypeMensage.error);

                return JsonRetono(new { sucesso = false, responseText = "", msg = "Ocorreu um erro contate o adiministrador do sistema!" });
            }
        }

        #endregion
    }
}
