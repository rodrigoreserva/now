﻿using Now.Domain;
using Now.Models;
using Now.Domain.Contracts.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Now.Controllers
{
    public class CatalogoProdutosController : BaseController
    {
        public ICatalogoProdutosService _serviceCatalogoProdutos;
        public CatalogoProdutosController(ICatalogoProdutosService serviceCatalogoProdutos)
        {
            _serviceCatalogoProdutos = serviceCatalogoProdutos;
        }

        public ActionResult Index()
        {
            //IList<Loja> lojas
            //if (!String.IsNullOrEmpty(UsuarioLogado.IdLojaSeleconado))
            //    UsuarioLogado.LojasUsuario.Where(x => x.id == Convert.ToInt32(UsuarioLogado.IdLojaSeleconado)).FirstOrDefault();

            return View();
        }

        public JsonResult BuscarProdutos(string strPesqProduto, int pageIndex)
        {
            try
            {
                int PageCount = 0;
                var _list = _serviceCatalogoProdutos.ListarCatalogoProdutos(strPesqProduto, pageIndex, ref PageCount);

                return JsonRetono(new { list = _list, pageCount = PageCount });

            }
            catch (Exception ex)
            {
                return JsonRetono(new { success = false });
            }
        }


        public JsonResult GetGriffe()
        {
            List<SelectListItem> items = new List<SelectListItem>();

            items.Add(new SelectListItem { Selected = true, Text = "Selecione...", Value = "" });

            var list = _serviceCatalogoProdutos.ListaGriffe();

            items.AddRange(list.Select(item => new SelectListItem
            {
                Text = item.ToString(),
                Value = item.ToString()
            }));

            return JsonRetono(new SelectList(items, "Value", "Text"));

        }

        public JsonResult GetGrupoProdutos(string griffe)
        {
            List<SelectListItem> items = new List<SelectListItem>();

            items.Add(new SelectListItem { Selected = true, Text = "Selecione...", Value = "" });

            var list = _serviceCatalogoProdutos.ListaGrupoProdutos(griffe);

            items.AddRange(list.Select(item => new SelectListItem
            {
                Text = item.ToString(),
                Value = item.ToString()
            }));

            return JsonRetono(new SelectList(items, "Value", "Text"));

        }

    }
}
