﻿using Now.Domain;
using Now.Models;
using Now.Domain.Contracts.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Now.Controllers
{
    public class NotificacaoController : BaseController
    {
        private INotificacaoService _notificacao;

        public NotificacaoController(INotificacaoService notificacao)
        {
            _notificacao = notificacao;
        }

        public ActionResult Index()
        {
            return View(CarregarTodasNotificacoes(""));
        }

        public IList<Notificacao> CarregarTodasNotificacoes(string titulo, int visto = 1)
        {
            IList<Notificacao> list = new List<Notificacao>();

            if (!String.IsNullOrEmpty(UsuarioLogado.IdVendedorSeleconado))
            {
                list = _notificacao.ObterTodas(UsuarioLogado.IdLojaSeleconado, UsuarioLogado.IdVendedorSeleconado);
                list = list.Where(x => (visto == 1 || (visto == 0 && x.data_visto == null))).ToList();

                if (!String.IsNullOrEmpty(titulo))
                    list = list.Where(x => x.titulo == titulo).ToList();
            }

            return list;
        }

        public IList<Notificacao> CarregarTodasNotificacoesPop()
        {
            IList<Notificacao> list = new List<Notificacao>();

            if (!String.IsNullOrEmpty(UsuarioLogado.IdVendedorSeleconado))
            {
                list = _notificacao.ObterTodas(UsuarioLogado.IdLojaSeleconado, UsuarioLogado.IdVendedorSeleconado);
            }

            return list;
        }

        public IList<Notificacao> CarregarNotificacao()
        {
            IList<Notificacao> list = new List<Notificacao>();

            if (!String.IsNullOrEmpty(UsuarioLogado.IdVendedorSeleconado))
            {
                list = _notificacao.Obter(UsuarioLogado.IdLojaSeleconado, UsuarioLogado.IdVendedorSeleconado);
            }

            return list;
        }

        public JsonResult BuscarNotificacoes()
        {
            return JsonRetono(new { dados = CarregarNotificacao() });
        }

        public JsonResult BuscarTodasNotificacoes(string titulo, int visto)
        {
            return JsonRetono(new { dados = CarregarTodasNotificacoes(titulo, visto) });
        }

        public JsonResult AtualizarVistoNotificacao(string Id)
        {
            try
            {
                _notificacao.AtualizarVistoNotificacao(Id);
                return JsonRetono(new { Sucesso = true });
            }
            catch (Exception)
            {

                return JsonRetono(new { Sucesso = false });
            }
        }
    }
}
