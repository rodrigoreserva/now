﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Now.Controllers
{
    public class ChkSessionController : Controller
    {
        //
        // GET: /chkSession/

        public ActionResult Index()
        {
            return View();
        }

        public JsonResult VerificaSessao()
        {
            bool valida = (Session["UsuarioLogado"] != null);

            return JsonRetono(new { valido = valida });            
        }

        public JsonResult JsonRetono(object data)
        {
            JsonResult jsonResult = Json(data, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = int.MaxValue;
            return jsonResult;
        }
    }
}
