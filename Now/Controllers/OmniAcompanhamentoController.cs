﻿using Now.Domain;
using Now.Models;
using Now.Domain.Contracts.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Now.Controllers
{
    public class OmniAcompanhamentoController : BaseController
    {
        public IOmniAcompanhamentoService _serviceOmniAcompanhamento;        
        public OmniAcompanhamentoController(IOmniAcompanhamentoService serviceOmniAcompanhamento)
        {
            _serviceOmniAcompanhamento = serviceOmniAcompanhamento;            
        }

        public ActionResult Index()
        {
            return View();
        }

        public JsonResult VitoOmni(string pedido)
        {
            _serviceOmniAcompanhamento.AtualizarStatusOmni(pedido);

            var list = UsuarioLogado.ListaOmniAcompanhamento;

            foreach (var item in list)
            {
                if (item.pedido == pedido)
                {
                    item.visto_flag = 1;
                    item.visto_data = DateTime.Now;
                }
            }
            UsuarioLogado.ListaOmniAcompanhamento = list;

            return JsonRetono(true);
        }

        public JsonResult VerificaOmni()
        {
            bool valida = (UsuarioLogado.ListaOmniAcompanhamento.Where(x => x.visto_flag == 0).Count() > 0);

            return JsonRetono(new { valido = valida });
        }

    }
}
