﻿using Now.Domain;
using Now.Models;
using Now.Domain.Contracts.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Now.Controllers.DTO;

namespace Now.Controllers
{
    public class DashboardController : BaseController
    {
        public DateTime data = DateTime.Now.AddDays(-1);

        private IDashboardService _service;
        private IRelatorioService _serviceRelatorio;
        private IMetricasService _serviceMetricas;
        private IUsuarioService _serviceUsuario;
        private IAgendaService _serviceAgenda;
        private IVendedorService _serviceVendedor;
        private IOmniAcompanhamentoService _serviceOmniAcompanhamento;
        public DashboardController(IDashboardService service, IRelatorioService serviceRelatorio, IMetricasService serviceMetricas, IUsuarioService serviceUsuario, IOmniAcompanhamentoService serviceOmniAcompanhamento, IVendedorService serviceVendedor, IAgendaService serviceAgenda)
        {
            _service = service;

            _serviceAgenda = serviceAgenda;
            _serviceVendedor = serviceVendedor;

            _serviceRelatorio = serviceRelatorio;
            _serviceMetricas = serviceMetricas;
            _serviceUsuario = serviceUsuario;
            _serviceOmniAcompanhamento = serviceOmniAcompanhamento;
        }

        //
        // GET: /Dashboard/

        public ActionResult Index()
        {
            var vendedorDashBoardDTO = new VendedorDashBoardDTO();
            // Carregar Omni
            //@ViewBag.OmniAcompanhamento = UsuarioLogado.ListaOmniAcompanhamento;
            //if (UsuarioLogado.ListaOmniAcompanhamento == null || _trocou)
            //{
            //    @ViewBag.OmniAcompanhamento = UsuarioLogado.ListaOmniAcompanhamento = ListaAcompOmni(UsuarioLogado.IdLojaSeleconado, UsuarioLogado.IdVendedorSeleconado);
            //}

            montarAgenda(vendedorDashBoardDTO);

            ModificaSessao(UsuarioLogado);


            DateTime TempoInicial = DateTime.Now;
            #region RankingVendedores

            var rankVendedores = UsuarioLogado.ListaRankingVendedores;

            if (UsuarioLogado.ListaRankingVendedores == null || _trocou)
            {
                //Vendedor da oficina não pode ver as informações da loja como um todo
                if (UsuarioLogado.Usuario.Perfil.ToString() == "Vendedor" && UsuarioLogado.IdLojaSeleconado == "390" && !String.IsNullOrEmpty(UsuarioLogado.IdVendedorSeleconado))
                    rankVendedores = ListarRankingVendedores().Where(c => c.vendedor_id == Convert.ToInt32(UsuarioLogado.IdVendedorSeleconado)).ToList();
                else
                    rankVendedores = ListarRankingVendedores();

                UsuarioLogado.ListaRankingVendedores = rankVendedores;
                ModificaSessao(UsuarioLogado);
            }

            var dashboardMes = UsuarioLogado.DashboardMes;

            if (!String.IsNullOrEmpty(UsuarioLogado.IdVendedorSeleconado))
            {
                // Guarda a listagem dos clientes Favoritos do Vendedor                
                if (UsuarioLogado.ListaReportClientesFavoritos == null || _trocou || _trocouVendedor)
                {
                    @ViewBag.ListaReportClientesFavoritos = UsuarioLogado.ListaReportClientesFavoritos = _serviceRelatorio.ListarRelatorioComVendedor<ReportClientesFavoritos>("SP_RelatorioClientesFavoritos", UsuarioLogado.IdVendedorSeleconado);
                }

                ViewBag.Efetividade = rankVendedores.Where(x => x.vendedor_id == Convert.ToInt32(UsuarioLogado.IdVendedorSeleconado)).Sum(x => x.valor_contato_now);
            }
            else
                ViewBag.Efetividade = rankVendedores.Sum(x => x.valor_contato_now);

            if (UsuarioLogado.DashboardMes == null || _trocou || _trocouVendedor)
            {
                //Vendedor da oficina não pode ver as informações da loja como um todo
                if (UsuarioLogado.Usuario.Perfil.ToString() == "Vendedor" && UsuarioLogado.IdLojaSeleconado == "390" && !String.IsNullOrEmpty(UsuarioLogado.IdVendedorSeleconado))
                    dashboardMes = ListarMetricas("M").Where(c => c.vendedor_id == Convert.ToInt32(UsuarioLogado.IdVendedorSeleconado)).FirstOrDefault();
                else
                    dashboardMes = ListarMetricas("M").FirstOrDefault();

                ViewBag.Metricas = dashboardMes == null ? new Dashboard() : dashboardMes;
            }

            ViewBag.MetricasDia = ListarMetricas("D").FirstOrDefault();
            //Vendedor da oficina não pode ver as informações da loja como um todo
            if (UsuarioLogado.Usuario.Perfil.ToString() == "Vendedor" && UsuarioLogado.IdLojaSeleconado == "390" && !String.IsNullOrEmpty(UsuarioLogado.IdVendedorSeleconado))
                ViewBag.MetricasDia = ListarMetricas("D").Where(c => c.vendedor_id == Convert.ToInt32(UsuarioLogado.IdVendedorSeleconado)).FirstOrDefault();

            ViewBag.MetricasDia = ViewBag.MetricasDia == null ? new Dashboard() : ViewBag.MetricasDia;

            #endregion RankingVendedores

            #region EfetividadeAgenda

            var efetividadeAgenda = UsuarioLogado.ListaEfetividadeAgenda;

            if (UsuarioLogado.ListaEfetividadeAgenda == null || _trocou || _trocouVendedor)
            {
                efetividadeAgenda = CalculoPercentualEfetividade(ListarEfetividadeAgenda());

                UsuarioLogado.ListaEfetividadeAgenda = efetividadeAgenda;
                ModificaSessao(UsuarioLogado);
            }

            ViewBag.EfetividadeAgenda = efetividadeAgenda;

            #endregion EfetividadeAgenda

            #region GraficoEfetividade

            var graficoEfetividade = UsuarioLogado.ListaDashboardGraficoEfetividade;

            if (UsuarioLogado.ListaDashboardGraficoEfetividade == null || _trocou || _trocouVendedor)
            {
                graficoEfetividade = ListarGraficoEfetividade();

                UsuarioLogado.ListaDashboardGraficoEfetividade = graficoEfetividade;
                ModificaSessao(UsuarioLogado);
            }

            ViewBag.UltiDiaGrafico = graficoEfetividade.LastOrDefault();

            #endregion GraficoEfetividade

            #region EfetividadeLojas

            var efetividadeLojas = UsuarioLogado.ListaEfetividadeLojas;

            if (UsuarioLogado.ListaEfetividadeLojas == null || _trocou)
            {
                efetividadeLojas = ListarEfetividadeLojas();

                UsuarioLogado.ListaEfetividadeLojas = efetividadeLojas;
                ModificaSessao(UsuarioLogado);
            }

            ViewBag.EfetividadeLojas = AjustaEfetividade(efetividadeLojas.ToList()).OrderByDescending(x => x.efetividade_sssg).ToList();

            #endregion EfetividadeLojas

            #region EfetividadeVendedores

            var efetividadeVendedores = UsuarioLogado.ListaEfetividadeVendedores;

            if (UsuarioLogado.ListaEfetividadeVendedores == null || _trocou)
            {
                efetividadeVendedores = ListarEfetividadeVendedores();

                UsuarioLogado.ListaEfetividadeVendedores = efetividadeVendedores;
                ModificaSessao(UsuarioLogado);
            }

            ViewBag.EfetividadeVendedores = efetividadeVendedores;

            #endregion EfetividadeVendedores

            // Buscar o PA e TM - Guardar na Sessão
            if (UsuarioLogado.Pa == null || UsuarioLogado.Tm == null)
            {
                var _TmPa = ListarMetricas().FirstOrDefault();

                UsuarioLogado.Pa = _TmPa.Pa;
                UsuarioLogado.Tm = _TmPa.Tm;
                ModificaSessao(UsuarioLogado);
            }

            _trocou = false;
            _trocouVendedor = false;

            DateTime TempoFinal = DateTime.Now;

            // Log do sistema
            LogSistema(new LogSistema { processo_minutos = TempoProcesso(TempoInicial, TempoFinal), acao = "Carregando Tela Dashboard", login = UsuarioLogado.Usuario.email, tipo_log = "NAVEGACAO", vendedor_id = (String.IsNullOrEmpty(UsuarioLogado.IdVendedorSeleconado) ? 0 : Convert.ToInt32(UsuarioLogado.IdVendedorSeleconado)), loja_id = (String.IsNullOrEmpty(UsuarioLogado.IdLojaSeleconado) ? 0 : Convert.ToInt32(UsuarioLogado.IdLojaSeleconado)), metodo = "Dashboard - Dashboard", tela = "Dashboard", user_id = UsuarioLogado.Usuario.id, ip = HttpContext.Request.UserHostAddress.ToString(), data = DateTime.Now });

            //redirecionar para dashboard do vendedor
            //window.location.href = '@Url.Action("Index", "Vendedor/Dashboard")?id=' + localStorage.getItem("vendedor");

            //@ViewBag.ativarNovaTelaVendedor = Session["ativarNovaTelaVendedor"] == null ? false : Session["ativarNovaTelaVendedor"];
            if (UsuarioLogado.AcessoNovoDashBoard)
            {
                ViewBag.ativarNovaTelaVendedor = true;
                if (VendedorDashboard == null)
                    VendedorDashboard = ObterVendedor(UsuarioLogado.IdVendedorSeleconado);

                return View("~/Views/Vendedor/Dashboard.cshtml", VendedorDashboard);
            }
            else
                return View("Index", rankVendedores);
        }

        public void montarAgenda(VendedorDashBoardDTO vendedorDashBoardDTO)
        {
            var verificarLembrete = false;
            var loja = UsuarioLogado.LojasUsuario.Where(x => x.tipo_loja == "EVA");
            verificarLembrete = loja.Count() > 0;

            var agenda = CarregarAgendas(verificarLembrete);

            foreach (var item in agenda)
            {
                vendedorDashBoardDTO.QtdLigacaoesDisponiveis += item.qtdeDisponivel;
                vendedorDashBoardDTO.QtdLigacaoesFeitas += item.qtdeLigacoes;
            }
            vendedorDashBoardDTO.agendamentos = agenda;
        }

        public VendedorDashBoardDTO ObterVendedor(string id)
        {
            var vendedorDashBoardDTO = new VendedorDashBoardDTO();

            var _vendedor = _serviceVendedor.ListarVendedores("", id);
            var usuario = _serviceUsuario.BuscarUsuarioPorCPF(_vendedor.FirstOrDefault().cpf.TrimEnd());

            var usuariologado = new UsuarioLogado();
            usuariologado.Usuario = usuario;

            IList<MenuAgendamento> agenda = new List<MenuAgendamento>();
            if (usuario.Lojas != null)
            {
                usuariologado.IdLojaSeleconado = usuario.Lojas.FirstOrDefault().id.ToString();
                //agenda = _serviceAgenda.TypeCalendar(usuario, usuario.id.ToString(), usuariologado.IdsLojasUsuario, id, "");
                //foreach (var item in agenda)
                //{
                //    vendedorDashBoardDTO.QtdLigacaoesDisponiveis += item.qtdeDisponivel;
                //    vendedorDashBoardDTO.QtdLigacaoesFeitas += item.qtdeLigacoes;
                //}
            }

            Session["ativarNovaTelaVendedor"] = true;
            @ViewBag.ativarNovaTelaVendedor = Session["ativarNovaTelaVendedor"];

            vendedorDashBoardDTO.nome = _vendedor.FirstOrDefault().nome;
            vendedorDashBoardDTO.cpf = _vendedor.FirstOrDefault().cpf;
            @ViewBag.img_perfil = _vendedor.FirstOrDefault().img_perfil;
            vendedorDashBoardDTO.img_perfil_intranet = _vendedor.FirstOrDefault().img_perfil_intranet;
            @ViewBag.img_perfil_intranet = _vendedor.FirstOrDefault().img_perfil_intranet;

            montarAgenda(vendedorDashBoardDTO);
            //vendedorDashBoardDTO.agendamentos = agenda;

            return vendedorDashBoardDTO;
        }

        public ActionResult Edicao()
        {
            return View();
        }

        private List<ReportEfetividade> AjustaEfetividade(List<ReportEfetividade> efetividadeLojas)
        {
            // Nova Regra: Alterar o formato do Relatório Efetividade Lojas, para que apareça apenas as 5 primeiras e mais a loja que estiver acessando, caso não esteja entre as 5 melhores.  (Visão da loja)

            List<ReportEfetividade> list = new List<ReportEfetividade>();

            if (!String.IsNullOrEmpty(UsuarioLogado.IdLojaSeleconado))
            {
                int i = 0;
                bool existLoja = false;
                foreach (var item in efetividadeLojas)
                {
                    if (item.loja_id.ToString() == UsuarioLogado.IdLojaSeleconado)
                        existLoja = true;

                    list.Add(item);
                    i++;
                    if (i > 5)
                        break;
                }

                if (!existLoja)
                    if (efetividadeLojas.Where(x => x.loja_id == Convert.ToInt32(UsuarioLogado.IdLojaSeleconado)).ToList().FirstOrDefault() != null)
                        list.Add(efetividadeLojas.Where(x => x.loja_id == Convert.ToInt32(UsuarioLogado.IdLojaSeleconado)).ToList().FirstOrDefault());
            }
            else
                list = efetividadeLojas;

            return list;
        }

        public IList<OmniAcompanhamento> ListaAcompOmni(string IdLojaSeleconado, string vendedorId)
        {
            return _serviceOmniAcompanhamento.Listar(IdLojaSeleconado, vendedorId);
        }

        private IList<Metricas> ListarMetricas()
        {
            //return _serviceMetricas.ListarMetricas("SP_DashboardMetricas", UsuarioLogado.IdsLojasUsuario, UsuarioLogado.IdVendedorSeleconado, data.ToString("yyyy-MM-") + "01", data.ToString("yyyy-MM-dd"));
            return _serviceMetricas.ListarMetricas(UsuarioLogado.IdsLojasUsuarioCanais, UsuarioLogado.IdVendedorSeleconado, data.ToString("yyyy-MM-") + "01", data.ToString("yyyy-MM-dd"));
        }

        private IList<DashboardRankingVendedores> ListarRankingVendedores()
        {
            // Não consulta pela procedure, por causa da performance
            return _service.ListarDashboardRankingVendedores("SP_DashboardRankingVendedores_3", UsuarioLogado.IdsLojasUsuarioCanais, data.ToString("yyyy-MM-") + "01", data.ToString("yyyy-MM-dd"));
        }

        private IList<DashboardGraficoEfetividade> ListarGraficoEfetividade()
        {
            // Não consulta pela procedure, por causa da performance
            return _service.ListarDashboardGraficoEfetividade("SP_DashboardGraficoEfetividade", UsuarioLogado.IdsLojasUsuarioCanais, UsuarioLogado.IdVendedorSeleconado, UsuarioLogado.IdsTodasLojasUsuario, data.ToString("yyyy-MM-") + "01", data.ToString("yyyy-MM-dd"));
        }

        private IList<ReportEfetividade> ListarEfetividadeLojas()
        {
            return _serviceRelatorio.ListarEfetividadeLojas("SP_RelatorioEfetividadeLojas", UsuarioLogado.IdsTodasLojasUsuario, data.ToString("yyyy-MM-") + "01", data.ToString("yyyy-MM-dd"));
            //return _serviceRelatorio.ListarEfetividadeLojas(UsuarioLogado.IdsTodasLojasUsuario, data.ToString("yyyy-MM-") + "01", data.ToString("yyyy-MM-dd"));
        }

        private IList<ReportEfetividadeAgenda> ListarEfetividadeAgenda()
        {
            // return _serviceRelatorio.ListarReportEfetividadeAgendaDashboard("SP_RelatorioEfetividadeAgenda_Dashboard", UsuarioLogado.IdVendedorSeleconado, UsuarioLogado.IdsLojasUsuario, data.ToString("yyyy-MM-") + "01", data.ToString("yyyy-MM-dd"));
            return _serviceRelatorio.ListarReportEfetividadeAgendaDashboard(UsuarioLogado.IdVendedorSeleconado, UsuarioLogado.IdsLojasUsuarioCanais, data.ToString("yyyy-MM-") + "01", data.ToString("yyyy-MM-dd"));
        }

        private IList<DashboardEfetividadesVendedores> ListarEfetividadeVendedores()
        {
            //return _serviceRelatorio.ListarDashboardEfetividadesVendedores("SP_DashboardEfetividadesVendedores", UsuarioLogado.IdsLojasUsuario, data.ToString("yyyy-MM-") + "01", data.ToString("yyyy-MM-dd"));
            return _serviceRelatorio.ListarDashboardEfetividadesVendedores(UsuarioLogado.IdsLojasUsuarioCanais, data.ToString("yyyy-MM-") + "01", data.ToString("yyyy-MM-dd"));
        }

        private IList<Dashboard> ListarMetricas(string tipo)
        {
            if (tipo == "D")
                return _service.ListarMetricas(UsuarioLogado.IdsLojasUsuarioCanais, UsuarioLogado.IdVendedorSeleconado, DateTime.Now.ToString("yyyy-MM-dd"), DateTime.Now.ToString("yyyy-MM-dd"));
            else
                return _service.ListarMetricas(UsuarioLogado.IdsLojasUsuarioCanais, UsuarioLogado.IdVendedorSeleconado, data.ToString("yyyy-MM-") + "01", data.ToString("yyyy-MM-dd"));
        }

        public IList<MenuAgendamento> CarregarAgendas(bool agendaLembreteAtiva)
        {
            try
            {
                return _serviceAgenda.TypeCalendar(UsuarioLogado.Usuario, UsuarioLogado.Usuario.id.ToString(), UsuarioLogado.IdsLojasUsuario, UsuarioLogado.IdVendedorSeleconado, "", agendaLembreteAtiva);
            }
            catch (Exception ex)
            {
                MsgErro();
            }

            return new List<MenuAgendamento>();
        }

        public JsonResult GetDados()
        {
            List<Object> resultado = new List<object>();

            if (String.IsNullOrEmpty(UsuarioLogado.IdVendedorSeleconado))
            {
                foreach (var item in UsuarioLogado.ListaDashboardGraficoEfetividade)
                {
                    resultado.Add(new
                    {
                        x = item.dia,
                        item1 = string.Format("{0:0.##}", item.media_perc_efet_empresa * 100).Replace(",", "."),
                        item2 = string.Format("{0:0.##}", item.media_perc_efet_loja * 100).Replace(",", ".")
                    });
                }
            }
            else
            {
                foreach (var item in UsuarioLogado.ListaDashboardGraficoEfetividade)
                {
                    resultado.Add(new
                    {
                        x = item.dia,
                        item1 = string.Format("{0:0.##}", item.media_perc_efet_empresa * 100).Replace(",", "."),
                        item2 = string.Format("{0:0.##}", item.media_perc_efet_loja * 100).Replace(",", "."),
                        item3 = string.Format("{0:0.##}", item.media_perc_efet_vendedor * 100).Replace(",", ".")
                    });
                }
            }

            return JsonRetono(resultado);
        }

        public JsonResult GetEfetLojaDados()
        {
            //if (ViewBag.EfetividadeAgenda == null)
            //    CalculoPercentualEfetividade(ListarEfetividadeAgenda());

            //var efetividade = (List<ReportEfetividadeAgenda>)ViewBag.EfetividadeAgenda;
            var i = 1;


            var resultado = UsuarioLogado.ListaEfetividadeAgenda.Select(item => new
            {
                value = item.valor_venda_now,
                color = Cores()[i],
                highlight = Cores()[i],
                label = item.agendamento,
                a = i++
            });

            return JsonRetono(resultado);
        }

        public JsonResult CarregarEfetLojas(bool check)
        {
            IList<ReportEfetividade> list = new List<ReportEfetividade>();
            if (check)
                list = UsuarioLogado.ListaEfetividadeLojas;
            else
                list = AjustaEfetividade(UsuarioLogado.ListaEfetividadeLojas.ToList());

            var totEfetividade = String.Format("{0:C}", list.Sum(x => x.efetividade));
            var totNow = String.Format("{0:C}", list.Sum(x => x.valor_now));
            var totContatoDireto = String.Format("{0:C}", list.Sum(x => x.contato_direto));
            var totPercEfetividade = String.Format("{0:P}", list.Count() > 0 ? (list.Sum(x => x.efetividade) / list.Sum(x => x.valor_linx)) : 0);
            var totEfetividadeSssg = String.Format("{0:P}", (list.Sum(x => x.efetividade_sssg) == 0 ? 0 : (list.Sum(x => x.efetividade) / list.Sum(x => x.efetividade_sssg)) - Convert.ToDecimal(1)));

            return JsonRetono(new { dados = list, totEfetividade = totEfetividade, totEfetividadeSssg = totEfetividadeSssg, totNow = totNow, totContatoDireto = totContatoDireto, totPercEfetividade = totPercEfetividade });
        }


        protected IList<ReportEfetividadeAgenda> CalculoPercentualEfetividade(IList<ReportEfetividadeAgenda> efetividade)
        {
            IList<ReportEfetividadeAgenda> listaPerce = new List<ReportEfetividadeAgenda>();

            decimal valorTotal = efetividade.Sum(x => x.valor_venda_now);

            var i = 1;
            foreach (ReportEfetividadeAgenda item in efetividade)
            {
                if (item.valor_venda_now != 0)
                    item.percentual = item.valor_venda_now / valorTotal;
                else
                    item.percentual = 0;

                item.cor = Cores()[i];

                item.ranking = i;

                listaPerce.Add(item);
                i++;
            }

            return listaPerce.OrderByDescending(x => x.percentual).ToList();
        }

    }
}
