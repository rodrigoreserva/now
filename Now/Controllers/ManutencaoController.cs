﻿using Now.Domain;
using Now.Models;
using Now.Domain.Contracts.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Now.Controllers
{
    public class ManutencaoController : BaseController
    {
        public Int32 _lojaId
        {
            get { return Session["_lojaId"] == null ? 0 : Convert.ToInt32(Session["_lojaId"].ToString()); }
            set { Session["_lojaId"] = value; }
        }


        public IUsuarioService _serviceUsuario;
        public ILojaService _serviceLoja;
        public IVendedorService _serviceVendedor;
        public IVisitaLojasService _serviceVisitaLojas;
        public IRelatorioService _serviceRelatorio;
        public IManualService _serviceManual;
        public ManutencaoController(IUsuarioService serviceUsuario, ILojaService serviceLoja, IVendedorService serviceVendedor, IVisitaLojasService serviceVisitaLojas, IRelatorioService serviceRelatorio, IManualService serviceManual)
        {
            _serviceUsuario = serviceUsuario;
            _serviceLoja = serviceLoja;
            _serviceVendedor = serviceVendedor;
            _serviceVisitaLojas = serviceVisitaLojas;
            _serviceRelatorio = serviceRelatorio;
            _serviceManual = serviceManual;
        }

        public ActionResult Index()
        {
            return RedirectToAction("ResetarSenha");
        }

        public ActionResult ResetarSenha()
        {
            string idsLojas = UsuarioLogado.IdsLojasUsuario;

            ViewBag.ListaUsuario = _serviceUsuario.ListarUsuarios(idsLojas);

            return View();
        }

        public ActionResult DetalheUsuario(Usuario usuario)
        {
            if (Request.QueryString["id"] != null)
            {
                if (!String.IsNullOrEmpty(Request.QueryString["id"].ToString()))
                {
                    usuario = _serviceUsuario.BuscarUsuario(Request.QueryString["id"].ToString(), "");

                    List<SelectListItem> items = new List<SelectListItem>();
                    var lisPerfis = _serviceUsuario.ListarPerfis();

                    List<SelectListItem> itemsControle = new List<SelectListItem>();
                    var listTipo = _serviceVendedor.ListarVendedoresControleTipo();

                    if (lisPerfis.Count() > 1)
                        itemsControle.Add(new SelectListItem() { Text = "Selecione...", Value = "", Selected = true });

                    if (listTipo.Count() > 1)
                        items.Add(new SelectListItem() { Text = "Selecione...", Value = "", Selected = true });

                    items.AddRange(lisPerfis.OrderBy(o => o.nome).Select(item => new SelectListItem
                    {
                        Text = item.nome,
                        Value = item.id_perfil.ToString()
                    }));

                    itemsControle.AddRange(listTipo.OrderBy(o => o.nome).Select(item => new SelectListItem
                    {
                        Text = item.nome,
                        Value = item.id.ToString()
                    }));

                    ViewBag.ListaPerfis = new SelectList(items, "Value", "Text", usuario.id_perfil);
                    ViewBag.ListaControle = new SelectList(itemsControle, "Value", "Text", "");

                    if (usuario.loja_id != null && usuario.loja_id != 0)
                        ViewBag.VendedorLojaSelect = _serviceVendedor.GetVendedor(usuario.loja_id.ToString());
                    else
                        ViewBag.VendedorLojaSelect = new List<Vendedor>();

                    @ViewBag.LojasTodas = _todasLojas;
                    @ViewBag.LojasUsuario = usuario.Lojas;

                    Session["usuarioLogin"] = usuario;

                    return View(usuario);
                }
                else
                    return RedirectToAction("ResetarSenha");
            }
            else
                return RedirectToAction("ResetarSenha");
        }

        public ActionResult Lojas()
        {
            var list = _serviceLoja.ListarTodasLojas();
            return View(list);
        }

        #region "Hierarquia de permissões"
        // Hierarquia de permissões
        /*
             * O supervisor terá autonomia para desabilitar o login da sua regional (gerentes/vendedores), e gerente terá autonomia para desabilitar o login dos seus vendedores.
                O objetivo é que não seja necessário aguardar a desativação do funcionário por parte do Dp.


                Criação de tela: Manutenção/ Desativação de usuário

                Pra quem?:
                Adm
                Supervisores
                Gerentes

                Quem faz o que?:
                Adm: desabilita qualquer funcionário da empresa
                Supervisores: desabilitam qualquer funcionário da sua regional
                Gerentes: desabilitam qualquer vendedor da sua loja
         * 
        */

        #endregion "Hierarquia de permissões"

        public ActionResult LojasDetalhe(string LojaId)
        {
            _lojaId = Convert.ToInt32(LojaId);

            var loja = _serviceLoja.ListarLojas(LojaId).FirstOrDefault();

            if (loja == null)
                return RedirectToAction("Lojas");
            else
                return View(loja);
        }

        public ActionResult SalvarLoja(Loja loja)
        {
            var _loja = _serviceLoja.ListarLojas(_lojaId.ToString()).FirstOrDefault();

            loja.id_loja = _loja.id_loja;
            loja.id = _loja.id;

            // Temporario - FALTA criar os campo na tela
            loja.supervisor = _loja.supervisor;
            loja.pais = _loja.pais;
            loja.regiao = _loja.regiao;
            loja.uf = _loja.uf;
            loja.cidade = _loja.cidade;
            loja.visivel = _loja.visivel;


            _serviceLoja.UpdateLoja(loja);

            MensagemRetorno("Loja salvo com sucesso!", TypeMensage.success);

            return RedirectToAction("Lojas", "Manutencao", new { Lojaid = loja.id });
        }

        public ActionResult LojasDistribuicao()
        {
            @ViewBag.SupervBrasil = RetornarSelectList(_serviceUsuario.ListarUsuariosPorPerfil("2"));
            @ViewBag.SupervRegional = RetornarSelectList(_serviceUsuario.ListarUsuariosPorPerfil("3"));
            @ViewBag.Gerente = RetornarSelectList(_serviceUsuario.ListarUsuariosPorPerfil("5"));

            @ViewBag.SupervisaoOrg = _serviceLoja.ListarSupervisaoDistribuicao();

            var lista = _serviceLoja.ListarLojasDistribuicao();

            // Validar as informações para geração dos alertas
            foreach (var item in lista)
            {

                //lista.Where(x => x.idGerente == item.idGerente).GroupBy(x => new { x.id, x.idGerente }).ToList();


                if (item.QtdeGerente == 0)
                    item.status += "Nenhum Gerente Associado";

                if (item.QtdeGerente > 1)
                    item.status += "Mais de 1 Gerente Associado";

                if (item.QtdeRegional == 0)
                    item.status += "Nenhum Supervisor Regional Associado";

                if (item.QtdeRegional > 1)
                    item.status += "Mais de 1 Supervisor Regional Associado";

                if (item.QtdeBrasil == 0)
                    item.status += "Nenhum Supervisor Brasil Associado";

                if (item.QtdeBrasil > 1)
                    item.status += "Mais de 1 Supervisor Brasil Associado";
            }

            return View(lista);
        }

        private IList<SelectListItem> RetornarSelectList(IList<Usuario> list)
        {
            List<SelectListItem> items = new List<SelectListItem>();

            if (list.Count() > 1)
                items.Add(new SelectListItem() { Text = "Selecione...", Value = "", Selected = true });

            items.AddRange(list.OrderBy(o => o.username).Select(item => new SelectListItem
            {
                Text = item.username,
                Value = item.id.ToString()
            }));

            return items;
        }

        public ActionResult DistribuicaoLojaOrigemDestino()
        {
            return View();
        }

        public ActionResult Email()
        {
            return View();
        }

        public ActionResult EmailDetalhe()
        {
            return View();
        }

        public ActionResult AlterarGerente()
        {
            return View();
        }

        public ActionResult Agenda()
        {
            return View();
        }

        public ActionResult Manual()
        {
            ViewBag.Manual = _serviceManual.ListarManual();

            return View();
        }

        public ActionResult VisitaLojas()
        {
            var list = _serviceVisitaLojas.ListarVisitaLojas(String.Empty);

            ViewBag.ListaVisitaLojas = list;

            //List<SelectListItem> items = new List<SelectListItem>();

            //if (list.Count() > 1)
            //    items.Add(new SelectListItem() { Text = "Selecione...", Value = "", Selected = true });

            //items.AddRange(list.OrderBy(o => o.data_visita).Select(item => new SelectListItem
            //{
            //    Text = item.data_visita.ToString("dd/MM/yyyy"),
            //    Value = item.data_visita.ToString("yyyy-MM-dd")
            //}));

            //ViewBag.Visitas = new SelectList(items, "Value", "Text");

            return View();
        }

        public ActionResult IncluirVisitaLojas(VisitaLojas visitaslojas)
        {
            try
            {
                _serviceVisitaLojas.SalvarVisitaLojas(visitaslojas);

                MsgSucesso();
            }
            catch (Exception ex)
            {
                //var serviceErro = new SecurityService();
                //serviceErro.AddErro("btnIncluir_Click", "Modulos.InfoVendas", ex.Message, (String.IsNullOrEmpty(IdVendedor) ? 0 : Convert.ToInt32(IdVendedor)), null, (String.IsNullOrEmpty(IdLoja) ? 0 : Convert.ToInt32(IdLoja)), UsuarioLogado.usuario.id);

                //MsgBox(Page, ex.Message, "Ocorreu um erro!", PageBase.TypeMensage.error);
                MsgErro();
            }

            return RedirectToAction("VisitaLojas");
        }

        public ActionResult ExcluirVisitaLojas(string codigo)
        {
            try
            {
                int id = Convert.ToInt32(codigo);

                _serviceVisitaLojas.DeleteVisitaLojas(id);

                MsgSucesso();
            }
            catch (Exception ex)
            {
                //var serviceErro = new SecurityService();
                //serviceErro.AddErro("btnIncluir_Click", "Modulos.InfoVendas", ex.Message, (String.IsNullOrEmpty(IdVendedor) ? 0 : Convert.ToInt32(IdVendedor)), null, (String.IsNullOrEmpty(IdLoja) ? 0 : Convert.ToInt32(IdLoja)), UsuarioLogado.usuario.id);

                //MsgBox(Page, ex.Message, "Ocorreu um erro!", PageBase.TypeMensage.error);
                MsgErro();
            }

            return RedirectToAction("VisitaLojas");
        }

        //public ActionResult ListarRelatorioVisitas(string data_inicial, string data_final)
        public ActionResult ListarRelatorioVisitas(RelatorioPesquisa dadosPesquisa)
        {
            var lista = _serviceRelatorio.ListarVisitasLojas("SP_RelatorioVisitasLojas", "", dadosPesquisa.data_inicial, dadosPesquisa.data_final);

            return PartialView("../Relatorio/_RelatorioVisitasLojas", lista);
        }

        public ActionResult ListarEfetividadePorMesVisitasLojas(string loja_id, string ano)
        {

            int _ano = Convert.ToInt32(ano);

            var lista = _serviceRelatorio.ListarEfetividadePorMes("SP_RelatorioEfetividadePorMes", "", _ano);
            //  var listaMedia = _serviceRelatorio.ListarEfetividadePorMes("SP_RelatorioEfetividadePorMesMedia", "", _ano);

            Session["GetDadosEfetividadePorMes"] = lista;

            //ViewBag.EfetividadePorMesMedia = listaMedia; ///PartialView("../Relatorio/_RelatorioEfetividadePorMesMedia", listaMedia);
            //ViewBag.EfetividadePorMes = lista;//PartialView("../Relatorio/_RelatorioEfetividadePorMes", lista);

            return PartialView("../Relatorio/_RelatorioEfetividadePorMes", lista);
        }

        public ActionResult ListarEfetividadePorMesMediaVisitasLojas(string loja_id, string ano)
        {

            int _ano = Convert.ToInt32(ano);

            var lista = _serviceRelatorio.ListarEfetividadePorMes("SP_RelatorioEfetividadePorMesMedia", "", _ano);

            return PartialView("../Relatorio/_RelatorioEfetividadePorMesMedia", lista);
        }

        public JsonResult GetDadosEfetividadePorMes()
        {
            List<object> resultado = new List<object>();

            if (Session["GetDadosEfetividadePorMes"] != null)
            {
                var dados = (IList<RelatorioEfetividadePorMes>)Session["GetDadosEfetividadePorMes"];

                resultado.Add(new
                {
                    janeiro = string.Format("{0:0.##}", dados.Sum(x => x.janeiro)),
                    fevereiro = string.Format("{0:0.##}", dados.Sum(x => x.fevereiro)),
                    marco = string.Format("{0:0.##}", dados.Sum(x => x.marco)),
                    abril = string.Format("{0:0.##}", dados.Sum(x => x.abril)),
                    maio = string.Format("{0:0.##}", dados.Sum(x => x.maio)),
                    junho = string.Format("{0:0.##}", dados.Sum(x => x.junho)),
                    julho = string.Format("{0:0.##}", dados.Sum(x => x.julho)),
                    agosto = string.Format("{0:0.##}", dados.Sum(x => x.agosto)),
                    setembro = string.Format("{0:0.##}", dados.Sum(x => x.setembro)),
                    outubro = string.Format("{0:0.##}", dados.Sum(x => x.outubro)),
                    novembro = string.Format("{0:0.##}", dados.Sum(x => x.novembro)),
                    dezembro = string.Format("{0:0.##}", dados.Sum(x => x.dezembro))
                });
            }
            return JsonRetono(resultado);
        }

        public JsonResult AlterarPerfilSupervisao(string idUsuario, string idLoja, string idPerfil)
        {
            _serviceLoja.UpdatePerfilSupervisao(idUsuario, idLoja, idPerfil);

            return JsonRetono(true);
        }

        public JsonResult SalvarManual(string titulo, string editor)
        {
            try
            {
                string edit = editor.Replace("%#", "<").Replace("#%", ">");

                _serviceManual.Salvar(new Manual()
                {
                    conteudo = edit,
                    data_inclusao = DateTime.Now,
                    ordem = 1,
                    titulo = titulo
                });

                return JsonRetono(new { Sucesso = true, msg = this.MsgSucesso() });
            }
            catch (Exception)
            {
                return JsonRetono(new { Sucesso = false, msg = this.MsgErro() });
            }
        }

        public JsonResult SalvarArquivos(object arquivios)
        {
            return JsonRetono(true);
        }
    }
}