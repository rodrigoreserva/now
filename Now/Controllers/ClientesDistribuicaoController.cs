﻿using Now.Domain;
using Now.Models;
using Now.Domain.Contracts.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Now.Controllers
{
    public class ClientesDistribuicaoController : BaseController
    {
        public IClientesDistribuicaoService _serviceClientesDistribuicao;
        public ClientesDistribuicaoController(IClientesDistribuicaoService serviceClientesDistribuicao)
        {
            _serviceClientesDistribuicao = serviceClientesDistribuicao;
        }

        public ActionResult Index()
        {
            var list = CarregarClientesDistribuicao("P", "V");
            ViewBag.ListaClientesDistribuicaoAprov = CarregarClientesDistribuicao("A", "V");
            ViewBag.ListaClientesDistribuicaoTopInativos = CarregarClientesDistribuicao("A", "G");

            return View(list);
        }

        public JsonResult TrocarVendedorPeloGerente(string vendedorId, string clienteId)
        {
            _serviceClientesDistribuicao.AtualizarTrocaVendedor(UsuarioLogado.IdLojaSeleconado, vendedorId, clienteId);

            return JsonRetono(true);
        }

        private IList<ClientesDistribuicao> CarregarClientesDistribuicao(string tipoAprovacao, string tipoDistribuicao)
        {
            return _serviceClientesDistribuicao.ListarClientesDistribuicao(Convert.ToInt32(UsuarioLogado.IdLojaSeleconado), tipoAprovacao, tipoDistribuicao);
        }

    }
}
