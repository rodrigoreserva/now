﻿using Now.Domain;
using Now.Models;
using Now.Domain.Contracts.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace Now.Controllers
{
    public class PerguntasQuemEleController : BaseController
    {
        public IPerguntasQuemEleService _servicePerguntasQuemEle;
        public IPerguntasQuemEleRespostasService _servicePerguntasQuemEleRespostas;
        public PerguntasQuemEleController(IPerguntasQuemEleService servicePerguntasQuemEle, IPerguntasQuemEleRespostasService servicePerguntasQuemEleRespostas)
        {
            _servicePerguntasQuemEle = servicePerguntasQuemEle;
            _servicePerguntasQuemEleRespostas = servicePerguntasQuemEleRespostas;
        }

        public ActionResult Index()
        {
            return View();
        }
        public JsonResult SalvarPergunta(string tipoPergunta, string ordem, string pergunta, string idPergunta)
        {
            try
            {
                PerguntasQuemEle perguntasQuemEle = new PerguntasQuemEle();

                if (!String.IsNullOrEmpty(idPergunta))
                {
                    perguntasQuemEle = _servicePerguntasQuemEle.ListarPerguntasQuemEle().Where(x => x.id == Convert.ToInt32(idPergunta)).FirstOrDefault();
                }

                perguntasQuemEle.data_inclusao = DateTime.Now;
                perguntasQuemEle.data_alteracao = null;
                perguntasQuemEle.data_desativacao = null;
                perguntasQuemEle.ordem = Convert.ToInt32(ordem);
                perguntasQuemEle.pergunta = pergunta;
                perguntasQuemEle.tipo_pergunta = tipoPergunta;

                var lista = _servicePerguntasQuemEle.SalvarPerguntasQuemEle(perguntasQuemEle);

                return JsonRetono(new { success = true, dados = lista, mensagem = "Pergunta salva com sucesso!" });

            }
            catch (Exception ex)
            {
                return JsonRetono(new { success = false, mensagem = "Erro ao Salvar a Pergunta! " + ex.Message });
            }
        }

        public JsonResult ExcluirPergunta(string id)
        {
            try
            {
                if (!String.IsNullOrEmpty(id))
                {
                    var lista = _servicePerguntasQuemEle.DeletePerguntasQuemEle(Convert.ToInt32(id));

                    return JsonRetono(new { success = true, dados = lista, mensagem = "Pergunta excluída com sucesso!" });
                }
                else
                    throw new Exception("A pergunta não foi selecionada.");
            }
            catch (Exception ex)
            {
                return JsonRetono(new
                {
                    success = false,
                    mensagem = "Erro ao excluir a Pergunta! " + ex.Message
                });
            }
        }

        public JsonResult SalvarResposta(string idPergunta, string idPerguntasPredecessora, string resposta, string idResposta)
        {
            try
            {
                int id = String.IsNullOrEmpty(idResposta) ? 0 : Convert.ToInt32(idResposta);
                int? id_subsequente = null;
                if (idPerguntasPredecessora != "0")
                    id_subsequente = Convert.ToInt32(idPerguntasPredecessora);

                PerguntasQuemEleRespostas perguntasQuemEleRespostas = new PerguntasQuemEleRespostas()
                {
                    data_inclusao = DateTime.Now,
                    resposta = resposta,
                    id_perguntas_quem_ele_subsequente = id_subsequente,
                    id_perguntas_quem_ele = Convert.ToInt32(idPergunta),
                    id = id

                };

                var lista = _servicePerguntasQuemEleRespostas.SalvarPerguntasQuemEleRespostas(perguntasQuemEleRespostas);

                return JsonRetono(new { success = true, dados = lista, mensagem = "Resposta salva com sucesso!" });

            }
            catch (Exception)
            {
                return JsonRetono(new { success = false, mensagem = "Erro ao Salvar a Resposta!" });
            }
        }

        public JsonResult ExcluirResposta(string id, string idPergunta)
        {
            try
            {
                if (!String.IsNullOrEmpty(id))
                {
                    var lista = _servicePerguntasQuemEleRespostas.DeletePerguntasQuemEleRespostas(Convert.ToInt32(id), Convert.ToInt32(idPergunta));

                    return JsonRetono(new { success = true, dados = lista, mensagem = "Resposta excluída com sucesso!" });
                }
                else
                {
                    return JsonRetono(new
                    {
                        sucesso = false,
                        msg = "Erro ao excluir a Resposta!"
                    });
                }
            }
            catch (Exception ex)
            {
                return JsonRetono(new
                {
                    sucesso = false,
                    msg = "Erro ao excluir a Resposta! " + ex.Message
                });
            }
        }
        public JsonResult ListaPerguntas()
        {
            try
            {
                Session["ClientePerguntasQuemEleRespostas"] = new List<ClientePerguntasQuemEleRespostas>();
                var lista = _servicePerguntasQuemEle.ListarPerguntasQuemEle();

                return JsonRetono(new { sucesso = true, dados = lista });
            }
            catch (Exception ex)
            {
                return JsonRetono(new
                {
                    sucesso = false,
                    msg = "Erro ao listar a pergunta! " + ex.Message
                });
            }
        }

        public JsonResult ListaRespostas(string idPergunta)
        {
            try
            {
                var lista = _servicePerguntasQuemEleRespostas.ListarPerguntasQuemEleRespostas(Convert.ToInt32(idPergunta));

                return JsonRetono(new { sucesso = true, dados = lista });
            }
            catch (Exception ex)
            {
                return JsonRetono(new
                {
                    sucesso = false,
                    msg = "Erro ao listar a resposta! " + ex.Message
                });
            }
        }

        public JsonResult ListaWorkFlowRespostas()
        {
            try
            {
                var obj = "";
                //IList<Object> obj = new List<Object>();

                var listaPerguntas = _servicePerguntasQuemEle.ListarPerguntasQuemEle();
                var listaRespostas = _servicePerguntasQuemEleRespostas.ListarTodasPerguntasQuemEleRespostas();

                var primeira_pergunta = listaPerguntas.Where(x => x.ordem == 1).ToList().FirstOrDefault();

                obj = "{ \"name\": \"Pergunta\", \"title\": \"" + primeira_pergunta.pergunta + "<br> Tipo:" + primeira_pergunta.tipo_pergunta + "\"";

                //obj.Add(new { name = primeira_pergunta.pergunta, title = primeira_pergunta.tipo_pergunta });

                //var te = "<ul id='ul-data'><li>" + primeira_pergunta.pergunta + " #FILHOS#</li></ul>";

                obj += CarregarWorkFlow(listaPerguntas, listaRespostas, primeira_pergunta.id);
                obj += "}";

                return JsonRetono(obj);
            }
            catch (Exception ex)
            {
                return JsonRetono(new { sucesso = false, msg = "Erro ao carregar o Work Flow de Respostas! " + ex.Message });
            }
        }

        public string CarregarWorkFlow(IList<PerguntasQuemEle> listaPerguntas, IList<PerguntasQuemEleRespostas> listaRespostas, int idPergunta)
        {
            var filhos = ",\"children\": [";
            string nmFilhos = "";
            string respSub = "";

            foreach (var item in listaRespostas.Where(x => x.id_perguntas_quem_ele == idPergunta).ToList().GroupBy(x => x.id_perguntas_quem_ele_subsequente).Select(y => new PerguntasQuemEleRespostas { id_perguntas_quem_ele = y.First().id_perguntas_quem_ele, id_perguntas_quem_ele_subsequente = y.First().id_perguntas_quem_ele_subsequente }).ToList())
            {
                nmFilhos = "";

                foreach (var listFilhos in listaRespostas.Where(x => x.id_perguntas_quem_ele == idPergunta && x.id_perguntas_quem_ele_subsequente == item.id_perguntas_quem_ele_subsequente))
                {
                    nmFilhos += listFilhos.resposta + "<br>";
                }

                if (listaPerguntas.Where(y => y.id == item.id_perguntas_quem_ele_subsequente).ToList().Count > 0)
                {
                    var perg = listaPerguntas.Where(y => y.id == item.id_perguntas_quem_ele_subsequente).FirstOrDefault();

                    respSub = ",\"children\": [{ \"name\": \"Pergunta\", \"title\": \"" + perg.pergunta + "<br> Tipo:" + perg.tipo_pergunta + "\"";

                    respSub += CarregarWorkFlow(listaPerguntas, listaRespostas, perg.id);

                    respSub += "}]";
                }
                filhos += "{ \"name\": \"Resposta\", \"title\": \"" + nmFilhos + "\"" + respSub + " },";
            }

            filhos = filhos.TrimEnd(',');

            filhos += "]";
            return filhos;
        }


        public JsonResult PerguntaExtra(string pergunta, string resposta)
        {
            try
            {
                // Sessão para guardar as perguntas e respostas do cliente
                IList<ClientePerguntasQuemEleRespostas> listPerguntasRespondidas = new List<ClientePerguntasQuemEleRespostas>();
                if (Session["ClientePerguntasQuemEleRespostas"] == null)
                    Session["ClientePerguntasQuemEleRespostas"] = new List<ClientePerguntasQuemEleRespostas>();
                else
                {
                    listPerguntasRespondidas = Session["ClientePerguntasQuemEleRespostas"] as List<ClientePerguntasQuemEleRespostas>;
                }

                listPerguntasRespondidas.Add(new ClientePerguntasQuemEleRespostas
                {
                    id_perguntas_quem_ele = null,
                    id_perguntas_quem_ele_resposta = null,
                    data_inclusao = DateTime.Now,
                    resposta = resposta,
                    pergunta = pergunta
                });

                return JsonRetono(new { sucesso = true });
            }
            catch (Exception ex)
            {
                return JsonRetono(new { sucesso = false, msg = "Erro ao carregar pergunta extra! " + ex.Message });
            }
        }

        public JsonResult ListaWorkFlowPerguntasAnterior(string IdPerguntaAnterior)
        {
            try
            {
                string _controle = "";
                string _botoes = "";

                // Sessão para guardar as perguntas e respostas do cliente
                IList<ClientePerguntasQuemEleRespostas> listPerguntasRespondidas = new List<ClientePerguntasQuemEleRespostas>();
                if (Session["ClientePerguntasQuemEleRespostas"] == null)
                    Session["ClientePerguntasQuemEleRespostas"] = new List<ClientePerguntasQuemEleRespostas>();
                else
                {
                    listPerguntasRespondidas = Session["ClientePerguntasQuemEleRespostas"] as List<ClientePerguntasQuemEleRespostas>;

                    var perguntaAnt = listPerguntasRespondidas.Where(a => a.id_perguntas_quem_ele == Convert.ToInt32(IdPerguntaAnterior)).FirstOrDefault();

                    // Retiro a Pergunta anterior respondida
                    Session["ClientePerguntasQuemEleRespostas"] = listPerguntasRespondidas.Where(a => a.id_perguntas_quem_ele != Convert.ToInt32(IdPerguntaAnterior)).ToList();

                    WorkFlowPerguntas(perguntaAnt.id_perguntas_quem_ele.ToString(), "", "", "ANTERIOR", ref _controle, ref _botoes);
                }
                return JsonRetono(new { controle = _controle, footer = _botoes });

            }
            catch (Exception ex)
            {
                return JsonRetono(new { success = false, msg = "Erro ao carregar o Work Flow de Perguntas Anterior! " + ex.Message });
            }
        }

        public JsonResult ListaWorkFlowPerguntas(string idPergunta, string idResposta, string textoResposta, string pergAnterior)
        {
            try
            {
                string _controle = "";
                string _botoes = "";

                WorkFlowPerguntas(idPergunta, idResposta, textoResposta, pergAnterior, ref _controle, ref _botoes);
                return JsonRetono(new { sucesso = true, controle = _controle, footer = _botoes });
            }
            catch (Exception ex)
            {
                return JsonRetono(new { sucesso = false, msg = "Erro ao carregar o Work Flow de Perguntas! " + ex.Message });
            }
        }

        public void WorkFlowPerguntas(string idPergunta, string idResposta, string textoResposta, string pergAnterior, ref string _controle, ref string _botoes)
        {

            var listaPerguntas = _servicePerguntasQuemEle.ListarPerguntasQuemEle();
            var listaRespostas = _servicePerguntasQuemEleRespostas.ListarTodasPerguntasQuemEleRespostas();

            // Sessão para guardar as perguntas e respostas do cliente
            IList<ClientePerguntasQuemEleRespostas> listPerguntasRespondidas = new List<ClientePerguntasQuemEleRespostas>();
            if (Session["ClientePerguntasQuemEleRespostas"] == null)
                Session["ClientePerguntasQuemEleRespostas"] = new List<ClientePerguntasQuemEleRespostas>();
            else
            {
                listPerguntasRespondidas = Session["ClientePerguntasQuemEleRespostas"] as List<ClientePerguntasQuemEleRespostas>;
            }

            PerguntasQuemEle pergunta = new PerguntasQuemEle();

            if (!String.IsNullOrEmpty(idPergunta))
            {
                pergunta = listaPerguntas.Where(x => x.id == Convert.ToInt32(idPergunta)).ToList().FirstOrDefault();

                if (string.IsNullOrEmpty(pergAnterior))
                    Session["ClientePerguntasQuemEleRespostas"] = new List<ClientePerguntasQuemEleRespostas>();
            }
            string _tipo_pergunta = "";
            string _id_pergunta_anterior = "";

            if (!String.IsNullOrEmpty(idResposta))
            {
                var respRespondida = listaRespostas.Where(r => r.id == Convert.ToInt32(idResposta)).FirstOrDefault();
                var pergRespondida = listaPerguntas.Where(x => x.id == respRespondida.id_perguntas_quem_ele).ToList().FirstOrDefault();

                _tipo_pergunta = pergRespondida.tipo_pergunta;
                _id_pergunta_anterior = pergRespondida.id.ToString();

                // Guarda na Sessão a Pergunta/Resposta -- Respondida
                listPerguntasRespondidas.Add(new ClientePerguntasQuemEleRespostas
                {
                    id_perguntas_quem_ele = respRespondida.id_perguntas_quem_ele,
                    id_perguntas_quem_ele_resposta = respRespondida.id,
                    data_inclusao = DateTime.Now,
                    resposta = textoResposta,
                    pergunta = pergRespondida.pergunta
                });

                if (respRespondida.id_perguntas_quem_ele_subsequente != null)
                    pergunta = listaPerguntas.Where(x => x.id == respRespondida.id_perguntas_quem_ele_subsequente).ToList().FirstOrDefault();
            }
            else
            {
                //Quando volto para pergunta anterior perco a referencia das perguntas, com isso percorro a lista para pegar a ultima pergunta
                foreach (var item in listPerguntasRespondidas)
                {
                    var pergRespondida = listaPerguntas.Where(x => x.id == item.id_perguntas_quem_ele).ToList().FirstOrDefault();

                    _tipo_pergunta = pergRespondida.tipo_pergunta;
                    _id_pergunta_anterior = pergRespondida.id.ToString();
                }
            }
            string resumoPerguntasRespostas = "<dl>";
            string stepsPerguntasRespostas = "<ol class='cd-multi-steps text-bottom count'>";
            foreach (var item in listPerguntasRespondidas)
            {
                //var perg = listaPerguntas.Where(x => x.id == item.id_perguntas_quem_ele).ToList().FirstOrDefault();
                resumoPerguntasRespostas += "<dt>" + item.pergunta + "</dt>";
                resumoPerguntasRespostas += "<dd>R: " + item.resposta + "</dd>";

                stepsPerguntasRespostas += "<li class='visited' ><a href='#0' onmouseover='ExibirPerguntaRespondida(\"" + item.pergunta + "\",\"" + item.resposta + "\")' onmouseout='FecharPerguntaRespondida()'></a></li>";
            }

            resumoPerguntasRespostas += "</dl>";

            string controle = "<div class='callout callout-success text-center'><h3>Sequência de Perguntas Encerradas!</h3><p>Resumo das perguntas e respostas: </p>" + resumoPerguntasRespostas + "</div>";

            string botoes = "";

            botoes += "<button class='btn2-gradient btn-warning btn pull-left' type='button' onclick='CarregarPerguntaAnterior(\"" + _id_pergunta_anterior + "\")'><i class='fa fa-arrow-left'></i> <b>VOLTAR</b></button>";

            botoes += "<button class='btn2-gradient btn-danger btn pull-left' type='button' onclick='ReiniciarPerguntas()'><i class='fa fa-retweet'></i> <b>REINICIAR PERGUNTAS</b></button>";

            if (pergunta.id != 0)
            {
                controle = MontarControleResposta(pergunta.id.ToString(), pergunta.pergunta, pergunta.tipo_pergunta, listaRespostas.Where(x => x.id_perguntas_quem_ele == pergunta.id).ToList());
                botoes += "<button class='btn2-gradient btn-primary btn' type='submit' onclick='CarregarProximaPergunta(\"" + pergunta.tipo_pergunta + "\", \"ctrl_" + pergunta.id.ToString() + "\")'> <b>PRÓXIMA</b> <i class='fa fa-arrow-right'></i></button>";
                stepsPerguntasRespostas += "<li><em></em></li>";
            }
            else
                botoes += "<button class='btn2-gradient btn-success btn' type='submit' onclick='CarregarProximaPergunta(\"" + pergunta.tipo_pergunta + "\",\"ctrl_" + pergunta.id.ToString() + "\")'> <i class='fa fa-check'></i> <b>FINALIZAR/SALVAR</b> </button>";

            stepsPerguntasRespostas += "</ol>";

            _controle = (stepsPerguntasRespostas + controle);
            _botoes = botoes;

        }

        private string MontarControleResposta(string idPergunta, string nome_pergunta, string tipo_pergunta, List<PerguntasQuemEleRespostas> list)
        {
            string _controle = "";

            switch (tipo_pergunta)
            {
                case "RESPOSTA":
                    {

                        _controle += "<div class='col-sm-12'><div class='form-group  form-group-lg'>";
                        _controle += "<label><h3>" + nome_pergunta + "</h3></label>";
                        _controle += "<textarea class='form-control' rows='5' placeholder='Informe a resposta...' id='text_ctrl_" + idPergunta + "'></textarea>";
                        _controle += "<input id='ctrl_" + idPergunta + "' type='hidden' value='" + list.FirstOrDefault().id + "' />";
                        _controle += "</div></div>";
                    }
                    break;
                case "ESCOLHA":
                    {
                        _controle += "<div class='col-sm-12'><div class='form-group form-group-lg'>";
                        _controle += "<label><h3>" + nome_pergunta + "</h3></label>";
                        _controle += "<select class='form-control' id='ctrl_" + idPergunta + "'>";
                        foreach (var item in list)
                        {
                            _controle += "<option value=" + item.id + ">" + item.resposta + "</option>";
                        }
                        _controle += "</select>";
                        _controle += "</div></div>";
                    }
                    break;
            }

            _controle += "<div class='col-sm-12 text-center'><hr><button class='btn2-gradient btn-info btn' type='button' onclick='AbrirPerguntaExtra()'><i class='fa fa-plus'></i> <b>PERGUNTA EXTRA</b></button></div>";
            return _controle;
        }
    }
}
