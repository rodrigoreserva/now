﻿using Now.Domain;
using Now.Models;
using Now.Domain.Contracts.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Uber.SDK;
using System.Web.Script.Serialization;
using System.Net.Mail;


namespace Now.Controllers
{
    public class AcessoExternoController : Controller
    {
        private IClienteService _serviceCliente;
        private IContatoService _serviceContato;
        private IAcessoExternoService _serviceAcessoExterno;
        private ILojaService _serviceLoja;

        public AcessoExternoController(IClienteService serviceCliente, IContatoService serviceContato, IAcessoExternoService serviceAcessoExterno, ILojaService serviceLoja)
        {
            _serviceCliente = serviceCliente;
            _serviceContato = serviceContato;
            _serviceAcessoExterno = serviceAcessoExterno;
            _serviceLoja = serviceLoja;
        }


        public ActionResult Index()
        {
            return View();
        }

        public ActionResult IndicacaoProdutos(string id)
        {
            ViewBag.idIndicacao = id;

            var indicacao = _serviceCliente.BuscarIndicacaoProdutosPorIndicacao(id);

            if (Session["ProdutosIndicados"] != null)
            {
                var listaProd = ((IList<IndicacaoProdutoItens>)Session["ProdutosIndicados"]);

                //Selececiona os produtos ja selecionados
                foreach (var item in indicacao.lista_produtos)
                {
                    var _item = listaProd.Where(x => x.produto_id == item.produto_id).FirstOrDefault();
                    if (_item != null)
                    {
                        item.data_cliente = _item.data_cliente;
                        item.observacao = _item.observacao;
                        item.cliente_escolheu = _item.cliente_escolheu;
                    }
                }
            }

            return View(indicacao);
        }

        public JsonResult Addproduto(string idIndicacao, string idProduto, string observacao)
        {
            IList<IndicacaoProdutoItens> listaProd = new List<IndicacaoProdutoItens>();

            if (Session["ProdutosIndicados"] != null)
            {
                listaProd = ((IList<IndicacaoProdutoItens>)Session["ProdutosIndicados"]);
            }

            IndicacaoProdutoItens indItens = new IndicacaoProdutoItens();

            indItens.cliente_indicacao_id = Convert.ToInt32(idIndicacao);
            indItens.data_cliente = DateTime.Now;
            indItens.cliente_escolheu = 1;
            indItens.observacao = observacao;
            indItens.produto_id = Convert.ToInt32(idProduto);

            if (listaProd.Where(x => x.produto_id == Convert.ToInt32(idProduto)).ToList().Count() == 0)
            {
                listaProd.Add(indItens);
            }

            Session["ProdutosIndicados"] = listaProd;

            return JsonRetono(new { success = true, dados = listaProd });
        }

        public JsonResult Delproduto(string idProduto)
        {
            IList<IndicacaoProdutoItens> listaProd = new List<IndicacaoProdutoItens>();

            if (Session["ProdutosIndicados"] != null)
            {
                listaProd = ((IList<IndicacaoProdutoItens>)Session["ProdutosIndicados"]);
            }

            listaProd = listaProd.Where(x => x.produto_id != Convert.ToInt32(idProduto)).ToList();

            Session["ProdutosIndicados"] = listaProd;

            return JsonRetono(new { success = true, dados = listaProd });
        }

        public JsonResult EnviaEmailIndicacao(string idIndicacao)
        {
            IList<IndicacaoProdutoItens> listaProd = new List<IndicacaoProdutoItens>();

            if (Session["ProdutosIndicados"] != null)
            {
                listaProd = ((IList<IndicacaoProdutoItens>)Session["ProdutosIndicados"]);
            }

            _serviceCliente.SalvarEnvioIndicacaoProdutos(Convert.ToInt32(idIndicacao), listaProd);

            // Salvar Produtos
            //_serviceCliente.

            //listaProd = listaProd.Where(x => x.produto_id != Convert.ToInt32(idProduto)).ToList();

            //Session["ProdutosIndicados"] = listaProd;

            return JsonRetono(new { success = true });
        }

        public JsonResult JsonRetono(object data)
        {
            JsonResult jsonResult = Json(data, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = int.MaxValue;
            return jsonResult;
        }

        public ActionResult AvaliacaoContato()
        {
            return View();
        }

        public ActionResult Cliente()
        {
            return View();
        }

        public JsonResult BuscarContato(string idContato)
        {
            var _contato = _serviceContato.BuscarContato(idContato);

            return JsonRetono(new { success = true, contato = _contato });
        }

        public JsonResult BuscarContatoCliente(string telefone)
        {
            var _contato = _serviceContato.BuscarContatoCliente(telefone);

            if (_contato == null)
                _contato = _serviceContato.BuscarCliente(telefone);

            bool encontrou = _contato == null ? false : true;

            return JsonRetono(new { success = encontrou, contato = _contato });
        }

        public JsonResult EnviarAvaliacao(string idContato, string vlrAvaliacao, string observacao, int flgContatoFeito, string texto, string termometro = null, string preferencia_contato = null)
        {
            if ((termometro != "") && (termometro != null))
            {
                _serviceContato.AtualizarTermometroContato(idContato, termometro);
            }

            if ((preferencia_contato != "") && (preferencia_contato != null))
            {
                var cliente = _serviceContato.BuscarContato(idContato);
                if (cliente != null)
                    _serviceCliente.AtualizarPreferenciaContato(cliente.cliente_id, preferencia_contato);
            }

            var _contato = _serviceContato.AdicionarAvaliacao(idContato, vlrAvaliacao, observacao, flgContatoFeito, texto);

            return JsonRetono(new { success = true, contato = _contato });
        }

        public JsonResult EnviarCadastroCliente(string idvendedor, string nome, string cpf, string telefone, string email)
        {
            UsuarioLogado usuario = Session["UsuarioLogado"] as UsuarioLogado;
            var id_vendedor = idvendedor == null ? usuario.IdVendedorSeleconado : idvendedor;

            var _contato = _serviceContato.AdicionarCliente(id_vendedor, nome, cpf, telefone, email);

            return JsonRetono(new { success = true, contato = _contato });
        }

        #region "Abandono de Carrinho"

        public ActionResult CarrinhoAbandonado(string cpf)
        {
            var list = _serviceLoja.ListarLojas().ToList();

            List<SelectListItem> items = new List<SelectListItem>();

            if (list.Count() > 1)
                items.Add(new SelectListItem() { Text = "Selecione...", Value = "", Selected = true });

            items.AddRange(list.OrderBy(o => o.filial).Select(item => new SelectListItem
            {
                Text = item.filial,
                Value = item.id_loja
            }));

            ViewBag.Lojas = new SelectList(items, "Value", "Text");

            var carrinhoAbandonado = _serviceAcessoExterno.BuscarAbandonoCarrinhoClienteProduto(cpf);
            Session["carrinhoAbandonado"] = carrinhoAbandonado;

            Session["Lojas"] = list;

            var listNowLinxSolicitacao = _serviceAcessoExterno.BuscarAbandonoCarrinhoNowLinxSolicitacao(cpf);
            ViewBag.SolicitacaoExperimente = listNowLinxSolicitacao;

            ViewBag.JaSolicitou = listNowLinxSolicitacao.Count > 0 ? 1 : 0;
            ViewBag.NaoExisteProduto = carrinhoAbandonado.Count > 0 ? 1 : 0;

            return View(carrinhoAbandonado);
        }

        public JsonResult EnviarExperimentacao(string cpf, string loja, string data, string periodo, string dataAbandono)
        {
            var _dataControle = DateTime.Now;
            if (Session["carrinhoAbandonado"] != null)
            {
                var listaProd = Session["carrinhoAbandonado"] as List<AbandonoCarrinhoClienteProduto>;

                foreach (var item in listaProd.Where(x => x.cliente_escolheu == 1).ToList())
                {
                    _serviceAcessoExterno.InserirProdutoAbandonoCarrinho(cpf, item.produto, item.cor_produto, loja, item.grade, item.sku, _dataControle, Convert.ToDateTime(data), periodo, Convert.ToDateTime(dataAbandono));
                }

                var _solicitacao = _serviceAcessoExterno.BuscarAbandonoCarrinhoNowLinxSolicitacao(cpf);

                Session["ProdutosIndicadosAbandonado"] = null;
                return JsonRetono(new { success = true, data = _solicitacao });
            }
            else
            {
                return JsonRetono(new { success = false, msg = "Nenhum Produto Selecionado!" });
            }
        }

        public JsonResult AddprodutoAbandonado(string produto, string cor_produto, string grade, string tipoInclusao)
        {
            IList<AbandonoCarrinhoClienteProduto> listaProd = new List<AbandonoCarrinhoClienteProduto>();
            if (Session["carrinhoAbandonado"] != null)
            {
                IList<AbandonoCarrinhoClienteProduto> listAbandono = Session["carrinhoAbandonado"] as List<AbandonoCarrinhoClienteProduto>;

                foreach (var item in listAbandono)
                {
                    if (item.produto == produto)
                    {
                        if (tipoInclusao == "INCLUIR")
                        {
                            item.cliente_escolheu = 1;
                            item.grade = grade;
                            item.cor_produto = cor_produto;
                            item.data_cliente = DateTime.Now;
                            item.sku = produto + cor_produto + grade;
                        }
                        else
                        {
                            item.cliente_escolheu = 0;
                            item.data_cliente = DateTime.Now;
                        }
                    }

                    listaProd.Add(item);
                }
            }

            Session["ProdutosIndicadosAbandonado"] = listaProd;

            return JsonRetono(new { success = true, dados = listaProd });
        }


        public JsonResult DetalheProduto(string produto)
        {
            Session["ProdutoEstoque"] = null;
            var listaProd = _serviceAcessoExterno.BuscarProdutoClienteExperimenteEstoque(produto);
            Session["ProdutoEstoque"] = listaProd;

            // Só retorna as cores disponíveis em estoque  
            IList<string> listCor = new List<string>();
            IList<string> _listCor = listaProd.Where(x => x.disponivel > 0).Select(x => x.cor_produto.TrimEnd()).Distinct().ToList();
            foreach (var item in _listCor)
            {
                //border-color:#88bb88!important; box-shadow: 0 1px 3px 0 #88bb88, 0 2px 10px 0 #29f929;
                string imgCor = "https://www.usereserva.com/file/general/" + item + ".jpg";
                listCor.Add("<a href='#' onclick='SelecionarCorTamanho(\"\", \"" + item + "\");' style='text-decoration:none; padding-left: 5px!important'><img src='" + imgCor + "' text-align: center;' onerror='$(this).hide()'></a>");
            }

            IList<string> listGrade = new List<string>();
            IList<string> _listGrade = listaProd.Select(x => x.grade).Distinct().ToList();

            // Só retorna o tamanho que esta disponível em estoque
            foreach (var item in _listGrade)
            {
                if (listaProd.Where(x => x.grade.TrimEnd() == item.TrimEnd() && x.disponivel > 0).ToList().Count() > 0)
                    //listGrade.Add("<span class='label label-default' data-toggle='tooltip'><a href='#' onclick='SelecionarGrade(\'' + item + '\');' style='text-decoration:none'>" + item + "</a></span>&nbsp;");
                    listGrade.Add("<div style='padding-left:5px; padding-bottom: 5px; position:relative; float: left;'><a href='#' style='text-decoration:none;' onclick='SelecionarCorTamanho(\"" + item + "\", \"\");'><div class='bg-gray-gradient' style='width: 100%;min-width: 33px;padding: 4px;text-align:center; font-size:18px;font-weight:bold; border-radius: .25em; border-color:black!important; border:solid; border-width:1px;font-weight: bold;'>" + item + "</div></a></div>");
                else
                    listGrade.Add("<div style='padding-left:5px; padding-bottom: 5px; position:relative; float: left;'><div style='color:lightgray; width: 100%;min-width: 33px;padding: 4px;text-align:center; font-size:18px;font-weight:bold; border-radius: .25em; border-color:lightgray!important; border:solid; border-width:1px;font-weight: bold;'>" + item + "</div></div>");


            }

            // Carregando o abandono Inicial
            string _produtoCor = "";
            if (Session["carrinhoAbandonado"] != null)
            {
                IList<AbandonoCarrinhoClienteProduto> listAbandono = Session["carrinhoAbandonado"] as List<AbandonoCarrinhoClienteProduto>;
                var _produto = listAbandono.Where(s => s.produto.TrimEnd() == produto.TrimEnd()).FirstOrDefault();

                _produtoCor = _produto.produto.TrimEnd() + _produto.cor_produto.TrimEnd();
            }

            return JsonRetono(new { success = true, produtoCor = _produtoCor, dados = listaProd, _listCor = listCor, _listGrade = listGrade });
        }

        public JsonResult DetalheProdutoValidaEstoque(string tamanho, string cor)
        {
            if (Session["ProdutoEstoque"] != null)
            {
                bool existeCor = false;
                bool existeTamanho = false;

                string _selecao = "border-color:#00FFFF!important; box-shadow: 0 1px 3px 0 #BFEFFF, 0 2px 10px 0 #9999FF; text-align: center;";

                IList<AbandonoCarrinhoClienteProdutoEstoque> listaProd = Session["ProdutoEstoque"] as List<AbandonoCarrinhoClienteProdutoEstoque>;

                // Só retorna as cores disponíveis em estoque  
                IList<string> listCor = new List<string>();
                IList<string> _listCor = listaProd.Where(x => x.disponivel > 0 && (String.IsNullOrEmpty(tamanho) || x.grade.TrimEnd() == tamanho.TrimEnd())).Select(x => x.cor_produto.TrimEnd()).Distinct().ToList();
                foreach (var item in _listCor)
                {
                    string imgCor = "https://www.usereserva.com/file/general/" + item + ".jpg";
                    listCor.Add("<a href='#' onclick='SelecionarCorTamanho(\"\", \"" + item + "\");' style='text-decoration:none; padding-left: 5px!important'><img src='" + imgCor + "' style='margin-left: 5px; " + (item.ToString() == cor.ToString() ? "border-color: #00FFFF!important; border:solid 1px; box-shadow: 0 1px 3px 0 #BFEFFF, 0 2px 10px 0 #9999FF;" : "") + "  text-align: center;'  onerror='$(this).hide()'></a>");

                    if (item.ToString() == cor.ToString())
                        existeCor = true;
                }


                IList<string> listGrade = new List<string>();
                IList<string> _listGrade = listaProd.Select(x => x.grade).Distinct().ToList();

                string _produtoCor = listaProd.FirstOrDefault().produto.TrimEnd() + listaProd.FirstOrDefault().cor_produto.TrimEnd();
                if (!String.IsNullOrEmpty(cor))
                    _produtoCor = listaProd.FirstOrDefault().produto.TrimEnd() + cor.TrimEnd();

                // Só retorna o tamanho que esta disponível em estoque
                foreach (var item in _listGrade)
                {
                    if (listaProd.Where(x => x.grade.TrimEnd() == item.TrimEnd() && x.disponivel > 0 && (String.IsNullOrEmpty(cor) || x.cor_produto.TrimEnd() == cor.TrimEnd())).ToList().Count() > 0)
                        listGrade.Add("<div style='padding-left:5px; padding-bottom: 5px; position:relative; float: left;'><a href='#' style='text-decoration:none;' onclick='SelecionarCorTamanho(\"" + item + "\", \"\");'><div class='bg-gray-gradient' style='width: 100%;min-width: 33px;padding: 4px;text-align:center; font-size:18px;font-weight:bold; border-radius: .25em; " + (item.ToString() == tamanho.ToString() ? _selecao : "border-color:black!important;") + "  border:solid; border-width:1px;font-weight: bold;'>" + item + "</div></a></div>");
                    else
                        listGrade.Add("<div style='padding-left:5px; padding-bottom: 5px; position:relative; float: left;'><div style='color:lightgray; width: 100%;min-width: 33px;padding: 4px;text-align:center; font-size:18px;font-weight:bold; border-radius: .25em; border-color:lightgray!important; border:solid; border-width:1px;font-weight: bold;'>" + item + "</div></div>");

                    if (item.ToString() == tamanho.ToString())
                        existeTamanho = true;
                }

                return JsonRetono(new { success = true, produtoCor = _produtoCor, dados = listaProd, _listCor = listCor, _listGrade = listGrade, _existeCor = existeCor, _existeTamanho = existeTamanho });
            }
            else
                return JsonRetono(new
                {
                    success = false
                });
        }

        //public JsonResult BuscaOccLoja(string idLoja)
        //{
        //    var client = new RestClient("https://api-prd.reservapto.com.br/occ-physicalstore-subscribe/store/");
        //    var request = new RestRequest(Method.GET);
        //    request.AddHeader("Postman-Token", "926f2a6c-5bf4-4735-a91b-9ccfa34d01b8");
        //    request.AddHeader("Cache-Control", "no-cache");
        //    request.AddHeader("Authorization", "Basic b2NjOjkxcjQjQG5yeDE3I0BydXQwY2MzIw==");
        //    IRestResponse response = client.Execute(request);

        //    return JsonRetono(new { loja = _loja });
        //}

        public JsonResult BuscaShippingTime(string idLoja)
        {
            string _shippingTime = "5";
            var list = _serviceLoja.ListarTodasLojas();
            Loja _loja = list.Where(r => r.id_loja == idLoja).FirstOrDefault();

            // BuscaShippingTime pelo WebApi 

            return JsonRetono(new { ShippingTime = _shippingTime });
        }

        #endregion "Abandono de Carrinho"


        #region "Reservado - Solicitação do Cliente Agendado"

        public ActionResult ReservadoAgendado(string tipo, string cpf, string loja)
        {
            var _cliente = _serviceAcessoExterno.BuscarClienteReservado(cpf, loja);

            if (_cliente.Count > 0)
            {
                if (_cliente.FirstOrDefault().solicitou == 0)
                {
                    // Insere o cliente no carrinho de abandono
                    _serviceAcessoExterno.InsereClienteReservadoAgenda(cpf, loja, _cliente.FirstOrDefault().telefone, tipo, null);
                }
            }



            //var list = _serviceLoja.ListarLojasFormatados().Where(x => (x.tipo_loja.Contains("RESERVA") || x.tipo_loja.Contains("MINI"))).ToList();
            //IList<Loja> _lojas = new List<Loja>();

            //// Valida a loja de preferencia, somente lojas da sua loja de preferencia (RESERVA ou FRANQUIA)
            //foreach (var item in list)
            //{                
            //    if (_cliente.FirstOrDefault().tipo_loja_adulto == "RES_ADULTO" && item.origem == "LOJA" && (item.tipo_loja == "RESERVA" || item.tipo_loja == "RESERVA+MINI"))
            //            _lojas.Add(item);

            //    if (_cliente.FirstOrDefault().tipo_loja_adulto == "FRA_ADULTO" && item.origem == "FRANQUIA" && (item.tipo_loja == "RESERVA" || item.tipo_loja == "RESERVA+MINI"))
            //        _lojas.Add(item);

            //    if (_cliente.FirstOrDefault().tipo_loja_mini == "RES_MINI" && item.origem == "LOJA" && item.tipo_loja == "MINI")
            //        _lojas.Add(item);

            //    if (_cliente.FirstOrDefault().tipo_loja_mini == "FRA_MINI" && item.origem == "FRANQUIA" && item.tipo_loja == "MINI")
            //        _lojas.Add(item);
            //}

            //list = _lojas.ToList();

            //foreach (var item in list)            
            //    item.filial = item.filial.Replace("FRANQUIA ", "");

            //Session["Lojas"] = list;

            //List<SelectListItem> items = new List<SelectListItem>();

            //if (list.Count() > 1)
            //    items.Add(new SelectListItem() { Text = "Selecione...", Value = "", Selected = true });

            //items.AddRange(list.OrderBy(o => o.filial).Select(item => new SelectListItem
            //{
            //    Text = item.filial,
            //    Value = item.id_loja
            //}));

            //ViewBag.Lojas = new SelectList(items, "Value", "Text"); 

            return View(_cliente);
        }

        public JsonResult ConfirmarReservadoAgendado(string cpf, string telefone, string loja, string tipo)
        {
            _serviceAcessoExterno.InsereClienteReservadoAgenda(cpf, loja, telefone, tipo, DateTime.Now);

            return JsonRetono(new
            {
                success = true
            });
        }

        public JsonResult BuscaUfLojaPorMarca(string marca)
        {
            IList<Loja> _lojas = Session["Lojas"] as List<Loja>;

            var _list = _lojas.Where(c => (marca == "RESERVA" && (c.tipo_loja == "RESERVA" || c.tipo_loja == "RESERVA+MINI")) || (marca == "MINI" && c.tipo_loja == "MINI")).Select(s => s.uf).Distinct().OrderBy(o => o.ToString()).ToList();

            return JsonRetono(new { list = _list });
        }

        public JsonResult BuscaLojaPorUf(string uf, string marca)
        {
            IList<Loja> _lojas = Session["Lojas"] as List<Loja>;

            var _list = _lojas.Where(c => (marca == "RESERVA" && (c.tipo_loja == "RESERVA" || c.tipo_loja == "RESERVA+MINI")) || (marca == "MINI" && c.tipo_loja == "MINI")).ToList();

            _list = _list.Where(c => c.uf == uf).OrderBy(o => o.filial).ToList();

            return JsonRetono(new { list = _list });
        }

        #endregion "Reservado - Solicitação do Cliente Agendado"

        public JsonResult BuscarLoja(string idLoja)
        {
            var list = _serviceLoja.ListarTodasLojas();
            Loja _loja = list.Where(r => r.id_loja == idLoja).FirstOrDefault();

            return JsonRetono(new { loja = _loja });
        }
    }
}
