﻿using Now.Domain;
using Now.Models;
using Now.Domain.Contracts.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Net.Mail;
using System.Net;
using Now.Token;
using Now.Common.Validation;
using FireSharp.Interfaces;
using FireSharp.Config;
using FireSharp.Response;
using FireSharp;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Now.Controllers.DTO;

namespace Now.Controllers
{
    public class LoginController : Controller
    {
        private IUsuarioService _service;
        private ILojaService _serviceLoja;
        private IVendedorService _serviceVendedor;
        private IAgendaService _serviceAgenda;
        private IOmniAcompanhamentoService _serviceOmniAcompanhamento;
        public LoginController(IUsuarioService service, ILojaService serviceLoja, IVendedorService serviceVendedor, IAgendaService serviceAgenda, IOmniAcompanhamentoService serviceOmniAcompanhamento)
        {
            _service = service;
            _serviceLoja = serviceLoja;
            _serviceVendedor = serviceVendedor;
            _serviceAgenda = serviceAgenda;
            _serviceOmniAcompanhamento = serviceOmniAcompanhamento;
        }

        public ActionResult Index()
        {
            //Response.Redirect("http://www.usenow.com.br/Login.aspx?msg=Estamos corrigindo o NOVO NOW aguarde!!!", false);    

            Session.Clear();
            Session["VendedorDashboard"] = null;
            Session["UsuarioLogado"] = null;

            if (Request.QueryString["msg"] != null)
                MsgInformacao(Request.QueryString["msg"]);

            Response.AppendHeader("Cache-Control", "no-cache, no-store, must-revalidate"); // HTTP 1.1.
            Response.AppendHeader("Pragma", "no-cache"); // HTTP 1.0.
            Response.AppendHeader("Expires", "0"); // Proxies.

            return View();
        }

        public JsonResult Login(string login, string senha)
        {
            var browser = Request.Browser;

            try
            {
                string msg = string.Empty;

                //Session["Usuario"] = _service.Autentica("tadeu.cavalcante@usereserva.com", "reserva#018");
                UsuarioLogado _usuarioLogado = new UsuarioLogado();

                //_usuarioLogado.RegraPesquisa = 0;
                _usuarioLogado.RegraPesquisa = 1;
                _usuarioLogado.VisualizarEndereco = 1;
                _usuarioLogado.AcessoNovoDashBoard = false;

                _usuarioLogado.Usuario = _service.Autentica(login, senha);

                // Bloquear acesso do Usuário
                if (_usuarioLogado.Usuario.acesso_bloqueado == 0)
                {
                    var lojas = _serviceLoja.ListarLojas();

                    Session["_TodasLojas"] = lojas;

                    _usuarioLogado.PermissaoAcesso = true;

                    _usuarioLogado.Canais = new List<string>();
                    _usuarioLogado.TodasLojasCanaisUsuario = new List<Loja>();
                    IList<Loja> _lojasUsuario = (_usuarioLogado.Usuario.Perfil.ToUpper().Contains("ADMINISTRADOR") || _usuarioLogado.Usuario.Lojas.Count == 0) ? lojas : _usuarioLogado.Usuario.Lojas;

                    foreach (var item in _lojasUsuario.Select(item => new Loja
                    {
                        tipo_loja = item.tipo_loja
                    }).ToList())
                    {
                        if (item.tipo_loja.Contains("RESERVA"))
                        {

                            // Adiciona o Canal
                            if (!_usuarioLogado.Canais.Contains("RESERVA,RESERVA+MINI"))
                                _usuarioLogado.Canais.Add("RESERVA,RESERVA+MINI");

                            // Adicina Lojas do Canal
                            foreach (var _item in lojas.Where(x => x.tipo_loja == "RESERVA" || x.tipo_loja == "RESERVA+MINI").ToList())
                            {
                                if (!_usuarioLogado.TodasLojasCanaisUsuario.Contains(_item))
                                    _usuarioLogado.TodasLojasCanaisUsuario.Add(_item);
                            }
                        }
                        else
                        {                            // Adiciona o Canal
                            if (!_usuarioLogado.Canais.Contains(item.tipo_loja))
                                _usuarioLogado.Canais.Add(item.tipo_loja);

                            // Adicina Lojas do Canal
                            foreach (var _item in lojas.Where(x => x.tipo_loja == item.tipo_loja).ToList())
                            {
                                if (!_usuarioLogado.TodasLojasCanaisUsuario.Contains(_item))
                                    _usuarioLogado.TodasLojasCanaisUsuario.Add(_item);
                            }
                        }
                    }
                    //_usuarioLogado.Canais.Add("RESERVA,RESERVA+MINI");
                    ////_usuarioLogado.Canais.Add("RESERVA");
                    ////_usuarioLogado.Canais.Add("RESERVA+MINI");
                    //_usuarioLogado.Canais.Add("MINI");                  

                    _usuarioLogado.SupervisorLojas = new List<SupervisorLojas>();

                    //if (_usuarioLogado.Usuario.Perfil.ToUpper() == "Administrador Sistema".ToUpper() || _usuarioLogado.Usuario.Perfil.ToUpper() == "Supervisor Brasil".ToUpper())
                    if (_usuarioLogado.Usuario.Perfil.ToUpper() == "Administrador Sistema".ToUpper())
                    {
                        _usuarioLogado.LojasUsuario = lojas;

                        if (_usuarioLogado.Usuario.Perfil.ToUpper() == "Supervisor Brasil".ToUpper())
                            _usuarioLogado.LojasUsuario = _usuarioLogado.Usuario.Lojas;


                        _usuarioLogado.SupervisorLojas = _service.ListarSupervisores();
                        _usuarioLogado.LojasSupervisores = lojas;

                        Session["_Supervisores"] = _usuarioLogado.SupervisorLojas.GroupBy(x => new { x.id_supervisor, x.nome_supervisor }).Select(item => new Supervisores
                        {
                            id_supervisor = item.Key.id_supervisor,
                            nome_supervisor = item.Key.nome_supervisor,
                            flg_selecao = 1
                        }).ToList();

                    }
                    else
                        _usuarioLogado.LojasUsuario = _usuarioLogado.Usuario.Lojas;

                    _usuarioLogado.LojasUsuarioCanais = _usuarioLogado.LojasUsuario;

                    _usuarioLogado.CodigoClima = "241";

                    if ((_usuarioLogado.Usuario.Perfil.ToUpper() == "GERENTE" || _usuarioLogado.Usuario.Perfil.ToUpper() == "VENDEDOR") || _usuarioLogado.Usuario.Lojas.Count() == 1)
                    {
                        _usuarioLogado.IdLojaSeleconado = _usuarioLogado.Usuario.Lojas.FirstOrDefault().id.ToString();

                        /*
                           O sistema não informará mais dados como nº do prédio e apartamento. Será responsabilidade do vendedor conferir os dados com o cliente ou verificar no Linx da loja.
                           Mostraremos apenas o bairro e cidade
                           Incluir numero do cadastro do Linx  
                        */
                        _usuarioLogado.VisualizarEndereco = 0;

                        if (_usuarioLogado.Usuario.Perfil.ToUpper() == "VENDEDOR")
                        {
                            /*
                            A partir de agora o vendedor só conseguirá consultar clientes que:

                            - sejam preferenciais da sua loja
                            - sejam seus por preferência
                            - já tenham sido atendidos por ele em outra loja da Cia.   
                        */
                            //VerificaLogin(login); //rotina para enviar o token para o vendedor

                            _usuarioLogado.RegraPesquisa = 1;

                            _usuarioLogado.IdVendedorSeleconado = _usuarioLogado.Usuario.vendedor_id.ToString();
                            Session["VendedorNome"] = _usuarioLogado.Usuario.username;

                        }

                        Session["OrigemLoja"] = _usuarioLogado.Usuario.Lojas.FirstOrDefault().origem == "FRANQUIA" ? _usuarioLogado.Usuario.Lojas.FirstOrDefault().origem : "VAREJO";
                        _usuarioLogado.CodigoClima = _usuarioLogado.Usuario.Lojas.FirstOrDefault().codigo_clima.ToString();
                    }

                    string idsLojasNow = string.Empty;

                    // Só retorna os vendedores se existir uma loja selecionada
                    if (_usuarioLogado.IdLojaSeleconado != null)
                        _usuarioLogado.VendedoresUsuario = _serviceVendedor.GetVendedor(_usuarioLogado.IdLojaSeleconado);

                    _usuarioLogado.TiposAgendas = _serviceAgenda.ListarAgendas(_usuarioLogado.Usuario.loja_id.ToString());


                    var tipoLojaReserva = _usuarioLogado.LojasUsuario.Where(x => x.tipo_loja == "RESERVA" || x.tipo_loja == "RESERVA+MINI");
                    var lojaPiloto = _usuarioLogado.LojasUsuario.Where(x => x.id == 303 || x.id == 315 || x.id == 305);
                    
                    if ((_usuarioLogado.Usuario.Perfil == "Vendedor") && (lojaPiloto.FirstOrDefault() != null))
                        _usuarioLogado.AcessoNovoDashBoard = true;

                    var acesso = login.Length == 11 ? true : false;

                    Session["UsuarioLogado"] = _usuarioLogado;
                    Session["_trocou"] = true;
                    Session["_trocouVendedor"] = true;
                    
                    //_usuarioLogado.ListaOmniAcompanhamento = ListaAcompOmni(_usuarioLogado.IdLojaSeleconado, _usuarioLogado.IdVendedorSeleconado);

                    _service.AdicionarLogAcesso("Name = " + browser.Browser + " - Version = " + browser.Version, login, senha, HttpContext.Request.UserHostAddress.ToString(), 1);

                    UsuarioLogado usuarioTemp = new UsuarioLogado();
                    usuarioTemp = _usuarioLogado;



                    if (senha != "reserva#admin")
                    {
                        string idLoja = "";
                        if (_usuarioLogado.Usuario.Lojas.Count() > 0)
                            idLoja = _usuarioLogado.Usuario.Lojas[0].id_loja;

                        return JsonRetono(new { success = true, responseText = msg, perfil = usuarioTemp.Usuario.Perfil.ToUpper(), usuario = usuarioTemp, recriarSenha = usuarioTemp.Usuario.recriar_senha, idLoja = idLoja });

                    }
                    else
                    {
                        return JsonRetono(new { success = true, responseText = msg, perfil = "", usuario = "", recriarSenha = usuarioTemp.Usuario.recriar_senha});
                    }

                }
                else
                {
                    // Mensagem de Bloqueio
                    return JsonRetono(new { success = false, responseText = _usuarioLogado.Usuario.username + " - Seu acesso foi bloqueado!" });
                }
            }
            catch (Exception ex)
            {
                _service.AdicionarLogAcesso("Name = " + browser.Browser + " - Version = " + browser.Version, login, senha, HttpContext.Request.UserHostAddress.ToString(), 0);

                return JsonRetono(new { success = false, responseText = ex.Message, perfil = "", usuario = "" });
            }

        }

        public JsonResult GrabarLogin(string check, string login, string senha)
        {
            if (check == "check")
            {
                HttpCookie userInfo = new HttpCookie("userInfo");
                userInfo["login"] = login;
                userInfo["senha"] = senha;
                userInfo["check"] = check;
                userInfo.Expires.Add(new TimeSpan(2050, 0, 0, 0));
                Response.Cookies.Add(userInfo);
            }
            else
            {
                HttpCookie userInfo = Request.Cookies["userInfo"];
                if (userInfo != null)
                {
                    userInfo.Expires = DateTime.Now.AddDays(-1d);
                    Response.Cookies.Add(userInfo);
                }
            }

            return JsonRetono(new { success = false, responseText = "Dados salvo com sucesso!" });
        }

        public IList<OmniAcompanhamento> ListaAcompOmni(string lojaId, string vendedorId)
        {
            return _serviceOmniAcompanhamento.Listar(lojaId, vendedorId);
        }

        public JsonResult GerarTokenPor(string login)
        {
            try
            {
                string email = login;
                string nomeVendedor = String.Empty;

                var usuario = _service.ExiteUsuario(login);
                string senhaProv = GeneratePassword();

                usuario.token_acesso = senhaProv;
                _service.UpdateUsuario(usuario);

                if (usuario.Perfil.ToUpper() == "VENDEDOR")
                {
                    nomeVendedor = usuario.username + "<br>";
                    if (usuario.Lojas.Count() > 0)
                        email = "rodrigo.depaula@usereserva.com";     //email = usuario.Lojas[0].email;
                }
                // Enviar Email
                EnviarEmailTrocaSenha(email, senhaProv, nomeVendedor);

                return JsonRetono(new { success = true, responseText = "" });
            }
            catch (Exception ex)
            {
                return JsonRetono(new { success = false, responseText = ex.Message });
            }

        }

        public JsonResult VerificaLogin(string login)
        {
            try
            {
                string email = login;
                string nomeVendedor = String.Empty;

                var usuario = _service.ExiteUsuario(login);
                string senhaProv = GeneratePassword();

                usuario.recriar_senha = true;
                usuario.remember_token = senhaProv;
                _service.UpdateUsuario(usuario);

                if (usuario.Perfil.ToUpper() == "VENDEDOR")
                {
                    nomeVendedor = usuario.username + "<br>";
                    if (usuario.Lojas.Count() > 0)
                        email = "rodrigo.depaula@usereserva.com";     //email = usuario.Lojas[0].email;
                }
                // Enviar Email
                EnviarEmailTrocaSenha(email, senhaProv, nomeVendedor);

                return JsonRetono(new { success = true, responseText = "" });
            }
            catch (Exception ex)
            {
                return JsonRetono(new { success = false, responseText = ex.Message });
            }

        }

        public Boolean acessoValidoDoToken(string chave)
        {
            IFirebaseConfig config = new FirebaseConfig
            {
                AuthSecret = "q7gMXehEBRZ6WuXscJNLjB9p4OV4JSdgOe62dNVJ",
                BasePath = "https://now-web-cac79.firebaseio.com/"
            };
            IFirebaseClient client = new FirebaseClient(config);
            UsuarioLogado usuarioLogado = Session["UsuarioLogado"] as UsuarioLogado;

            var response = client.Get("token/" + usuarioLogado.LojasUsuario.FirstOrDefault().id_loja + "/chave");
            var acessodto = JsonConvert.DeserializeObject<AcessoDTO>(response.Body);

            var isValid = acessodto.codigo == chave;
            if (isValid)
            {
                acessodto.ativo = false;
                client.Set("token/" + "TLPLZ" + "/chave", acessodto);
            }

            return isValid;
        }

        public JsonResult AcessarToken(string token, string idLoja)
        {
            var isValid = false;
            UsuarioLogado usuarioLogado = Session["UsuarioLogado"] as UsuarioLogado;

            if (idLoja == "")
            {
                //var usuario = _service.BuscarUsuarioPor(PasswordAssertionConcern.Encrypt(token));
                isValid = acessoValidoDoToken(token);
                usuarioLogado.PermissaoAcesso = isValid;
                usuarioLogado.AcessoNovoDashBoard = isValid;
                Session["UsuarioLogado"] = usuarioLogado;
            }
            else
            {
                var otpTeste = new TokenOneTime(idLoja);
                isValid = otpTeste.IsCodeValid(token); //to verify one that user entered

                usuarioLogado.AcessoNovoDashBoard = isValid;
                usuarioLogado.PermissaoAcesso = isValid;
                Session["UsuarioLogado"] = usuarioLogado;
            }

            if (isValid)
                return JsonRetono(new { success = isValid, responseText = "" });
            else
                return JsonRetono(new { success = isValid, responseText = "Token de Acesso Inválido!" });
        }

        private void EnviarEmailTrocaSenha(string email, string senhaProvisoria, string NomeVendedor = "")
        {
            MailAddress to = new MailAddress(email);
            MailAddress from = new MailAddress(email);
            MailMessage message = new MailMessage(from, to);

            message.Subject = "NOW - Solicitação de Troca de Senha (" + DateTime.Now.ToString("HH:mm") + "h)";
            message.Body = CorpoEmailTrocaSenha(senhaProvisoria.Trim()).Replace("#VENDEDOR#", NomeVendedor);
            message.IsBodyHtml = true;

            SmtpClient client = new SmtpClient();

            client.Send(message);
        }

        private string GeneratePassword()
        {
            string strPwdchar = "abcdefghijklmnopqrstuvwxyz0123456789#+@&$ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            string strPwd = "";
            Random rnd = new Random();
            for (int i = 0; i <= 7; i++)
            {
                int iRandom = rnd.Next(0, strPwdchar.Length - 1);
                strPwd += strPwdchar.Substring(iRandom, 1);
            }
            return strPwd;
        }

        public JsonResult TrocarSenha(string login, string senhaRecebida, string senhaNova, string senhaConfirmar)
        {
            try
            {
                _service.TrocarSenha(login, senhaRecebida, senhaNova, senhaConfirmar);

                return JsonRetono(new { success = true, responseText = "Senha alterada com sucesso" });
            }
            catch (Exception ex)
            {
                return JsonRetono(new { success = false, responseText = ex.Message });
            }
        }

        public string CorpoEmailTrocaSenha(string senha)
        {
            string corpoEmail = @" 

            <html xmlns='http://www.w3.org/1999/xhtml'>
<head>
    <meta http-equiv='content-type' content='text/html; charset=utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1.0;'>
    <meta name='format-detection' content='telephone=no' />
    <!-- Responsive Mobile-First Email Template by Konstantin Savchenko, 2015.
    https://github.com/konsav/email-templates/  -->
    <style>
        /* Reset styles */
        body {
            margin: 0;
            padding: 0;
            min-width: 100%;
            width: 100% !important;
            height: 100% !important;
        }

        body, table, td, div, p, a {
            -webkit-font-smoothing: antialiased;
            text-size-adjust: 100%;
            -ms-text-size-adjust: 100%;
            -webkit-text-size-adjust: 100%;
            line-height: 100%;
        }

        table, td {
            mso-table-lspace: 0pt;
            mso-table-rspace: 0pt;
            border-collapse: collapse !important;
            border-spacing: 0;
        }

        img {
            border: 0;
            line-height: 100%;
            outline: none;
            text-decoration: none;
            -ms-interpolation-mode: bicubic;
        }

        #outlook a {
            padding: 0;
        }

        .ReadMsgBody {
            width: 100%;
        }

        .ExternalClass {
            width: 100%;
        }

            .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {
                line-height: 100%;
            }

        /* Rounded corners for advanced mail clients only */
        @media all and (min-width: 560px) {
            .container {
                border-radius: 8px;
                -webkit-border-radius: 8px;
                -moz-border-radius: 8px;
                -khtml-border-radius: 8px;
            }
        }

        /* Set color for auto links (addresses, dates, etc.) */
        a, a:hover {
            color: #127DB3;
        }

        .footer a, .footer a:hover {
            color: #999999;
        }
    </style>
    <!-- MESSAGE SUBJECT -->
    <title>Solicitação de troca de senha NOW</title>
</head>
<!-- BODY -->
<!-- Set message background color (twice) and text color (twice) -->
<body topmargin='0' rightmargin='0' bottommargin='0' leftmargin='0' marginwidth='0' marginheight='0' width='100%' style='border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0; width: 100%; height: 100%; -webkit-font-smoothing: antialiased; text-size-adjust: 100%; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; line-height: 100%;
	background-color: #F0F0F0;
	color: #000000;'
      bgcolor='#F0F0F0'
      text='#000000'>
    <!-- SECTION / BACKGROUND -->
    <!-- Set message background color one again -->
    <table width='100%' align='center' border='0' cellpadding='0' cellspacing='0' style='border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0; width: 100%;' class='background'>
        <tr>
            <td align='center' valign='top' style='border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0;'
                bgcolor='#F0F0F0'>
                <!-- WRAPPER -->
                <!-- Set wrapper width (twice) -->
                <table border='0' cellpadding='0' cellspacing='0' align='center'
                       width='560' style='border-collapse: collapse; border-spacing: 0; padding: 0; width: inherit;
	max-width: 560px;' class='wrapper'>
                    <tr>
                        <td align='center' valign='top' style='border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0; padding-left: 6.25%; padding-right: 6.25%; width: 87.5%;
			padding-top: 20px;
			padding-bottom: 20px;'>
                            <!-- PREHEADER -->
                            <!-- Set text color to background color -->
                            <div style='display: none; visibility: hidden; overflow: hidden; opacity: 0; font-size: 1px; line-height: 1px; height: 0; max-height: 0; max-width: 0;
			color: #F0F0F0;' class='preheader'>
                                Available on&nbsp;GitHub and&nbsp;CodePen. Highly compatible. Designer friendly. More than 50%&nbsp;of&nbsp;total email opens occurred on&nbsp;a&nbsp;mobile device&nbsp;— a&nbsp;mobile-friendly design is&nbsp;a&nbsp;must for&nbsp;email campaigns.
                            </div>
                            <!-- LOGO -->
                            <!-- Image text color should be opposite to background color. Set your url, image src, alt and title. Alt text should fit the image size. Real image size should be x2. URL format: http://domain.com/?utm_source={{Campaign-Source}}&utm_medium=email&utm_content=logo&utm_campaign={{Campaign-Name}} -->
                            <a target='_blank' style='text-decoration: none;'
                               href='https://github.com/konsav/email-templates/'>
                                <img border='0' vspace='0' hspace='0'
                                     src='http://usenow.com.br/Images/LogoNOW.png'
                                     width='100' height='30'
                                     alt='Logo' title='Logo' style='
				color: #000000;
				font-size: 10px; margin: 0; padding: 0; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; border: none; display: block;' />
                            </a>
                        </td>
                    </tr>
                    <!-- End of WRAPPER -->
                </table>
                <!-- WRAPPER / CONTEINER -->
                <!-- Set conteiner background color -->
                <table border='0' cellpadding='0' cellspacing='0' align='center'
                       bgcolor='#FFFFFF'
                       width='560' style='border-collapse: collapse; border-spacing: 0; padding: 0; width: inherit;
	max-width: 560px;' class='container'>
                    <!-- HEADER -->
                    <!-- Set text color and font family ('sans-serif' or 'Georgia, serif') -->
                    <tr>
                        <td align='center' valign='top' style='border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0; padding-left: 6.25%; padding-right: 6.25%; width: 87.5%; font-size: 24px; font-weight: bold; line-height: 130%;
			padding-top: 25px;
			color: #000000;
			font-family: sans-serif;' class='header'>
                            Solicitação de troca de senha NOW
                        </td>
                    </tr>

                    <!-- SUBHEADER -->
                    <!-- Set text color and font family ('sans-serif' or 'Georgia, serif') -->
                    <tr>
                        <td align='center' valign='top' style='border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0; padding-bottom: 3px; padding-left: 6.25%; padding-right: 6.25%; width: 87.5%; font-size: 18px; font-weight: 300; line-height: 150%;
			padding-top: 5px;
			color: #000000;
			font-family: sans-serif;' class='subheader'>
                            
                            #VENDEDOR#
                            Senha de confirmação de segurança
                        </td>
                    </tr>
                    <!-- HERO IMAGE -->
                    <!-- Image text color should be opposite to background color. Set your url, image src, alt and title. Alt text should fit the image size. Real image size should be x2 (wrapper x2). Do not set height for flexible images (including 'auto'). URL format: http://domain.com/?utm_source={{Campaign-Source}}&utm_medium=email&utm_content={{Ìmage-Name}}&utm_campaign={{Campaign-Name}} -->
                    <!-- PARAGRAPH -->
                    <!-- Set text color and font family ('sans-serif' or 'Georgia, serif'). Duplicate all text styles in links, including line-height -->
                    <!-- Set line color -->
                    <tr>
                        <td align='center' valign='top' style='border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0; padding-left: 6.25%; padding-right: 6.25%; width: 87.5%;
			padding-top: 25px;' class='line'>
                            <hr color='#E0E0E0' align='center' width='100%' size='1' noshade style='margin: 0; padding: 0;' />
                        </td>
                    </tr>
                    <tr>
                        <td align='center' valign='top' style='border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0; padding-left: 6.25%; padding-right: 6.25%; width: 87.5%; font-size: 17px; font-weight: 400; line-height: 160%;
			padding-top: 25px;
			color: #000000;
			font-family: sans-serif;' class='paragraph'>
                            Senha abaixo deverá ser informada na solicitação de troca de senha, basta copiar e colar no campo Senha Recebida no sistema NOW.
                        </td>
                    </tr>
                    <!-- BUTTON -->
                    <!-- Set button background color at TD, link/text color at A and TD, font family ('sans-serif' or 'Georgia, serif') at TD. For verification codes add 'letter-spacing: 5px;'. Link format: http://domain.com/?utm_source={{Campaign-Source}}&utm_medium=email&utm_content={{Button-Name}}&utm_campaign={{Campaign-Name}} -->
                    <tr>
                        <td align='center' valign='top' style='border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0; padding-left: 6.25%; padding-right: 6.25%; width: 87.5%;
			padding-top: 25px;
			padding-bottom: 5px;' class='button'>
                            
                                <table border='0' cellpadding='0' cellspacing='0' align='center' style='max-width: 240px; min-width: 120px; border-collapse: collapse; border-spacing: 0; padding: 0;'>
                                    <tr>
                                        <td align='center' valign='middle' style='padding: 12px 24px; margin: 0; border-collapse: collapse; border-spacing: 0; border-radius: 4px; -webkit-border-radius: 4px; -moz-border-radius: 4px; -khtml-border-radius: 4px;'
                                            bgcolor='#E9703E'>
                                            <div style='
					color: #FFFFFF; font-family: sans-serif; font-size: 17px; font-weight: 400; line-height: 120%;'
                                               >
                                                #SENHA#
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            
                        </td>
                    </tr>
                    <!-- LINE -->
                    <!-- LINE -->
                    <!-- Set line color -->
                    <tr>
                        <td align='center' valign='top' style='border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0; padding-left: 6.25%; padding-right: 6.25%; width: 87.5%;
			padding-top: 25px;' class='line'>
                            <hr color='#E0E0E0' align='center' width='100%' size='1' noshade style='margin: 0; padding: 0;' />
                        </td>
                    </tr>
                    <!-- PARAGRAPH -->
                    <!-- Set text color and font family ('sans-serif' or 'Georgia, serif'). Duplicate all text styles in links, including line-height -->
                    <tr>
                        <td align='center' valign='top' style='border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0; padding-left: 6.25%; padding-right: 6.25%; width: 87.5%; font-size: 17px; font-weight: 400; line-height: 160%;
			padding-top: 20px;
			padding-bottom: 25px;
			color: #000000;
			font-family: sans-serif;' class='paragraph'>
                            Dúvidas? <a href='mailto:suporte@usereserva.com' target='_blank' style='color: #127DB3; font-family: sans-serif; font-size: 17px; font-weight: 400; line-height: 160%;'>suporte@usereserva.com</a>
                        </td>
                    </tr>
                    <!-- End of WRAPPER -->
                </table>
                <!-- WRAPPER -->
                <!-- Set wrapper width (twice) -->
                <table border='0' cellpadding='0' cellspacing='0' align='center'
                       width='560' style='border-collapse: collapse; border-spacing: 0; padding: 0; width: inherit;
	max-width: 560px;' class='wrapper'>

                    <!-- Set text color and font family ('sans-serif' or 'Georgia, serif'). Duplicate all text styles in links, including line-height -->
                    <tr>
                        <td align='center' valign='top' style='border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0; padding-left: 6.25%; padding-right: 6.25%; width: 87.5%; font-size: 13px; font-weight: 400; line-height: 150%;
			padding-top: 20px;
			padding-bottom: 20px;
			color: #999999;
			font-family: sans-serif;' class='footer'>
                            ® Reserva 2016 <a href='https://www.usenow.com.br' target='_blank' style='text-decoration: underline; color: #999999; font-family: sans-serif; font-size: 13px; font-weight: 400; line-height: 150%;'>UseNow</a>.

                            <!-- ANALYTICS -->
                            <!-- http://www.google-analytics.com/collect?v=1&tid={{UA-Tracking-ID}}&cid={{Client-ID}}&t=event&ec=email&ea=open&cs={{Campaign-Source}}&cm=email&cn={{Campaign-Name}} -->
                            <img width='1' height='1' border='0' vspace='0' hspace='0' style='margin: 0; padding: 0; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; border: none; display: block;'
                                 src='https://raw.githubusercontent.com/konsav/email-templates/master/images/tracker.png' />
                        </td>
                    </tr>
                    <!-- End of WRAPPER -->
                </table>
                <!-- End of SECTION / BACKGROUND -->
            </td>
        </tr>
    </table>
</body>
</html>";

            return corpoEmail.Replace("#SENHA#", senha);
        }

        //public ActionResult Login(Usuario usuario)
        //{
        //    try
        //    {
        //        //Session["Usuario"] = _service.Autentica("tadeu.cavalcante@usereserva.com", "reserva#018");
        //        UsuarioLogado _usuarioLogado = new UsuarioLogado();

        //        _usuarioLogado.Usuario = _service.Autentica(usuario.email, usuario.password);

        //        _usuarioLogado.Canais = new List<string>();
        //        _usuarioLogado.Canais.Add("RESERVA,RESERVA+MINI");

        //        _usuarioLogado.LojasUsuario = _serviceLoja.ListarLojasVisiveisPorUsuario(_usuarioLogado.Usuario.id, "");

        //        // Seleciona o Canal Default
        //        if (_usuarioLogado.LojasUsuario.Count() > 1)
        //            _usuarioLogado.LojasUsuarioCanais = _usuarioLogado.LojasUsuario.Where(x => (x.tipo_loja == "RESERVA" || x.tipo_loja == "RESERVA+MINI")).ToList();

        //        string idsLojasNow = string.Empty;
        //        foreach (var item in _serviceLoja.ListarLojas())
        //            idsLojasNow += item.id.ToString() + ",";

        //        idsLojasNow.TrimEnd(',');

        //        Session["_LojasAtivasNow"] = idsLojasNow;

        //        //itemsLojas.AddRange(lojas.OrderBy(o => o.filial).Select(item => new SelectListItem
        //        //{
        //        //    Text = item.filial,
        //        //    Value = item.id.ToString()
        //        //}));

        //        //List<SelectListItem> itemsVend = new List<SelectListItem>();

        //        if (_usuarioLogado.LojasUsuario.Count == 1)
        //        {
        //            _usuarioLogado.VendedoresUsuario = _serviceVendedor.GetVendedor(_usuarioLogado.LojasUsuario.FirstOrDefault().id.ToString());

        //            //itemsVend.Add(new SelectListItem() { Text = "Selecione...", Value = "", Selected = true });

        //            //itemsVend.AddRange(vendedores.OrderBy(o => o.nome).Select(item => new SelectListItem
        //            //{
        //            //    Text = item.nome,
        //            //    Value = item.id.ToString()
        //            //}));

        //            ///ViewBag.Vendedores = itemsVend;
        //        }

        //        _usuarioLogado.TiposAgendas = _serviceAgenda.ListarAgendas(_usuarioLogado.Usuario.loja_id.ToString());

        //        //Session["Vendedores"] = new SelectList(itemsVend, "Value", "Text", usuario.loja_id);

        //        //#region "Obter Agendas"

        //        //List<SelectListItem> itemsAgendas = new List<SelectListItem>();



        //        //itemsAgendas.Add(new SelectListItem() { Text = "Selecione...", Value = "", Selected = true });

        //        //itemsAgendas.AddRange(tipoAgendas.OrderBy(o => o.titulo).Select(item => new SelectListItem
        //        //{
        //        //    Text = item.titulo,
        //        //    Value = item.parametro
        //        //}));

        //        //#endregion "Obter Agendas"

        //        //Session["tipo_agendas"] = new SelectList(itemsAgendas, "Value", "Text", usuario.loja_id);            

        //        //Session["LojasUsuario"] = new SelectList(itemsLojas, "Value", "Text", usuario.loja_id);

        //        Session["UsuarioLogado"] = _usuarioLogado;

        //        //ViewBag.ReturnUrl = returnUrl;
        //        // Provisorio
        //        return RedirectToAction("Index", "Dashboard");
        //    }
        //    catch (Exception ex)
        //    {
        //        MsgAtencao(ex.Message);
        //        return View();
        //    }
        //}

        #region Mensagems

        protected void MsgSucesso(string msg = "Operação Realizada com Sucesso!")
        {
            TempData["Sucesso"] = msg;
        }

        protected void MsgErro(string msg = "Ocorreu um erro contate o adiministrador do sistema!")
        {
            TempData["Erro"] = msg;
        }

        protected void MsgInformacao(string msg)
        {
            TempData["Info"] = msg;
        }

        protected void MsgAtencao(string msg)
        {
            TempData["Atencao"] = msg;
        }

        #endregion

        public JsonResult JsonRetono(object data)
        {
            JsonResult jsonResult = Json(data, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = int.MaxValue;
            return jsonResult;
        }

    }
}
