﻿using Now.Domain;
using Now.Models;
using Now.Domain.Contracts.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Now.Controllers
{
    public class LicencaVendedoresController : BaseController
    {
        public IVendedoresLicenciadosService _serviceLicencaVendedores;
        public LicencaVendedoresController(IVendedoresLicenciadosService serviceLicencaVendedores)
        {
            _serviceLicencaVendedores = serviceLicencaVendedores;

        }

        public ActionResult Index()
        {
            return View();
        }

        public JsonResult SalvarLicenca(string lojaId, string vendedorId, string motivoId, string dataInicial, string dataFinal, string tbxObservacao, string flgIndeterminado)
        {
            try
            {

                VendedoresLicenciados licenca = new VendedoresLicenciados();
                licenca.loja_id = Convert.ToInt32(lojaId);
                licenca.vendedor_id = Convert.ToInt32(vendedorId);
                licenca.vendedor_licenciados_tipo_id = Convert.ToInt32(motivoId);

                if (Convert.ToInt32(flgIndeterminado) == 0)
                {
                    licenca.data_inicio_bloqueio = Convert.ToDateTime(dataInicial);
                    licenca.data_fim_bloqueio = Convert.ToDateTime(dataFinal);
                }

                licenca.data_inclusao = DateTime.Now;
                licenca.data_alteracao = DateTime.Now;
                licenca.motivo = tbxObservacao;
                licenca.flg_bloqueio_indeterminado = Convert.ToInt32(flgIndeterminado);
                licenca.data_desativacao = null;

                _serviceLicencaVendedores.Salvar(licenca);
                var _list = _serviceLicencaVendedores.Listar(lojaId, vendedorId);

                return Json(new { success = true, msg = "Licença salva com sucesso!", dados = _list });

            }
            catch (Exception ex)
            {
                return Json(new { success = false, msg = "Erro ao salvar a licença!" });
            }
        }

        public JsonResult ListarMotivos()
        {
            try
            {
                var listTipo = _serviceLicencaVendedores.ListarMotivos();

                return Json(new { data = listTipo, success = true, msg = "Licença salva com sucesso!" });
            }
            catch (Exception ex)
            {
                return Json(new { success = false, msg = "Erro ao salvar a licença!" });
            }
        }

        public JsonResult ListarLicencaVendedores(string lojaId, string vendedorId)
        {
            try
            {
                var _list = _serviceLicencaVendedores.Listar(lojaId, vendedorId);

                return Json(new { dados = _list });
            }
            catch (Exception ex)
            {
                return Json(new { success = false, msg = "Erro ao salvar a licença!" });
            }
        }

    }
}
