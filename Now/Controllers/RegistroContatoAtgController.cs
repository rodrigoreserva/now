﻿using Now.Domain;
using Now.Models;
using Now.Domain.Contracts.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Now.Controllers
{
    public class RegistroContatoAtgController : Controller
    {
        public IContatoAtgService _serviceContatoAtg;
        public RegistroContatoAtgController(IContatoAtgService serviceContatoAtg)
        {
            _serviceContatoAtg = serviceContatoAtg;
        }

        public ActionResult Index()
        {
            IList<ContatoAtg> list = _serviceContatoAtg.BuscarContatosPorData(DateTime.Now);

            return View(list);
        }

        public JsonResult RegistroContato(string cpf, string status, string modo, string termometro, string quemligou, string quemeele, string obsligacao)
        {
            try
            {
                string msg = "Contato inserido com sucesso!";

                ContatoAtg contatoAtg = new ContatoAtg() { cpf = cpf, status = status, modo = modo, termometro = termometro, quem_ligou = quemligou, quem_ele = quemeele, obs_ligacao = obsligacao };

                _serviceContatoAtg.AdicionarContatoAtg(contatoAtg);

                return JsonRetono(new { sucesso = true, mensagem = msg });
            }
            catch (Exception ex)
            {
                return JsonRetono(new { sucesso = false, mensagem = ex.Message });
                throw;
            }
        }


        public JsonResult JsonRetono(object data)
        {
            JsonResult jsonResult = Json(data, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = int.MaxValue;
            return jsonResult;
        }
    }
}
