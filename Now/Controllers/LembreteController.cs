﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using DotNetOpenAuth.AspNet;
using Microsoft.Web.WebPages.OAuth;
using WebMatrix.WebData;
using Now.Models;
using Now.Domain.Contracts.Services;
using Now.Domain;


namespace Now.Controllers
{
    public class LembreteController : BaseController
    {
        private ILembreteService _service;
        private IAgendaService _serviceAgenda;
        public LembreteController(ILembreteService service, IAgendaService serviceAgenda)
        {
            _serviceAgenda = serviceAgenda;
            _service = service;
        }

        public ActionResult Index()
        {
            ViewBag.ListaLembretes = _service.ListarLembretes(UsuarioLogado.Usuario.id, UsuarioLogado.IdLojaSeleconado, "");

            ViewBag.AgendaLembretes = _serviceAgenda.FindCalendars("8","", UsuarioLogado.Usuario.id.ToString(), UsuarioLogado.IdsLojasUsuario, UsuarioLogado.IdVendedorSeleconado);

            @ViewBag.AgendaTipo = UsuarioLogado.TiposAgendas.Where(x => x.id == 8).FirstOrDefault();

            // Log do sistema
            LogSistema(new LogSistema { acao = "Tela de Lembrete", login = UsuarioLogado.Usuario.email, tipo_log = "NAVEGANDO", vendedor_id = (String.IsNullOrEmpty(UsuarioLogado.IdVendedorSeleconado) ? 0 : Convert.ToInt32(UsuarioLogado.IdVendedorSeleconado)), loja_id = (String.IsNullOrEmpty(UsuarioLogado.IdLojaSeleconado) ? 0 : Convert.ToInt32(UsuarioLogado.IdLojaSeleconado)), metodo = "Index - Lembrete", tela = "Lembrete", user_id = UsuarioLogado.Usuario.id, ip = HttpContext.Request.UserHostAddress.ToString(), data = DateTime.Now });

            return View();
        }


        public JsonResult CarregarCalendario()
        {
            DateTime TempoInicial = DateTime.Now;
            List<Object> resultado = new List<object>();

            int i = 0;
            foreach (var item in _service.ListarLembretes(UsuarioLogado.Usuario.id, UsuarioLogado.IdLojaSeleconado, ""))
            {
                string bkg = "#E7E7E7";
                string brc = "#BAB8B8";
                string txc = "#000000";

                if (item.data_final.Value >= DateTime.Now)
                {
                    i++;
                    bkg = Cores()[i];
                    brc = Cores()[i];
                    txc = "#FFFFFF";
                }

                resultado.Add(new
                {
                    id = item.id,
                    title = "Cliente:" + item.nome_cliente.ToUpper() + " - " + "Motivo: " + item.titulo,
                    start = item.data_inicio.Value.ToString("yyyy-MM-ddT00:00"),
                    end = item.data_final.Value.ToString("yyyy-MM-ddT23:59"),
                    backgroundColor = bkg,
                    borderColor = brc,
                    textColor = txc
                });

            }
            DateTime TempoFinal = DateTime.Now;
            // Log do sistema
            LogSistema(new LogSistema { processo_minutos = TempoProcesso(TempoInicial, TempoFinal), acao = "Carregando Calendário de Lembrete", login = UsuarioLogado.Usuario.email, tipo_log = "CARREGANDO", vendedor_id = (String.IsNullOrEmpty(UsuarioLogado.IdVendedorSeleconado) ? 0 : Convert.ToInt32(UsuarioLogado.IdVendedorSeleconado)), loja_id = (String.IsNullOrEmpty(UsuarioLogado.IdLojaSeleconado) ? 0 : Convert.ToInt32(UsuarioLogado.IdLojaSeleconado)), metodo = "Calendário - Lembrete", tela = "Lembrete", user_id = UsuarioLogado.Usuario.id, ip = HttpContext.Request.UserHostAddress.ToString(), data = DateTime.Now });

            return JsonRetono(resultado);
        }

        public JsonResult DetalheAgendamento(string id)
        {
            DateTime TempoInicial = DateTime.Now;
            Object resultado = new Object();

            var lembrete = _service.ListarLembretes(0, "", id).FirstOrDefault();

            resultado = new
            {
                cliente =lembrete.nome_cliente,
                agenda = lembrete.titulo,
                data_inicial =lembrete.data_inicio.Value.ToString("dd/MM/yyyy"),
                data_final = lembrete.data_final.Value.ToString("dd/MM/yyyy"),
                dica = lembrete.dica,
                filial = lembrete.filial,
                vendedor = lembrete.nomeVendedor,
                cpf = lembrete.cpf
            };

            DateTime TempoFinal = DateTime.Now;
            // Log do sistema
            LogSistema(new LogSistema { processo_minutos = TempoProcesso(TempoInicial, TempoFinal), acao = "Carregando Detalhe do Lembrete", login = UsuarioLogado.Usuario.email, tipo_log = "CARREGANDO", vendedor_id = (String.IsNullOrEmpty(UsuarioLogado.IdVendedorSeleconado) ? 0 : Convert.ToInt32(UsuarioLogado.IdVendedorSeleconado)), loja_id = (String.IsNullOrEmpty(UsuarioLogado.IdLojaSeleconado) ? 0 : Convert.ToInt32(UsuarioLogado.IdLojaSeleconado)), metodo = "Detalhe - Lembrete", tela = "Lembrete", user_id = UsuarioLogado.Usuario.id, ip = HttpContext.Request.UserHostAddress.ToString(), data = DateTime.Now });


            return JsonRetono(resultado);
        }


    }
}
