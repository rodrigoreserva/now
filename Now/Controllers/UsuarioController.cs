﻿using Now.Domain;
using Now.Models;
using Now.Domain.Contracts.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Now.Controllers
{
    public class UsuarioController : BaseController
    {
        public IVendedorService _serviceVendedor;
        public IUsuarioService _serviceUsuario;
        public IVendedoresLicenciadosService _serviceLicencaVendedores;
        public UsuarioController(IVendedorService serviceVendedor, IUsuarioService serviceUsuario, IVendedoresLicenciadosService serviceLicencaVendedores)
        {
            _serviceVendedor = serviceVendedor;
            _serviceUsuario = serviceUsuario;
            _serviceLicencaVendedores = serviceLicencaVendedores;
        }

        public ActionResult Index()
        {
            return View();
        }

        public JsonResult CarregarLoja(string idLoja)
        {
            UsuarioLogado.VendedoresUsuario = _serviceVendedor.GetVendedor(idLoja);

            _trocou = true;

            UsuarioLogado.IdLojaSeleconado = idLoja;
            UsuarioLogado.IdVendedorSeleconado = null;
            UsuarioLogado.CodigoClima = string.IsNullOrEmpty(idLoja) ? "241" : UsuarioLogado.LojasUsuario.Where(x => x.id.ToString() == idLoja).FirstOrDefault().codigo_clima.ToString();

            OrigemLoja = string.IsNullOrEmpty(idLoja) ? "" : UsuarioLogado.LojasUsuario.Where(x => x.id.ToString() == idLoja).FirstOrDefault().origem == "FRANQUIA" ? UsuarioLogado.LojasUsuario.Where(x => x.id.ToString() == idLoja).FirstOrDefault().origem : "VAREJO";

            LimparSessaoDashboard();

            ModificaSessao(UsuarioLogado);

            return JsonRetono(true);
        }

        public JsonResult AdicionarErro(string tela, string metodo, string exception, string dadosErro)
        {
            _serviceUsuario.AdicionarErro(metodo, tela, exception + dadosErro, (String.IsNullOrEmpty(UsuarioLogado.IdVendedorSeleconado) ? 0 : Convert.ToInt32(UsuarioLogado.IdVendedorSeleconado)), null, (String.IsNullOrEmpty(UsuarioLogado.IdLojaSeleconado) ? 0 : Convert.ToInt32(UsuarioLogado.IdLojaSeleconado)), UsuarioLogado.Usuario.id);

            return JsonRetono(true);
        }

        #region Guardar Acesso
        public JsonResult SalvarLogSistema()
        {
            if (_logSistema != null)
            {
                if (_logSistema.Count > 0)
                {
                    foreach (var item in _logSistema)
                        _serviceUsuario.AdicionarLogSistema(item);

                    _logSistema = null;
                }
            }

            return JsonRetono(true);
        }

        public JsonResult SalvarUsuario(string nomeUsuario, string login, string idLoja, string idVendedor, string aBloquearUsuario, string aResetar)
        {
            if (Session["usuarioLogin"] != null)
            {
                var usuario = Session["usuarioLogin"] as Usuario;

                usuario.acesso_bloqueado = Convert.ToInt32(aBloquearUsuario);

                if (aResetar == "0")
                {
                    usuario.password = "a8w6+tebmgiuKGXjPOToeA==";
                    usuario.recriar_senha = false;
                }

                usuario.loja_id = Convert.ToInt32(idLoja);
                usuario.vendedor_id = Convert.ToInt32(idVendedor);
                usuario.email = login;
                usuario.username = nomeUsuario;

                _serviceUsuario.UpdateUsuario(usuario, false);

                Session["usuarioLogin"] = usuario;
            }

            return JsonRetono(true);
        }


        public JsonResult Acoes(string idUsuario, string uBloqueado)
        {
            var usuario = _serviceUsuario.BuscarUsuario(idUsuario, "");

            usuario.acesso_bloqueado = Convert.ToInt32(uBloqueado);

            _serviceUsuario.UpdateUsuario(usuario, false);


            return JsonRetono(true);
        }
        #endregion

        #region PerfilNOW

        public ActionResult Perfil()
        {
            string idsLojas = UsuarioLogado.IdsLojasUsuario;

            ViewBag.ListaUsuario = _serviceUsuario.ListarUsuarios(idsLojas);

            return View();
        }

        public ActionResult PerfilDetalhe(Usuario usuario)
        {
            if (Request.QueryString["id"] != null)
            {
                if (!String.IsNullOrEmpty(Request.QueryString["id"].ToString()))
                {
                    var vendedor = _serviceVendedor.GetVendedor("", Request.QueryString["id"].ToString()).FirstOrDefault();

                    ViewBag.Vendedor = vendedor;

                    usuario = _serviceUsuario.BuscarUsuario("", Request.QueryString["id"].ToString());

                    if (usuario != null)
                    {
                        List<SelectListItem> items = new List<SelectListItem>();
                        var lisPerfis = _serviceUsuario.ListarPerfis();

                        List<SelectListItem> itemsControle = new List<SelectListItem>();
                        var listTipo = _serviceVendedor.ListarVendedoresControleTipo();

                        if (lisPerfis.Count() > 1)
                            itemsControle.Add(new SelectListItem() { Text = "Selecione...", Value = "", Selected = true });

                        if (listTipo.Count() > 1)
                            items.Add(new SelectListItem() { Text = "Selecione...", Value = "", Selected = true });

                        items.AddRange(lisPerfis.OrderBy(o => o.nome).Select(item => new SelectListItem
                        {
                            Text = item.nome,
                            Value = item.id_perfil.ToString()
                        }));

                        itemsControle.AddRange(listTipo.OrderBy(o => o.nome).Select(item => new SelectListItem
                        {
                            Text = item.nome,
                            Value = item.id.ToString()
                        }));

                        ViewBag.ListaPerfis = new SelectList(items, "Value", "Text", usuario.id_perfil);
                        ViewBag.ListaControle = new SelectList(itemsControle, "Value", "Text", "");

                        if (usuario.loja_id != null && usuario.loja_id != 0)
                            ViewBag.VendedorLojaSelect = _serviceVendedor.GetVendedor(usuario.loja_id.ToString());
                        else
                            ViewBag.VendedorLojaSelect = new List<Vendedor>();

                        @ViewBag.LojasTodas = _todasLojas;
                        @ViewBag.LojasUsuario = usuario.Lojas;

                        ViewBag.Usuario = usuario;
                    }
                    else
                        ViewBag.Usuario = new Usuario();

                    return View();
                }
                else
                    return RedirectToAction("Perfil");
            }
            else
                return RedirectToAction("Perfil");
        }

        public JsonResult SalvarConfiguracoes(string lojaId, string vendedorId, string cargo, string ativoNow)
        {
            VendedoresConfiguracao _configuracao = new VendedoresConfiguracao();

            _configuracao.loja_id = Convert.ToInt32(lojaId);
            _configuracao.vendedor_id = Convert.ToInt32(vendedorId);
            _configuracao.flg_gerente = (cargo == "GERENTE" ? 1 : 0);
            _configuracao.flg_vendedor = (cargo == "VENDEDOR" ? 1 : 0);
            _configuracao.flg_operador_caixa = (cargo == "CAIXA" ? 1 : 0);
            _configuracao.flg_bloqueado_now = Convert.ToInt32(ativoNow);

            _serviceUsuario.SalvaConfiguracoes(_configuracao);

            return JsonRetono(true);
        }

        public JsonResult SalvarLicenca(string lojaId, string vendedorId, string motivoId, string dataInicial, string dataFinal, string tbxObservacao, string flgIndeterminado)
        {
            try
            {

                VendedoresLicenciados licenca = new VendedoresLicenciados();
                licenca.loja_id = Convert.ToInt32(lojaId);
                licenca.vendedor_id = Convert.ToInt32(vendedorId);
                licenca.vendedor_licenciados_tipo_id = Convert.ToInt32(motivoId);

                if (Convert.ToInt32(flgIndeterminado) == 0)
                {
                    licenca.data_inicio_bloqueio = Convert.ToDateTime(dataInicial);
                    licenca.data_fim_bloqueio = Convert.ToDateTime(dataFinal);
                }

                licenca.data_inclusao = DateTime.Now;
                licenca.data_alteracao = DateTime.Now;
                licenca.motivo = tbxObservacao;
                licenca.flg_bloqueio_indeterminado = Convert.ToInt32(flgIndeterminado);
                licenca.data_desativacao = null;

                _serviceLicencaVendedores.Salvar(licenca);
                var _list = _serviceLicencaVendedores.Listar(lojaId, vendedorId);

                return Json(new { success = true, msg = "Licença salva com sucesso!", dados = _list });

            }
            catch (Exception ex)
            {
                return Json(new { success = false, msg = "Erro ao salvar a licença!" });
            }
        }

        public JsonResult ListarMotivos()
        {
            try
            {
                var listTipo = _serviceLicencaVendedores.ListarMotivos();

                return Json(new { data = listTipo, success = true, msg = "Licença salva com sucesso!" });
            }
            catch (Exception ex)
            {
                return Json(new { success = false, msg = "Erro ao salvar a licença!" });
            }
        }

        public JsonResult ListarLicencaVendedores(string lojaId, string vendedorId)
        {
            try
            {
                var _list = _serviceLicencaVendedores.Listar(lojaId, vendedorId);

                return Json(new { dados = _list });
            }
            catch (Exception ex)
            {
                return Json(new { success = false, msg = "Erro ao salvar a licença!" });
            }
        }

        public JsonResult DesativarLicencaVendedores(string idLicenca)
        {
            try
            {
                _serviceLicencaVendedores.Deletar(Convert.ToInt32(idLicenca));

                return Json(new { success = true, msg = "Licença excluída com sucesso!" });
            }
            catch (Exception ex)
            {
                return Json(new { success = false, msg = "Erro ao excluir a licença!" });
            }
        }

        public JsonResult BuscarAgendaDistribuidas(string vendedorId)
        {
            try
            {
                var _list = _serviceLicencaVendedores.BuscarAgendaDistribuidas(Convert.ToInt32(vendedorId));

                return Json(new { dados = _list });
            }
            catch (Exception ex)
            {
                return Json(new { success = false, msg = "Erro ao salvar a licença!" });
            }
        }

        public JsonResult DistribuirAgendas(string vendedorId)
        {
            try
            {
                _serviceLicencaVendedores.DistribuirClientes(Convert.ToInt32(vendedorId));

                return Json(new { success = true, msg = "Todos os Clientes foram redistribuídos para os vendedores da loja!" });
            }
            catch (Exception ex)
            {
                return Json(new { success = false, msg = "Erro ao salvar a licença!" });
            }
        }

        #endregion PerfilNOW
    }
}
