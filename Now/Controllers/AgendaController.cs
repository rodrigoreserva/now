﻿using Now.Domain;
using Now.Models;
using Now.Domain.Contracts.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;

namespace Now.Controllers
{
    public class AgendaController : BaseController
    {
        private IAgendaService _service;
        public AgendaController(IAgendaService service)
        {
            _service = service;
        }
        //
        // GET: /Agenda/

        public ActionResult Index()
        {
            DateTime TempoInicial = DateTime.Now;

            var verificarLembrete = false;
            var loja = UsuarioLogado.LojasUsuario.Where(x => x.tipo_loja == "EVA");
            verificarLembrete = loja.Count() > 0;

            @ViewBag.ativarNovaTelaVendedor = Session["ativarNovaTelaVendedor"] == null ? false : Session["ativarNovaTelaVendedor"];
            
            var agenda = CarregarAgendas(verificarLembrete);

            DateTime TempoFinal = DateTime.Now;

            // Log do sistema
            LogSistema(new LogSistema { processo_minutos = TempoProcesso(TempoInicial, TempoFinal), acao = "Acesso Tela Principal de Agenda", login = UsuarioLogado.Usuario.email, tipo_log = "NAVEGACAO", vendedor_id = (String.IsNullOrEmpty(UsuarioLogado.IdVendedorSeleconado) ? 0 : Convert.ToInt32(UsuarioLogado.IdVendedorSeleconado)), loja_id = (String.IsNullOrEmpty(UsuarioLogado.IdLojaSeleconado) ? 0 : Convert.ToInt32(UsuarioLogado.IdLojaSeleconado)), metodo = "Index - Agenda", tela = "Agenda", user_id = UsuarioLogado.Usuario.id, ip = HttpContext.Request.UserHostAddress.ToString(), data = DateTime.Now });

            return View("Index.min", agenda);
        }

        public ActionResult Agenda(string idAgenda, string parametro, string titulo)
        {
            DateTime TempoInicial = DateTime.Now;

            //var agendas = CarregarAgenda(idAgenda, "");
            @ViewBag.TituloAgenda = titulo;
            @ViewBag.idAgenda = Convert.ToInt32(idAgenda);
            @ViewBag.AgendaTipo = UsuarioLogado.TiposAgendas.Where(x => x.id == Convert.ToInt32(idAgenda)).FirstOrDefault();

            DateTime TempoFinal = DateTime.Now;

            @ViewBag.ativarNovaTelaVendedor = Session["ativarNovaTelaVendedor"] == null ? false : Session["ativarNovaTelaVendedor"];

            // Log do sistema
            LogSistema(new LogSistema { processo_minutos = TempoProcesso(TempoInicial, TempoFinal), acao = "Acesso Tela de Agenda: " + titulo + " - parametro: " + parametro, login = UsuarioLogado.Usuario.email, tipo_log = "NAVEGACAO", vendedor_id = (String.IsNullOrEmpty(UsuarioLogado.IdVendedorSeleconado) ? 0 : Convert.ToInt32(UsuarioLogado.IdVendedorSeleconado)), loja_id = (String.IsNullOrEmpty(UsuarioLogado.IdLojaSeleconado) ? 0 : Convert.ToInt32(UsuarioLogado.IdLojaSeleconado)), metodo = "Agenda - Agenda", tela = "Agenda", user_id = UsuarioLogado.Usuario.id, ip = HttpContext.Request.UserHostAddress.ToString(), data = DateTime.Now });

            return View();

            //View("Agenda", agendas);
        }

        public IList<MenuAgendamento> CarregarAgendas(bool agendaLembreteAtiva)
        {
            try
            {
                return _service.TypeCalendar(UsuarioLogado.Usuario, UsuarioLogado.Usuario.id.ToString(), UsuarioLogado.IdsLojasUsuario, UsuarioLogado.IdVendedorSeleconado, "", agendaLembreteAtiva);
            }
            catch (Exception ex)
            {
                MsgErro();
            }

            return new List<MenuAgendamento>();
        }

        public IList<Agendamento> CarregarAgenda(string idAgenda, string strBusca)
        {
            try
            {
                return _service.FindCalendars(idAgenda, strBusca, UsuarioLogado.Usuario.id.ToString(), UsuarioLogado.IdsLojasUsuario, UsuarioLogado.IdVendedorSeleconado);
            }
            catch (Exception)
            {
                MsgErro();
            }

            return new List<Agendamento>();
        }

        public JsonResult BuscarAgendas()
        {
            return JsonRetono(CarregarAgendas(false));
        }

        public ActionResult Pesquisar(Agendamento agenda)
        {
            try
            {
                var agendas = CarregarAgenda(agenda.tipo_agendamento_id.ToString(), agenda.nome);

                return PartialView("_DadosAgenda", agendas);
            }
            catch (Exception ex)
            {
                return PartialView("_DadosAgenda", new List<Agendamento>());
            }
        }
    }
}
