﻿using Now.Domain;
using Now.Models;
using Now.Domain.Contracts.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Now.Controllers
{
    public class SharedController : Controller
    {
        private IContatoService _serviceContato;
        private IAgendaService _service;
        
        public SharedController(IContatoService serviceContato, IAgendaService service)
        {
            _serviceContato = serviceContato;
            _service = service;
            
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult _Layout()
        {         

            return View();
        }


        public JsonResult BuscarContatoCliente(string telefone)
        {
            var _contato = _serviceContato.BuscarContatoCliente(telefone);

            if (_contato == null)
                _contato = _serviceContato.BuscarCliente(telefone);

            return JsonRetono(new { success = true, contato = _contato });
        }

        public JsonResult JsonRetono(object data)
        {
            JsonResult jsonResult = Json(data, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = int.MaxValue;
            return jsonResult;
        }

        public IList<MenuAgendamento> CarregarAgenda()
        {
            Usuario usuario = Session["Usuario"] as Usuario;

            var agendas = _service.TypeCalendar(usuario, usuario.id.ToString(), "", "", "");

            return agendas;
        }

    }
}
