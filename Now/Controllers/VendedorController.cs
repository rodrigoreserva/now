﻿using Now.Domain;
using Now.Models;
using Now.Domain.Contracts.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Now.Controllers.DTO;

namespace Now.Controllers
{
    public class VendedorController : BaseController
    {
        private IDashboardService _serviceDashBoard;
        private IUsuarioService _serviceUsuario;
        private IAgendaService _service;
        public IVendedorService _serviceVendedor;
        public VendedorController(IVendedorService serviceVendedor, IAgendaService service, IUsuarioService serviceUsuario, IDashboardService serviceDashBoard)
        {
            _serviceUsuario = serviceUsuario;
            _service = service;
            _serviceVendedor = serviceVendedor;
            _serviceDashBoard = serviceDashBoard;
        }

        public ActionResult Index()
        {
            ViewBag.ListaVendedores = _serviceVendedor.ListarVendedores(UsuarioLogado.IdLojaSeleconado);

            return View();
        }

        public ActionResult VendedorPerfil(string id)
        {
            //ViewBag.ListaVendedores = _serviceVendedor.ListarVendedores(UsuarioLogado.IdLojaSeleconado);

            var _vendedor = _serviceVendedor.ListarVendedores("", id);

            return View(_vendedor);
        }

        public ActionResult DashBoard(string id)
        {
            ViewBag.ativarNovaTelaVendedor = true;
            if (VendedorDashboard == null)
                VendedorDashboard = ObterVendedor(id);

            return View(VendedorDashboard);
        }

        public VendedorDashBoardDTO ObterVendedor(string id)
        {
            var vendedorDashBoardDTO = new VendedorDashBoardDTO();

            var _vendedor = _serviceVendedor.ListarVendedores("", id);
            var usuario = _serviceUsuario.BuscarUsuarioPorCPF(_vendedor.FirstOrDefault().cpf.TrimEnd());

            var usuariologado = new UsuarioLogado();
            usuariologado.Usuario = usuario;

            IList<MenuAgendamento> agenda = new List<MenuAgendamento>();
            if (usuario.Lojas != null)
            {
                usuariologado.IdLojaSeleconado = usuario.Lojas.FirstOrDefault().id.ToString();
                agenda = _service.TypeCalendar(usuario, usuario.id.ToString(), usuariologado.IdsLojasUsuario, id, "");
                foreach (var item in agenda)
                {
                    vendedorDashBoardDTO.QtdLigacaoesDisponiveis += item.qtdeDisponivel;
                    vendedorDashBoardDTO.QtdLigacaoesFeitas += item.qtdeLigacoes;
                }
            }

            Session["ativarNovaTelaVendedor"] = true;
            @ViewBag.ativarNovaTelaVendedor = Session["ativarNovaTelaVendedor"];

            vendedorDashBoardDTO.nome = _vendedor.FirstOrDefault().nome;
            vendedorDashBoardDTO.cpf = _vendedor.FirstOrDefault().cpf;
            @ViewBag.img_perfil = _vendedor.FirstOrDefault().img_perfil;
            vendedorDashBoardDTO.img_perfil_intranet = _vendedor.FirstOrDefault().img_perfil_intranet;
            @ViewBag.img_perfil_intranet = _vendedor.FirstOrDefault().img_perfil_intranet;

            vendedorDashBoardDTO.agendamentos = agenda;

            return vendedorDashBoardDTO;
        }

        public JsonResult GetVendedores(string id)
        {
            if (String.IsNullOrEmpty(UsuarioLogado.IdVendedorSeleconado))
            {
                List<SelectListItem> items = new List<SelectListItem>();

                items.Add(new SelectListItem { Selected = true, Text = "Selecione...", Value = "" });

                if (id != "")
                {
                    var list = _serviceVendedor.GetVendedor(id);

                    items.AddRange(list.OrderBy(o => o.nome).Select(item => new SelectListItem
                    {
                        Text = item.nome,
                        Value = item.id.ToString()
                    }));
                }

                return JsonRetono(new SelectList(items, "Value", "Text"));
            }
            else
            {
                List<SelectListItem> items = new List<SelectListItem>();

                if (!String.IsNullOrEmpty(UsuarioLogado.IdVendedorSeleconado))
                {
                    items = UsuarioLogado.VendedoresUsuario.Where(x => x.id == Convert.ToInt32(UsuarioLogado.IdVendedorSeleconado)).ToList().Select(item => new SelectListItem
                    {
                        Text = item.nome,
                        Value = item.id.ToString()
                    }).ToList();
                }
                else
                {
                    items.Add(new SelectListItem() { Selected = true, Text = "Selecione...", Value = "" });

                    items = UsuarioLogado.VendedoresUsuario.ToList().Select(item => new SelectListItem
                    {
                        Text = item.nome,
                        Value = item.id.ToString()
                    }).ToList();
                }

                return JsonRetono(new SelectList(items, "Value", "Text", UsuarioLogado.IdVendedorSeleconado));
            }
        }

        public JsonResult SalvarControleVendedor(string loja, string vendedor, string tipoControle, string periodoControleIni, string periodoControleFim)
        {
            VendedoresControle controle = new VendedoresControle()
            {
                data_inclusao = DateTime.Now,
                loja_id = Convert.ToInt32(loja),
                vendedor_id = Convert.ToInt32(vendedor),
                vendedores_controle_tipo_id = Convert.ToInt32(tipoControle),
                vigencia_inicio = Convert.ToDateTime(periodoControleIni),
                vigencia_fim = Convert.ToDateTime(periodoControleFim)
            };

            var _listControle = _serviceVendedor.SalvarVendedoresControle(controle);

            return JsonRetono(new { sucesso = true, listControle = _listControle });
        }

    }
}
