﻿using Now.Domain;
using Now.Models;
using Now.Domain.Contracts.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Now.Controllers.DTO;
using Now.Domain.DTO;
using FireSharp.Config;
using FireSharp.Interfaces;
using FireSharp;

namespace Now.Controllers
{
    public class InfoVendasController : BaseController
    {
        public IClienteService _serviceCliente;
        public IInfoVendasService _serviceInfoVendas;
        public ILojaService _serviceLoja;
        private IVendaService _serviceVenda;
        public IVendedorService _serviceVendedor;
        public IAgendaService _serviceAgenda;
        public IUsuarioService _serviceUsuario;
        public InfoVendasController(IClienteService serviceCliente
                                  , IInfoVendasService serviceInfoVendas
                                  , ILojaService serviceLoja
                                  , IVendaService serviceVenda
                                  , IVendedorService serviceVendedor
                                  , IAgendaService serviceAgenda
                                  , IUsuarioService serviceUsuario)
        {
            _serviceCliente = serviceCliente;
            _serviceInfoVendas = serviceInfoVendas;
            _serviceVenda = serviceVenda;
            _serviceLoja = serviceLoja;
            _serviceVendedor = serviceVendedor;
            _serviceAgenda = serviceAgenda;

            _serviceUsuario = serviceUsuario;
        }


        public ActionResult Index(string codigo)
        {
            @ViewBag.ativarNovaTelaVendedor = Session["ativarNovaTelaVendedor"] == null ? false : Session["ativarNovaTelaVendedor"];

            DateTime TempoInicial = DateTime.Now;

            InfoVendas infoVendas = new InfoVendas()
            {
                filial = "",
                nome_cliente = "-",
                vendedor = ""
            };

            List<SelectListItem> items = new List<SelectListItem>();
            items.Add(new SelectListItem() { Text = "Selecione...", Value = "", Selected = true });
            SelectList listaVendas = new SelectList(items, "Value", "Text", "");
            ViewBag.DataVendaDefault = "";

            if (!string.IsNullOrEmpty(codigo))
            {
                infoVendas = _serviceInfoVendas.BuscarInfoVendas(Convert.ToInt32(codigo));

                listaVendas = ClienteVendas(infoVendas.cpf, infoVendas.loja_id.ToString(), _serviceCliente, _serviceVenda, infoVendas.data_registro.ToString("dd/MM/yyyy"));
            }

            ViewBag.DataVendas = listaVendas;

            ViewBag.Lojas = new SelectList(BuscarLojas(), "id", "filial", UsuarioLogado.IdLojaSeleconado);

            ViewBag.ListaInfoVendas = ListarInfoVendas();

            ViewBag.ListaClienteNovo = ListarClientesNovos();

            DateTime TempoFinal = DateTime.Now;
            // Log do sistema
            LogSistema(new LogSistema { processo_minutos = TempoProcesso(TempoInicial, TempoFinal), acao = "Acesso Tela de Info Vendas", login = UsuarioLogado.Usuario.email, tipo_log = "NAVEGACAO", vendedor_id = (String.IsNullOrEmpty(UsuarioLogado.IdVendedorSeleconado) ? 0 : Convert.ToInt32(UsuarioLogado.IdVendedorSeleconado)), loja_id = (String.IsNullOrEmpty(UsuarioLogado.IdLojaSeleconado) ? 0 : Convert.ToInt32(UsuarioLogado.IdLojaSeleconado)), metodo = "Index - Info Vendas", tela = "Info Vendas", user_id = UsuarioLogado.Usuario.id, ip = HttpContext.Request.UserHostAddress.ToString(), data = DateTime.Now });

            return View(infoVendas);
        }

        public ActionResult Incluir(InfoVendas infoVendas)
        {

            try
            {
                @ViewBag.ativarNovaTelaVendedor = Session["ativarNovaTelaVendedor"] == null ? false : Session["ativarNovaTelaVendedor"];

                DateTime TempoInicial = DateTime.Now;
                // Insere um novo

                var cliente = new Cliente();

                if (Session["cliente"] == null)
                    cliente = _serviceCliente.BuscarClienteCpfOrigemLoja(OrigemLoja, infoVendas.cpf).FirstOrDefault();
                else
                {
                    if ((Session["cliente"] as Cliente).cpf == infoVendas.cpf)
                        cliente = (Session["cliente"] as Cliente);
                    else
                        cliente = _serviceCliente.BuscarClienteCpfOrigemLoja(OrigemLoja, infoVendas.cpf).FirstOrDefault();
                }

                if (cliente != null)
                {
                    infoVendas.status = 1;
                    infoVendas.created_at = DateTime.Now;
                    infoVendas.updated_at = DateTime.Now;
                    infoVendas.deleted_at = null;
                    infoVendas.data_registro = infoVendas.data_registro;
                    infoVendas.cliente_id = cliente.id;
                    infoVendas.loja_id = String.IsNullOrEmpty(infoVendas.loja_id.ToString()) || infoVendas.loja_id.ToString() == "0" ? Convert.ToInt64(UsuarioLogado.IdLojaSeleconado) : Convert.ToInt64(infoVendas.loja_id.ToString());
                    infoVendas.vendedor_id = String.IsNullOrEmpty(infoVendas.vendedor_id.ToString()) || infoVendas.vendedor_id.ToString() == "0" ? Convert.ToInt64(UsuarioLogado.IdVendedorSeleconado) : Convert.ToInt64(infoVendas.vendedor_id.ToString());

                    _serviceInfoVendas.SalvarInfoVenda(infoVendas);
                }
                DateTime TempoFinal = DateTime.Now;
                // Log do sistema
                LogSistema(new LogSistema { processo_minutos = TempoProcesso(TempoInicial, TempoFinal), acao = "Tela Cadastro InfoVendas: ---- Dados: Info - " + infoVendas.info, login = UsuarioLogado.Usuario.email, cliente_id = cliente.id, tipo_log = "CADASTRANDO", vendedor_id = (String.IsNullOrEmpty(UsuarioLogado.IdVendedorSeleconado) ? 0 : Convert.ToInt32(UsuarioLogado.IdVendedorSeleconado)), loja_id = (String.IsNullOrEmpty(UsuarioLogado.IdLojaSeleconado) ? 0 : Convert.ToInt32(UsuarioLogado.IdLojaSeleconado)), metodo = "Incluir - Info Vendas", tela = "Info Vendas", user_id = UsuarioLogado.Usuario.id, ip = HttpContext.Request.UserHostAddress.ToString(), data = DateTime.Now });

                AtualizarInfoVendasFirebase(infoVendas.vendedor_id.ToString(), infoVendas.loja_id.ToString());

                VendedorDashboard = ObterVendedor(infoVendas.vendedor_id.ToString());

                MsgSucesso();
            }
            catch (Exception ex)
            {
                //var serviceErro = new SecurityService();
                //serviceErro.AddErro("btnIncluir_Click", "Modulos.InfoVendas", ex.Message, (String.IsNullOrEmpty(IdVendedor) ? 0 : Convert.ToInt32(IdVendedor)), null, (String.IsNullOrEmpty(IdLoja) ? 0 : Convert.ToInt32(IdLoja)), UsuarioLogado.usuario.id);

                //MsgBox(Page, ex.Message, "Ocorreu um erro!", PageBase.TypeMensage.error);
                MsgErro();
            }

            return RedirectToAction("Index");
        }

        public void AtualizarInfoVendasFirebase(string vendedor_id, string loja_id)
        {
            var infoVendas = _serviceVenda.ObterInfoVendas(vendedor_id, loja_id, DateTime.Now.ToString("yyyy-MM-dd"));

            if (infoVendas == null) return;

            IFirebaseConfig config = new FirebaseConfig
            {
                AuthSecret = "q7gMXehEBRZ6WuXscJNLjB9p4OV4JSdgOe62dNVJ",
                BasePath = "https://now-web-cac79.firebaseio.com/"
            };
            IFirebaseClient client = new FirebaseClient(config);

            client.Set("lojas/" + loja_id + "/" + vendedor_id + "/qtd_info_vendas_disponiveis/", infoVendas.qtd_info_vendas_disponiveis);
            client.Set("lojas/" + loja_id + "/" + vendedor_id + "/qtd_info_vendas_feitos/", infoVendas.qtd_info_vendas_feitos);
        }

        public VendedorDashBoardDTO ObterVendedor(string id)
        {
            var vendedorDashBoardDTO = new VendedorDashBoardDTO();

            var _vendedor = _serviceVendedor.ListarVendedores("", id);
            var usuario = _serviceUsuario.BuscarUsuarioPorCPF(_vendedor.FirstOrDefault().cpf.TrimEnd());

            var usuariologado = new UsuarioLogado();
            usuariologado.Usuario = usuario;

            IList<MenuAgendamento> agenda = new List<MenuAgendamento>();
            if (usuario.Lojas != null)
            {
                usuariologado.IdLojaSeleconado = usuario.Lojas.FirstOrDefault().id.ToString();
                agenda = _serviceAgenda.TypeCalendar(usuario, usuario.id.ToString(), usuariologado.IdsLojasUsuario, id, "");
                foreach (var item in agenda)
                {
                    vendedorDashBoardDTO.QtdLigacaoesDisponiveis += item.qtdeDisponivel;
                    vendedorDashBoardDTO.QtdLigacaoesFeitas += item.qtdeLigacoes;
                }
            }

            Session["ativarNovaTelaVendedor"] = true;
            @ViewBag.ativarNovaTelaVendedor = Session["ativarNovaTelaVendedor"];

            vendedorDashBoardDTO.nome = _vendedor.FirstOrDefault().nome;
            vendedorDashBoardDTO.cpf = _vendedor.FirstOrDefault().cpf;
            @ViewBag.img_perfil = _vendedor.FirstOrDefault().img_perfil;
            vendedorDashBoardDTO.img_perfil_intranet = _vendedor.FirstOrDefault().img_perfil_intranet;
            @ViewBag.img_perfil_intranet = _vendedor.FirstOrDefault().img_perfil_intranet;

            vendedorDashBoardDTO.agendamentos = agenda;

            return vendedorDashBoardDTO;
        }

        public ActionResult Excluir(string codigo)
        {

            try
            {
                @ViewBag.ativarNovaTelaVendedor = Session["ativarNovaTelaVendedor"] == null ? false : Session["ativarNovaTelaVendedor"];

                DateTime TempoInicial = DateTime.Now;
                int id = Convert.ToInt32(codigo);

                _serviceInfoVendas.DeleteInfoVenda(id);

                DateTime TempoFinal = DateTime.Now;
                // Log do sistema
                LogSistema(new LogSistema { processo_minutos = TempoProcesso(TempoInicial, TempoFinal), acao = "Tela Cadastro InfoVendas: ---- Dados: idInfo - " + id, login = UsuarioLogado.Usuario.email, tipo_log = "EXLUINDO", vendedor_id = (String.IsNullOrEmpty(UsuarioLogado.IdVendedorSeleconado) ? 0 : Convert.ToInt32(UsuarioLogado.IdVendedorSeleconado)), loja_id = (String.IsNullOrEmpty(UsuarioLogado.IdLojaSeleconado) ? 0 : Convert.ToInt32(UsuarioLogado.IdLojaSeleconado)), metodo = "Excluir - Info vendas", tela = "Info Vendas", user_id = UsuarioLogado.Usuario.id, ip = HttpContext.Request.UserHostAddress.ToString(), data = DateTime.Now });

                MsgSucesso();
            }
            catch (Exception ex)
            {
                //var serviceErro = new SecurityService();
                //serviceErro.AddErro("btnIncluir_Click", "Modulos.InfoVendas", ex.Message, (String.IsNullOrEmpty(IdVendedor) ? 0 : Convert.ToInt32(IdVendedor)), null, (String.IsNullOrEmpty(IdLoja) ? 0 : Convert.ToInt32(IdLoja)), UsuarioLogado.usuario.id);

                //MsgBox(Page, ex.Message, "Ocorreu um erro!", PageBase.TypeMensage.error);
                MsgErro();
            }

            return RedirectToAction("Index");
        }

        private IList<InfoVendas> ListarInfoVendas()
        {
            try
            {
                @ViewBag.ativarNovaTelaVendedor = Session["ativarNovaTelaVendedor"] == null ? false : Session["ativarNovaTelaVendedor"];

                return _serviceInfoVendas.ListarInfoVendas(UsuarioLogado.Usuario.id, UsuarioLogado.IdLojaSeleconado, UsuarioLogado.IdVendedorSeleconado);
            }
            catch (Exception ex)
            {
                MsgErro();
                return new List<InfoVendas>();
            }
        }

        private IList<ClienteNovo> ListarClientesNovos()
        {
            try
            {
                @ViewBag.ativarNovaTelaVendedor = Session["ativarNovaTelaVendedor"] == null ? false : Session["ativarNovaTelaVendedor"];

                return _serviceCliente.BuscarClienteNovo(UsuarioLogado.IdLojaSeleconado, UsuarioLogado.IdVendedorSeleconado, "");
            }
            catch (Exception ex)
            {
                MsgErro();
                return new List<ClienteNovo>();
            }
        }

        private IList<Loja> BuscarLojas()
        {
            @ViewBag.ativarNovaTelaVendedor = Session["ativarNovaTelaVendedor"] == null ? false : Session["ativarNovaTelaVendedor"];
            //return _serviceLoja.ListarLojasVisiveisPorUsuario(clienteID, UsuarioLogado.usuario.id);
            return _serviceLoja.ListarLojasVisiveisPorUsuario(UsuarioLogado.Usuario.id, UsuarioLogado.IdLojaSeleconado);
        }

        public JsonResult ExluirInfoVendas(string clienteId, string data, string LojaId)
        {
            try
            {
                @ViewBag.ativarNovaTelaVendedor = Session["ativarNovaTelaVendedor"] == null ? false : Session["ativarNovaTelaVendedor"];

                if (string.IsNullOrEmpty(clienteId))
                {
                    if (Session["_clienteId"] != null)
                    {
                        // Salvar o info vendas
                        clienteId = ((Cliente)Session["_clienteId"]).id.ToString();
                    }
                }

                DateTime TempoInicial = DateTime.Now;

                if (!String.IsNullOrWhiteSpace(clienteId))
                {
                    var infoVendas = _serviceInfoVendas.BuscarInfoVendasPorCliente(Convert.ToInt32(clienteId), Convert.ToDateTime(data), Convert.ToInt32(LojaId));

                    _serviceInfoVendas.DeleteInfoVenda(infoVendas.id);

                    DateTime TempoFinal = DateTime.Now;
                    // Log do sistema
                    LogSistema(new LogSistema { processo_minutos = TempoProcesso(TempoInicial, TempoFinal), acao = "DELETA INFO VENDAS: ---- Dados: Info - " + infoVendas.info, login = UsuarioLogado.Usuario.email, cliente_id = Convert.ToInt32(clienteId), tipo_log = "DELETANDO", vendedor_id = (String.IsNullOrEmpty(UsuarioLogado.IdVendedorSeleconado) ? 0 : Convert.ToInt32(UsuarioLogado.IdVendedorSeleconado)), loja_id = (String.IsNullOrEmpty(UsuarioLogado.IdLojaSeleconado) ? 0 : Convert.ToInt32(UsuarioLogado.IdLojaSeleconado)), metodo = "Excluir - Info Vendas", tela = "Info Vendas", user_id = UsuarioLogado.Usuario.id, ip = HttpContext.Request.UserHostAddress.ToString(), data = DateTime.Now });

                }

                return Json(new { success = true, Mensagem = "Info Vendas excluído com sucesso!" });

            }
            catch (Exception ex)
            {
                return Json(new { success = false, Mensagem = "Erro ao exluir o Info Vendas!" });
            }
        }

        public JsonResult SalvarInfoVendas(string clienteId, string strInfovendas, string LojaId, string dataVenda, string IdVendedor)
        {
            try
            {
                @ViewBag.ativarNovaTelaVendedor = Session["ativarNovaTelaVendedor"] == null ? false : Session["ativarNovaTelaVendedor"];
                if (string.IsNullOrEmpty(clienteId))
                {
                    if (Session["_clienteId"] != null)
                    {
                        // Salvar o info vendas
                        clienteId = ((Cliente)Session["_clienteId"]).id.ToString();
                    }
                }

                DateTime TempoInicial = DateTime.Now;

                if (!String.IsNullOrWhiteSpace(clienteId))
                {
                    var infoVendas = _serviceInfoVendas.BuscarInfoVendasPorCliente(Convert.ToInt32(clienteId), Convert.ToDateTime(dataVenda), Convert.ToInt32(LojaId));

                    if (infoVendas != null)
                    {
                        infoVendas.info = strInfovendas;
                        infoVendas.data_registro = Convert.ToDateTime(Convert.ToDateTime(dataVenda).ToString("dd/MM/yyyy"));
                        infoVendas.updated_at = DateTime.Now;
                        infoVendas.cliente_id = Convert.ToInt32(clienteId);
                        infoVendas.loja_id = Convert.ToInt32(LojaId);
                        infoVendas.vendedor_id = String.IsNullOrEmpty(infoVendas.vendedor_id.ToString()) || infoVendas.vendedor_id.ToString() == "0" ? Convert.ToInt64(UsuarioLogado.IdVendedorSeleconado) : Convert.ToInt64(infoVendas.vendedor_id.ToString());

                    }
                    else
                    {
                        infoVendas = new InfoVendas();
                        infoVendas.info = strInfovendas;
                        infoVendas.data_registro = Convert.ToDateTime(Convert.ToDateTime(dataVenda).ToString("dd/MM/yyyy"));
                        infoVendas.updated_at = DateTime.Now;
                        infoVendas.status = 1;
                        infoVendas.created_at = DateTime.Now;
                        infoVendas.deleted_at = null;
                        infoVendas.cliente_id = Convert.ToInt32(clienteId);
                        infoVendas.loja_id = Convert.ToInt32(LojaId);
                        infoVendas.vendedor_id = Convert.ToInt64(IdVendedor);
                    }

                    _serviceInfoVendas.SalvarInfoVenda(infoVendas);

                    DateTime TempoFinal = DateTime.Now;
                    // Log do sistema
                    LogSistema(new LogSistema { processo_minutos = TempoProcesso(TempoInicial, TempoFinal), acao = "Tela Cadastro InfoVendas: ---- Dados: Info - " + infoVendas.info, login = UsuarioLogado.Usuario.email, cliente_id = Convert.ToInt32(clienteId), tipo_log = "CADASTRANDO", vendedor_id = (String.IsNullOrEmpty(UsuarioLogado.IdVendedorSeleconado) ? 0 : Convert.ToInt32(UsuarioLogado.IdVendedorSeleconado)), loja_id = (String.IsNullOrEmpty(UsuarioLogado.IdLojaSeleconado) ? 0 : Convert.ToInt32(UsuarioLogado.IdLojaSeleconado)), metodo = "Incluir - Info Vendas", tela = "Info Vendas", user_id = UsuarioLogado.Usuario.id, ip = HttpContext.Request.UserHostAddress.ToString(), data = DateTime.Now });

                }

                return Json(new { success = true, Mensagem = "Info Vendas salvo com sucesso!" });

            }
            catch (Exception ex)
            {
                return Json(new { success = false, Mensagem = "Erro ao salvar o Info Vendas!" });
            }
        }

        public JsonResult GetVendedores(string id)
        {
            @ViewBag.ativarNovaTelaVendedor = Session["ativarNovaTelaVendedor"] == null ? false : Session["ativarNovaTelaVendedor"];
            List<SelectListItem> items = new List<SelectListItem>();

            items.Add(new SelectListItem { Selected = true, Text = "Senecione", Value = "0" });

            if (id != "")
            {
                var list = _serviceVendedor.GetVendedor(id);

                items.AddRange(list.OrderBy(o => o.nome).Select(item => new SelectListItem
                {
                    Text = item.nome,
                    Value = item.id.ToString()
                }));
            }

            return JsonRetono(new SelectList(items, "Value", "Text"));
        }

        public JsonResult ListaClientesNovos()
        {
            var lista = ListarClientesNovos();

            return JsonRetono(lista);
        }

        public JsonResult ListaInfoVendas(string lojaId, string vendedorId, string dataInicio, string dataFim)
        {
            try
            {
                @ViewBag.ativarNovaTelaVendedor = Session["ativarNovaTelaVendedor"] == null ? false : Session["ativarNovaTelaVendedor"];
                var lista = _serviceInfoVendas.ListarInfoVendas(0, lojaId, vendedorId);

                lista = lista.Where(x => x.data_registro >= Convert.ToDateTime(dataInicio) && x.data_registro <= Convert.ToDateTime(dataFim)).ToList();

                return JsonRetono(new { success = true, lista = lista });
            }
            catch (Exception ex)
            {
                MsgErro();
                return JsonRetono(new { success = false });
            }
        }



    }
}
