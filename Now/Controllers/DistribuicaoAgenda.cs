﻿using Now.Domain;
using Now.Models;
using Now.Domain.Contracts.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Net.Mail;
using System.Net;

namespace Now.Controllers
{
    public class DistribuicaoAgendaController : BaseController
    {
        public IDistribuicaoAgendaService _serviceDistribuicaoAgenda;
        private IAgendaService _agenda;
        private IUsuarioService _serviceUsuario;
        public DistribuicaoAgendaController(IDistribuicaoAgendaService serviceDistribuicaoAgenda, IAgendaService agenda, IUsuarioService serviceUsuario)
        {
            _serviceDistribuicaoAgenda = serviceDistribuicaoAgenda;
            _agenda = agenda;
            _serviceUsuario = serviceUsuario;
        }

        public ActionResult Index()
        {
            //var list = CarregarDistribuicaoAgenda("P", "V");
            //ViewBag.ListaDistribuicaoAgendaAprov = CarregarDistribuicaoAgenda("A", "V");
            //ViewBag.ListaDistribuicaoAgendaTopInativos = CarregarDistribuicaoAgenda("A", "G");

            ViewBag.Agendas = _agenda.FindCalendars("","", UsuarioLogado.Usuario.id.ToString(), UsuarioLogado.IdsLojasUsuario, "");
            ViewBag.EmailGerente = UsuarioLogado.Usuario.email;

            return View();
        }

        public JsonResult SalvarLogDistribuicao(string idLog, string clienteId, string agendaId, string vendedorIdOrigem, string vendedorIdDestino)
        {
            try
            {
                LogTransferenciaAgenda entity = new LogTransferenciaAgenda();

                if (!String.IsNullOrEmpty(idLog))
                {
                    entity.data_cancelamento = DateTime.Now;
                }

                entity.vendedor_id_origem = Convert.ToInt32(vendedorIdOrigem);
                entity.vendedor_id_destino = Convert.ToInt32(vendedorIdDestino);
                entity.cliente_id = Convert.ToInt32(clienteId);
                entity.loja_id = Convert.ToInt32(UsuarioLogado.IdLojaSeleconado);
                entity.tipo_agendamento_id = Convert.ToInt32(agendaId);
                entity.data_transferencia = DateTime.Now;
                entity.vendedor_id_transferencia = UsuarioLogado.Usuario.id;

                _serviceDistribuicaoAgenda.SalvarLogTransferencia(entity);               

                // Falta Transferir na Agenda 

                var list = _serviceDistribuicaoAgenda.ListarLogDistribuicao(UsuarioLogado.IdLojaSeleconado);

                return JsonRetono(new { success = true, dados = list, mensagem = "Cliente transferido com sucesso!" });
            }
            catch (Exception ex)
            {
                return JsonRetono(new { success = false, mensagem = "Erro ao transferir o cliente! " + ex.Message });
            }
        }

        public JsonResult ListarLog()
        {
            try
            {
                var list = _serviceDistribuicaoAgenda.ListarLogDistribuicao(UsuarioLogado.IdLojaSeleconado);

                return JsonRetono(new { success = true, dados = list, mensagem = "Carga log com sucesso!" });
            }
            catch (Exception ex)
            {
                return JsonRetono(new { success = false, mensagem = "Erro Carga log! " + ex.Message });
            }
        }        

        public JsonResult CancelarTransferencia(string idLog)
        {
            try
            {
                LogTransferenciaAgenda entity = new LogTransferenciaAgenda();

                entity = _serviceDistribuicaoAgenda.ObterLogDistribuicao(idLog);

                if (!String.IsNullOrEmpty(idLog))
                {
                    entity.data_cancelamento = DateTime.Now;
                }
                              
                entity.vendedor_id_transferencia = UsuarioLogado.Usuario.id;

                _serviceDistribuicaoAgenda.SalvarLogTransferencia(entity);

                var listAgendaVendedorOrigem = _agenda.FindCalendars("","", UsuarioLogado.Usuario.id.ToString(), UsuarioLogado.IdsLojasUsuario, entity.vendedor_id_origem.ToString());
                var listAgendaVendedorDestino = _agenda.FindCalendars("","", UsuarioLogado.Usuario.id.ToString(), UsuarioLogado.IdsLojasUsuario, entity.vendedor_id_destino.ToString());

                // Falta Cancelar na Agenda 
                var list = _serviceDistribuicaoAgenda.ListarLogDistribuicao(UsuarioLogado.IdLojaSeleconado);

                return JsonRetono(new { success = true, dados = list, mensagem = "Cliente cancelado com sucesso!", entityExcluido = entity, listAgendaOrigem = listAgendaVendedorOrigem, listAgendaDestino = listAgendaVendedorDestino });
            }
            catch (Exception ex)
            {
                return JsonRetono(new { success = false, mensagem = "Erro ao cancelado o cliente! " + ex.Message });
            }
        }

        public JsonResult LoginAcessoDistribuicao(string pwd)
        {
            bool acessoSistema = false;
            try
            {               
                string usuarioGerente = UsuarioLogado.Usuario.email;

                _serviceUsuario.AutenticaDistribuicao(usuarioGerente, pwd);

                return JsonRetono(new { success = true, acessoSistema = acessoSistema});
            }
            catch (Exception ex)
            {
                return JsonRetono(new { success = false, acessoSistema = acessoSistema, mensagem = "Não foi possível acessar! " + ex.Message });
            }
        }

        public JsonResult GerarSenhaDistribuicao()
        {            
            try
            {

                MailAddress to = new MailAddress(UsuarioLogado.Usuario.email);
                MailAddress from = new MailAddress(UsuarioLogado.Usuario.email);
                MailMessage message = new MailMessage(from, to);

                var senha = GeneratePassword();

                message.Subject = "NOW - Senha Distribuição de Agendas (" + DateTime.Now.ToString("HH:mm") + "h)";
                message.Body = CorpoEmailSenhaDistribuicao(senha).Replace("#VENDEDOR#", UsuarioLogado.Usuario.username);
                message.IsBodyHtml = true;

                SmtpClient client = new SmtpClient();              

                string usuarioGerente = UsuarioLogado.Usuario.email;

                _serviceUsuario.TrocarSenhaAgenda(usuarioGerente, senha);
                client.Send(message);

                return JsonRetono(new { success = true, mensagem = "Senha gerada e enviada para seu email com sucesso! " });
            }
            catch (Exception ex)
            {
                return JsonRetono(new { success = false, mensagem = "Não foi possível gerar a senha! " + ex.Message });
            }
        }
    }
}
