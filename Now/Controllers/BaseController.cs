﻿using Now.Models;
using Now.Domain.Contracts.Services;
using Now.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Net;
using System.IO;
using System.Xml.Linq;
using System.Configuration;
using Now.Controllers.DTO;
using Now.Domain.DTO;

namespace Now.Controllers
{
    public class BaseController : Controller
    {
        #region Propriedades
        public enum TypeMensage
        {
            success,
            info,
            warning,
            error
        };

        public VendedorDashBoardDTO VendedorDashboard
        {
            get
            {
                return Session["VendedorDashboard"] as VendedorDashBoardDTO;
            }
            set
            {
                Session["VendedorDashboard"] = value;
            }
        }


        public UsuarioLogado UsuarioLogado
        {
            get
            {
                return Session["UsuarioLogado"] as UsuarioLogado;
            }
            set
            {
                Session["UsuarioLogado"] = value;
            }
        }

        public string OrigemLoja
        {
            get { return (Session["OrigemLoja"] = Session["OrigemLoja"] ?? "") as string; }
            set { Session["OrigemLoja"] = value; }
        }

        public bool _trocou
        {
            get { return (bool)Session["_trocou"]; }
            set { Session["_trocou"] = value; }
        }
        public bool _trocouVendedor
        {
            get { return (bool)Session["_trocouVendedor"]; }
            set { Session["_trocouVendedor"] = value; }
        }

        public string _msgRetornoPag
        {
            get { return (Session["_msgRetornoPag"] = Session["_msgRetornoPag"] ?? "") as string; }
            set { Session["_msgRetornoPag"] = value; }
        }

        public TypeMensage _typeMensage
        {
            get { return (TypeMensage)Session["_typeMensage"]; }
            set { Session["_typeMensage"] = value; }
        }

        public string Ambiente
        {
            get { return ConfigurationManager.AppSettings["ambiente"].ToString(); }
        }

        //public string _agenda
        //{
        //    get { return (Session["_agenda"] = Session["_agenda"] ?? "") as string; }
        //    set { Session["_agenda"] = value; }
        //}

        //public string _agendaTitulo
        //{
        //    get { return (Session["_agendaTitulo"] = Session["_agendaTitulo"] ?? "") as string; }
        //    set { Session["_agendaTitulo"] = value; }
        //}


        public IList<Loja> _todasLojas
        {
            get
            {
                return Session["_TodasLojas"] as List<Loja>;
            }
            set
            {
                Session["_TodasLojas"] = value;
            }
        }

        public IList<supervisorRetorno> CanaisExcluidoSupervisores
        {
            get
            {
                return Session["supervisorRetorno"] as List<supervisorRetorno>;
            }
            set
            {
                Session["supervisorRetorno"] = value;
            }
        }



        public IList<LogSistema> _logSistema
        {
            get
            {
                return Session["_logSistema"] as List<LogSistema>;
            }
            set
            {
                Session["_logSistema"] = value;
            }
        }

        public IList<LogSistema> _logSistemaTmp
        {
            get
            {
                return Session["_logSistemaTmp"] as List<LogSistema>;
            }
            set
            {
                Session["_logSistemaTmp"] = value;
            }
        }

        public IList<Clima> _clima
        {
            get
            {
                return Session["_clima"] as List<Clima>;
            }
            set
            {
                Session["_clima"] = value;
            }
        }

        public IList<Supervisores> _supervisores
        {
            get
            {
                return Session["_Supervisores"] as List<Supervisores>;
            }
            set
            {
                Session["_Supervisores"] = value;
            }
        }

        public string ImgTemp
        {
            get { return (Session["ImgTemp"] = Session["ImgTemp"] ?? "") as string; }
            set { Session["ImgTemp"] = value; }
        }

        public string ImgTempOrig
        {
            get { return (Session["ImgTempOrig"] = Session["ImgTempOrig"] ?? "") as string; }
            set { Session["ImgTempOrig"] = value; }
        }


        //public Int32 _clienteId
        //{
        //    //get { return Request.QueryString["clienteId"] == null ? 0 : Convert.ToInt32(Request.QueryString["clienteId"].ToString()); }            

        //    get { return TempData["_clienteId"] == null ? 0 : Convert.ToInt32(TempData["_clienteId"].ToString()); }
        //    set { TempData["_clienteId"] = value; }
        //}
        // Nome do Vendedor que foi selecionado
        protected string VendedorNome
        {
            get { return (Session["VendedorNome"] = Session["VendedorNome"] ?? "") as string; }
            set { Session["VendedorNome"] = value; }
        }


        #endregion

        #region Mensagems

        protected string MsgSucesso()
        {
            return "Operação Realizada com Sucesso!";
        }

        protected string MsgErro()
        {
            return "Ocorreu um erro contate o adiministrador do sistema!";
        }

        protected void MsgInformacao(string msg)
        {
            TempData["Info"] = msg;
        }

        protected void MsgAtencao(string msg)
        {
            TempData["Atencao"] = msg;
        }

        protected void MsgSucesso(string msg)
        {
            TempData["Sucesso"] = msg;
        }

        protected void MsgError(string msg)
        {
            TempData["Erro"] = msg;
        }

        #endregion


        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (Session["UsuarioLogado"] == null)
            {
                filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new
                {
                    controller = "Login",
                    action = "Index"
                }));
            }
            else if (!UsuarioLogado.PermissaoAcesso)
            {
                filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new
                {
                    controller = "Login",
                    action = "Index"
                }));
            }
            else
            {
                @ViewBag.NomeUsuario = UsuarioLogado.Usuario.username;

                @ViewBag.ListaReportClientesFavoritos = UsuarioLogado.ListaReportClientesFavoritos == null ? new List<ReportClientesFavoritos>() : UsuarioLogado.ListaReportClientesFavoritos;
                @ViewBag.Perfil = UsuarioLogado.Usuario.Perfil;
                @ViewBag.VendedoresContatos = VendedoresSelectList(UsuarioLogado.IdVendedorSeleconado, String.Empty);
                @ViewBag.LojasFiltrado = LojasSelectList(UsuarioLogado.IdLojaSeleconado, UsuarioLogado.IdLojaSeleconado);
                @ViewBag.LojasFiltradoLojaId = LojasLojaIdSelectList(UsuarioLogado.IdLojaSeleconado, UsuarioLogado.IdLojaSeleconado);
                @ViewBag.LojasFiltradoListBox = LojasSelectListBox(UsuarioLogado.IdLojaSeleconado, UsuarioLogado.IdLojaSeleconado);
                @ViewBag.TipoAgendas = TipoAgendaSelectList("");
                @ViewBag.PerfilSelecionado = String.IsNullOrEmpty(UsuarioLogado.IdVendedorSeleconado) ? UsuarioLogado.Usuario.Perfil : "Vendedor";
                @ViewBag.NomeSelecionado = String.IsNullOrEmpty(UsuarioLogado.IdVendedorSeleconado) ? UsuarioLogado.Usuario.username : (VendedorNome.Substring(0, (VendedorNome.Length <= 16 ? VendedorNome.Length : 16)) + (VendedorNome.Length > 16 ? "..." : ""));
                @ViewBag.NomeSelecionadoCompleto = String.IsNullOrEmpty(UsuarioLogado.IdVendedorSeleconado) ? UsuarioLogado.Usuario.username : VendedorNome;
                @ViewBag.ImgSelecionado = String.IsNullOrEmpty(UsuarioLogado.IdVendedorSeleconado) ? UsuarioLogado.Usuario.img_perfil : UsuarioLogado.VendedoresUsuario.Where(x => x.id.ToString() == UsuarioLogado.IdVendedorSeleconado).FirstOrDefault().img_perfil;
                @ViewBag.ImgSelecionadoIntraNet = String.IsNullOrEmpty(UsuarioLogado.IdVendedorSeleconado) ? UsuarioLogado.Usuario.img_perfil : UsuarioLogado.VendedoresUsuario.Where(x => x.id.ToString() == UsuarioLogado.IdVendedorSeleconado).FirstOrDefault().img_perfil_intranet;
                @ViewBag.Idvendedor = UsuarioLogado.IdVendedorSeleconado;
                @ViewBag.CodigoClima = UsuarioLogado.CodigoClima;
                // @ViewBag.Clima = RetornarClima();
                @ViewBag.Canais = CanaisSelectList();
                @ViewBag.CanalTexto = CanaisTexto();
                @ViewBag.CanaisSelect = UsuarioLogado.Canais;
                @ViewBag.SupervisorLojas = Session["_Supervisores"] == null ? new List<Supervisores>() : Session["_Supervisores"] as IList<Supervisores>;

                @ViewBag.NomeLojaSelecionada = String.IsNullOrEmpty(UsuarioLogado.IdLojaSeleconado) ? "TODAS" : UsuarioLogado.LojasUsuario.Where(x => x.id.ToString() == UsuarioLogado.IdLojaSeleconado).FirstOrDefault().filial;
                @ViewBag.LojaIdLojaSelecionada = String.IsNullOrEmpty(UsuarioLogado.IdLojaSeleconado) ? "" : RetornaLojaIdTipoLoja(UsuarioLogado.LojasUsuario.Where(x => x.id.ToString() == UsuarioLogado.IdLojaSeleconado).FirstOrDefault());
                @ViewBag.TipoLoja = String.IsNullOrEmpty(UsuarioLogado.IdLojaSeleconado) ? "" : UsuarioLogado.LojasUsuario.Where(x => x.id.ToString() == UsuarioLogado.IdLojaSeleconado).FirstOrDefault().tipo_loja;
                @ViewBag.IdLojaSelecionada = UsuarioLogado.IdLojaSeleconado;
                @ViewBag.ImagemPerfil = String.IsNullOrEmpty(UsuarioLogado.Usuario.img_perfil) ? @"\dist\img\user2-160x160.jpg" : UsuarioLogado.Usuario.img_perfil;
                
                @ViewBag.VisualizarEndereco = UsuarioLogado.VisualizarEndereco;
                @ViewBag.RegraPesquisa = UsuarioLogado.RegraPesquisa;

                @ViewBag.ativarNovaTelaVendedor = false;

                @ViewBag.Vendedores = VendedoresSelectList(String.Empty, String.Empty);

                @ViewBag.ListaVendedores = UsuarioLogado.VendedoresUsuario == null ? new List<Vendedor>() : UsuarioLogado.VendedoresUsuario.ToList();

                @ViewBag.UsuarioLogado = UsuarioLogado;

                UsuarioLogado.LojasUsuarioCanais = LojasPorCanais();

                ExecMensagemRetorno();
            }
        }

        private string RetornaLojaIdTipoLoja(Loja loja)
        {
            return loja.id_loja + "#" + (loja.tipo_loja == "MINI" ? "RESERVA MINI" : "OUTRAS");
        }

        private string CanaisTexto()
        {
            string _tituloCanais = string.Empty;
            foreach (var item in CanaisSelectList())
            {
                if (UsuarioLogado.Canais.Contains(item.Value))
                    _tituloCanais += item.Text.ToString() + "|";
            }

            _tituloCanais = _tituloCanais.TrimEnd('|');

            return _tituloCanais;
        }

        private void ExecMensagemRetorno()
        {
            if (_msgRetornoPag != null)
            {
                if (!String.IsNullOrEmpty(_msgRetornoPag))
                {
                    switch (_typeMensage)
                    {
                        case TypeMensage.success:
                            {
                                MsgSucesso(_msgRetornoPag);
                            }
                            break;
                        case TypeMensage.info:
                            break;
                        case TypeMensage.warning:
                            {
                                MsgAtencao(_msgRetornoPag);
                            }
                            break;
                        case TypeMensage.error:
                            {
                                MsgError(MsgErro());
                            }
                            break;
                    }

                    _msgRetornoPag = null;
                }
            }
        }

        public void MensagemRetorno(string msg, TypeMensage tipo)
        {
            if (msg != null)
            {
                if (!String.IsNullOrEmpty(msg))
                {
                    _msgRetornoPag = msg;
                    _typeMensage = tipo;
                }
            }
        }

        /// <summary>
        /// Método utilizado para selecionar a loja do usuário
        /// </summary>
        protected void SelecionaLoja()
        {
            //Verificando se o id veio da querystring - Por LINK
            var auxidloja = Request.QueryString["idloja"];

            if (UsuarioLogado.Usuario.Lojas.Count > 0)
                Session["OrigemLoja"] = UsuarioLogado.Usuario.Lojas[0].origem == "FRANQUIA" ? UsuarioLogado.Usuario.Lojas[0].origem : "VAREJO";

            if (auxidloja == null) return;

            /* 

             var loja = iLoja.ListarLojasVisiveisPorUsuario(UsuarioLogado.usuario.id, auxidloja).FirstOrDefault();

             if (loja == null) return;

             //TODO MUDAR DEPOIS A SESSÃO PARA UM OBJETO LOJA
             Session["NomeLoja"] = loja.filial;
             Session["OrigemLoja"] = loja.origem == "FRANQUIA" ? loja.origem : "VAREJO";
             Session["IdLoja"] = auxidloja;
             * 
             * */

        }

        /// <summary>
        /// Método utilizado para selecionar a loja do usuário
        /// </summary>
        protected void SelecionaVendedor()
        {
            //Verificando se o id veio da querystring - Por LINK
            var auxidvendedor = Request.QueryString["IdVendedor"];

            if (auxidvendedor == null) return;


            /*
            var vendedor = _serviceVendedor.GetVendedor(UsuarioLogado.IdLojaSeleconado, auxidvendedor, UsuarioLogado.usuario.id).FirstOrDefault();

            if (vendedor == null) return;

            //TODO Mudar a sessão para objeto vendedor
            Session["NomeVendedor"] = vendedor.nome;
            Session["IdVendedor"] = auxidvendedor;
            Session["PerfilVendedor"] = vendedor.perfil;
             * 
             * */

        }

        public IList<Clima> RetornarClima()
        {
            if (_clima == null)
            {
                IList<Clima> clima = new List<Clima>();

                try
                {
                    string pagina = "http://servicos.cptec.inpe.br/XML/cidade/7dias/" + UsuarioLogado.CodigoClima + "/previsao.xml";
                    HttpWebRequest request = (HttpWebRequest)WebRequest.Create(new Uri(pagina));
                    request.ServicePoint.ConnectionLimit = 5000;
                    request.Method = "GET";
                    request.Accept = "application/xml";

                    XDocument doc;
                    using (WebResponse response = request.GetResponse())
                    {
                        using (Stream stream = response.GetResponseStream())
                        {
                            doc = XDocument.Load(stream);
                            string cidade = doc.Element("cidade").Element("nome").Value;
                            string uf = doc.Element("cidade").Element("uf").Value;
                            foreach (XElement item in doc.Root.Nodes())
                            {
                                if (item.Name == "previsao")
                                {
                                    clima.Add(new Clima()
                                    {
                                        cidade = cidade,
                                        uf = uf,
                                        dia = Convert.ToDateTime(item.Element("dia").Value),
                                        tempo = item.Element("tempo").Value,
                                        maxima = item.Element("maxima").Value,
                                        minima = item.Element("minima").Value,
                                        descricao = ListaCondicoes().Where(x => x.codigo == item.Element("tempo").Value).FirstOrDefault().descricao,
                                        titulo = ListaCondicoes().Where(x => x.codigo == item.Element("tempo").Value).FirstOrDefault().titulo,
                                        iuv = item.Element("iuv").Value
                                    });
                                }
                            }
                        }
                    }

                }
                catch (Exception)
                {
                    clima.Add(new Clima()
                    {
                        cidade = "Indisponíveis",
                        uf = "Indisponíveis",
                        dia = DateTime.Now,
                        tempo = "Indisponíveis",
                        maxima = "Indisponíveis",
                        minima = "Indisponíveis",
                        descricao = "Indisponíveis",
                        titulo = "Indisponíveis",
                        iuv = "Indisponíveis"
                    });
                }

                _clima = clima;
            }

            return _clima;
        }


        public IList<string> Cores()
        {
            IList<string> cor = new List<string>();

            cor.Add("#EEEEE0");
            cor.Add("#836FFF");
            cor.Add("#009ACD");
            cor.Add("#C6E2FF");
            cor.Add("#68838B");
            cor.Add("#C1FFC1");
            cor.Add("#00EE00");
            cor.Add("#BCEE68");
            cor.Add("#FFF68F");
            cor.Add("#FFFF00");
            cor.Add("#FFD700");
            cor.Add("#CD9B1D");
            cor.Add("#FFC1C1");
            cor.Add("#EE6363");
            cor.Add("#EE30A7");
            cor.Add("#EE00EE");
            cor.Add("#912CEE");
            cor.Add("#9F79EE");
            cor.Add("#EED2EE");
            cor.Add("#1C1C1C");
            cor.Add("#FF0202");
            cor.Add("#035101");
            cor.Add("#C71585");
            cor.Add("#836FFF");
            cor.Add("#0000FF");
            cor.Add("#8B3A62");
            cor.Add("#EE9A00");
            cor.Add("#CD0000");
            cor.Add("#FFC1C1");
            cor.Add("#EEEEE0");
            cor.Add("#836FFF");
            cor.Add("#009ACD");
            cor.Add("#C6E2FF");
            cor.Add("#68838B");
            cor.Add("#C1FFC1");
            cor.Add("#00EE00");
            cor.Add("#BCEE68");
            cor.Add("#FFF68F");
            cor.Add("#FFFF00");
            cor.Add("#FFD700");
            cor.Add("#CD9B1D");
            cor.Add("#FFC1C1");
            cor.Add("#EE6363");
            cor.Add("#EE30A7");
            cor.Add("#EE00EE");
            cor.Add("#912CEE");
            cor.Add("#9F79EE");
            cor.Add("#EED2EE");
            cor.Add("#1C1C1C");
            cor.Add("#FF0202");
            cor.Add("#035101");
            cor.Add("#C71585");
            cor.Add("#836FFF");
            cor.Add("#0000FF");
            cor.Add("#8B3A62");
            cor.Add("#EE9A00");
            cor.Add("#CD0000");
            cor.Add("#FFC1C1");
            cor.Add("#C71585");
            cor.Add("#836FFF");
            cor.Add("#0000FF");
            cor.Add("#8B3A62");
            cor.Add("#EE9A00");
            cor.Add("#CD0000");
            cor.Add("#FFC1C1");
            cor.Add("#EEEEE0");
            cor.Add("#836FFF");
            cor.Add("#009ACD");
            cor.Add("#C6E2FF");
            cor.Add("#68838B");
            cor.Add("#C1FFC1");
            cor.Add("#00EE00");
            cor.Add("#BCEE68");
            cor.Add("#FFF68F");
            cor.Add("#FFFF00");
            cor.Add("#FFD700");
            cor.Add("#CD9B1D");

            return cor;
        }

        public IList<ClimaCondicoes> ListaCondicoes()
        {
            IList<ClimaCondicoes> condicoes = new List<ClimaCondicoes>();

            condicoes.Add(new ClimaCondicoes { codigo = "ec", titulo = "Encoberto com Chuvas Isoladas", descricao = "Céu totalmente encoberto com chuvas em algumas regiões, sem aberturas de sol." });
            condicoes.Add(new ClimaCondicoes { codigo = "ci", titulo = "Chuvas Isoladas", descricao = "Muitas nuvens com curtos períodos de sol e chuvas em algumas áreas." });
            condicoes.Add(new ClimaCondicoes { codigo = "c", titulo = "Chuva", descricao = "Muitas nuvens e chuvas periódicas." });
            condicoes.Add(new ClimaCondicoes { codigo = "in", titulo = "Instável", descricao = "Nebulosidade variável com chuva a qualquer hora do dia." });
            condicoes.Add(new ClimaCondicoes { codigo = "pp", titulo = "Poss. de Pancadas de Chuva", descricao = "Nebulosidade variável com pequena chance (inferior a 30%) de pancada de chuva." });
            condicoes.Add(new ClimaCondicoes { codigo = "cm", titulo = "Chuva pela Manhã", descricao = "Chuva pela manhã melhorando ao longo do dia." });
            condicoes.Add(new ClimaCondicoes { codigo = "cn", titulo = "Chuva a Noite", descricao = "Nebulosidade em aumento e chuvas durante a noite." });
            condicoes.Add(new ClimaCondicoes { codigo = "pt", titulo = "Pancadas de Chuva a Tarde", descricao = "Predomínio de sol pela manhã. À tarde chove com trovoada." });
            condicoes.Add(new ClimaCondicoes { codigo = "pm", titulo = "Pancadas de Chuva pela Manhã", descricao = "Chuva com trovoada pela manhã. À tarde o tempo abre e não chove." });
            condicoes.Add(new ClimaCondicoes { codigo = "np", titulo = "Nublado e Pancadas de Chuva", descricao = "Muitas nuvens com curtos períodos de sol e pancadas de chuva com trovoadas." });
            condicoes.Add(new ClimaCondicoes { codigo = "pc", titulo = "Pancadas de Chuva", descricao = "Chuva de curta duração e pode ser acompanhada de trovoadas a qualquer hora do dia." });
            condicoes.Add(new ClimaCondicoes { codigo = "pn", titulo = "Parcialmente Nublado", descricao = "Sol entre poucas nuvens." });
            condicoes.Add(new ClimaCondicoes { codigo = "cv", titulo = "Chuvisco", descricao = "Muitas nuvens e chuva fraca composta de pequenas gotas d´ água." });
            condicoes.Add(new ClimaCondicoes { codigo = "ch", titulo = "Chuvoso", descricao = "Nublado com chuvas contínuas ao longo do dia." });
            condicoes.Add(new ClimaCondicoes { codigo = "t", titulo = "Tempestade", descricao = "Chuva forte capaz de gerar granizo e ou rajada de vento, com força destrutiva (Veloc. aprox. de 90 Km/h) e ou tornados." });
            condicoes.Add(new ClimaCondicoes { codigo = "ps", titulo = "Predomínio de Sol", descricao = "Sol na maior parte do período." });
            condicoes.Add(new ClimaCondicoes { codigo = "e", titulo = "Encoberto", descricao = "Céu totalmente encoberto, sem aberturas de sol." });
            condicoes.Add(new ClimaCondicoes { codigo = "n", titulo = "Nublado", descricao = "Muitas nuvens com curtos períodos de sol." });
            condicoes.Add(new ClimaCondicoes { codigo = "cl", titulo = "Céu Claro", descricao = "Sol durante todo o período. Ausência de nuvens." });
            condicoes.Add(new ClimaCondicoes { codigo = "nv", titulo = "Nevoeiro", descricao = "Gotículas de água em suspensão que reduzem a visibilidade." });
            condicoes.Add(new ClimaCondicoes { codigo = "g", titulo = "Geada", descricao = "Cobertura de cristais de gelo que se formam por sublimação direta sobre superfícies expostas cuja temperatura está abaixo do ponto de congelamento." });
            condicoes.Add(new ClimaCondicoes { codigo = "ne", titulo = "Neve", descricao = "Vapor de água congelado na nuvem, que cai em forma de cristais e flocos." });
            condicoes.Add(new ClimaCondicoes { codigo = "nd", titulo = "Não Definido", descricao = "Não definido." });
            condicoes.Add(new ClimaCondicoes { codigo = "pnt", titulo = "Pancadas de Chuva a Noite", descricao = "Chuva de curta duração podendo ser acompanhada de trovoadas à noite." });
            condicoes.Add(new ClimaCondicoes { codigo = "psc", titulo = "Possibilidade de Chuva", descricao = "Nebulosidade variável com pequena chance (inferior a 30%) de chuva." });
            condicoes.Add(new ClimaCondicoes { codigo = "pcm", titulo = "Possibilidade de Chuva pela Manhã", descricao = "Nebulosidade variável com pequena chance (inferior a 30%) de chuva pela manhã." });
            condicoes.Add(new ClimaCondicoes { codigo = "pct", titulo = "Possibilidade de Chuva a Tarde", descricao = "Nebulosidade variável com pequena chance (inferior a 30%) de chuva pela tarde." });
            condicoes.Add(new ClimaCondicoes { codigo = "pcn", titulo = "Possibilidade de Chuva a Noite", descricao = "Nebulosidade variável com pequena chance (inferior a 30%) de chuva à noite." });
            condicoes.Add(new ClimaCondicoes { codigo = "npt", titulo = "Nublado com Pancadas a Tarde", descricao = "Muitas nuvens com curtos períodos de sol e pancadas de chuva com trovoadas à tarde." });
            condicoes.Add(new ClimaCondicoes { codigo = "npn", titulo = "Nublado com Pancadas a Noite", descricao = "Muitas nuvens com curtos períodos de sol e pancadas de chuva com trovoadas à noite." });
            condicoes.Add(new ClimaCondicoes { codigo = "ncn", titulo = "Nublado com Poss. de Chuva a Noite", descricao = "Muitas nuvens com curtos períodos de sol com pequena chance (inferior a 30%) de chuva à noite." });
            condicoes.Add(new ClimaCondicoes { codigo = "nct", titulo = "Nublado com Poss. de Chuva a Tarde", descricao = "Muitas nuvens com curtos períodos de sol com pequena chance (inferior a 30%) de chuva à tarde." });
            condicoes.Add(new ClimaCondicoes { codigo = "ncm", titulo = "Nubl. c/ Poss. de Chuva pela Manhã", descricao = "Muitas nuvens com curtos períodos de sol com pequena chance (inferior a 30%) de chuva pela manhã." });
            condicoes.Add(new ClimaCondicoes { codigo = "npm", titulo = "Nublado com Pancadas pela Manhã", descricao = "Muitas nuvens com curtos períodos de sol e chuva com trovoadas pela manhã." });
            condicoes.Add(new ClimaCondicoes { codigo = "npp", titulo = "Nublado com Possibilidade de Chuva", descricao = "Muitas nuvens com curtos períodos de sol com pequena chance (inferior a 30%) de chuva a qualquer hora do dia." });
            condicoes.Add(new ClimaCondicoes { codigo = "vn", titulo = "Variação de Nebulosidade", descricao = "Períodos curtos de sol intercalados com períodos de nuvens." });
            condicoes.Add(new ClimaCondicoes { codigo = "ct", titulo = "Chuva a Tarde", descricao = "Nebulosidade em aumento e chuvas a partir da tarde." });
            condicoes.Add(new ClimaCondicoes { codigo = "ppn", titulo = "Poss. de Panc. de Chuva a Noite", descricao = "Nebulosidade variável com pequena chance (inferior a 30%) de chuva à noite." });
            condicoes.Add(new ClimaCondicoes { codigo = "ppt", titulo = "Poss. de Panc. de Chuva a Tarde", descricao = "Nebulosidade variável com pequena chance (inferior a 30%) de chuva pela tarde." });
            condicoes.Add(new ClimaCondicoes { codigo = "ppm", titulo = "Poss. de Panc. de Chuva pela Manhã", descricao = "Nebulosidade variável com pequena chance (inferior a 30%) de chuva pela manhã." });


            return condicoes;
        }

        public void ModificaSessao(UsuarioLogado alterado)
        {
            UsuarioLogado = alterado;
        }

        // Retorna Um SelectList de uma lista
        public SelectList TipoAgendaSelectList(string paramAgenda)
        {
            IList<MenuTipoAgendamento> list = new List<MenuTipoAgendamento>();

            if (String.IsNullOrEmpty(paramAgenda))
                list = UsuarioLogado.TiposAgendas;
            else
                list = UsuarioLogado.TiposAgendas.Where(x => x.parametro == paramAgenda).ToList();

            List<SelectListItem> items = new List<SelectListItem>();

            if (list.Count() > 1)
                items.Add(new SelectListItem() { Text = "Selecione...", Value = "", Selected = true });

            items.AddRange(list.OrderBy(o => o.prioridade).Select(item => new SelectListItem
            {
                Text = item.titulo,
                Value = item.id.ToString()
            }));

            return new SelectList(items, "Value", "Text");
        }

        // Retorna Um SelectList de uma lista
        public SelectList VendedoresSelectList(string IdVendedorFiltro, string IdVendedorSelect)
        {
            IList<Vendedor> list = new List<Vendedor>();

            // Quando uma loja não for selecionada não exite vendedor
            if (UsuarioLogado.VendedoresUsuario != null)
            {
                if (String.IsNullOrEmpty(IdVendedorFiltro))
                    list = UsuarioLogado.VendedoresUsuario.ToList();
                else if (UsuarioLogado.VendedoresUsuario.Where(v => v.id == Convert.ToInt32(IdVendedorFiltro)).Count() > 0)
                {// Se for um gerente selecionado a lista dos vendedores tem que estar disponíveis
                    if (UsuarioLogado.VendedoresUsuario.Where(v => v.id == Convert.ToInt32(IdVendedorFiltro)).FirstOrDefault().perfil == "Gerente")
                        list = UsuarioLogado.VendedoresUsuario.ToList();
                    else
                        list = UsuarioLogado.VendedoresUsuario.Where(v => v.id == Convert.ToInt32(IdVendedorFiltro)).ToList();
                }
                else
                    list = UsuarioLogado.VendedoresUsuario.Where(v => v.id == Convert.ToInt32(IdVendedorFiltro)).ToList();
            }

            List<SelectListItem> items = new List<SelectListItem>();

            if (list.Count() > 1)
                items.Add(new SelectListItem() { Text = "Selecione...", Value = "", Selected = true });

            items.AddRange(list.OrderBy(o => o.nome).Select(item => new SelectListItem
            {
                Text = item.nome,
                Value = item.id.ToString()
            }));

            return new SelectList(items, "Value", "Text", IdVendedorSelect);
        }

        public SelectList LojasLojaIdSelectList(string IdLojasFiltro, string IdLojasSelect)
        {
            IList<Loja> list = new List<Loja>();

            //list = UsuarioLogado.Usuario.Perfil.ToUpper().Contains("ADMINISTRADOR") ? UsuarioLogado.LojasCanais : UsuarioLogado.LojasUsuario;
            list = UsuarioLogado.LojasUsuario;

            if (!String.IsNullOrEmpty(IdLojasFiltro))
                list = list.Where(v => v.id == Convert.ToInt32(IdLojasFiltro)).ToList();

            List<SelectListItem> items = new List<SelectListItem>();

            if (list.Count() > 1)
                items.Add(new SelectListItem() { Text = "Selecione...", Value = "", Selected = true });

            items.AddRange(list.OrderBy(o => o.filial).Select(item => new SelectListItem
            {
                Text = item.filial,
                Value = item.id_loja + "#" + (item.tipo_loja == "MINI" ? "RESERVA MINI" : "OUTRAS")
            }));

            return new SelectList(items, "Value", "Text", IdLojasSelect);
        }

        public Loja lojaUsuario()
        {
            if (!String.IsNullOrEmpty(UsuarioLogado.IdLojaSeleconado))
                return UsuarioLogado.LojasUsuario.Where(x => x.id == Convert.ToInt32(UsuarioLogado.IdLojaSeleconado)).FirstOrDefault();
            else
                return new Loja();
        }

        public string TipoLoja()
        {
            if (lojaUsuario().id != 0)
                return lojaUsuario().tipo_loja.Contains("RESERVA") ? "RESERVA" : (lojaUsuario().tipo_loja == "MINI") ? "MINI" : (lojaUsuario().tipo_loja == "EVA") ? "EVA" : "";
            else
                return "";
        }



        // Retorna Um SelectList de uma lista
        public SelectList LojasSelectList(string IdLojasFiltro, string IdLojasSelect)
        {
            IList<Loja> list = new List<Loja>();

            //list = UsuarioLogado.Usuario.Perfil.ToUpper().Contains("ADMINISTRADOR") ? UsuarioLogado.LojasCanais : UsuarioLogado.LojasUsuario;
            list = UsuarioLogado.LojasUsuario;

            if (!String.IsNullOrEmpty(IdLojasFiltro))
                list = list.Where(v => v.id == Convert.ToInt32(IdLojasFiltro)).ToList();

            List<SelectListItem> items = new List<SelectListItem>();

            if (list.Count() > 1)
                items.Add(new SelectListItem() { Text = "Selecione...", Value = "", Selected = true });

            items.AddRange(list.OrderBy(o => o.filial).Select(item => new SelectListItem
            {
                Text = item.filial,
                Value = item.id.ToString()
            }));

            return new SelectList(items, "Value", "Text", IdLojasSelect);
        }

        public IList<SelectListItem> LojasSelectListBox(string IdLojasFiltro, string IdLojasSelect)
        {
            IList<Loja> list = new List<Loja>();

            //list = UsuarioLogado.Usuario.Perfil.ToUpper().Contains("ADMINISTRADOR") ? UsuarioLogado.LojasCanais : UsuarioLogado.LojasUsuario;
            list = UsuarioLogado.LojasUsuario;

            if (!String.IsNullOrEmpty(IdLojasFiltro))
                list = list.Where(v => v.id == Convert.ToInt32(IdLojasFiltro)).ToList();

            List<SelectListItem> items = new List<SelectListItem>();

            if (list.Count() > 1)
                items.Add(new SelectListItem() { Text = "Selecione...", Value = "", Selected = true });

            items.AddRange(list.OrderBy(o => o.filial).Select(item => new SelectListItem
            {
                Text = item.filial,
                Value = item.id.ToString()
            }));

            return items;
        }

        // Retorna lista de canais
        public SelectList CanaisSelectList()
        {
            List<SelectListItem> items = new List<SelectListItem>();

            IList<string> _canais = new List<string>();

            IList<Loja> _lojasUsuario = (UsuarioLogado.Usuario.Perfil.ToUpper().Contains("ADMINISTRADOR") || UsuarioLogado.Usuario.Lojas.Count == 0) ? _todasLojas : UsuarioLogado.Usuario.Lojas;

            foreach (var item in _lojasUsuario.Select(item => new Loja
            {
                tipo_loja = item.tipo_loja
            }).ToList())
            {
                if (item.tipo_loja.Contains("RESERVA"))
                {
                    if (!_canais.Contains("RESERVA,RESERVA+MINI"))
                    {
                        items.Add(new SelectListItem() { Text = "Reserva", Value = "RESERVA,RESERVA+MINI", Selected = (UsuarioLogado.Canais.Where(x => x.ToString() == "RESERVA,RESERVA+MINI").Count() > 0) });
                        _canais.Add("RESERVA,RESERVA+MINI");
                    }
                }
                else
                {

                    if (!_canais.Contains(item.tipo_loja))
                    {
                        items.Add(new SelectListItem() { Text = item.tipo_loja, Value = item.tipo_loja, Selected = (UsuarioLogado.Canais.Where(x => x.ToString() == item.tipo_loja).Count() > 0) });
                        _canais.Add(item.tipo_loja);
                    }
                }
            }

            return new SelectList(items, "Value", "Text");
        }

        public SelectList ObterTemometroList()
        {
            List<SelectListItem> items = new List<SelectListItem>();

            items.Add(new SelectListItem() { Text = "Gostou", Value = "fa fa-smile-o ", Selected = false });
            items.Add(new SelectListItem() { Text = "Então tá", Value = "fa fa-meh-o ", Selected = false });
            items.Add(new SelectListItem() { Text = "Odiou", Value = "fa fa-frown-o ", Selected = false });

            return new SelectList(items, "Value", "Text");
        }

        public List<ListaProdutosComercioDigitalDTO> listagemProdutos()
        {
            var data_atual = DateTime.Today.Day.ToString("00") + "/" + DateTime.Today.Month.ToString("00") + "/" + DateTime.Today.Year;
            var localizar_cupom = ListaCupomDesconto().Where(x => x.data_inicio == data_atual);
            var cupom = localizar_cupom.Count() > 0 ? "?bl_pr=" + localizar_cupom.First().parametro : "";

            var listagem = new List<ListaProdutosComercioDigitalDTO>
            {
                new ListaProdutosComercioDigitalDTO() { texto = "Tshirts", valor = "https://www.usereserva.com/c/adulto/roupas/reserva-roupas-camisetas"+cupom+" (Tshirt)" },
                new ListaProdutosComercioDigitalDTO() { texto = "Camisarias", valor = "https://www.usereserva.com/c/adulto/roupas/reserva-roupas-camisas"+cupom+" (Camisaria)" },
                new ListaProdutosComercioDigitalDTO() { texto = "Polos", valor = "https://www.usereserva.com/c/adulto/roupas/reserva-roupas-polos"+cupom+" (Polo)" },

                new ListaProdutosComercioDigitalDTO() { texto = "Bermudas", valor= "https://www.usereserva.com/c/adulto/roupas/reserva-roupas-bermudas"+cupom+" (Bermudas)" },
                new ListaProdutosComercioDigitalDTO() { texto = "Calças", valor = "https://www.usereserva.com/c/adulto/roupas/reserva-roupas-calcas"+cupom+" (Calças)" },
                new ListaProdutosComercioDigitalDTO() { texto = "Clássicos", valor = "https://www.usereserva.com/c/adulto/roupas/reserva-roupas-classicos"+cupom+" (Clássicos)" },
                new ListaProdutosComercioDigitalDTO() { texto = "Casacos e T.", valor= "https://www.usereserva.com/c/adulto/roupas/reserva-roupas-casacos"+cupom+" (Casacos)" },

                new ListaProdutosComercioDigitalDTO() { texto = "Pima", valor= "https://www.usereserva.com/c/adulto/colecoes/reserva-colecoes-algodao-pima"+cupom+" (Pima)" },
                new ListaProdutosComercioDigitalDTO() { texto = "Calçados", valor= "https://www.usereserva.com/c/adulto/reserva/reserva-calcados"+cupom+" (Calçados)" },
                new ListaProdutosComercioDigitalDTO() { texto = "Penetras", valor = "https://www.usereserva.com/c/adulto/reserva/reserva-penetras"+cupom+" (Penetras)" }

                //new ListaProdutosComercioDigitalDTO() { texto = "Tshirts", valor = "https://tinyurl.com/t-shirt-reserva (Tshirt)" },
                //new ListaProdutosComercioDigitalDTO() { texto = "Camisarias", valor = "https://tinyurl.com/camisaria-reserva (Camisaria)" },
                //new ListaProdutosComercioDigitalDTO() { texto = "Polos", valor = "https://tinyurl.com/polo-reserva (Polo)" },

                //new ListaProdutosComercioDigitalDTO() { texto = "Bermudas", valor= "https://tinyurl.com/bermuda-reserva (Bermudas)" },
                //new ListaProdutosComercioDigitalDTO() { texto = "Calças", valor = "https://tinyurl.com/calca-reserva (Calças)" },
                //new ListaProdutosComercioDigitalDTO() { texto = "Clássicos", valor = "https://tinyurl.com/classicos-reserva (Clássicos)" },
                //new ListaProdutosComercioDigitalDTO() { texto = "Casacos e T.", valor= "https://tinyurl.com/casacos-reserva (Casacos)" },

                //new ListaProdutosComercioDigitalDTO() { texto = "Algodão P.", valor= "https://tinyurl.com/algodao-pima-reserva (Pima)" },
                //new ListaProdutosComercioDigitalDTO() { texto = "Calçados", valor= "https://tinyurl.com/calcados-reserva (Calçados)" },
                //new ListaProdutosComercioDigitalDTO() { texto = "Penetras", valor = "https://tinyurl.com/penetras-reserva (Penetras)" }
            };

            return listagem;
        }

        public List<CupomDesconto> ListaCupomDesconto()
        {
            var listagem = new List<CupomDesconto>
            {
                new CupomDesconto(){parametro = "rsvnowA1", data_inicio="19/09/2019", data_final="20/09/2019"},
                new CupomDesconto(){parametro = "rsvnowB2", data_inicio="20/09/2019", data_final="21/09/2019"},
                new CupomDesconto(){parametro = "rsvnowC3", data_inicio="21/09/2019", data_final="22/09/2019"},
                new CupomDesconto(){parametro = "rsvnowD4", data_inicio="22/09/2019", data_final="23/09/2019"},
                new CupomDesconto(){parametro = "rsvnowE5", data_inicio="23/09/2019", data_final="24/09/2019"},
                new CupomDesconto(){parametro = "rsvnowF6", data_inicio="24/09/2019", data_final="25/09/2019"},
                new CupomDesconto(){parametro = "rsvnowG7", data_inicio="25/09/2019", data_final="26/09/2019"},
                new CupomDesconto(){parametro = "rsvnowH8", data_inicio="26/09/2019", data_final="27/09/2019"},
                new CupomDesconto(){parametro = "rsvnowI9", data_inicio="27/09/2019", data_final="28/09/2019"},
                new CupomDesconto(){parametro = "rsvnowJ10", data_inicio="28/09/2019", data_final="29/09/2019"},
                new CupomDesconto(){parametro = "rsvnowK11", data_inicio="29/09/2019", data_final="30/09/2019"},

                new CupomDesconto(){parametro = "rsvnowL12", data_inicio="30/09/2019", data_final="01/10/2019"},
                new CupomDesconto(){parametro = "rsvnowM13", data_inicio="01/10/2019", data_final="02/10/2019"},
                new CupomDesconto(){parametro = "rsvnowN14", data_inicio="02/10/2019", data_final="03/10/2019"},
                new CupomDesconto(){parametro = "rsvnowO15", data_inicio="03/10/2019", data_final="04/10/2019"},
                new CupomDesconto(){parametro = "rsvnowP16", data_inicio="04/10/2019", data_final="05/10/2019"},
                new CupomDesconto(){parametro = "rsvnowQ17", data_inicio="05/10/2019", data_final="06/10/2019"},
                new CupomDesconto(){parametro = "rsvnowR18", data_inicio="06/10/2019", data_final="07/10/2019"},
                new CupomDesconto(){parametro = "rsvnowS19", data_inicio="07/10/2019", data_final="08/10/2019"},
                new CupomDesconto(){parametro = "rsvnowT20", data_inicio="08/10/2019", data_final="09/10/2019"},
                new CupomDesconto(){parametro = "rsvnowU21", data_inicio="09/10/2019", data_final="10/10/2019"},
                new CupomDesconto(){parametro = "rsvnowV22", data_inicio="10/10/2019", data_final="11/10/2019"},

                new CupomDesconto(){parametro = "rsvnowX23", data_inicio="11/10/2019", data_final="12/10/2019"},
                new CupomDesconto(){parametro = "rsvnowY24", data_inicio="12/10/2019", data_final="13/10/2019"},
                new CupomDesconto(){parametro = "rsvnowZ25", data_inicio="13/10/2019", data_final="14/10/2019"},
                new CupomDesconto(){parametro = "rsvnowAA26", data_inicio="14/10/2019", data_final="15/10/2019"},
                new CupomDesconto(){parametro = "rsvnowAB27", data_inicio="15/10/2019", data_final="16/10/2019"},
                new CupomDesconto(){parametro = "rsvnowAC28", data_inicio="17/10/2019", data_final="18/10/2019"},

                new CupomDesconto(){parametro = "rsvnowAD29", data_inicio="18/10/2019", data_final="19/10/2019"},
                new CupomDesconto(){parametro = "rsvnowAE30", data_inicio="19/10/2019", data_final="20/10/2019"}
            };

            return listagem;
        }

        public SelectList ObterModoContatoComercioDigitalList()
        {

            List<SelectListItem> items = new List<SelectListItem>();
            var listagem = listagemProdutos();

            for (int i = 0; i < listagem.Count(); i++)
            {
                items.Add(new SelectListItem() { Text = listagem[i].texto, Value = listagem[i].valor });
            };

            return new SelectList(items, "Value", "Text");
        }

        public SelectList ObterModoContatoList()
        {
            //new SelectListItem { Text = "Ligação", Value = "Ligação" },
            //new SelectListItem { Text = "WhatsApp", Value = "WhatsApp" },
            //new SelectListItem { Text = "Facebook", Value = "Facebook" },
            //new SelectListItem { Text = "Cara a cara", Value = "Cara a cara" },
            //new SelectListItem { Text = "SMS", Value = "SMS" },
            //new SelectListItem { Text = "Instagram", Value = "Instagram" },
            //new SelectListItem { Text = "Nenhum", Value = "Nenhum" }

            List<SelectListItem> items = new List<SelectListItem>();

            items.Add(new SelectListItem() { Text = "Ligação", Value = "fa fa-phone" });
            items.Add(new SelectListItem() { Text = "WhatsApp", Value = "fa fa-whatsapp" });
            items.Add(new SelectListItem() { Text = "Facebook", Value = "fa fa-facebook" });

            items.Add(new SelectListItem() { Text = "Cara a cara", Value = "fa fa-user" });
            items.Add(new SelectListItem() { Text = "SMS", Value = "fa fa-commenting-o" });
            items.Add(new SelectListItem() { Text = "Instagram", Value = "fa fa-instagram" });
            //items.Add(new ModoContatoDTO() { Text = "Nenhum", Value = "Nenhum", Icon = "fa fa-user" });

            return new SelectList(items, "Value", "Text");
        }

        public JsonResult CarregarCanais()
        {
            string listCanais = string.Empty;

            foreach (SelectListItem item in CanaisSelectList().Items)
            {
                string _checked = "";
                if (item.Selected)
                {
                    _checked = "checked";
                }

                listCanais += @"
                        <tr>
                            <td style='width: 20px'>
                                <input type='checkbox' id='chekCanal' checked='" + _checked + @"' value='" + item.Value + @"' class='chekCanal checkCss'>                                
                            </td>
                            <td class='mailbox-subject'><b>" + item.Text + @"</b></td>
                        </tr>
                          ";
            }

            return JsonRetono(listCanais);
        }

        // Retorna Os Tipos de reltórios
        public SelectList TiposRelatoriosSelectList()
        {
            List<SelectListItem> items = new List<SelectListItem>();

            items.Add(new SelectListItem() { Text = "Ranking Vendedores", Value = "SP_RelatorioRankingVendedores" });

            /*Solução paliativa para Retirar o mini dos relatórios e agendas, pois nenhuma loja da franquia tem MINI;*/
            if (UsuarioLogado.LojasUsuario != null && UsuarioLogado.Usuario.Perfil.ToUpper() != "Administrador Sistema".ToUpper())
            {
                if (UsuarioLogado.LojasUsuario.Count > 0)
                {
                    if (!(UsuarioLogado.LojasUsuario.Where(x => x.filial.ToUpper().Contains("FRANQUIA")).ToList().Count() > 0))
                    {
                        items.Add(new SelectListItem() { Text = "Ranking Corner Mini", Value = "SP_RelatorioRankingCornerMini" });
                        //items.Add(new SelectListItem() { Text = "Ranking Corner Eva", Value = "SP_RelatorioRankingCornerEva" });
                    }
                }
                else
                {
                    items.Add(new SelectListItem() { Text = "Ranking Corner Mini", Value = "SP_RelatorioRankingCornerMini" });
                    // items.Add(new SelectListItem() { Text = "Ranking Corner Eva", Value = "SP_RelatorioRankingCornerEva" });
                }
            }
            //08/05/2019 - RETIRADO POIS NAO EH USADO E NAO FUNCIONA
            //else
            //{
            //    items.Add(new SelectListItem() { Text = "Ranking Corner Mini", Value = "SP_RelatorioRankingCornerMini" });
            //    //items.Add(new SelectListItem() { Text = "Ranking Corner Eva", Value = "SP_RelatorioRankingCornerEva" });
            //}
            //items.Add(new SelectListItem() { Text = "Ranking Corner Oficina", Value = "SP_RelatorioRankingCornerOficina" });

            items.Add(new SelectListItem() { Text = "Ranking Oficina", Value = "SP_RelatorioRankingOficina" });

            //items.Add(new SelectListItem() { Text = "Efetividade Vendedores", Value = "SP_RelatorioDedoDuro#Vendedores#" });
            //items.Add(new SelectListItem() { Text = "Efetividade Geral", Value = "SP_RelatorioEfetividadeGeral" });
            items.Add(new SelectListItem() { Text = "Efetividade Agenda Loja", Value = "SP_RelatorioEfetividadeAgenda" });
            items.Add(new SelectListItem() { Text = "Efetividade Agenda Vendedor", Value = "SP_RelatorioEfetividadeAgenda_por_vendedor" });
            //items.Add(new SelectListItem() { Text = "Efetividade Corner", Value = "SP_RelatorioEfetividadeMini" });
            items.Add(new SelectListItem() { Text = "Efetividade Pós Vendas Vendedores", Value = "SP_Email_EfetividadesPorAgendaLojaVendedor#47" });
            items.Add(new SelectListItem() { Text = "Efetividade Aniversariantes Dia Vendedores", Value = "SP_Email_EfetividadesPorAgendaLojaVendedor#3" });
            items.Add(new SelectListItem() { Text = "Efetividade Inativando Vendedores", Value = "SP_Email_EfetividadesPorAgendaLojaVendedor#49" });
            items.Add(new SelectListItem() { Text = "Efetividade Contato Direto Vendedores", Value = "SP_Email_EfetividadesPorAgendaLojaVendedor#1" });
            items.Add(new SelectListItem() { Text = "Produtividade Gerente", Value = "SP_RelatorioProdutividadeGerente" });
            items.Add(new SelectListItem() { Text = "Efetividade Lojas", Value = "SP_RelatorioEfetividadeLojas" });
            items.Add(new SelectListItem() { Text = "Clientes Contatados", Value = "SP_RelatorioClienteContatado" });
            items.Add(new SelectListItem() { Text = "Dedo Duro", Value = "SP_RelatorioDedoDuro" });
            items.Add(new SelectListItem() { Text = "Info Vendas", Value = "SP_RelatorioInfoVendas" });
            items.Add(new SelectListItem() { Text = "Cadastro de Dependentes", Value = "SP_RelatorioCadastroDependentes" });
            items.Add(new SelectListItem() { Text = "Acompanhamento Delivery", Value = "SP_RelatorioAcompanhamentoOmni" });

            //if (UsuarioLogado.LojasUsuario.Where(x => x.filial.Contains("COMMERCE")).ToList().Count() > 0 || UsuarioLogado.Usuario.Perfil.ToUpper() == "Administrador Sistema".ToUpper())
            //{
            //    items.Add(new SelectListItem() { Text = "Ranking Vendedores Dia", Value = "SP_RelatorioECommerceReitireLojaVendaDia" });
            //    items.Add(new SelectListItem() { Text = "Ranking Vendedores Após", Value = "SP_RelatorioECommerceReitireLojaVendaApos" });
            //}            

            return new SelectList(items, "Value", "Text");
        }

        public IList<Loja> LojasPorCanais()
        {
            IList<Loja> _lojas = new List<Loja>();

            IList<Loja> _lojasUsuario = (UsuarioLogado.Usuario.Perfil.ToUpper().Contains("ADMINISTRADOR") || UsuarioLogado.Usuario.Lojas.Count == 0) ? _todasLojas : UsuarioLogado.Usuario.Lojas;

            foreach (var item in UsuarioLogado.Canais)
            {
                string[] tipo = item.Split(',');
                string canal1 = string.Empty;
                string canal2 = string.Empty;

                if (tipo.Length == 1)
                {
                    canal1 = tipo[0];
                    canal2 = tipo[0];
                }
                else if (tipo.Length > 1)
                {
                    canal1 = tipo[0];
                    canal2 = tipo[1];
                }

                foreach (var loja in _lojasUsuario.Where(x => (x.tipo_loja == canal1 || x.tipo_loja == canal2)).ToList())
                {
                    _lojas.Add(loja);
                }

                //foreach (var itemLoja in UsuarioLogado.LojasSupervisores)
                //{
                //    foreach (var loja in _lojasUsuario.Where(x => (x.tipo_loja == canal1 || x.tipo_loja == canal2) && x.id == itemLoja.id).ToList())
                //    {
                //        _lojas.Add(loja);
                //    }
                //}
            }

            return _lojas;
        }

        public static string GetPictureUrl(string faceBookId)
        {
            WebResponse response = null;
            string pictureUrl = string.Empty;
            try
            {
                WebRequest request = WebRequest.Create(string.Format("https://graph.facebook.com/{0}/picture", faceBookId));
                response = request.GetResponse();
                pictureUrl = response.ResponseUri.ToString();
            }
            catch (Exception ex)
            {
                //? handle
            }
            finally
            {
                if (response != null) response.Close();
            }
            return pictureUrl;
        }

        #region AJAX

        public JsonResult TrocarVendedor(string id)
        {


            if (UsuarioLogado.IdVendedorSeleconado != id)
            {
                // pega o Nome do Vendedor Selecionado
                VendedorNome = UsuarioLogado.VendedoresUsuario.Where(x => x.id == Convert.ToInt32(id)).FirstOrDefault().nome;
                UsuarioLogado.IdVendedorSeleconado = id;

                LogSistema(new LogSistema { acao = "Tocando o vendedor para: " + VendedorNome, login = UsuarioLogado.Usuario.email, tipo_log = "NAVEGACAO", vendedor_id = (String.IsNullOrEmpty(UsuarioLogado.IdVendedorSeleconado) ? 0 : Convert.ToInt32(UsuarioLogado.IdVendedorSeleconado)), loja_id = (String.IsNullOrEmpty(UsuarioLogado.IdLojaSeleconado) ? 0 : Convert.ToInt32(UsuarioLogado.IdLojaSeleconado)), metodo = "TrocarVendedor", tela = "Base", user_id = UsuarioLogado.Usuario.id, ip = HttpContext.Request.UserHostAddress.ToString(), data = DateTime.Now });
            }
            else
            {
                UsuarioLogado.IdVendedorSeleconado = null;
                id = String.Empty;
                VendedorNome = UsuarioLogado.Usuario.username;
                _trocou = true;

                LogSistema(new LogSistema { acao = "Retirando o vendedor selecionado:  " + VendedorNome, login = UsuarioLogado.Usuario.email, tipo_log = "NAVEGACAO", vendedor_id = (String.IsNullOrEmpty(UsuarioLogado.IdVendedorSeleconado) ? 0 : Convert.ToInt32(UsuarioLogado.IdVendedorSeleconado)), loja_id = (String.IsNullOrEmpty(UsuarioLogado.IdLojaSeleconado) ? 0 : Convert.ToInt32(UsuarioLogado.IdLojaSeleconado)), metodo = "TrocarVendedor", tela = "Base", user_id = UsuarioLogado.Usuario.id, ip = HttpContext.Request.UserHostAddress.ToString(), data = DateTime.Now });
            }

            ModificaSessao(UsuarioLogado);

            _trocouVendedor = true;

            return JsonRetono(VendedoresSelectList(id, String.Empty));
        }

        public void LogSistema(LogSistema log)
        {
            IList<LogSistema> logSistema = new List<LogSistema>();
            if (_logSistema != null)
                logSistema = _logSistema;

            if (_logSistemaTmp == null)
                _logSistemaTmp = new List<LogSistema>();

            if (_logSistemaTmp.Where(x => x.acao == log.acao && x.cliente_id == log.cliente_id && x.data.ToString("dd-MM-yyyy hh:mm") == log.data.ToString("dd-MM-yyyy hh:mm") && x.erro == log.erro && x.ip == log.ip && x.login == log.login && x.loja_id == log.loja_id && x.metodo == log.metodo && x.tela == log.tela && x.tipo_log == log.tipo_log && x.user_id == log.user_id && x.vendedor_id == log.vendedor_id).ToList().Count() == 0)
            {
                if (!log.login.Contains("peterson.andrade"))
                {
                    logSistema.Add(log);
                    _logSistemaTmp.Add(log);
                }
            }

            _logSistema = logSistema;
        }

        public double TempoProcesso(DateTime dtInicio, DateTime ftFim)
        {
            TimeSpan ts = ftFim - dtInicio;

            return ts.TotalSeconds;
        }

        public JsonResult AdicionarCanal(string addCanal, string delCanal)
        {
            // Limpa Lista Todas as Lojas
            UsuarioLogado.TodasLojasCanaisUsuario = new List<Loja>();

            _trocou = true;

            UsuarioLogado.Canais.Remove(delCanal);

            if (!UsuarioLogado.Canais.Contains(addCanal) && addCanal != "")
                UsuarioLogado.Canais.Add(addCanal);

            // Se as lojas estiverem zeradas eu volto a alteração do canal
            if (LojasPorCanais().Count == 0)
            {
                if (!UsuarioLogado.Canais.Contains(delCanal))
                    UsuarioLogado.Canais.Add(delCanal);
            }

            //UsuarioLogado.LojasCanais = LojasPorCanais();
            UsuarioLogado.IdLojaSeleconado = null;
            UsuarioLogado.IdVendedorSeleconado = null;

            LimparSessaoDashboard();

            ModificaSessao(UsuarioLogado);

            IList<Loja> lojasTodas = Session["_TodasLojas"] as List<Loja>;

            foreach (var canal in UsuarioLogado.Canais)
            {
                if (canal.Contains("RESERVA"))
                {
                    // Adicina Lojas do Canal
                    foreach (var _item in lojasTodas.Where(x => x.tipo_loja == "RESERVA" || x.tipo_loja == "RESERVA+MINI").ToList())
                    {
                        if (!UsuarioLogado.TodasLojasCanaisUsuario.Contains(_item))
                            UsuarioLogado.TodasLojasCanaisUsuario.Add(_item);
                    }
                }
                else
                {
                    // Adicina Lojas do Canal
                    foreach (var _item in lojasTodas.Where(x => x.tipo_loja == canal).ToList())
                    {
                        if (!UsuarioLogado.TodasLojasCanaisUsuario.Contains(_item))
                            UsuarioLogado.TodasLojasCanaisUsuario.Add(_item);
                    }
                }
            }

            // Se o canal tiver uma unica loja já atribuir essa loja como selecionada.
            if (UsuarioLogado.TodasLojasCanaisUsuario.Count == 1)
            {
                UsuarioLogado.IdLojaSeleconado = UsuarioLogado.TodasLojasCanaisUsuario.FirstOrDefault().id.ToString();
            }

            return JsonRetono(CanaisSelectList());
        }

        public void SelecionarTermometroContato(string item, string value)
        {
            //TermometroSelecionado = item ?? value;

            //var result = ObterTemometroList(item);

            //ViewBag.Termometro = result;

            //return JsonRetono(result);
        }

        public JsonResult AdicionarSuperv(string addSuper, string delSuper)
        {
            IList<Supervisores> list = Session["_Supervisores"] == null ? new List<Supervisores>() : Session["_Supervisores"] as IList<Supervisores>;

            IList<string> canaisSupervisao = new List<string>();
            IList<supervisorRetorno> canaisSupervisaoNovo = new List<supervisorRetorno>();

            string retSuperv = "";

            _trocou = true;

            TrocarSupervisor(list, addSuper, delSuper);

            // Se as lojas estiverem zeradas eu volto a alteração do ssupervisor
            if (UsuarioLogado.LojasSupervisores.Count() == 0)
            {
                // Inverti a deleção pela inserção
                TrocarSupervisor(list, delSuper, addSuper);
                retSuperv = delSuper;
            }

            //UsuarioLogado.LojasCanais = UsuarioLogado.LojasSupervisores;
            UsuarioLogado.IdLojaSeleconado = null;
            UsuarioLogado.IdVendedorSeleconado = null;

            LimparSessaoDashboard();

            foreach (var item in UsuarioLogado.LojasSupervisores)
            {
                string _item = (item.tipo_loja == "RESERVA+MINI" || item.tipo_loja == "RESERVA") ? "RESERVA,RESERVA+MINI" : item.tipo_loja;

                if (canaisSupervisao.Where(x => x.ToString() == _item).Count() == 0)
                    canaisSupervisao.Add(_item);
            }

            foreach (var item in CanaisSelectList())
            {
                if (canaisSupervisao.Where(x => x.ToString() == item.Value).Count() == 0)
                    canaisSupervisaoNovo.Add(new supervisorRetorno() { canal = item.Value.Replace("+", "_").Replace(",", "_"), idvolta_supervisao = retSuperv });
            }

            CanaisExcluidoSupervisores = canaisSupervisaoNovo;

            Session["_Supervisores"] = list;

            ModificaSessao(UsuarioLogado);

            return JsonRetono(canaisSupervisaoNovo);
        }

        public struct supervisorRetorno
        {
            public string canal;
            public string idvolta_supervisao;
        }

        private void TrocarSupervisor(IList<Supervisores> list, string addSuper, string delSuper)
        {
            if (!String.IsNullOrEmpty(addSuper))
            {
                foreach (var item in list)
                {
                    if (item.id_supervisor == Convert.ToInt32(addSuper))
                    {
                        item.flg_selecao = 1;
                    }
                }
            }

            if (!String.IsNullOrEmpty(delSuper))
            {
                foreach (var item in list)
                {
                    if (item.id_supervisor == Convert.ToInt32(delSuper))
                    {
                        item.flg_selecao = 0;
                    }
                }
            }

            UsuarioLogado.LojasSupervisores.Clear();
            // Só Deixa Selecionado as lojas dos supervisores ativos            

            foreach (var item in list.Where(x => x.flg_selecao == 1).ToList())
            {
                foreach (var itemLojas in UsuarioLogado.SupervisorLojas.Where(x => x.id_supervisor == item.id_supervisor).ToList())
                {
                    var _itemLoja = UsuarioLogado.LojasUsuario.Where(x => x.id == itemLojas.id_loja).FirstOrDefault();

                    if (_itemLoja != null)
                    {
                        UsuarioLogado.LojasSupervisores.Add(_itemLoja);
                    }
                }
            }
        }

        public void LimparSessaoDashboard()
        {
            // Limpa as Listas do Dashboard para ser carregadas novamente
            UsuarioLogado.ListaEfetividadeAgenda = null;
            UsuarioLogado.ListaEfetividadeLojas = null;
            UsuarioLogado.ListaEfetividadeVendedores = null;
            UsuarioLogado.ListaRankingVendedores = null;
            UsuarioLogado.Pa = null;
            UsuarioLogado.Tm = null;
            _clima = null;
        }

        #endregion

        WebClient wc;

        public BaseController()
        {
            this.wc = new WebClient();

            this.wc.Headers.Add("Authorization", "Token Vwhe63ifG2iPNQD4CmwJR-OHzQPp33WXQMb35mzJ");
        }

        public string ObterPrecoUber(string inicioLatitude, string inicioLongitude, string fimLatitude, string fimLongitude)
        {
            return string.Empty;
            //return wc.DownloadString(new System.Uri(string.Format("https://api.uber.com/v1.2/estimates/price?start_latitude={0}&start_longitude={1}&end_latitude={2}&end_longitude={3}", inicioLatitude, inicioLongitude, fimLatitude, fimLongitude)));
        }

        public SelectList ClienteVendas(string cpf, string IdLoja, IClienteService _serviceCliente, IVendaService _serviceVenda, string _dataDefault = "")
        {
            DateTime TempoInicial = DateTime.Now;

            IList<string> list = new List<string>();
            List<SelectListItem> items = new List<SelectListItem>();

            // Se só existir uma única loja pegar da sessão a loja selecionada
            if (String.IsNullOrEmpty(IdLoja))
                IdLoja = UsuarioLogado.IdLojaSeleconado;

            if (!String.IsNullOrEmpty(IdLoja) && !String.IsNullOrEmpty(cpf.Trim()))
            {
                var cliente = _serviceCliente.BuscarClienteCpfOrigemLoja(OrigemLoja, cpf).FirstOrDefault();

                Session["cliente"] = cliente;
                // Limpa os dependentes do info vendas
                Session["ClienteNovoDependente"] = null;

                if (cliente != null)
                {
                    foreach (var item in _serviceVenda.HistoricoVendas(cliente.id.ToString()).Where(x => x.IdLoja == Convert.ToInt32(IdLoja)).OrderByDescending(d => d.Data).ToList())
                    {
                        if (!list.Contains(item.Data.ToString("dd/MM/yyyy")))
                            list.Add(item.Data.ToString("dd/MM/yyyy"));
                    }
                }

                // Se não existir data de venda só vai existir a data de hoje
                if (list.Count > 0)
                {
                    if (String.IsNullOrEmpty(_dataDefault))
                        items.Add(new SelectListItem() { Text = "Selecione...", Value = "", Selected = true });
                    else
                        items.Add(new SelectListItem() { Text = "Selecione...", Value = "" });
                }

                // Se a Venda for hoje seleciona a data de hoje
                items.Add(new SelectListItem
                {
                    Text = DateTime.Now.ToString("dd/MM/yyyy"),
                    Value = DateTime.Now.ToString("dd/MM/yyyy"),
                    Selected = (_dataDefault == DateTime.Now.ToString("dd/MM/yyyy"))

                });

                items.AddRange(list.Select(item => new SelectListItem
                {
                    Text = item,
                    Value = item,
                    Selected = (item == _dataDefault)
                }));
            }

            DateTime TempoFinal = DateTime.Now;

            // Log do sistema
            LogSistema(new LogSistema { processo_minutos = TempoProcesso(TempoInicial, TempoFinal), acao = "Buscando Cliente pelo cpf no info vendas. Dados: cpf - " + cpf, login = UsuarioLogado.Usuario.email, tipo_log = "CONSULTANDO", vendedor_id = (String.IsNullOrEmpty(UsuarioLogado.IdVendedorSeleconado) ? 0 : Convert.ToInt32(UsuarioLogado.IdVendedorSeleconado)), loja_id = (String.IsNullOrEmpty(UsuarioLogado.IdLojaSeleconado) ? 0 : Convert.ToInt32(UsuarioLogado.IdLojaSeleconado)), metodo = "Info Vendas - Consullta", tela = "Info Vendas", user_id = UsuarioLogado.Usuario.id, ip = HttpContext.Request.UserHostAddress.ToString(), data = DateTime.Now });

            return new SelectList(items, "Value", "Text", _dataDefault);
        }

        public JsonResult JsonRetono(object data)
        {
            JsonResult jsonResult = Json(data, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = int.MaxValue;
            return jsonResult;
        }

        public string GeneratePassword()
        {
            string strPwdchar = "abcdefghijklmnopqrstuvwxyz0123456789#+@&$ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            string strPwd = "";
            Random rnd = new Random();
            for (int i = 0; i <= 7; i++)
            {
                int iRandom = rnd.Next(0, strPwdchar.Length - 1);
                strPwd += strPwdchar.Substring(iRandom, 1);
            }
            return strPwd;
        }


        public string CorpoEmailSenhaDistribuicao(string senha)
        {
            string corpoEmail = @" 

            <html xmlns='http://www.w3.org/1999/xhtml'>
<head>
    <meta http-equiv='content-type' content='text/html; charset=utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1.0;'>
    <meta name='format-detection' content='telephone=no' />
    <!-- Responsive Mobile-First Email Template by Konstantin Savchenko, 2015.
    https://github.com/konsav/email-templates/  -->
    <style>
        /* Reset styles */
        body {
            margin: 0;
            padding: 0;
            min-width: 100%;
            width: 100% !important;
            height: 100% !important;
        }

        body, table, td, div, p, a {
            -webkit-font-smoothing: antialiased;
            text-size-adjust: 100%;
            -ms-text-size-adjust: 100%;
            -webkit-text-size-adjust: 100%;
            line-height: 100%;
        }

        table, td {
            mso-table-lspace: 0pt;
            mso-table-rspace: 0pt;
            border-collapse: collapse !important;
            border-spacing: 0;
        }

        img {
            border: 0;
            line-height: 100%;
            outline: none;
            text-decoration: none;
            -ms-interpolation-mode: bicubic;
        }

        #outlook a {
            padding: 0;
        }

        .ReadMsgBody {
            width: 100%;
        }

        .ExternalClass {
            width: 100%;
        }

            .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {
                line-height: 100%;
            }

        /* Rounded corners for advanced mail clients only */
        @media all and (min-width: 560px) {
            .container {
                border-radius: 8px;
                -webkit-border-radius: 8px;
                -moz-border-radius: 8px;
                -khtml-border-radius: 8px;
            }
        }

        /* Set color for auto links (addresses, dates, etc.) */
        a, a:hover {
            color: #127DB3;
        }

        .footer a, .footer a:hover {
            color: #999999;
        }
    </style>
    <!-- MESSAGE SUBJECT -->
    <title>Solicitação de troca de senha NOW</title>
</head>
<!-- BODY -->
<!-- Set message background color (twice) and text color (twice) -->
<body topmargin='0' rightmargin='0' bottommargin='0' leftmargin='0' marginwidth='0' marginheight='0' width='100%' style='border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0; width: 100%; height: 100%; -webkit-font-smoothing: antialiased; text-size-adjust: 100%; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; line-height: 100%;
	background-color: #F0F0F0;
	color: #000000;'
      bgcolor='#F0F0F0'
      text='#000000'>
    <!-- SECTION / BACKGROUND -->
    <!-- Set message background color one again -->
    <table width='100%' align='center' border='0' cellpadding='0' cellspacing='0' style='border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0; width: 100%;' class='background'>
        <tr>
            <td align='center' valign='top' style='border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0;'
                bgcolor='#F0F0F0'>
                <!-- WRAPPER -->
                <!-- Set wrapper width (twice) -->
                <table border='0' cellpadding='0' cellspacing='0' align='center'
                       width='560' style='border-collapse: collapse; border-spacing: 0; padding: 0; width: inherit;
	max-width: 560px;' class='wrapper'>
                    <tr>
                        <td align='center' valign='top' style='border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0; padding-left: 6.25%; padding-right: 6.25%; width: 87.5%;
			padding-top: 20px;
			padding-bottom: 20px;'>
                            <!-- PREHEADER -->
                            <!-- Set text color to background color -->
                            <div style='display: none; visibility: hidden; overflow: hidden; opacity: 0; font-size: 1px; line-height: 1px; height: 0; max-height: 0; max-width: 0;
			color: #F0F0F0;' class='preheader'>
                                Available on&nbsp;GitHub and&nbsp;CodePen. Highly compatible. Designer friendly. More than 50%&nbsp;of&nbsp;total email opens occurred on&nbsp;a&nbsp;mobile device&nbsp;— a&nbsp;mobile-friendly design is&nbsp;a&nbsp;must for&nbsp;email campaigns.
                            </div>
                            <!-- LOGO -->
                            <!-- Image text color should be opposite to background color. Set your url, image src, alt and title. Alt text should fit the image size. Real image size should be x2. URL format: http://domain.com/?utm_source={{Campaign-Source}}&utm_medium=email&utm_content=logo&utm_campaign={{Campaign-Name}} -->
                            <a target='_blank' style='text-decoration: none;'
                               href='https://github.com/konsav/email-templates/'>
                                <img border='0' vspace='0' hspace='0'
                                     src='http://usenow.com.br/Images/LogoNOW.png'
                                     width='100' height='30'
                                     alt='Logo' title='Logo' style='
				color: #000000;
				font-size: 10px; margin: 0; padding: 0; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; border: none; display: block;' />
                            </a>
                        </td>
                    </tr>
                    <!-- End of WRAPPER -->
                </table>
                <!-- WRAPPER / CONTEINER -->
                <!-- Set conteiner background color -->
                <table border='0' cellpadding='0' cellspacing='0' align='center'
                       bgcolor='#FFFFFF'
                       width='560' style='border-collapse: collapse; border-spacing: 0; padding: 0; width: inherit;
	max-width: 560px;' class='container'>
                    <!-- HEADER -->
                    <!-- Set text color and font family ('sans-serif' or 'Georgia, serif') -->
                    <tr>
                        <td align='center' valign='top' style='border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0; padding-left: 6.25%; padding-right: 6.25%; width: 87.5%; font-size: 24px; font-weight: bold; line-height: 130%;
			padding-top: 25px;
			color: #000000;
			font-family: sans-serif;' class='header'>
                #VENDEDOR#, 
                            
                        </td>
                    </tr>

                    <!-- SUBHEADER -->
                    <!-- Set text color and font family ('sans-serif' or 'Georgia, serif') -->
                    <tr>
                        <td align='center' valign='top' style='border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0; padding-bottom: 3px; padding-left: 6.25%; padding-right: 6.25%; width: 87.5%; font-size: 18px; font-weight: 300; line-height: 150%;
			padding-top: 5px;
			color: #000000;
			font-family: sans-serif;' class='subheader'>
                            Nova senha Criada, Acesso a tela distribuição de Agendas!
                        </td>
                    </tr>
                    <!-- HERO IMAGE -->
                    <!-- Image text color should be opposite to background color. Set your url, image src, alt and title. Alt text should fit the image size. Real image size should be x2 (wrapper x2). Do not set height for flexible images (including 'auto'). URL format: http://domain.com/?utm_source={{Campaign-Source}}&utm_medium=email&utm_content={{Ìmage-Name}}&utm_campaign={{Campaign-Name}} -->
                    <!-- PARAGRAPH -->
                    <!-- Set text color and font family ('sans-serif' or 'Georgia, serif'). Duplicate all text styles in links, including line-height -->
                    <!-- Set line color -->
                    <tr>
                        <td align='center' valign='top' style='border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0; padding-left: 6.25%; padding-right: 6.25%; width: 87.5%;
			padding-top: 25px;' class='line'>
                            <hr color='#E0E0E0' align='center' width='100%' size='1' noshade style='margin: 0; padding: 0;' />
                        </td>
                    </tr>
                    <tr>
                        <td align='center' valign='top' style='border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0; padding-left: 6.25%; padding-right: 6.25%; width: 87.5%; font-size: 17px; font-weight: 400; line-height: 160%;
			padding-top: 25px;
			color: #000000;
			font-family: sans-serif;' class='paragraph'>
                            Utilize a senha para acessar a tela da distribuição de agendas.
                        </td>
                    </tr>
                    <!-- BUTTON -->
                    <!-- Set button background color at TD, link/text color at A and TD, font family ('sans-serif' or 'Georgia, serif') at TD. For verification codes add 'letter-spacing: 5px;'. Link format: http://domain.com/?utm_source={{Campaign-Source}}&utm_medium=email&utm_content={{Button-Name}}&utm_campaign={{Campaign-Name}} -->
                    <tr>
                        <td align='center' valign='top' style='border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0; padding-left: 6.25%; padding-right: 6.25%; width: 87.5%;
			padding-top: 25px;
			padding-bottom: 5px;' class='button'>
                            
                                <table border='0' cellpadding='0' cellspacing='0' align='center' style='max-width: 240px; min-width: 120px; border-collapse: collapse; border-spacing: 0; padding: 0;'>
                                    <tr>
                                        <td align='center' valign='middle' style='padding: 12px 24px; margin: 0; border-collapse: collapse; border-spacing: 0; border-radius: 4px; -webkit-border-radius: 4px; -moz-border-radius: 4px; -khtml-border-radius: 4px;'
                                            bgcolor='#E9703E'>
                                            <div style='
					color: #FFFFFF; font-family: sans-serif; font-size: 17px; font-weight: 400; line-height: 120%;'
                                               >
                                                #SENHA#
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            
                        </td>
                    </tr>
                    <!-- LINE -->
                    <!-- LINE -->
                    <!-- Set line color -->
                    <tr>
                        <td align='center' valign='top' style='border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0; padding-left: 6.25%; padding-right: 6.25%; width: 87.5%;
			padding-top: 25px;' class='line'>
                            <hr color='#E0E0E0' align='center' width='100%' size='1' noshade style='margin: 0; padding: 0;' />
                        </td>
                    </tr>
                    <!-- PARAGRAPH -->
                    <!-- Set text color and font family ('sans-serif' or 'Georgia, serif'). Duplicate all text styles in links, including line-height -->
                    <tr>
                        <td align='center' valign='top' style='border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0; padding-left: 6.25%; padding-right: 6.25%; width: 87.5%; font-size: 17px; font-weight: 400; line-height: 160%;
			padding-top: 20px;
			padding-bottom: 25px;
			color: #000000;
			font-family: sans-serif;' class='paragraph'>
                            Dúvidas? <a href='mailto:suporte@usereserva.com' target='_blank' style='color: #127DB3; font-family: sans-serif; font-size: 17px; font-weight: 400; line-height: 160%;'>suporte@usereserva.com</a>
                        </td>
                    </tr>
                    <!-- End of WRAPPER -->
                </table>
                <!-- WRAPPER -->
                <!-- Set wrapper width (twice) -->
                <table border='0' cellpadding='0' cellspacing='0' align='center'
                       width='560' style='border-collapse: collapse; border-spacing: 0; padding: 0; width: inherit;
	max-width: 560px;' class='wrapper'>

                    <!-- Set text color and font family ('sans-serif' or 'Georgia, serif'). Duplicate all text styles in links, including line-height -->
                    <tr>
                        <td align='center' valign='top' style='border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0; padding-left: 6.25%; padding-right: 6.25%; width: 87.5%; font-size: 13px; font-weight: 400; line-height: 150%;
			padding-top: 20px;
			padding-bottom: 20px;
			color: #999999;
			font-family: sans-serif;' class='footer'>
                            ® Reserva 2016 <a href='https://www.usenow.com.br' target='_blank' style='text-decoration: underline; color: #999999; font-family: sans-serif; font-size: 13px; font-weight: 400; line-height: 150%;'>UseNow</a>.

                            <!-- ANALYTICS -->
                            <!-- http://www.google-analytics.com/collect?v=1&tid={{UA-Tracking-ID}}&cid={{Client-ID}}&t=event&ec=email&ea=open&cs={{Campaign-Source}}&cm=email&cn={{Campaign-Name}} -->
                            <img width='1' height='1' border='0' vspace='0' hspace='0' style='margin: 0; padding: 0; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; border: none; display: block;'
                                 src='https://raw.githubusercontent.com/konsav/email-templates/master/images/tracker.png' />
                        </td>
                    </tr>
                    <!-- End of WRAPPER -->
                </table>
                <!-- End of SECTION / BACKGROUND -->
            </td>
        </tr>
    </table>
</body>
</html>";

            return corpoEmail.Replace("#SENHA#", senha);
        }

    }
}
