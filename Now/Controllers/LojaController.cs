﻿using Now.Domain;
using Now.Models;
using Now.Domain.Contracts.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Now.Controllers
{
    public class LojaController : BaseController
    {
        public ILojaService _serviceLoja;

        public LojaController(ILojaService serviceLoja)
        {
            _serviceLoja = serviceLoja;
        }

        public ActionResult Index()
        {
            return View();
        }

        public JsonResult BuscarLojaUsuario()
        {
            return JsonRetono(new { dados = LojasSelectList(String.Empty, UsuarioLogado.IdLojaSeleconado), perfil = UsuarioLogado.Usuario.Perfil.ToUpper().ToUpper() });
        }

        public JsonResult ListarLojaDistribuicaoOrigemDestino()
        {
            var list = _serviceLoja.ListarLojaDistribuicaoOrigemDestino();

            return JsonRetono(new { lista = list });
        }

        public JsonResult SalvarLojaDistribuicaoOrigemDestino(string id, string lojaOrigem, string lojaDestino, string total, string acao)
        {
            LojaDistribuicaoOrigemDestino obj = new LojaDistribuicaoOrigemDestino();

            obj.id = String.IsNullOrEmpty(id) ? obj.id : Convert.ToInt32(id);
            obj.origem_loja_id = Convert.ToInt32(lojaOrigem);
            obj.destino_loja_id = Convert.ToInt32(lojaDestino);
            obj.total_distribuicao = Convert.ToInt32(total);
            obj.data_alteracao = DateTime.Now;
            obj.data_exclusao = null;

            if (acao == "EXCLUIR")
                obj.data_exclusao = DateTime.Now;
            else if (acao == "INCLUIR")
                obj.data_inclusao = DateTime.Now;

            var list = _serviceLoja.SalvarLojaDistribuicaoOrigemDestino(obj);

            return JsonRetono(new { lista = list });
        }

       

    }
}
