var gulp = require('gulp')
, minifyCss = require("gulp-minify-css")
, uglify = require("gulp-uglify")
, imagemin = require('gulp-imagemin');
var minifyCshtml = require('gulp-cshtml-minifier');
var rename = require("gulp-rename"); // install gulp-rename
var header = require('gulp-header'); // install gulp-header
var useref = require('gulp-useref');
var gulpIf = require('gulp-if');
var concatCss = require('gulp-concat-css');
var concat = require('gulp-concat');
 
 
//gulp.task('default', ['minify-cshtml']);
gulp.task('html', (done) => {
  gulp.src(['./Views/Cliente/_HistoricoCompras.cshtml'])
    .pipe(rename('_HistoricoCompras.min.cshtml')) // File Rename
    .pipe(minifyCshtml({
      htmlComments: true,   // Remove HTML comments <!-- -->
      jsComments: true,     // Remove JS comments /* */
      razorComments: true,  // Remove Razor comments @* *@
      whitespace: true      // Remove white-space
      //replaceHash: /hash/ig // Replace 'hash' to datetime string
    }))
    .pipe(header('\ufeff')) // Fixed encode issue (可解決中文亂碼問題)
    .pipe(gulp.dest('./Views/Cliente'));
	
	done();
});

gulp.task('css', function () {
	return gulp.src('./Content/**/*.css') // path to your file
	//.pipe(concatCss('main.min.css'))
	.pipe(minifyCss())
	.pipe(gulp.dest('app/css'));
});

gulp.task('js', function () {
	return gulp.src('./plugins/js/*.js') // path to your files
	//.pipe(concat('main.min.js'))
    // Minifies only if it's a JavaScript file
    .pipe(gulpIf('*.js', uglify()))
	.pipe(gulp.dest('app/js'));
});

gulp.task('img', function(){
  return gulp.src('./images/**/*.+(png|jpg|gif|svg)')
  .pipe(imagemin())
  .pipe(gulp.dest('app/img'))
});
