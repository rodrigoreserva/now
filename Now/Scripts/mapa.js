var map;
var directionsDisplay;
var directionsService = new google.maps.DirectionsService();

function initialize() {
    $("#tbxEndLoja").val($("#hddEndLoja").val());
    $("#tbxEndCliente").val($("#lblEndereco").text());
    montarMapa();
}

function procurarMapa() {    
    montarMapa();    
}

function montarMapa() {
    $("#trajeto-texto").html('');    
    
        map = new google.maps.Map(document.getElementById("mapa"), options);

    var rendererOptions = {
        map: map,
        suppressMarkers: true
    }

    directionsDisplay = new google.maps.DirectionsRenderer(rendererOptions);
    var latlng = new google.maps.LatLng(-18.8800397, -47.05878999999999);

    var options = {
        zoom: 6,
        center: latlng,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };


    directionsDisplay.setMap(map);
    directionsDisplay.setPanel(document.getElementById("trajeto-texto"));

    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function (position) {
            pontoPadrao = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
            map.setCenter(pontoPadrao);

            var geocoder = new google.maps.Geocoder();

            geocoder.geocode({
                "location": new google.maps.LatLng(position.coords.latitude, position.coords.longitude)
            },
            function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    $("#txtEnderecoPartida").val(results[0].formatted_address);
                }
            });
        });
    }
    setTimeout(function () { google.maps.event.trigger(map, 'resize'); direcaoMapa(); }, 1000);

    setTimeout(function () { $(".maps-header").html("Distancia da Loja x Cliente - " + $(".adp-summary").text()); ObterPrecoUber(); }, 3000);

}

function direcaoMapa() {
    var enderecoPartida = $("#tbxEndLoja").val(); ///$("#txtEnderecoPartida").val();
    var enderecoChegada = $("#tbxEndCliente").val();

    //alert(enderecoPartida);
    //alert(enderecoChegada);
    //$("#txtEnderecoChegada").val();

    var request = {
        origin: enderecoPartida,
        destination: enderecoChegada,
        travelMode: google.maps.TravelMode.DRIVING
    };



    directionsService.route(request, function (result, status) {
        if (status == google.maps.DirectionsStatus.OK) {
            directionsDisplay.setDirections(result);
            showSteps(result);
        }
        else {
            if ($("#hddEndLoja").val().trim() == ',') {
                $('#localizacaoBody').html('<div class="row"><div class="col-sm-12 text-center" style="padding-top:60px; padding-bottom:60px"><img src="..//Images//endereconaolocalizado.jpg"><p><h2>A Loja n�o foi selecionada, endere�o n�o encontrado!</h2></p></div></div>');
            } else {
                $('#localizacaoBody').html('<div class="row"><div class="col-sm-12 text-center" style="padding-top:60px; padding-bottom:60px"><img src="..//Images//endereconaolocalizado.jpg"><p><h2>O endere�o do cliente n�o foi encontrado!</h2></p></div></div>');
            }
        }
    });
}
/////http://googlemaps.googlermania.com/google_maps_api_v3/en/map_example_direction_customicon.html
function showSteps(directionResult) {
    // For each step, place a marker, and add the text to the marker's
    // info window. Also attach the marker to an array so we
    // can keep track of it and remove it when calculating new
    // routes.
    var myRoute = directionResult.routes[0].legs[0];

    for (var i = 0; i < myRoute.steps.length; i++) {
        var icon = "";//"https://chart.googleapis.com/chart?chst=d_map_pin_letter&chld=" + i + "|FF0000|000000";
        if (i == 0) {
            icon = "../Images/map-marker-picapau.fw.png";
            var marker = new google.maps.Marker({
                position: myRoute.steps[i].start_point,
                map: map,
                icon: icon
            });
            attachInstructionText(marker, myRoute.steps[i].instructions);

            $("#inicioLatitude").val(myRoute.steps[i].start_point.lat());
            $("#inicioLongitude").val(myRoute.steps[i].start_point.lng());
        }
    }

    var marker = new google.maps.Marker({
        position: myRoute.steps[i - 1].end_point,
        map: map,
        icon: "../Images/marker_cliente.fw.png"
    });

    $("#fimLatitude").val(myRoute.steps[i - 1].start_point.lat());
    $("#fimLongitude").val(myRoute.steps[i - 1].start_point.lng());
  
    markerArray.push(marker);

    //google.maps.event.trigger(markerArray[0], "click");
}

function attachInstructionText(marker, text) {
    google.maps.event.addListener(marker, 'click', function () {
        // Open an info window when the marker is clicked on,
        // containing the text of the step.
        stepDisplay.setContent(text);
        stepDisplay.open(map, marker);
    });
}

//google.maps.event.addDomListener(window, "load", initialize);
