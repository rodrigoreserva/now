﻿(function () {

    angular
        .module('appReserva')
        .controller('vendedorController', vendedorController);

    vendedorController.$inject = ['$scope', '$timeout', '$http'];


    function vendedorController($scope, $timeout, $http) {

        firebase.initializeApp({
            apiKey: "AIzaSyAQVZMRCnMBvzcPyznTDRCltGVPBZsX7Pw",
            authDomain: "now-web-cac79.firebaseapp.com",
            databaseURL: "https://now-web-cac79.firebaseio.com",
            projectId: "now-web-cac79",
            storageBucket: "now-web-cac79.appspot.com",
            messagingSenderId: "299199186520",
            appId: "1:299199186520:web:3cd2465457c76aa8"
        });


        firebase.database().ref(".info/connected").on("value", function (snap) {
            if (snap.val() === true) {

                $scope.online = true;
                $scope.result = true;
        
            } else {
                $scope.online = false;
                $scope.result = false;
                // $scope.$apply();
                atualizarFilas();
            }
        });

        function atualizarFilas() {
                        
        }

        $scope.atualizarTotal = function (valor_1, valor_2) {
            return parseFloat(valor_1) + parseFloat(valor_2)
        }

        $scope.calcularTaxaConversaoReservado = function (malaVendida, malaEnviada) {
            return malaEnviada == 0 ? 0 : (parseFloat(malaVendida) * 100) / parseFloat(malaEnviada)
        }

        $scope.calcularPercentualMeta = function (valor, meta) {
            var valor_meta_final = meta == 0 ? 0 : parseFloat(valor) * 100 / parseFloat(meta);

            if (valor_meta_final > 100)
                valor_meta_final = 100;

            return valor_meta_final;
        }

        $scope.validarRespostaQuestionario = function (questionario, respostaQuestionario) {
            angular.element('#respostaQuestionario').removeClass("erro-input-agenda");
            if (respostaQuestionario == $scope.vendedor[questionario.resposta].toString()) {
                if ($scope.respostas < 1) {
                    $scope.respostas++;
                    var min = 1;
                    var max = $scope.listaQuestionario.length;
                    var random = Math.floor(Math.random() * (+max - +min)) + +min;
                    $scope.questionario = $scope.listaQuestionario[random];
                    $scope.respostaQuestionario = "";
                }
                else {
                    $scope.fezQuestionario = true;
                    firebase.database().ref("lojas/" + localStorage.getItem('loja') + "/" + localStorage.getItem('vendedor') + "/acessado_em").set(moment().format())
                }

                $scope.avisoErro = "";
                angular.element('#respostaQuestionario').removeClass("erro-input-agenda");
            }
            else {
                angular.element('#respostaQuestionario').addClass("erro-input-agenda");
            }

            $timeout(function () {
                $scope.$apply();

            });
        }

        firebase.database().ref("gamification/perguntas").on("value", function (snap) {
            firebase.database().ref("lojas/" + localStorage.getItem('loja') + "/" + localStorage.getItem('vendedor') + "/acessado_em").on("value", function (hora_value) {

                if ((hora_value.val() != "0001-01-01T02:00:00Z") && (moment(hora_value.val()).isBefore(moment(hora_value.val()).add(1, "hour")))) {
                    $scope.fezQuestionario = true;
                } else {

                    $scope.fezQuestionario = false;
                    $scope.avisoErro = "";

                    if (snap.val() == null) return;

                    var min = 1;
                    var max = snap.val().length;
                    var random = Math.floor(Math.random() * (+max - +min)) + +min;
                    $scope.questionario = snap.val()[random];
                    $scope.listaQuestionario = snap.val();
                    $scope.respostas = 0;
                }
                $timeout(function () {
                    $scope.$apply();
                });
            });
        })

        firebase.database().ref("lojas/" + localStorage.getItem('loja') + "/" + localStorage.getItem('vendedor')).on("value", function (snap) {

            if (snap.val() == null) return;

            //CONFERIR DEDO DURO
            //CONFERIR INFO VENDAS
            //VERIFICAR ACESSAR RELATORIO
            //VERIFICAR ACESSAR OUTRAS AGENDAS
            //CRIPTOGRAFAR URL
            //VOLTAR PARA A URL GERAL QUANDO FOR NA HOME

            //$scope.vendedor = snap.val()[localStorage.getItem('vendedor')];
            $scope.vendedor = snap.val();

            $scope.vendedor.meta_valor_mes = $scope.vendedor.meta_valor_mes == undefined ? 0 : $scope.vendedor.meta_valor_mes;
            $scope.vendedor.meta = $scope.vendedor.meta_valor_dia == undefined ? 0 : $scope.vendedor.meta_valor_dia;
            $scope.vendedor.pa_mes = $scope.vendedor.pa_mes == undefined ? 0 : parseInt($scope.vendedor.pa_mes).toFixed(2);
            $scope.vendedor.pa_hoje = $scope.vendedor.pa_hoje == undefined ? 0 : parseInt($scope.vendedor.pa_hoje).toFixed(2);
            $scope.vendedor.pa_reservado = $scope.vendedor.pa_reservado == undefined ? 0 : parseInt($scope.vendedor.pa_reservado).toFixed(2);

            $scope.vendedor.valorCorner = $scope.atualizarTotal($scope.vendedor.valor_corner_mini, $scope.vendedor.valor_corner_oficina)
            $scope.vendedor.PercentualDiario = $scope.calcularPercentualMeta($scope.vendedor.vendas_hoje, $scope.vendedor.meta_valor_dia)
            $scope.vendedor.PercentualMes = $scope.calcularPercentualMeta($scope.vendedor.vendas_mes + + $scope.vendedor.vendas_hoje, $scope.vendedor.meta_valor_mes)
            $scope.vendedor.taxaConversaoReservado = $scope.calcularTaxaConversaoReservado($scope.vendedor.qtd_malas_vendidas, $scope.vendedor.qtd_malas_enviadas)

            var listagem = JSON.parse(localStorage.getItem('dados_vendedor'));

            if ((listagem != null) || (listagem != undefined)) {
                for (var i = 0; i < Object.keys($scope.vendedor).length; i++) {
                    var campo = Object.keys($scope.vendedor)[i];
                    angular.element('#' + campo).removeClass('updatefield');

                    if (listagem[campo] != $scope.vendedor[campo]) {
                        angular.element('#' + campo).addClass('updatefield');
                    }
                }
            }

            $timeout(function () {
                $scope.$apply();
            });

            localStorage.setItem('dados_vendedor', JSON.stringify($scope.vendedor));


        })
    }

})();