﻿using Microsoft.Owin;
using Now;
using Owin;

[assembly: OwinStartup(typeof(Startup1))]

namespace Now
{
    public class Startup1
    {
        public void Configuration(IAppBuilder app)
        {
            // For more information on how to configure your application, visit http://go.microsoft.com/fwlink/?LinkID=316888
            app.MapSignalR();
        }
    }
}
