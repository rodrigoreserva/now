﻿function testImage(url, timeoutT) {
    return new Promise(function (resolve, reject) {
        var timeout = timeoutT || 5000;
        var timer, img = new Image();
        img.onerror = img.onabort = function () {
            clearTimeout(timer);
            reject("error");
        };
        img.onload = function () {
            clearTimeout(timer);
            resolve("success");
        };
        timer = setTimeout(function () {
            // reset .src to invalid URL so it stops previous
            // loading, but doens't trigger new load
            img.src = "//!!!!/noexist.jpg";
            reject("timeout");
        }, timeout);
        img.src = url;
    });
}


function record(url, result) {
    document.body.innerHTML += "<span class='" + result + "'>" +
        result + ": " + url + "</span><br>";
}

function runImage(url) {
    testImage(url).then(record.bind(null, url), record.bind(null, url));
}


function checkImgOnline(imageUrl) {

    var img = new Image();
    img.src = imageUrl;
    if (img.height > 0) {
        return imageUrl;
    } else {
        return "../Images/notFound70x94.png";
    }
}

function ExibirVendas(objExibir, objEsconder) {
    if (objExibir == "HistCompraGrid")
    {
        ExibirImagensProdutos();
    }

    $('.' + objEsconder).hide('show');
    $('.' + objExibir).show('show');
}

function ExibirImagensProdutos() {
    $(".fotoProduto").each(function (index, element) {
        var id = $(this).prop("id");
        $(this).attr("src", $("#_hidden_" + id).val());
    });
}

(function ($) {
    $.ajaxSubmitLoadResultCallback = function (obj, form, result, callbackResult) {
        $(form).ajaxSubmit({
            success: function OnSuccess(data) {
                $(result).html(data);

                if (callbackResult)
                    callbackResult(data);

                //$("#" + obj).DataTable();
                //$.unblockUI();
            },
            error: function (data) {
                alert("Erro: " + data);
            }
        });
    };

})(jQuery);

$('.datepicker').datepicker({
    autoclose: true,

});

$('.datepicker').inputmask("dd/mm/yyyy");


function OnSuccess() {
    $.msg({ content: 'Operação realizada com sucesso!' });
}

function OnFailure() {
    $.msg({ content: 'Falha na operação!' });
}


function formatReal(moeda) {
    moeda = moeda.replace(".", "");

    moeda = moeda.replace(",", ".");

    return moeda;
}

function arredondar(d, casas) {
    var aux = Math.pow(10, casas)
    return Math.floor(d * aux) / aux
}

function formatReal(w, e, m, r, a) {
    // Cancela se o evento for Backspace
    if (!e) var e = window.event
    if (e.keyCode) code = e.keyCode;
    else if (e.which) code = e.which;
    // Variáveis da função
    var txt = (!r) ? w.value.replace(/[^\d]+/gi, '') : w.value.replace(/[^\d]+/gi, '').reverse();
    var mask = (!r) ? m : m.reverse();
    var pre = (a) ? a.pre : "";
    var pos = (a) ? a.pos : "";
    var ret = "";
    if (code == 9 || code == 8 || txt.length == mask.replace(/[^#]+/g, '').length) return false;
    // Loop na máscara para aplicar os caracteres
    for (var x = 0, y = 0, z = mask.length; x < z && y < txt.length;) {
        if (mask.charAt(x) != '#') {
            ret += mask.charAt(x); x++;
        }
        else {
            ret += txt.charAt(y); y++; x++;
        }
    }
    // Retorno da função
    ret = (!r) ? ret : ret.reverse()
    w.value = pre + ret + pos;
}

//if ($('.container > div').hasClass('home')) {
//    $('body').attr('class', 'home');
//} else {
//    $('body').attr('class', 'page');
//}

$('.drop-mult').multiselect({
    includeSelectAllOption: true,
    enableFiltering: true,
    maxHeight: 400,
    buttonWidth: '100%'
});


$('.drop-mult_loja').multiselect({
    includeSelectAllOption: true,
    selectAllText: 'Selecionar Todas!',
    enableFiltering: true,
    maxHeight: 400,
    buttonWidth: '100%',
    buttonClass: 'btn2-gradient blue small checkCss',
    buttonText: function (options, select) {
        if (options.length === 0) {
            return 'Nenhuma Loja Selecionada ...';
        }
        else if (options.length > 3) {
            return options.length + ' Lojas Selecionadas!';
        }
        else {
            var labels = [];
            options.each(function () {
                if ($(this).attr('label') !== undefined) {
                    labels.push($(this).attr('label'));
                }
                else {
                    labels.push($(this).html());
                }
            });
            return labels.join(', ') + '';
        }
    }
});

////Gambi
//$(window).resize(ajusteMenu());
//ajusteMenu();

function ajusteMenu() {
    if ($(window).width() < 1000);
    $("#navbar-pd").css('overflow', 'hidden !important');
}

$(".datepicker").datepicker({
    dateFormat: 'dd/mm/yy',
    dayNames: ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
    dayNamesMin: ['D', 'S', 'T', 'Q', 'Q', 'S', 'S', 'D'],
    dayNamesShort: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sáb', 'Dom'],
    monthNames: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
    monthNamesShort: ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez'],
    nextText: 'Próximo',
    prevText: 'Anterior'
}).on('change', function () {
    validaData($(this).val());
    $('#form-validation-dependente').data('bootstrapValidator').revalidateField('tbxDataNascDependente');
});

//var _dataIni = new Date();
//alert(_dataIni.setDate(_dataIni.getDate() - 1));
$(".datepickerAgendamentoIni").datepicker({
    dateFormat: 'dd/mm/yy',
    //minDate: ,
    dayNames: ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
    dayNamesMin: ['D', 'S', 'T', 'Q', 'Q', 'S', 'S', 'D'],
    dayNamesShort: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sáb', 'Dom'],
    monthNames: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
    monthNamesShort: ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez'],
    nextText: 'Próximo',
    prevText: 'Anterior'
}).on('change', function () {
    validaData($(this).val());
    $('#form-validation-agendamento').data('bootstrapValidator').revalidateField($(this));
});

$(".datepickerRangeFrom").datepicker({
    changeMonth: true,
    numberOfMonths: 1,
    autoclose: true,
    dateFormat: 'dd/mm/yy',
    dayNames: ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
    dayNamesMin: ['D', 'S', 'T', 'Q', 'Q', 'S', 'S', 'D'],
    dayNamesShort: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sáb', 'Dom'],
    monthNames: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
    monthNamesShort: ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez'],
    nextText: 'Próximo',
    prevText: 'Anterior',
    onClose: function (selectedDate) {
        $(".datepickerRangeTo").datepicker("option", "minDate", selectedDate);
    }
}).on('change', function () {
    validaData($(this).val());
});

$(".datepickerRangeTo").datepicker({
    changeMonth: true,
    numberOfMonths: 1,
    autoclose: true,
    dateFormat: 'dd/mm/yy',
    dayNames: ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
    dayNamesMin: ['D', 'S', 'T', 'Q', 'Q', 'S', 'S', 'D'],
    dayNamesShort: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sáb', 'Dom'],
    monthNames: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
    monthNamesShort: ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez'],
    nextText: 'Próximo',
    prevText: 'Anterior',
    onClose: function (selectedDate) {
        $(".datepickerRangeFrom").datepicker("option", "maxDate", selectedDate);
    }
}).on('change', function () {
    validaData($(this).val());
});


$('.datepickerAgendamentoIni').on('change', function () {
    if (validaData($(this).val())) {
        //alert($(this).val());
        var splitData = $(this).val().split('/');
        //alert(splitData);
        var dataini = new Date(splitData); //new Date(splitData[1] + '-' + splitData[0] + '-' + splitData[2]);
        //  alert(dataini);
        // var data_final = new Date(dataini.setDate(dataini.getDate() + 7));

        $('.datepickerAgendamentoFim').attr('value', SomarData($(this).val(), 6));
        // $('#Corpo_tbxPeriodoFim').attr('value', ('0' + (data_final.getDate())).slice(-2) + '/' + ('0' + (data_final.getMonth() + 1)).slice(-2) + '/' + data_final.getFullYear());
        // $('#Corpo_hddDataFim').attr('value', ('0' + (data_final.getDate())).slice(-2) + '/' + ('0' + (data_final.getMonth() + 1)).slice(-2) + '/' + data_final.getFullYear());
    }
});

$('.btn-bar .new-register').click(function () {
    $('table tbody .new-row td').toggle();
});

$('div.boxes.call a.btn').click(function () {
    $('div.layer-bg, div.layer').show();
});

$('div.layer .close').click(function () {
    $('div.layer-bg, div.layer').hide();
});

$('div.historico-compras li p a').click(function () {
    var o = $(this);
    var op = $(this).parents('li');
    if (op.hasClass('opened')) {
        o.text('Abrir');
        op.removeClass('opened');
        op.find('p.bg-primary span').show();
    } else {
        o.text('Fechar');
        op.addClass('opened');
        op.find('p.bg-primary span').hide();
    }
});

$('div.caracteristica a.bnteditar').click(function () {

    $(this).hide();
    $('div.caracteristica a.bntsalvar').show();
    $('textarea#caracteristicas').removeAttr('readonly').focus();
});

$('div.caracteristica a.bntsalvar').click(function () {

    $(this).hide();
    $('div.caracteristica a.bnteditar').show();

    $('textarea#caracteristicas').attr('readonly', 'true');

    var idcliente = $("#HddId").val();

    var caracteristica = $('textarea#caracteristicas').val();
    var param = "{'idcliente' : '" + idcliente + "', 'caracteristica': '" + caracteristica + "' }";

    $.ajax(
        {
            type: "POST",
            url: location.pathname + "/SalvarQuemEEle",
            data: param,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (msg) {
                toastr['success'](msg.d, 'Atenção');
                $('#TxtCaracteristica').val(caracteristica);
            },
            error: function () {
                toastr['error']('Fora da rede, Tente novamente', 'Atenção');
            }
        }
    );
});

$('#ddlLojaAgendamento').on('change', function () {
    CarregarDropVendedor();
});

$('#btnAgendamento').on('click', function () {
    CarregarDropVendedor();
});

function CarregarDropVendedor() {
    var DropdownLoja = $('#ddlLojaAgendamento');
    var Dropdown = $('#ddlvendedorAgendamento');

    if (DropdownLoja.val() != '' && DropdownLoja.val() != '0') {
        $('div.modal').block({
            message: '<h3>Carregando...</h3>',
            css: { border: '1px solid #000' }
        });
        $.ajax({
            type: "POST",
            url: location.pathname + "/getDadosVededores",
            data: "{'idLoja' : '" + DropdownLoja.val() + "'}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                Dropdown.empty();
                Dropdown.append(new Option("Selecione", ""));
                $.each(response.d, function (index, item) {
                    Dropdown.append(new Option(item.Nome, item.ID));
                });
                $('div.modal').unblock();
            },
            error: function () {
                $.unblockUI();
                SessaoAtiva();

                alert();
                toastr['error']('Falha ao carregar dados', 'Atenção');
                $('div.modal').unblock();
            }
        });
    } else {
        Dropdown.empty();
    }
}



function validaData(stringData) {
    /******** VALIDA DATA NO FORMATO DD/MM/AAAA *******/

    var regExpCaracter = /[^\d]/;     //Expressão regular para procurar caracter não-numérico.
    var regExpEspaco = /^\s+|\s+$/g;  //Expressão regular para retirar espaços em branco.

    if (stringData.length != 10) {
        $.msg({ content: 'Data fora do padrão DD/MM/AAAA', tipo: 'alert-warning', titulo: 'Atenção', img: 'fa-warning' });

        return false;
    }

    splitData = stringData.split('/');

    if (splitData.length != 3) {
        $.msg({ content: 'Data fora do padrão DD/MM/AAAA', tipo: 'alert-warning', titulo: 'Atenção', img: 'fa-warning' });
        return false;
    }

    /* Retira os espaços em branco do início e fim de cada string. */
    splitData[0] = splitData[0].replace(regExpEspaco, '');
    splitData[1] = splitData[1].replace(regExpEspaco, '');
    splitData[2] = splitData[2].replace(regExpEspaco, '');

    if ((splitData[0].length != 2) || (splitData[1].length != 2) || (splitData[2].length != 4)) {
        $.msg({ content: 'Data fora do padrão DD/MM/AAAA', tipo: 'alert-warning', titulo: 'Atenção', img: 'fa-warning' });
        return false;
    }

    /* Procura por caracter não-numérico. EX.: o "x" em "28/09/2x11" */
    if (regExpCaracter.test(splitData[0]) || regExpCaracter.test(splitData[1]) || regExpCaracter.test(splitData[2])) {
        $.msg({ content: 'Caracter inválido encontrado!', tipo: 'alert-warning', titulo: 'Atenção', img: 'fa-warning' });
        return false;
    }

    dia = parseInt(splitData[0], 10);
    mes = parseInt(splitData[1], 10) - 1; //O JavaScript representa o mês de 0 a 11 (0->janeiro, 1->fevereiro... 11->dezembro)
    ano = parseInt(splitData[2], 10);

    var novaData = new Date(ano, mes, dia);

    /* O JavaScript aceita criar datas com, por exemplo, mês=14, porém a cada 12 meses mais um ano é acrescentado à data
         final e o restante representa o mês. O mesmo ocorre para os dias, sendo maior que o número de dias do mês em
         questão o JavaScript o converterá para meses/anos.
         Por exemplo, a data 28/14/2011 (que seria o comando "new Date(2011,13,28)", pois o mês é representado de 0 a 11)
         o JavaScript converterá para 28/02/2012.
         Dessa forma, se o dia, mês ou ano da data resultante do comando "new Date()" for diferente do dia, mês e ano da
         data que está sendo testada esta data é inválida. */
    if ((novaData.getDate() != dia) || (novaData.getMonth() != mes) || (novaData.getFullYear() != ano)) {
        $.msg({ content: 'Data Inválida!', tipo: 'alert-warning', titulo: 'Atenção', img: 'fa-warning' });
        return false;
    }
    else {
        return true;
    }
}


// txtData - é a data inicial.
// DiasAdd - É quantos dias você quer adicionar a txtData.
function SomarData(txtData, DiasAdd) {

    if (txtData == '') return;

    txtData = txtData.split('/');

    var date = new Date();
    date.setFullYear(txtData[2]);
    date.setMonth(txtData[1]);// mes de 0 a 11
    date.setDate(txtData[0]);

    date.setDate(date.getDate() + DiasAdd);

    var _data = date.getDate() + '/' + date.getMonth() + '/' + date.getFullYear();

    return _data;
}

///antigo ajaxSubmitResultDataRefresh
(function ($) {
    $.ajaxSubmitLoadResultCallback = function (obj, form, result, titulo, callbackResult) {
        $(form).ajaxSubmit({
            success: function OnSuccess(data) {
                $(result).html(data);

                if (callbackResult)
                    callbackResult(data);

                if (obj != '') {
                    $("#" + obj).DataTable(
                        {
                            dom: 'Bfrtip',
                            buttons: [{
                                extend: 'excelHtml5',
                                customize: function (xlsx) {
                                    var sheet = xlsx.xl.worksheets['sheet1.xml'];

                                    $('row c[r^="C"]', sheet).attr('s', '2');
                                }
                            },
                            {
                                extend: 'pdfHtml5',
                                title: titulo
                            }

                            ]
                        }
                        );

                    $(".buttons-excel").addClass("btn2-gradient green btnExportar text-uppercase text-bold");
                    $(".buttons-excel").html('<i class="glyphicon glyphicon-export"></i> EXCEL');

                    $(".buttons-pdf").addClass("btn2-gradient blue btnExportar text-uppercase text-bold");
                    $(".buttons-pdf").html('<i class="glyphicon glyphicon-article"></i> PDF');

                }
                $.unblockUI();
            },
            error: function (data) {
                alert("Erro: " + data);
            }

        });
    };

})(jQuery);


function loadingObj(obj, texto) {
    $('#' + obj).block({
        message: texto,
        css: {
            width: '200px',
            top: '10px',
            left: '',
            right: '10px',
            border: 'none',
            padding: '5px',
            backgroundColor: '#000',
            //'-webkit-border-radius': '10px',
            //'-moz-border-radius': '10px',
            opacity: .50,
            color: '#fff'
        }
    });
}

function loadingExistObj(obj) {
    $('#' + obj).unblock();
}

/// ANK - autocomplete -- INICIO --


/// ANK - autocomplete -- FINAL --	





