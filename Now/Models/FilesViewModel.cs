﻿
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Now.Helpers;
namespace Now.Models
{
    public class FilesViewModel
    {
        public ViewDataUploadFilesResult[] Files { get; set; }
    }
}