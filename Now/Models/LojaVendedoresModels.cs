﻿using Now.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Now.Models
{
    public class LojaVendedoresModels
    {

        public IList<Loja> Lojas { get; set; }

        public IList<Vendedor> Vendedores { get; set; }
    }
}